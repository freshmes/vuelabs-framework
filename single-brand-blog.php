<?php
global $post;

/**
 * Get Some information first.
 */
$post_id   = $post->ID;
$author_id = $post->post_author;
$author    = get_userdata( $author_id );
$user      = new WP_User( $author_id );

$gallery   = get_field( 'brand_gallery' );
$tagline   = get_field( 'brand_tagline' );

$author_image       = get_field( 'brand_profile_image' );
$author_name        = $author->first_name . ' ' . $author->last_name;
$author_description = get_field( 'brand_blog_description' );

$header_title = $author->first_name . ' ' . $author->last_name;

if ( in_array( 'freshmes_business', $user->roles ) || in_array( 'freshmes_organization', $user->roles ) ) {

    $header_title = get_field( 'brand_business_name' );

}

if ( isset( $gallery ) && ! empty( $gallery ) ) {

    $header_image_link = $gallery[0]['url'];
    $header_image = 'style="background-image: url(' . esc_url( $header_image_link ) . ');"';

}

$content = new WP_Query( [
    'post_type'      => 'post',
    'author'         => intval( $author_id ),
    'post_status'    => 'publish',
    'posts_per_page' => 10
] );

?>

<div class="brand-blog">
    <header class="brand-blog__header" <?php echo $header_image; ?>>
        <div class="brand-blog__header__overlay"></div>
        <div class="brand-blog__header__content">
            <h1 class="brand-blog__header__content__title"><?php echo esc_html( $header_title ) . esc_html__( ' Blog', 'vuelabs-framework' ); ?></h1>
            <?php if ( isset( $tagline ) && ! empty( $tagline ) ) : ?>
                <h2 class="brand-blog__header__content__subtitle"><?php echo esc_html( $tagline ); ?></h2>
            <?php endif; ?>
        </div>
    </header>
    <div class="brand-blog__author">
        <div class="container">
            <div class="columns is-gapless">
                <div class="column is-9">
                    <div class="columns is-vcentered">
                        <div class="column is-2">
                            <div class="brand-blog__author__image">
                                <?php echo wp_get_attachment_image( $author_image, 'thumbnail' ); ?>
                            </div>
                        </div>
                        <div class="column is-7">
                            <div class="brand-blog__author__by"><?php esc_html_e( 'Written by:', 'vuelabs-framework' ); ?></div>
                            <div class="brand-blog__author__name"><?php echo esc_html( $author_name ); ?></div>
                            <div class="brand-blog__author__description"><?php echo wp_kses( $author_description, [ 'br' => [] ] ); ?></div>
                        </div>
                        <div class="column is-3 has-text-right">
                            <a href="#" class="button brand-blog__author__subscribe"><?php esc_html_e( 'Subscribe', 'vuelabs-framework' ); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="brand-blog__content">
        <div class="container">
            <div class="brand-blog__content__header">
                <div class="brand-blog__content__header__title"><?php esc_html_e( 'All Stories', 'vuelabs-framework' ); ?></div>
                <div class="brand-blog__content__header__breadcrumbs">
                    <a href="<?php echo esc_url( get_permalink( $post_id ) ); ?>"><?php echo esc_html( $header_title ); ?></a>
                    <span class="brand-blog__content__header__breadcrumbs__separator">/</span>
                    <a href="<?php echo esc_url( get_permalink( $post_id ) . '/blog' ); ?>"><?php esc_html_e( 'Blog', 'vuelabs-framework' ); ?></a>
                    <span class="brand-blog__content__header__breadcrumbs__separator">/</span>
                    <span class="brand-blog__content__header__breadcrumbs__current"><?php esc_html_e( 'All Stories', 'vuelabs-framework' ); ?></span>
                </div>
            </div>
            <div class="brand-blog__content__hero">
                <div class="columns">
                    <div class="column is-8">
                        <?php
                        if ( $content->have_posts() ) {

                            $content_counter = 1;
                            while ( $content->have_posts() ) : $content->the_post();

                                if ( $content_counter === 1 ) {

                                    vlf_display_post( get_the_ID(), 'medium-single-image' );

                                }

                            $content_counter++; endwhile; wp_reset_postdata();

                        } else {

                            get_template_part( 'template-parts/brand/blog/part-content', 'none' );

                        }
                        ?>
                    </div>
                    <div class="column is-3 is-offset-1">
                        <div class="brand-blog__content__hero__sidebar">
                            <?php dynamic_sidebar( 'brand-blog-sidebar' ); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php if ( $content->have_posts() ) : ?>
                <div class="brand-blog__content__main">
                    <div class="brand-blog__content__main__posts columns is-multiline">
                        <?php
                        $content_counter = 1;
                        while ( $content->have_posts() ) : $content->the_post();

                            if ( $content_counter !== 1 ) {

                                echo '<div class="column is-4">';
                                vlf_display_post( get_the_ID(), 'medium-single-image' );
                                echo '</div>';

                            }

                            $content_counter++; endwhile; wp_reset_postdata();
                        ?>
                    </div>
                    <div class="brand-blog__content__main__footer has-text-centered">
                        <?php if ( $content->post_count >= 10 ) : ?>
                            <a href="#" class="button brand-blog__content__main__footer__load-more"><?php esc_html_e( 'Load More', 'vuelabs-framework' ); ?></a>
                        <?php endif; ?>
                        <div class="brand-blog__content__main__footer__subscribe">
                            <div class="columns">
                                <div class="column is-8 is-offset-2">
                                    <div class="columns is-vcentered">
                                        <div class="column is-8 has-text-centered">
                                            <div class="brand-blog__content__main__footer__subscribe__title"><?php esc_html_e( 'Subscribe to' ); echo ' ' . $header_title; ?></div>
                                            <div class="brand-blog__content__main__footer__subscribe__subtitle"><?php esc_html_e( 'Subscribe to ensure all of these great stories show up in your content feed.', 'vuelabs-framework' ); ?></div>
                                        </div>
                                        <div class="column is-4 has-text-right">
                                            <a href="#" class="button brand-blog__content__main__footer__subscribe__button"><?php esc_html_e( 'Subscribe', 'vuelabs-framework' ); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="brand-blog__footer">
        <div class="container">
            <div class="brand-blog__footer__share has-text-centered">
                <span class="brand-blog__footer__share__text"><?php esc_html_e( 'Share:', 'vuelabs-framework' ); ?></span>
                <a href="#" class="brand-blog__footer__share__item brand-blog__footer__share__item--facebook"><i class="fab fa-facebook-square"></i></a>
                <a href="#" class="brand-blog__footer__share__item brand-blog__footer__share__item--linkedin"><i class="fab fa-linkedin"></i></a>
                <a href="#" class="brand-blog__footer__share__item brand-blog__footer__share__item--twitter"><i class="fab fa-twitter-square"></i></a>
            </div>
        </div>
    </div>
</div>

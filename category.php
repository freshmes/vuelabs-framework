<?php
get_header();
global $post;

/**
 * Get Some information first.
 */
$post_id   = $post->ID;
$author_id = $post->post_author;
$author    = get_userdata( $author_id );
$user      = new WP_User( $author_id );

$gallery   = get_field( 'brand_gallery' );
$tagline   = get_field( 'brand_tagline' );

$author_image       = get_field( 'brand_profile_image' );
$author_name        = $author->first_name . ' ' . $author->last_name;
$author_description = get_field( 'brand_blog_description' );

$header_title = $author->first_name . ' ' . $author->last_name;

if ( in_array( 'freshmes_business', $user->roles ) || in_array( 'freshmes_organization', $user->roles ) ) {

    $header_title = get_field( 'brand_business_name' );

}

if ( isset( $gallery ) && ! empty( $gallery ) ) {

    $header_image_link = $gallery[0]['url'];
    $header_image = 'style="background-image: url(' . esc_url( $header_image_link ) . ');"';

}

$author_name = 'Freshmes';
$header_image_id = get_field( 'shop_image', 'option' );
$header_image_link = wp_get_attachment_image_url( $header_image_id, 'full' );
$header_image = 'style="background-image: url(' . esc_url( $header_image_link ) . ');"';

$content = new WP_Query( [
    'post_type'      => 'post',
    'post_status'    => 'publish',
    'posts_per_page' => 10,
    'tax_query'      => [
        [
            'taxonomy' => 'category',
            'field'    => 'term_id',
            'terms'    => intval( get_queried_object_id() )
        ]
    ]
] );

?>

    <div class="brand-blog">
        <header class="brand-blog__header" <?php echo $header_image; ?>>
            <div class="brand-blog__header__overlay"></div>
            <div class="brand-blog__header__content">
                <h1 class="brand-blog__header__content__title"><?php echo esc_html__( 'Freshmes Blog', 'vuelabs-framework' ); ?></h1>
            </div>
        </header>
        <div class="brand-blog__content">
            <div class="container">
                <div class="brand-blog__content__header">
                    <div class="brand-blog__content__header__title"><?php esc_html_e( 'All Stories', 'vuelabs-framework' ); ?></div>
                    <div class="brand-blog__content__header__breadcrumbs">
                        <a href="<?php echo esc_url( get_site_url() . '/blog/' ); ?>"><?php esc_html_e( 'Blog', 'vuelabs-framework' ); ?></a>
                        <span class="brand-blog__content__header__breadcrumbs__separator">/</span>
                        <span class="brand-blog__content__header__breadcrumbs__current"><?php echo esc_html( get_queried_object()->name ); ?></span>
                    </div>
                </div>
                <div class="brand-blog__content__hero">
                    <div class="columns">
                        <div class="column is-8">
                            <?php
                            if ( $content->have_posts() ) {

                                $content_counter = 1;
                                while ( $content->have_posts() ) : $content->the_post();

                                    if ( $content_counter === 1 ) {

                                        vlf_display_post( get_the_ID(), 'medium-single-image' );

                                    }

                                    $content_counter++; endwhile; wp_reset_postdata();

                            } else {

                                get_template_part( 'template-parts/brand/blog/part-content', 'none' );

                            }
                            ?>
                        </div>
                        <div class="column is-3 is-offset-1">
                            <div class="brand-blog__content__hero__sidebar">
                                <?php dynamic_sidebar( 'brand-blog-sidebar' ); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ( $content->have_posts() ) : ?>
                    <div class="brand-blog__content__main">
                        <div class="brand-blog__content__main__posts columns is-multiline">
                            <?php
                            $content_counter = 1;
                            while ( $content->have_posts() ) : $content->the_post();

                                if ( $content_counter !== 1 ) {

                                    echo '<div class="column is-4">';
                                    vlf_display_post( get_the_ID(), 'medium-single-image' );
                                    echo '</div>';

                                }

                                $content_counter++; endwhile; wp_reset_postdata();
                            ?>
                        </div>
                        <div class="brand-blog__content__main__footer has-text-centered">
                            <?php if ( $content->post_count >= 10 ) : ?>
                                <a href="#" class="button brand-blog__content__main__footer__load-more" data-category="<?php echo esc_attr( get_queried_object_id() ); ?>"><?php esc_html_e( 'Load More', 'vuelabs-framework' ); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="brand-blog__footer">
            <div class="container">
                <div class="brand-blog__footer__share has-text-centered">
                    <span class="brand-blog__footer__share__text"><?php esc_html_e( 'Share:', 'vuelabs-framework' ); ?></span>
                    <a href="#" class="brand-blog__footer__share__item brand-blog__footer__share__item--facebook"><i class="fab fa-facebook-square"></i></a>
                    <a href="#" class="brand-blog__footer__share__item brand-blog__footer__share__item--linkedin"><i class="fab fa-linkedin"></i></a>
                    <a href="#" class="brand-blog__footer__share__item brand-blog__footer__share__item--twitter"><i class="fab fa-twitter-square"></i></a>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>
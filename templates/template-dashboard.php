<?php
/**
 * Template Name: Dashboard
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/templates
 * @since 1.0.0
 */
get_header( 'dashboard' );

$current_brand_page = get_query_var( 'fpage' );

switch ( $current_brand_page ) {
    case 'brand':
        get_template_part( 'template-parts/dashboard/part', 'brand' );
        break;
    case 'connect':
        get_template_part( 'template-parts/dashboard/part', 'connect' );
        break;
    case 'promote':
    	get_template_part( 'template-parts/dashboard/part', 'promote' );
    	break;
    case 'sell':
    	get_template_part( 'template-parts/dashboard/part', 'sell' );
    	break;
    case 'settings':
    	get_template_part( 'template-parts/dashboard/part', 'settings' );
    	break;
}

get_footer( 'dashboard' );
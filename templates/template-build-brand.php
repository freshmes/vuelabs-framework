<?php
/**
 * Template Name: Build your Brand
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/templates
 * @since 1.0.0
 */

// Redirect if not logged in.
if ( ! is_user_logged_in() ) {

    wp_safe_redirect( get_home_url( '/' ) );

} else {

    $current_user_id = get_current_user_id();
    $brand_id        = vlf_get_brand_id_from_author_id( intval( $current_user_id ) );

    $next_step       = get_post_meta( $brand_id, 'brand_onboarding_step', true );
    $next_step       = vlf_check_field( $next_step );
    if ( ! $next_step ) {

        $next_step = 1;

    } else {

        if ( $next_step === 'done' ) {

            wp_safe_redirect( get_permalink( $brand_id ) );

        } else {

            $next_step = intval( $next_step );

        }

    }

}

get_header();

if ( have_posts() ) : while ( have_posts() ) : the_post();

    $page_id = get_the_ID();

    $image = '';
    if ( has_post_thumbnail() ) {

        $image_url = get_the_post_thumbnail_url( $page_id, 'full' );
        $image = 'style="background-image: url(' . esc_url( $image_url ) . ');"';

    }

    $tagline = get_field( 'build_brand_tagline', $page_id );
    $tagline = vlf_check_field( $tagline );

    ?>

    <div class="build-your-brand">
        <input type="hidden" id="user-id" value="<?php echo esc_attr( get_current_user_id() ); ?>">
        <header class="build-your-brand__header" <?php echo $image; ?>>
            <div class="container">
                <h1 class="build-your-brand__header__title"><?php the_title(); ?></h1>
                <?php if ( $tagline ) : ?>
                    <p class="build-your-brand__header__tagline"><?php echo esc_html( $tagline ); ?></p>
                <?php endif; ?>
            </div>
        </header>
        <div class="build-your-brand__content">
            <div class="build-your-brand__content__form">
                <div class="build-your-brand__content__form__carousel">
                    <!-- Personal Information -->
                    <div class="build-your-brand__content__form__carousel__item <?php echo ( $next_step === 1 ? 'is-show' : '' ); ?>" data-step-id="1">
                        <h3 class="build-your-brand__content__form__title"><?php esc_html_e( 'Personal Information', 'vuelabs-framework' ); ?></h3>
                        <div class="form-card">
                            <header class="form-card__header">
                                <div class="form-card__header__percentage">0%</div>
                                <div class="form-card__header__percentage-fill form-card__header__percentage-fill--2" data-below="<?php esc_attr_e( 'Brand Page Completion', 'vuelabs-framework' ); ?>"></div>
                            </header>
                            <div class="form-card__content">
                                <div class="field">
                                    <label for="first_name" class="label"><?php esc_html_e( 'First Name', 'vuelabs-framework' ); ?></label>
                                    <div class="control">
                                        <input type="text" class="input" id="first_name" placeholder="<?php esc_attr_e( 'Your first name', 'vuelabs-framework' ); ?>" autocorrect="off" autocomplete="given-name">
                                    </div>
                                </div>
                                <div class="field">
                                    <label for="last_name" class="label"><?php esc_html_e( 'Last Name', 'vuelabs-framework' ); ?></label>
                                    <div class="control">
                                        <input type="text" class="input" id="last_name" placeholder="<?php esc_attr_e( 'Your last name', 'vuelabs-framework' ); ?>" autocorrect="off" autocomplete="family-name">
                                    </div>
                                </div>
                                <div class="field">
                                    <label for="phone" class="label"><?php esc_html_e( 'Phone (Cell)', 'vuelabs-framework' ); ?></label>
                                    <div class="control">
                                        <input type="tel" class="input" id="phone" placeholder="<?php esc_attr_e( '123-456-7890', 'vuelabs-framework' ); ?>" autocorrect="off" autocomplete="tel">
                                        <div class="note"><?php esc_html_e( 'We\'ll use this for security to verify your account via text', 'vuelabs-framework' ); ?></div>
                                    </div>
                                </div>
                                <div class="field">
                                    <label for="job_title" class="label"><?php esc_html_e( 'Job Title', 'vuelabs-framework' ); ?></label>
                                    <div class="control">
                                        <input type="text" class="input" id="job_title" placeholder="<?php esc_attr_e( 'Enter your job title', 'vuelabs-framework' ); ?>">
                                        <div class="note"><?php esc_html_e( 'e.g. CEO, Blogger, Student, Stay-at-home mom, etc.', 'vuelabs-framework' ); ?></div>
                                    </div>
                                </div>
                                <div class="field">
                                    <label for="description" class="label"><?php esc_html_e( 'Description', 'vuelabs-framework' ); ?></label>
                                    <div class="control">
                                        <textarea id="description" rows="2" class="textarea" placeholder="<?php esc_attr_e( 'Describe what you do', 'vuelabs-framework' ); ?>"></textarea>
                                        <div class="note"><?php esc_html_e( 'Tell the world what you do, how you do it, and who you do it for', 'vuelabs-framework' ); ?></div>
                                    </div>
                                </div>
                            </div>
                            <footer class="form-card__footer">
                                <button class="button is-fullwidth form-card__footer__continue" type="submit"><?php esc_html_e( 'Continue', 'vuelabs-framework' ); ?></button>
                            </footer>
                        </div>
                    </div>

                    <!-- Business Information -->
                    <div class="build-your-brand__content__form__carousel__item <?php echo ( $next_step === 2 ? 'is-show' : '' ); ?>" data-step-id="2">
                        <h3 class="build-your-brand__content__form__title"><?php esc_html_e( 'Business Information', 'vuelabs-framework' ); ?></h3>
                        <div class="form-card">
                            <header class="form-card__header">
                                <div class="form-card__header__percentage">20%</div>
                                <div class="form-card__header__percentage-fill form-card__header__percentage-fill--20" data-below="<?php esc_attr_e( 'Brand Page Completion', 'vuelabs-framework' ); ?>"></div>
                            </header>
                            <div class="form-card__content">
                                <div class="field">
                                    <label for="business_name" class="label"><?php esc_html_e( 'Business Name', 'vuelabs-framework' ); ?></label>
                                    <div class="control">
                                        <input type="text" class="input" id="business_name" placeholder="<?php esc_attr_e( 'The name of your business', 'vuelabs-framework' ); ?>">
                                    </div>
                                </div>
                                <div class="field">
                                    <label for="tagline" class="label"><?php esc_html_e( 'Tagline', 'vuelabs-framework' ); ?></label>
                                    <div class="control">
                                        <input type="text" class="input" id="tagline" placeholder="<?php esc_attr_e( 'e.g. Making Life Better', 'vuelabs-framework' ); ?>">
                                        <div class="note"><?php esc_html_e( 'Give it something that describes what you do', 'vuelabs-framework' ); ?></div>
                                    </div>
                                </div>
                                <div class="field">
                                    <label for="business_description" class="label"><?php esc_html_e( 'Business Description', 'vuelabs-framework' ); ?></label>
                                    <div class="control">
                                        <textarea id="business_description" rows="2" class="textarea" placeholder="<?php esc_attr_e( 'Describe what you do', 'vuelabs-framework' ); ?>"></textarea>
                                        <div class="note"><?php esc_html_e( 'Tell the world what you do, how you do it, and who you do it for', 'vuelabs-framework' ); ?></div>
                                    </div>
                                </div>
                                <div class="field">
                                    <label for="tags" class="label"><?php esc_html_e( 'Tags', 'vuelabs-framework' ); ?></label>
                                    <div class="control">
                                        <select id="tags" name="tags[]" class="is-select2" multiple="multiple" placeholder="<?php esc_attr_e( 'Add Tags', 'vuelabs-framework' ); ?>">
                                            <option></option>
                                            <?php
                                                $tags = get_terms( [
                                                    'hide_empty' => false,
                                                    'taxonomy'   => 'brand_tag'
                                                ] );
                                                if ( ! is_wp_error( $tags ) ) {

                                                    foreach ( $tags as $tag ) {

                                                        echo '<option value="' . esc_attr( $tag->term_id ) . '">' . esc_html( $tag->name ) . '</option>';

                                                    }

                                                }
                                            ?>
                                        </select>
                                        <div class="note"><?php esc_html_e( 'Add as many as you’d like. These could be services you provide or areas relevant to your business', 'vuelabs-framework' ); ?></div>
                                    </div>
                                </div>
                            </div>
                            <footer class="form-card__footer">
                                <button class="button is-fullwidth form-card__footer__continue" type="submit"><?php esc_html_e( 'Continue', 'vuelabs-framework' ); ?></button>
                                <button class="button is-fullwidth form-card__footer__previous"><?php esc_html_e( 'Previous', 'vuelabs-framework' ); ?></button>
                            </footer>
                        </div>
                    </div>

                    <!-- Business Address -->
                    <div class="build-your-brand__content__form__carousel__item <?php echo ( $next_step === 3 ? 'is-show' : '' ); ?>" data-step-id="3">
                        <h3 class="build-your-brand__content__form__title"><?php esc_html_e( 'Business Address', 'vuelabs-framework' ); ?></h3>
                        <div class="form-card">
                            <header class="form-card__header">
                                <div class="form-card__header__percentage">40%</div>
                                <div class="form-card__header__percentage-fill form-card__header__percentage-fill--40" data-below="<?php esc_attr_e( 'Brand Page Completion', 'vuelabs-framework' ); ?>"></div>
                            </header>
                            <div class="form-card__content">
                                <input type="hidden" id="street_number">
                                <input type="hidden" id="route">
                                <input type="hidden" id="locality">
                                <input type="hidden" id="administrative_area_level_1">
                                <input type="hidden" id="postal_code">
                                <input type="hidden" id="country">
                                <div class="field">
                                    <label for="business_address" class="label"><?php esc_html_e( 'Business Address', 'vuelabs-framework' ); ?></label>
                                    <div class="control">
                                        <input type="text" class="input" id="business_address" placeholder="<?php esc_attr_e( '123 Anywhere Street', 'vuelabs-framework' ); ?>">
                                        <div class="note"><?php esc_html_e( 'If you are not a business, please enter your personal address. You address will never be shown publicly.', 'vuelabs-framework' ); ?></div>
                                    </div>
                                </div>
                                <div class="field">
                                    <label for="business_phone" class="label"><?php esc_html_e( 'Business Phone Number', 'vuelabs-framework' ); ?></label>
                                    <div class="control">
                                        <input type="tel" class="input" id="business_phone" placeholder="<?php esc_attr_e( '123-456-7890', 'vuelabs-framework' ); ?>" autocorrect="off" autocomplete="tel">
                                        <div class="note"><?php esc_html_e( 'The phone number you want listed for your business', 'vuelabs-framework' ); ?></div>
                                    </div>
                                </div>
                                <div class="field">
                                    <label for="website" class="label"><?php esc_html_e( 'Website (Optional)', 'vuelabs-framework' ); ?></label>
                                    <div class="control">
                                        <input type="url" class="input" id="website" placeholder="<?php esc_attr_e( 'e.g. https://website.com/', 'vuelabs-framework' ); ?>">
                                        <div class="note"><?php esc_html_e( 'Leave it blank if you don’t have one', 'vuelabs-framework' ); ?></div>
                                    </div>
                                </div>
                            </div>
                            <footer class="form-card__footer">
                                <button class="button is-fullwidth form-card__footer__continue" type="submit"><?php esc_html_e( 'Continue', 'vuelabs-framework' ); ?></button>
                                <button class="button is-fullwidth form-card__footer__previous"><?php esc_html_e( 'Previous', 'vuelabs-framework' ); ?></button>
                            </footer>
                        </div>
                    </div>

                    <!-- Social Media Information -->
                    <div class="build-your-brand__content__form__carousel__item <?php echo ( $next_step === 4 ? 'is-show' : '' ); ?>" data-step-id="4">
                        <h3 class="build-your-brand__content__form__title"><?php esc_html_e( 'Social Media Information', 'vuelabs-framework' ); ?></h3>
                        <div class="form-card">
                            <header class="form-card__header">
                                <div class="form-card__header__percentage">60%</div>
                                <div class="form-card__header__percentage-fill form-card__header__percentage-fill--60" data-below="<?php esc_attr_e( 'Brand Page Completion', 'vuelabs-framework' ); ?>"></div>
                            </header>
                            <div class="form-card__content">
                                <div class="field">
                                    <label for="facebook" class="label"><?php esc_html_e( 'Facebook Link', 'vuelabs-framework' ); ?></label>
                                    <div class="control">
                                        <input type="text" class="input" id="facebook" placeholder="<?php esc_attr_e( 'e.g. https://facebook.com/facebook-name/', 'vuelabs-framework' ); ?>">
                                        <div class="note"><?php esc_html_e( 'This is your Facebook handle', 'vuelabs-framework' ); ?></div>
                                    </div>
                                </div>
                                <div class="field">
                                    <label for="instagram" class="label"><?php esc_html_e( 'Instagram Link', 'vuelabs-framework' ); ?></label>
                                    <div class="control">
                                        <input type="text" class="input" id="instagram" placeholder="<?php esc_attr_e( 'e.g. https://instagram.com/instagram-name/', 'vuelabs-framework' ); ?>">
                                        <div class="note"><?php esc_html_e( 'This is your Instagram handle', 'vuelabs-framework' ); ?></div>
                                    </div>
                                </div>
                                <div class="field">
                                    <label for="linkedin" class="label"><?php esc_html_e( 'LinkedIn Link', 'vuelabs-framework' ); ?></label>
                                    <div class="control">
                                        <input type="text" class="input" id="linkedin" placeholder="<?php esc_attr_e( 'e.g. https://www.linkedin.com/in/linkedin-name/', 'vuelabs-framework' ); ?>">
                                        <div class="note"><?php esc_html_e( 'This is your LinkedIn handle', 'vuelabs-framework' ); ?></div>
                                    </div>
                                </div>
                            </div>
                            <footer class="form-card__footer">
                                <button class="button is-fullwidth form-card__footer__continue" type="submit"><?php esc_html_e( 'Continue', 'vuelabs-framework' ); ?></button>
                                <button class="button is-fullwidth form-card__footer__previous"><?php esc_html_e( 'Previous', 'vuelabs-framework' ); ?></button>
                            </footer>
                        </div>
                    </div>

                    <!-- Add Your Images -->
                    <div class="build-your-brand__content__form__carousel__item <?php echo ( $next_step === 5 ? 'is-show' : '' ); ?>" data-step-id="5">
                        <h3 class="build-your-brand__content__form__title"><?php esc_html_e( 'Add Your Images', 'vuelabs-framework' ); ?></h3>
                        <div class="form-card">
                            <header class="form-card__header">
                                <div class="form-card__header__percentage">80%</div>
                                <div class="form-card__header__percentage-fill form-card__header__percentage-fill--80" data-below="<?php esc_attr_e( 'Brand Page Completion', 'vuelabs-framework' ); ?>"></div>
                            </header>
                            <div class="form-card__content">
                                <div class="w-500">
                                    <div class="columns">
                                        <div class="column is-4">
                                            <label for="profile_image" class="is-file is-file-rounded">
                                                <input type="file" id="profile_image" name="profile_image" accept="image/*">
                                                <span class="top-label"><?php esc_html_e( 'Profile Image', 'vuelabs-framework' ); ?></span>
                                                <span class="image-container">
                                                    <span class="image-placeholder is-hidden"></span>
                                                    <span class="center">
                                                        <i class="fal fa-image"></i>
                                                        <span class="image-name" data-label="<?php esc_attr_e( 'Add Photo', 'vuelabs-framework' ); ?>"><?php esc_html_e( 'Add Photo', 'vuelabs-framework' ); ?></span>
                                                    </span>
                                                </span>
                                                <span class="note"><?php esc_html_e( 'Your headshot or selfie', 'vuelabs-framework' ); ?></span>
                                            </label>
                                        </div>
                                        <div class="column is-4">
                                            <label for="logo" class="is-file is-file-rounded">
                                                <input type="file" id="logo" name="logo" accept="image/*">
                                                <span class="top-label"><?php esc_html_e( 'Logo', 'vuelabs-framework' ); ?></span>
                                                <span class="image-container">
                                                    <span class="image-placeholder is-hidden"></span>
                                                    <span class="center">
                                                        <i class="fal fa-image"></i>
                                                        <span class="image-name" data-label="<?php esc_attr_e( 'Add Photo', 'vuelabs-framework' ); ?>"><?php esc_html_e( 'Add Photo', 'vuelabs-framework' ); ?></span>
                                                    </span>
                                                </span>
                                                <span class="note"><?php esc_html_e( 'Skip if you don’t have one', 'vuelabs-framework' ); ?></span>
                                            </label>
                                        </div>
                                        <div class="column is-4">
                                            <label for="logo_icon" class="is-file is-file-rounded">
                                                <input type="file" id="logo_icon" name="logo_icon" accept="image/*">
                                                <span class="top-label"><?php esc_html_e( 'Logo Icon', 'vuelabs-framework' ); ?></span>
                                                <span class="image-container">
                                                    <span class="image-placeholder is-hidden"></span>
                                                    <span class="center">
                                                        <i class="fal fa-image"></i>
                                                        <span class="image-name" data-label="<?php esc_attr_e( 'Add Photo', 'vuelabs-framework' ); ?>"><?php esc_html_e( 'Add Photo', 'vuelabs-framework' ); ?></span>
                                                    </span>
                                                </span>
                                                <span class="note"><?php esc_html_e( 'Skip if you don’t have one', 'vuelabs-framework' ); ?></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="special-label"><?php esc_html_e( 'Gallery Images', 'vuelabs-framework' ); ?></div>
                                <div class="columns">
                                    <div class="column">
                                        <label for="gallery_image_1" class="is-file">
                                            <input type="file" id="gallery_image_1" name="gallery_image_1" accept="image/*">
                                            <span class="image-container">
                                                <span class="image-placeholder is-hidden"></span>
                                                <span class="center">
                                                    <i class="fal fa-image"></i>
                                                    <span class="image-name" data-label="<?php esc_attr_e( 'Add Photo', 'vuelabs-framework' ); ?>"><?php esc_html_e( 'Add Photo', 'vuelabs-framework' ); ?></span>
                                                </span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="column">
                                        <label for="gallery_image_2" class="is-file">
                                            <input type="file" id="gallery_image_2" name="gallery_image_2" accept="image/*">
                                            <span class="image-container">
                                                <span class="image-placeholder is-hidden"></span>
                                                <span class="center">
                                                    <i class="fal fa-image"></i>
                                                    <span class="image-name" data-label="<?php esc_attr_e( 'Add Photo', 'vuelabs-framework' ); ?>"><?php esc_html_e( 'Add Photo', 'vuelabs-framework' ); ?></span>
                                                </span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="column">
                                        <label for="gallery_image_3" class="is-file">
                                            <input type="file" id="gallery_image_3" name="gallery_image_3" accept="image/*">
                                            <span class="image-container">
                                                <span class="image-placeholder is-hidden"></span>
                                                <span class="center">
                                                    <i class="fal fa-image"></i>
                                                    <span class="image-name" data-label="<?php esc_attr_e( 'Add Photo', 'vuelabs-framework' ); ?>"><?php esc_html_e( 'Add Photo', 'vuelabs-framework' ); ?></span>
                                                </span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="special-note"><?php esc_html_e( 'The first image will be shown on the profile card.', 'vuelabs-framework' ); ?></div>
                            </div>
                            <footer class="form-card__footer">
                                <button class="button is-fullwidth form-card__footer__preview" type="submit"><?php esc_html_e( 'Preview', 'vuelabs-framework' ); ?></button>
                                <button class="button is-fullwidth form-card__footer__previous"><?php esc_html_e( 'Previous', 'vuelabs-framework' ); ?></button>
                            </footer>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <?php

endwhile; endif;

get_footer();
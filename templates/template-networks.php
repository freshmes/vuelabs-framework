<?php
/**
 * Template Name: Networks
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/templates
 * @since 1.0.0
 */
get_header();

if ( isset( $_GET['preview'] ) && ! empty( $_GET['preview'] ) ) {

    if ( isset( $_GET['network'] ) && ! empty( $_GET['network'] ) ) {

        ?>

        <input type="hidden" id="freshmes-template-url" value="<?php echo esc_url( get_permalink( get_queried_object_id() ) ); ?>">

        <?php

        /**
         * Filters
         */
        get_template_part( 'template-parts/pages/part', 'filters-network' );

        get_template_part( 'template-parts/brand/part', 'share' );
        get_template_part( 'template-parts/brand/part', 'connect' );
        get_template_part( 'template-parts/brand/part', 'join-organization' );

        $network_id = intval( $_GET['network'] );
        $author_id  = get_post_field( 'post_author', $network_id );

        $brand_business_name = get_field( 'brand_business_name', $network_id );

        $member_button_class = 'button member__connect';
        $member_button_label = esc_html__( 'Connect', 'vuelabs-framework' );
        if ( vlf_has_connection( get_current_user_id(), $author_id ) ) {

            $member_button_class .= ' member__connect--disconnect';
            $member_button_label = esc_html__( 'Disconnect', 'vuelabs-framework' );

        }

        $organizations = get_the_terms( $network_id, 'brand_organization' );
        $organization_to_use = '';

        if ( isset( $organizations ) && ! empty( $organizations ) ) {

            foreach ( $organizations as $organization ) {

                if ( $organization->parent === 0 ) {

                    $organization_to_use = $organization->term_id;

                }

            }

        }

        $tags = new WP_Term_Query( [
            'taxonomy' => 'product_tag',
            'meta_query' => [
                [
                    'key'   => 'vlf_products_vendor',
                    'value' => $author_id
                ]
            ]
        ] );
        $tags = $tags->get_terms();
        $organization_tag =  $tags[0]->slug;

        ?>

        <div class="network-preview">
            <header class="network-preview__header">
                <div class="container">
                    <div class="columns">
                        <div class="column is-half">
                            <h1 class="network-preview__header__title"><?php echo esc_html( $brand_business_name ); ?></h1>
                        </div>
                        <div class="column is-half">

                        </div>
                    </div>
                </div>
            </header>
            <div class="network-preview__content">
                <div class="container">
                    <div class="columns">
                        <div class="column is-8">
                            <div class="network-preview__content__box">
                                <?php
                                the_widget( 'Freshmes_Brand_Preview' );
                                the_widget( 'Freshmes_Brand_About' );
                                the_widget( 'Freshmes_Brand_Tags' );
                                the_widget( 'Freshmes_Brand_Actions' );
                                ?>
                            </div>
                        </div>
                        <div class="column is-4">
                            <div>
                                <div class="member member--network has-text-centered" data-user="<?php echo esc_attr( $author_id ); ?>">
                                    <div class="member__owner"><?php esc_html_e( 'Network Owner', 'vuelabs-framework' ); ?></div>
                                    <div class="member__content">
                                        <div class="member__image">
                                            <?php echo wp_get_attachment_image( get_field( 'brand_profile_image', $network_id ), 'thumbnail' ); ?>
                                        </div>
                                        <div class="member__name"><?php echo get_the_title( $network_id ); ?></div>
                                        <div class="member__subtitle"><?php the_field( 'brand_job_title', $network_id ); ?></div>
                                    </div>
                                    <footer class="member__footer">
                                        <?php if ( is_user_logged_in() ) : ?>
                                            <a href="#" class="<?php echo esc_attr( $member_button_class ); ?>" data-user-a="<?php echo esc_attr( get_current_user_id() ); ?>" data-user-b="<?php echo esc_attr( $author_id ); ?>"><?php echo $member_button_label; ?></a>
                                        <?php endif; ?>
                                        <div class="has-text-centered">
                                            <a href="<?php the_permalink(); ?>" class="member__view-member"><?php esc_html_e( 'View Member', 'vuelabs-framework' ); ?></a>
                                        </div>
                                    </footer>
                                </div>
                            </div>
                            <?php $benefits = get_field( 'brand_benefits', $network_id ); ?>
                            <?php if ( isset( $benefits ) && ! empty( $benefits ) ) : ?>
                                <div class="network-preview__content__box-side">
                                    <div class="benefits">
                                        <header class="benefits__header"><?php esc_html_e( 'Benefits', 'vuelabs-framework' ); ?></header>
                                        <div class="benefits__content">
                                            <div class="benefits__content__icon"><i class="fal fa-address-card"></i></div>
                                            <div class="benefits__content__title"><?php echo esc_html( $brand_business_name . ' ' ); esc_html_e( 'Benefits', 'vuelabs-framework' ); ?></div>
                                            <p class="benefits__content__list">
                                                <?php foreach ( $benefits as $benefit ) : ?>
                                                <?php echo '• ' . esc_html( $benefit['benefit'] ); ?><br>
                                                <?php endforeach; ?>
                                            </p>
                                        </div>
                                        <footer class="benefits__footer">
                                            <a href="#" class="button benefits__footer__join-now" data-brand-id="<?php echo esc_attr( $network_id ); ?>" data-user-id="<?php echo esc_attr( get_current_user_id() ); ?>"><?php esc_html_e( 'Join Now', 'vuelabs-framework' ); ?></a>
                                        </footer>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <section class="freshmes-home freshmes-home-featured-networks">
                    <div class="container">
                        <nav class="freshmes-home-nav">
                            <div class="columns">
                                <div class="column">
                                    <h3 class="freshmes-home-title"><?php esc_html_e( 'Members', 'freshmes' ); ?></h3>
                                </div>
                                <div class="column has-text-right">
                                    <a href="<?php echo get_site_url() . '/members/'; ?>" class="freshmes-home-view-all"><?php esc_html_e( 'View All', 'freshmes' ); ?></a>
                                </div>
                            </div>
                        </nav>
                        <?php
                        $args = [
                            'post_type'      => 'brand',
                            'posts_per_page' => 8,
                            'post_status'    => 'publish',
                            'post__not_in'   => [ intval( $network_id ) ],
                            'tax_query' => [
                                [
                                    'taxonomy' => 'brand_organization',
                                    'field'    => 'term_id',
                                    'terms'    => $organization_to_use
                                ]
                            ]
                        ];

                        if ( isset( $_GET['chapter'] ) && ! empty( $_GET['chapter'] ) ) {

                            $args['tax_query'] = [
                                [
                                    'taxonomy' => 'brand_organization',
                                    'field'    => 'slug',
                                    'terms'    => $_GET['chapter']
                                ]
                            ];

                        }

                        $query = new WP_Query( $args );
                        ?>
                        <?php if ( $query->have_posts() ) : ?>
                            <div class="freshmes-home-content">
                                <div class="columns is-multiline">
                                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                                        <div class="column is-3">
                                            <?php
                                            $post_id = get_the_ID();

                                            $post_author_id   = get_post_field( 'post_author', $post_id );
                                            $post_author_data = get_userdata( $post_author_id );

                                            $post_author_image = get_field( 'brand_profile_image', 'user_' . $post_author_id );
                                            $post_author_image = vlf_check_field( $post_author_image );

                                            $locations = get_the_terms( $post_id, 'brand_location' );

                                            $categories = get_the_terms( $post_id, 'brand_category' );

                                            $header_class = 'freshmes-member__header';
                                            if ( ! has_post_thumbnail( $post_id ) ) {

                                                $header_class .= ' freshmes-member__header--no-image';

                                            }
                                            ?>
                                            <article id="post-<?php echo esc_attr( $post_id ); ?>" class="<?php echo esc_attr( join( ' ', get_post_class( 'freshmes-member', $post_id ) ) ); ?>">
                                                <header class="<?php echo esc_attr( $header_class ); ?>">
                                                    <?php
                                                    if ( has_post_thumbnail( $post_id ) ) {

                                                        echo '<div class="freshmes-member__header__image">';
                                                        echo get_the_post_thumbnail( $post_id, 'single-image' );
                                                        echo '</div>';

                                                    } else {

                                                        echo '<div class="freshmes-member__header__image freshmes-member__header__image--no-image"></div>';

                                                    }
                                                    ?>
                                                    <div class="freshmes-member__header__author freshmes-member__header__author--network">
                                                        <div class="freshmes-member__header__author__by"><?php echo esc_html( $post_author_data->first_name . ' ' . $post_author_data->last_name ); ?></div>
                                                        <div class="freshmes-member__header__author__image">
                                                            <?php echo wp_get_attachment_image( $post_author_image, 'thumbnail' ); ?>
                                                        </div>
                                                    </div>
                                                    <div class="freshmes-member__header__brand">
                                                        <div class="freshmes-member__header__brand__image">
                                                            <?php echo wp_get_attachment_image( get_field( 'brand_logo', $network_id ), 'thumbnail' ); ?>
                                                        </div>
                                                        <div class="freshmes-member__header__brand__by"><?php the_field( 'brand_business_name', $network_id ); ?> / Network Member</div>
                                                    </div>
                                                </header>
                                                <div class="freshmes-member__content">
                                                    <div class="columns">
                                                        <div class="column has-text-left">
                                                            <div class="freshmes-member__content__job-title"><?php the_field( 'brand_job_title', $post_id ); ?></div>
                                                        </div>
                                                        <div class="column has-text-right">
                                                            <div class="freshmes-member__content__location">
                                                                <?php if ( isset( $locations ) && ! empty( $locations ) ) : ?>
                                                                    <span>
                                                        <?php
                                                        foreach( $locations as $location ) {

                                                            if ( $location === end( $locations ) ) {

                                                                echo $location->name;

                                                            } else {

                                                                echo $location->name . ', ';

                                                            }

                                                        }
                                                        ?>
                                                    </span>
                                                                    <i class="fal fa-map-marker-alt"></i>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h2 class="freshmes-member__content__title"><?php echo esc_html( get_the_title( $post_id ) ); ?></h2>
                                                    <div class="freshmes-member__content__tagline"><?php the_field( 'brand_tagline' ); ?></div>
                                                </div>
                                                <footer class="freshmes-member__footer">
                                                    <div class="columns is-v-centered">
                                                        <div class="column">
                                                            <a href="<?php echo esc_url( get_permalink( $post_id ) ); ?>" class="button freshmes-member__footer__button"><?php esc_html_e( 'Connect', 'vuelabs-framework' ); ?></a>
                                                        </div>
                                                        <div class="column has-text-right">
                                                            <div class="freshmes-member__footer__meta">
                                                                <a href="<?php echo esc_url( get_permalink( vlf_get_brand_id_from_author_id( $post_author_id ) ) ); ?>" class="freshmes-member__footer__meta__item freshmes-member__footer__meta__item--profile">
                                                                    <i class="fal fa-user-circle"></i>
                                                                </a>
                                                                <a href="#" class="freshmes-member__footer__meta__item freshmes-member__footer__meta__item--share">
                                                                    <i class="fal fa-share"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </footer>
                                            </article>
                                        </div>
                                    <?php endwhile; wp_reset_postdata(); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </section>

                <section class="freshmes-home freshmes-home-featured-offers">
                    <div class="container">
                        <nav class="freshmes-home-nav">
                            <div class="columns">
                                <div class="column">
                                    <h3 class="freshmes-home-title"><?php esc_html_e( 'Products', 'freshmes' ); ?></h3>
                                </div>
                                <div class="column has-text-right">
                                    <a href="<?php echo esc_url( get_site_url() . '/product-tag/' . $organization_tag ); ?>" class="freshmes-home-view-all"><?php esc_html_e( 'View All', 'freshmes' ); ?></a>
                                </div>
                            </div>
                        </nav>
                        <?php
                        $args = [
                            'post_type'      => 'product',
                            'posts_per_page' => 4,
                            'tax_query'      => [
                                [
                                    'taxonomy' => 'product_type',
                                    'field'    => 'slug',
                                    'terms'    => 'simple'
                                ]
                            ]
                        ];

                        if ( isset( $_GET['chapter'] ) && ! empty( $_GET['chapter'] ) ) {

                            $chapter_query = [
                                'taxonomy' => 'brand_organization',
                                'field'    => 'slug',
                                'terms'    => $_GET['chapter']
                            ];

                            $args['tax_query']['relation'] = 'AND';
                            array_push( $args['tax_query'], $chapter_query );

                        }

                        $query = new WP_Query( $args );
                        ?>
                        <?php if ( $query->have_posts() ) : ?>
                            <div class="freshmes-home-content">
                                <div class="columns">
                                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                                        <div class="column is-3">
                                            <?php vlf_display_product( get_the_ID(), 'preview-page' ); ?>
                                        </div>
                                    <?php endwhile; wp_reset_postdata(); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </section>

                <section class="freshmes-home freshmes-home-featured-events">
                    <div class="container">
                        <nav class="freshmes-home-nav">
                            <div class="columns">
                                <div class="column">
                                    <h3 class="freshmes-home-title"><?php esc_html_e( 'Events', 'freshmes' ); ?></h3>
                                </div>
                                <div class="column has-text-right">
                                    <a href="<?php echo esc_url( get_site_url() . '/product-tag/' . $organization_tag ); ?>" class="freshmes-home-view-all"><?php esc_html_e( 'View All', 'freshmes' ); ?></a>
                                </div>
                            </div>
                        </nav>
                        <?php
                        $args = [
                            'post_type'      => 'product',
                            'posts_per_page' => 4,
                            'tax_query'      => [
                                [
                                    'taxonomy' => 'product_type',
                                    'field'    => 'slug',
                                    'terms'    => 'vlf_event'
                                ]
                            ]
                        ];

                        if ( isset( $_GET['chapter'] ) && ! empty( $_GET['chapter'] ) ) {

                            $chapter_query = [
                                'taxonomy' => 'brand_organization',
                                'field'    => 'slug',
                                'terms'    => $_GET['chapter']
                            ];

                            $args['tax_query']['relation'] = 'AND';
                            array_push( $args['tax_query'], $chapter_query );

                        }

                        $query = new WP_Query( $args );
                        ?>
                        <?php if ( $query->have_posts() ) : ?>
                            <div class="freshmes-home-content">
                                <div class="columns">
                                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                                        <div class="column is-3">
                                            <?php vlf_display_product( get_the_ID(), 'preview-page' ); ?>
                                        </div>
                                    <?php endwhile; wp_reset_postdata(); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </section>

                <section class="freshmes-home freshmes-home-featured-posts">
                    <div class="container">
                        <nav class="freshmes-home-nav">
                            <div class="columns">
                                <div class="column">
                                    <h3 class="freshmes-home-title"><?php esc_html_e( 'Posts', 'freshmes' ); ?></h3>
                                </div>
                                <div class="column has-text-right">
                                    <a href="<?php echo esc_url( get_permalink( $network_id ) ); ?>" class="freshmes-home-view-all"><?php esc_html_e( 'View All', 'freshmes' ); ?></a>
                                </div>
                            </div>
                        </nav>
                        <?php
                        $args = [
                            'post_type'      => 'post',
                            'posts_per_page' => 4,
                            'tax_query'      => [
                                [
                                    'taxonomy' => 'brand_organization',
                                    'field'    => 'term_id',
                                    'terms'    => $organization_to_use
                                ]
                            ]
                        ];
                        $query = new WP_Query( $args );
                        ?>
                        <?php if ( $query->have_posts() ) : ?>
                            <div class="freshmes-home-content">
                                <div class="columns">
                                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                                        <div class="column is-3">
                                            <?php vlf_display_post( get_the_ID() ); ?>
                                        </div>
                                    <?php endwhile; wp_reset_postdata(); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </section>
            </div>
        </div>

        <?php

    }

} else {

    /**
     * Filters
     */
    get_template_part( 'template-parts/pages/part', 'filters' );

    /**
     * Networks
     */
    $authors = vlf_get_network_authors();
    $posts_per_page = 20;
    $args = [
        'post_type'      => 'brand',
        'posts_per_page' => $posts_per_page,
        'post_status'    => 'publish',
        'author__in'     => $authors
    ];

    if ( isset( $_GET['category'] ) || isset( $_GET['location'] ) || isset( $_GET['tag'] ) ) {

        $tax_query = [];
        if ( isset( $_GET['category'] ) ) {

            $category_query = [
                'taxonomy' => 'brand_category',
                'field'    => 'slug',
                'terms'    => $_GET['category']
            ];

            array_push( $tax_query, $category_query );

        }

        if ( isset( $_GET['location'] ) ) {

            $location_query = [
                'taxonomy' => 'brand_location',
                'field'    => 'slug',
                'terms'    => $_GET['location']
            ];

            array_push( $tax_query, $location_query );

        }

        if ( isset( $_GET['tag'] ) ) {

            $tag_query = [
                'taxonomy' => 'brand_tag',
                'field'    => 'slug',
                'terms'    => $_GET['tag']
            ];

            array_push( $tax_query, $tag_query );

        }

        $args['tax_query'] = $tax_query;

    }

    ?>

    <input type="hidden" id="freshmes-template-url" value="<?php echo esc_url( get_permalink( get_queried_object_id() ) ); ?>">

    <?php

    $networks = new WP_Query( $args );
    if ( $networks->have_posts() ) {

        $networks_count = $networks->post_count;

        ?>

        <input type="hidden" id="freshmes-post-type" value="brand">

        <section class="freshmes-networks">
            <div class="container">
                <header class="freshmes-networks__header">
                    <div class="columns is-v-centered">
                        <div class="column has-text-left">
                            <h1 class="freshmes-networks__header__title"><?php the_title(); ?></h1>
                            <div class="freshmes-networks__header__results"><?php printf( _n( '%s Network', '%s Networks', intval( $networks_count ), 'vuelabs-framework' ), intval( $networks_count ) ); ?></div>
                            <?php if ( isset( $_GET ) && ! empty( $_GET ) ) : ?>
                                <a href="<?php echo esc_url( get_site_url() . '/networks/' ); ?>" class="freshmes-networks__header__reset"><?php esc_html_e( 'Reset', 'vuelabs-framework' ); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </header>
                <div class="freshmes-networks__content">
                    <div class="columns is-multiline">
                        <?php
                        while ( $networks->have_posts() ) : $networks->the_post();

                            get_template_part( 'template-parts/pages/part', 'network' );

                        endwhile; wp_reset_postdata();
                        ?>
                    </div>
                </div>
                <?php if ( intval( $networks_count ) >= $posts_per_page ) : ?>
                    <footer class="freshmes-networks__footer has-text-centered">
                        <button class="button freshmes-networks__footer__load-more"><?php esc_html_e( 'Load More', 'vuelabs-framework' ); ?></button>
                    </footer>
                <?php endif; ?>
            </div>
        </section>

        <?php

    } else {

        get_template_part( 'template-parts/pages/part', 'networks-none' );

    }

}

get_footer();
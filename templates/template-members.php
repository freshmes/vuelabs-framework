<?php
/**
 * Template Name: Members
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/templates
 * @since 1.0.0
 */
get_header();

/**
 * Filters
 */
get_template_part( 'template-parts/pages/part', 'filters' );

/**
 * Members
 */
$args = [
    'post_type'      => 'brand',
    'posts_per_page' => 8,
    'post_status'    => 'publish'
];

if ( isset( $_GET['position'] ) && ! empty( $_GET['position'] ) ) {

    $args['posts_per_page'] = -1;
    $args['meta_query'] = [
        [
            'key'     => 'brand_job_title',
            'value'   => esc_html( sanitize_text_field( $_GET['position'] ) ),
            'compare' => 'LIKE'
        ]
    ];

}

if ( isset( $_GET['membership'] ) && ! empty( $_GET['membership'] ) ) {

    $product_id = get_page_by_path( $_GET['membership'], OBJECT, 'product' );
    $product_id = $product_id->ID;

    $membership_query = [
        'key'   => 'brand_membership',
        'value' => intval( $product_id ),
    ];

    if ( isset( $args['meta_query'] ) && ! empty( $args['meta_query'] ) ) {

        array_push( $args['meta_query'], $membership_query );
        $args['meta_query']['relation'] = 'AND';

    } else {

        $args['meta_query'] = [];
        array_push( $args['meta_query'], $membership_query );

    }

}

if ( isset( $_GET['category'] ) || isset( $_GET['location'] ) || isset( $_GET['tag'] ) ) {

    $tax_query = [];
    if ( isset( $_GET['category'] ) ) {

        $category_query = [
            'taxonomy' => 'brand_category',
            'field'    => 'slug',
            'terms'    => $_GET['category']
        ];

        array_push( $tax_query, $category_query );

    }

    if ( isset( $_GET['location'] ) ) {

        $location_query = [
            'taxonomy' => 'brand_location',
            'field'    => 'slug',
            'terms'    => $_GET['location']
        ];

        array_push( $tax_query, $location_query );

    }

    if ( isset( $_GET['tag'] ) ) {

        $tag_query = [
            'taxonomy' => 'brand_tag',
            'field'    => 'slug',
            'terms'    => $_GET['tag']
        ];

        array_push( $tax_query, $tag_query );

    }

    $args['tax_query'] = $tax_query;

}

$members = new WP_Query( $args );

?>
    <input type="hidden" id="freshmes-template-url" value="<?php echo esc_url( get_permalink( get_queried_object_id() ) ); ?>">
<?php

if ( $members->have_posts() ) {

    $members_count = $members->post_count;

    ?>

    <input type="hidden" id="freshmes-post-type" value="brand">

    <section class="freshmes-members">
        <div class="container">
            <header class="freshmes-members__header">
                <div class="columns is-v-centered">
                    <div class="column has-text-left">
                        <h1 class="freshmes-members__header__title"><?php the_title(); ?></h1>
                        <div class="freshmes-members__header__results"><?php printf( _n( '%s Member', '%s Members', intval( $members_count ), 'vuelabs-framework' ), intval( $members_count ) ); ?></div>
                    </div>
                    <?php if ( is_user_logged_in() ) : ?>
                        <div class="column has-text-right">
                            <button class="button freshmes-members__header__add-members" data-user="<?php echo esc_attr( get_current_user_id() ); ?>"><?php esc_html_e( 'Add Members', 'vuelabs-framework' ); ?></button>
                            <div class="freshmes-members__header__select-all">
                                <span class="freshmes-members__header__select-all__text"><?php esc_html_e( 'Select All', 'vuelabs-framework' ); ?></span>
                                <span class="freshmes-members__header__select-all__box"></span>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </header>
            <div class="freshmes-members__content">
                <div class="columns is-multiline">
                    <?php
                    while ( $members->have_posts() ) : $members->the_post();

                    get_template_part( 'template-parts/pages/part', 'member' );

                    endwhile; wp_reset_postdata();
                    ?>
                </div>
            </div>
            <?php if ( intval( $members_count ) >= 8 ) : ?>
                <footer class="freshmes-members__footer has-text-centered">
                    <button class="button freshmes-members__footer__load-more"><?php esc_html_e( 'Load More', 'vuelabs-framework' ); ?></button>
                </footer>
            <?php endif; ?>
        </div>
    </section>

    <?php

} else {

    get_template_part( 'template-parts/pages/part', 'members-none' );

}

get_footer();
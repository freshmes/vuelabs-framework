<?php
global $post;

/**
 * Get Some information first.
 */
$post_id   = $post->ID;
$author_id = $post->post_author;
$author    = get_userdata( $author_id );
$user      = new WP_User( $author_id );

$gallery   = get_field( 'brand_gallery' );
$tagline   = get_field( 'brand_tagline' );

$author_image       = get_field( 'brand_profile_image' );
$author_name        = $author->first_name . ' ' . $author->last_name;
$author_description = get_field( 'brand_blog_description' );

$header_title = $author->first_name . ' ' . $author->last_name;

if ( in_array( 'freshmes_business', $user->roles ) || in_array( 'freshmes_organization', $user->roles ) ) {

    $header_title = get_field( 'brand_business_name' );

}

if ( isset( $gallery ) && ! empty( $gallery ) ) {

    $header_image_link = $gallery[0]['url'];
    $header_image = 'style="background-image: url(' . esc_url( $header_image_link ) . ');"';

}

$content = new WP_Query( [
    'post_type'      => 'product',
    'author'         => intval( $author_id ),
    'post_status'    => 'publish',
    'posts_per_page' => 6
] );

$categories = [];

?>

<div class="brand-shop">
    <header class="brand-shop__header" <?php echo $header_image; ?>>
        <div class="brand-shop__header__overlay"></div>
        <div class="brand-shop__header__content">
            <div class="brand-shop__header__content__image">
                <?php echo wp_get_attachment_image( $author_image, 'thumbnail' ); ?>
                <div class="brand-shop__header__content__image__text"><?php echo esc_html__( 'Sold by ', 'vuelabs-framework' ) . esc_html( $author_name ); ?></div>
            </div>
            <h1 class="brand-shop__header__content__title"><?php echo esc_html( $header_title ) . esc_html__( ' Shop', 'vuelabs-framework' ); ?></h1>
            <?php if ( isset( $tagline ) && ! empty( $tagline ) ) : ?>
                <h2 class="brand-shop__header__content__subtitle"><?php echo esc_html( $tagline ); ?></h2>
            <?php endif; ?>
        </div>
    </header>
    <div class="brand-shop__content">
        <div class="container">
            <header class="brand-shop__content__header">
                <div class="columns">
                    <div class="column is-6">
                        <h2 class="brand-shop__content__header__title"><?php esc_html_e( 'The Shop', 'vuelabs-framework' ); ?></h2>
                        <ul class="brand-shop__content__header__meta">
                            <li class="brand-shop__content__header__meta__item brand-shop__content__header__meta__item--filter">
                                <a href="#">
                                    <span class="brand-shop__content__header__meta__item__icon"><i class="fal fa-sliders-h"></i></span>
                                    <span class="brand-shop__content__header__meta__item__label"><?php esc_html_e( 'Filter', 'vuelabs-framework' ); ?></span>
                                </a>
                            </li>
                            <li class="brand-shop__content__header__meta__item brand-shop__content__header__meta__item--results"><?php printf( _n( '%s Result', '%s Results', intval( $content->post_count ), 'vuelabs-framework' ), intval( $content->post_count ) ); ?></li>
                        </ul>
                    </div>
                    <div class="column is-6 has-text-right">
                        <div class="brand-shop__content__header__sort-by">
                            <select class="brand-shop__content__header__sort-by__select">
                                <option value="date" selected><?php esc_html_e( 'Sort by Latest', 'vuelabs-framework' ); ?></option>
                                <option value="price"><?php esc_html_e( 'Sort by price: low to high', 'vuelabs-framework' ); ?></option>
                                <option value="price-desc"><?php esc_html_e( 'Sort by price: high to low', 'vuelabs-framework' ); ?></option>
                            </select>
                        </div>
                        <div class="brand-shop__content__header__breadcrumbs">
                            <a href="<?php echo esc_url( get_permalink( $post_id ) ); ?>"><?php echo esc_html( $header_title ); ?></a>
                            <span class="brand-shop__content__header__breadcrumbs__separator">/</span>
                            <span class="brand-shop__content__header__breadcrumbs__current"><?php esc_html_e( 'Shop', 'vuelabs-framework' ); ?></span>
                        </div>
                    </div>
                </div>
            </header>
            <div class="brand-shop__content__products">
                <?php
                $prices = [];
                if ( $content->have_posts() ) {

                    echo '<div class="columns is-multiline">';

                    while ( $content->have_posts() ) : $content->the_post();

                        $product = wc_get_product( get_the_ID() );
                        $product_price = $product->get_price();
                        array_push( $prices, intval( round( $product_price ) ) );

                        // Categories
                        $product_cats = get_the_terms( get_the_ID(), 'product_cat' );
                        if ( isset( $product_cats ) && ! empty( $product_cats ) ) {

                            foreach ( $product_cats as $product_cat ) {

                                if ( ! isset( $categories[ $product_cat->term_id ] ) ) {

                                    $categories[ $product_cat->term_id ] = 0;

                                }
                                $categories[ $product_cat->term_id ]++;

                            }

                        }

                        echo '<div class="column is-4">';
                        vlf_display_product( get_the_ID(), 'brand-page', true );
                        echo '</div>';

                    endwhile; wp_reset_postdata();

                    echo '</div>';

                } else {

                    get_template_part( 'template-parts/brand/shop/part-content', 'none' );

                }
                ?>
                <div class="brand-shop__content__products__filters">
                    <?php do_action( 'woocommerce_sidebar' ); ?>
                </div>
            </div>
            <?php if ( intval( $content->post_count ) >= 6 ) : ?>
            <div class="brand-shop__content__footer has-text-centered">
                <a href="#" class="button brand-shop__content__footer__load-more" data-author-id="<?php echo esc_attr( $author_id ); ?>"><?php esc_html_e( 'Load More', 'vuelabs-framework' ); ?></a>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php
get_header();

global $post;

/**
 * Get Some information first.
 */
$author_id = $post->post_author;
$post_id   = vlf_get_brand_id_from_author_id( $author_id );
$author    = get_userdata( $author_id );
$user      = new WP_User( $author_id );

$author_name        = $author->first_name . ' ' . $author->last_name;
$author_image       = get_field( 'brand_profile_image', $post_id );
$author_description = get_field( 'brand_blog_description', $post_id );

$header_title = $author->first_name . ' ' . $author->last_name;

if ( in_array( 'freshmes_business', $user->roles ) || in_array( 'freshmes_organization', $user->roles ) ) {

    $header_title = get_field( 'brand_business_name', $post_id );

}

$header_image = '';
if ( has_post_thumbnail( $post_id ) ) {

    $header_image_link = get_the_post_thumbnail_url( $post_id, 'full' );
    $header_image = 'style="background-image: url(' . esc_url( $header_image_link ) . ');";';

}

$member_button_class = 'button brand-blog-single__author__connect';
$member_button_label = esc_html__( 'Connect', 'vuelabs-framework' );
if ( vlf_has_connection( get_current_user_id(), $author_id ) ) {

    $member_button_class .= ' brand-blog-single__author__connect--disconnect';
    $member_button_label = esc_html__( 'Disconnect', 'vuelabs-framework' );

}

$post_tags = get_the_tags( $post->ID );
$post_categories = get_the_terms( $post->ID, 'category' );

?>

<div class="brand-blog-single">
    <header class="brand-blog-single__header" <?php echo $header_image; ?>></header>
    <div class="brand-blog-single__main">
        <div class="container">
            <div class="brand-blog-single__breadcrumbs">
                <a href="<?php echo esc_url( get_permalink( $post_id ) ); ?>"><?php echo esc_html( $header_title ); ?></a>
                <span class="brand-blog-single__breadcrumbs__separator">/</span>
                <a href="<?php echo esc_url( get_permalink( $post_id ) . '/blog' ); ?>"><?php esc_html_e( 'Blog', 'vuelabs-framework' ); ?></a>
                <span class="brand-blog-single__breadcrumbs__separator">/</span>
                <span class="brand-blog-single__breadcrumbs__current"><?php esc_html_e( 'All Stories', 'vuelabs-framework' ); ?></span>
            </div>
            <div class="columns">
                <div class="column is-8">
                    <article id="post-<?php the_ID(); ?>" <?php post_class( 'brand-blog-single__main__post' ); ?>>
                        <h1 class="title brand-blog-single__main__post__title"><?php the_title(); ?></h1>
                        <div class="brand-blog-single__main__post__content content">
                            <?php the_content(); ?>
                        </div>
                    </article>
                </div>
                <div class="column is-3 is-offset-1">
                    <div class="brand-blog-single__main__sidebar">
                        <?php dynamic_sidebar( 'brand-blog-sidebar' ); ?>
                    </div>
                </div>
            </div>
            <div class="brand-blog-single__author">
                <div class="columns is-gapless">
                    <div class="column is-9">
                        <div class="columns is-vcentered">
                            <div class="column is-2">
                                <div class="brand-blog-single__author__image">
                                    <?php echo wp_get_attachment_image( $author_image, 'thumbnail' ); ?>
                                </div>
                            </div>
                            <div class="column is-7">
                                <div class="brand-blog-single__author__by"><?php esc_html_e( 'Written by:', 'vuelabs-framework' ); ?></div>
                                <div class="brand-blog-single__author__name"><?php echo esc_html( $author_name ); ?></div>
                                <div class="brand-blog-single__author__description"><?php echo wp_kses( $author_description, [ 'br' => [] ] ); ?></div>
                            </div>
                            <?php if ( is_user_logged_in() ) : ?>
                                <div class="column is-3 has-text-left">
                                    <a href="#" class="<?php echo esc_attr( $member_button_class ); ?>" data-user-a="<?php echo esc_attr( get_current_user_id() ); ?>" data-user-b="<?php echo esc_attr( $author_id ); ?>"><?php echo esc_html( $member_button_label ); ?></a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="brand-blog-single__footer">
                <div class="columns is-v-centered">
                    <div class="column">
                        <div class="brand-blog-single__footer__share">
                            <span class="brand-blog-single__footer__share__text"><?php esc_html_e( 'Share this post:', 'vuelabs-framework' ); ?></span>
                            <a href="#" class="brand-blog-single__footer__share__item brand-blog-single__footer__share__item--facebook"><i class="fab fa-facebook-square"></i></a>
                            <a href="#" class="brand-blog-single__footer__share__item brand-blog-single__footer__share__item--linkedin"><i class="fab fa-linkedin"></i></a>
                            <a href="#" class="brand-blog-single__footer__share__item brand-blog-single__footer__share__item--twitter"><i class="fab fa-twitter-square"></i></a>
                        </div>
                    </div>
                    <?php if ( ! is_wp_error( $post_tags ) && $post_tags !== false ) : ?>
                        <div class="column has-text-centered">
                            <div class="columns is-multiline is-gapless">
                                <?php
                                foreach ( $post_tags as $post_tag ) {

                                    echo '<div class="column is-4 has-text-left brand-blog-single__footer__post-tag">' . esc_html( $post_tag->name ) . '</div>';

                                }
                                ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php if ( ! is_wp_error( $post_categories ) ) : ?>
                <?php
                    $categories = [];
                    foreach ( $post_categories as $post_category ) {

                        array_push( $categories, $post_category->term_id );

                    }
                    $related = new WP_Query( [
                        'posts_per_page' => 4,
                        'post_type'      => 'post',
                        'post_status'    => 'publish',
                        'cat'            => $categories
                    ] );
                    if ( $related->have_posts() ) {

                        ?>

                        <div class="brand-blog-single__related-posts">
                            <div class="brand-blog-single__related-posts__title"><?php esc_html_e( 'Related Posts', 'vuelabs-framework' ); ?></div>
                            <div class="columns">
                                <?php
                                while ( $related->have_posts() ) : $related->the_post();

                                echo '<div class="column is-3 brand-blog-single__related-posts__post">';
                                vlf_display_post( get_the_ID() );
                                echo '</div>';

                                endwhile; wp_reset_postdata();
                                ?>
                            </div>
                        </div>

                        <?php

                    }
                ?>
            <?php endif; ?>
            <div class="brand-blog-single__comments">
                <?php
                if ( comments_open() || 0 !== intval( get_comments_number() ) ) :
                    comments_template();
                endif;
                ?>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
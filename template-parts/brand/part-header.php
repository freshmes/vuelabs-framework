<?php
global $post;
$user_id    = get_post_field( 'post_author', $post->ID );
$user       = new WP_User( $user_id );
$categories = get_the_terms( get_the_ID(), 'brand_category' );
$rating     = vlf_get_brand_rating( get_the_ID() );
$email      = $user->user_email;

$phone      = get_field( 'brand_phone' );
$phone      = vlf_check_field( $phone );

$website    = get_field( 'brand_website' );
$website    = vlf_check_field( $website );

$facebook   = get_field( 'brand_facebook' );
$facebook   = vlf_check_field( $facebook );

$instagram  = get_field( 'brand_instagram' );
$instagram  = vlf_check_field( $instagram );

$linkedin   = get_field( 'brand_linkedin' );
$linkedin   = vlf_check_field( $linkedin );

// Get ACF fields.
if ( in_array( 'freshmes_organization', $user->roles ) || in_array( 'freshmes_business', $user->roles ) ) {

    $business_name = get_field( 'brand_business_name' );
    $business_name = vlf_check_field( $business_name );

    $verified      = get_field( 'brand_is_verified' );
    $verified      = vlf_check_field( $verified );

    $address       = get_field( 'brand_address' );
    $address       = vlf_check_field( $address );

    $phone         = get_field( 'brand_business_phone' );
    $phone         = vlf_check_field( $phone );

    $logo          = get_field( 'brand_logo' );
    $logo          = vlf_check_field( $logo );

    $logo_icon     = get_field( 'brand_logo_icon' );
    $logo_icon     = vlf_check_field( $logo_icon );

}

?>
<header class="brand-single-page__header">
    <div class="container">
        <div class="columns is-v-centered">
            <div class="column is-8 brand-single-page__header__column">
                <div class="columns is-v-centered is-gapless is-multiline">
                    <div class="column is-8">
                        <?php if ( isset( $categories ) && ! empty( $categories ) ) : ?>
                            <div class="brand-single-page__header__categories">
                                <?php foreach ( $categories as $category ) : ?>
                                    <span class="brand-single-page__header__categories__category"><?php echo esc_html( $category->name ); ?></span>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                        <?php if ( in_array( 'freshmes_organization', $user->roles ) || in_array( 'freshmes_business', $user->roles ) ) : ?>
                            <?php if ( $business_name ) : ?>
                                <h1 class="title brand-single-page__header__title">
                                    <?php echo esc_html( $business_name ); ?>
                                    <?php if ( $verified ) : ?>
                                        <i class="fas fa-check-circle"></i>
                                    <?php endif; ?>
                                </h1>
                            <?php endif; ?>
                        <?php else : ?>
                            <h1 class="title brand-single-page__header__title"><?php echo esc_html( $user->first_name . ' ' . $user->last_name ); ?></h1>
                        <?php endif; ?>
                    </div>
                    <div class="column is-4 has-text-right">
                        <?php if ( in_array( 'freshmes_organization', $user->roles ) || in_array( 'freshmes_business', $user->roles ) ) : ?>

                            <div class="brand-single-page__header__logo-icon">
                                <?php echo wp_get_attachment_image( $logo_icon, 'thumbnail' ); ?>
                            </div>

                        <?php else : ?>
                            <?php
                                // TODO :: Check if user is a member of some organization.
                            ?>
                        <?php endif; ?>
                    </div>
                    <div class="column is-12">
                        <ul class="brand-single-page__header__meta">
                            <?php if ( ! empty( $rating ) ) : ?>
                                <li class="brand-single-page__header__meta__item brand-single-page__header__meta__item--rating">
                                    <?php vlf_show_brand_rating( get_the_ID(), $rating ); ?>
                                </li>
                            <?php endif; ?>
                            <?php if ( $phone ) : ?>
                                <li class="brand-single-page__header__meta__item brand-single-page__header__meta__item--phone">
                                    <a href="tel:<?php echo esc_attr( $phone ); ?>">
                                        <span class="meta-icon"><i class="fal fa-phone"></i></span>
                                        <span class="meta-label"><?php echo esc_html( $phone ); ?></span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if ( $email ) : ?>
                                <li class="brand-single-page__header__meta__item brand-single-page__header__meta__item--email">
                                    <a href="mailto:<?php echo esc_attr( $email ); ?>">
                                        <span class="meta-icon"><i class="fal fa-envelope"></i></span>
                                        <span class="meta-label"><?php esc_html_e( 'Email Member', 'vuelabs-framework' ); ?></span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if ( $website ) : ?>
                                <li class="brand-single-page__header__meta__item brand-single-page__header__meta__item--website">
                                    <a href="<?php echo esc_url( $website ); ?>" target="_blank" rel="nofollow">
                                        <span class="meta-icon"><i class="fal fa-globe"></i></span>
                                        <span class="meta-label"><?php esc_html_e( 'Visit Website', 'vuelabs-framework' ); ?></span>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
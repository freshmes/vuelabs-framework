<div class="share-popup">
    <a href="#" class="share-popup__close"><i class="fal fa-times"></i></a>
    <header class="share-popup__header">
        <div class="share-popup__header__title"><?php esc_html_e( 'Share this profile', 'vuelabs-framework' ); ?></div>
    </header>
    <div class="share-popup__content">
        <div class="columns">
            <div class="column is-4 has-text-centered">
                <a href="#" class="share-popup__content__item share-popup__content__item--facebook" target="_blank">
                    <i class="fab fa-facebook"></i>
                </a>
            </div>
            <div class="column is-4 has-text-centered">
                <a href="#" class="share-popup__content__item share-popup__content__item--linkedin" target="_blank">
                    <i class="fab fa-linkedin"></i>
                </a>
            </div>
            <div class="column is-4 has-text-centered">
                <a href="#" class="share-popup__content__item share-popup__content__item--twitter" data-title="<?php echo esc_attr( get_the_title( get_the_ID() ) ); ?>">
                    <i class="fab fa-twitter-square"></i>
                </a>
            </div>
        </div>
    </div>
</div>
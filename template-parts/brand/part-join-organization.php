<div class="join-organization-popup">
    <a href="#" class="join-organization-popup__close"><i class="fal fa-times"></i></a>
    <header class="join-organization-popup__header">
        <div class="join-organization-popup__header__title"></div>
    </header>
    <div class="join-organization-popup__content">
    </div>
</div>
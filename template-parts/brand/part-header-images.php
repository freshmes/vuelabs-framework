<?php
    $images = get_field( 'brand_gallery' );
    $images = vlf_check_field( $images );
?>
<header class="brand-single-page__header-images">
    <?php
    if ( $images ) {

        // Check number of images and show different layout based on that.
        if ( count( $images ) < 4 ) {

            ?>

            <div class="columns is-gapless">
                <?php

                foreach ( $images as $image ) {

                    ?>

                    <div class="column">
                        <div class="brand-single-page__header-images__column-image" style="background-image: url('<?php echo esc_url( $image['sizes']['large'] ); ?>');"></div>
                    </div>

                    <?php

                }

                ?>
            </div>

            <?php

        } else {

            ?>

            <div class="brand-single-page__header-images__carousel">
                <?php

                foreach ( $images as $image ) {

                    ?>

                    <div class="brand-single-page__header-images__carousel__item" style="background-image: url('<?php echo esc_url( $image['sizes']['large'] ); ?>');"></div>

                    <?php

                }

                ?>
            </div>

            <?php

        }

    }
    ?>
</header>
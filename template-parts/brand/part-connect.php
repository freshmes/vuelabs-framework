<?php
global $post;
$author_id = $post->post_author;
$author    = get_userdata( $author_id );
?>
<div class="connect-popup">
    <a href="#" class="connect-popup__close"><i class="fal fa-times"></i></a>
    <header class="connect-popup__header">
        <div class="connect-popup__header__title connect-popup__header__title--has-subtitle"></div>
        <div class="connect-popup__header__subtitle"></div>
    </header>
    <div class="connect-popup__content">
    </div>
</div>
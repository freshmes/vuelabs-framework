<?php
$special_note = get_field( 'account_special_note', 'option' );
$special_note = vlf_check_field( $special_note );
?>
<div class="account-popup">
    <a href="#" class="account-popup__close"><i class="fal fa-times"></i></a>
    <header class="account-popup__header has-text-centered">
        <div class="account-popup__header__title"><?php esc_html_e( 'Join Freshmes', 'vuelabs-framework' ); ?></div>
        <div class="account-popup__header__subtitle"><?php esc_html_e( 'Join, Connect & Master the Juggle', 'vuelabs-framework' ); ?></div>
    </header>
    <div class="account-popup__content">
        <div class="account-popup__content__item is-show" data-step-id="1">
            <button class="button is-fullwidth account-popup__content__item__email"><?php esc_html_e( 'Sign up with Email', 'vuelabs-framework' ); ?></button>
            <div class="account-popup__content__item__or"><?php esc_html_e( 'OR', 'vuelabs-framework' ); ?></div>
            <button class="button is-fullwidth account-popup__content__item__social account-popup__content__item__social--google" disabled>
                <span class="icon"><i class="fab fa-google fa-fw"></i></span>
                <span class="text"><?php esc_html_e( 'Sign up with Google', 'vuelabs-framework' ); ?></span>
            </button>
            <button class="button is-fullwidth account-popup__content__item__social account-popup__content__item__social--facebook" disabled>
                <span class="icon"><i class="fab fa-facebook-f fa-fw"></i></span>
                <span class="text"><?php esc_html_e( 'Sign up with Facebook', 'vuelabs-framework' ); ?></span>
            </button>
        </div>
        <div class="account-popup__content__item" data-step-id="2">
            <form>
                <div class="field">
                    <div class="control">
                        <select id="account-register-membership" class="is-select2" placeholder="<?php esc_attr_e( 'Membership Type', 'vuelabs-framework' ); ?>">
                            <option></option>
                            <?php
                                $memberships = get_field( 'memberships', 'option' );
                                $memberships = vlf_check_field( $memberships );
                                if ( $memberships ) {

                                    foreach ( $memberships as $membership ) {

                                        echo '<option value="' . esc_attr( $membership->ID ) . '">' . esc_html( $membership->post_title ) . '</option>';

                                    }

                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="field">
                    <div class="control">
                        <input type="email" class="input" id="account-register-email" placeholder="<?php esc_attr_e( 'Email', 'vuelabs-framework' ); ?>" autocapitalize="off" autocorrect="off" autocomplete="email">
                    </div>
                </div>
                <div class="field">
                    <div class="control">
                        <input type="password" class="input" id="account-register-password" placeholder="<?php esc_attr_e( 'Create password', 'vuelabs-framework' ); ?>">
                    </div>
                </div>
                <?php if ( $special_note ) : ?>
                <div class="special-note"><?php echo wp_kses( $special_note, [ 'a' => [ 'target' => [], 'href' => [] ] ] ); ?></div>
                <?php endif; ?>
                <button type="submit" class="button is-fullwidth account-popup__content__item__sign-up"><?php esc_html_e( 'Sign Up', 'vuelabs-framework' ); ?></button>
            </form>
        </div>
    </div>
    <footer class="account-popup__footer">
        <div class="account-popup__footer__content"><?php echo wp_kses( __( 'Already a Freshmes member? <a href="' . esc_url( wp_login_url( get_home_url( '/' ) ) ) . '" class="is-login-toggle">Sign in here</a>', 'vuelabs-framework' ), [ 'a' => [ 'class' => [], 'href' => [] ] ] ); ?></div>
    </footer>
</div>
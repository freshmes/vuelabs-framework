<button class="navigator-toggle"></button>
<div class="navigator-popup" data-popup-id="" data-popup-type="" data-user-id="<?php echo esc_attr( get_current_user_id() ); ?>">
    <a href="#" class="navigator-popup__close"><i class="fal fa-times"></i></a>
    <header class="navigator-popup__header">
        <div class="navigator-popup__header__title"></div>
        <a href="#" class="navigator-popup__header__view-all is-hidden"><?php esc_html_e( 'View All', 'vuelabs-framework' ); ?></a>
        <a href="#" class="navigator-popup__header__add-new is-hidden"></a>
    </header>
    <div class="navigator-popup__content">

    </div>
    <footer class="navigator-popup__footer">

    </footer>
</div>
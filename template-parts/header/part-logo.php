<?php
if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ) {

    $logo = get_custom_logo();
    $html = is_home() ? '<h1 class="site-header__site-logo">' . $logo . '</h1>' : $logo;

} else {

    $tag = is_home() ? 'h1' : 'div';
    $html = '<' . esc_attr( $tag ) . ' class="site-header__site-title title is-5"><a href="' . esc_url( home_url( '/' ) ) . '" rel="home">' . esc_html( get_bloginfo( 'name' ) ) . '</a></' . esc_attr( $tag ) . '>';
    if ( '' !== get_bloginfo( 'description' ) ) {
        $html .= '<p class="site-header__site-description subtitle is-6">' . esc_html( get_bloginfo( 'description', 'display' ) ) . '</p>';
    }

}

echo $html;
<nav id="site-navigation" class="site-header__site-navigation" role="navigation">
    <?php
    wp_nav_menu( [
        'theme_location'  => 'primary',
        'menu_class'      => 'site-header__primary-navigation is-hidden-touch'
    ] );
    ?>
    <button class="button site-header__site-menu-toggle is-hidden-desktop" aria-controls="site-navigation" aria-expanded="false">
        <i class="fal fa-bars"></i>
    </button>
</nav>

<script>
    jQuery(document).ready(function($) {
      if ( $('.menu-brand-page').length > 0 ) {
        $('.menu-brand-page > a').attr('href', '<?php echo esc_url( get_permalink( vlf_get_brand_id_from_author_id( get_current_user_id() ) ) ); ?>');
      }
    });
</script>
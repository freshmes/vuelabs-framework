<?php
if ( is_user_logged_in() ) {

    $user_id = get_current_user_id();
    if ( is_singular( 'brand' ) ) {

        global $post;
        if ( intval( $post->post_author ) === intval( $user_id ) && $post->post_status === 'private' ) {

            ?>

            <a href="#" class="button site-header__site-button is-brand-publish" data-brand-id="<?php echo esc_attr( vlf_get_brand_id_from_author_id( $user_id ) ); ?>"><?php esc_html_e( 'Publish', 'vuelabs-framework' ); ?></a>

            <?php

        }

    } else {

        $brand_id = vlf_get_brand_id_from_author_id( $user_id );
        $author   = new WP_User( $user_id );
        $author_image = get_field( 'brand_profile_image', $brand_id );

        ?>

        <a href="<?php echo esc_url( get_permalink( $brand_id ) ); ?>" class="site-header__site-button is-brand-profile">
            <?php echo wp_get_attachment_image( $author_image, 'thumbnail' ); ?>
            <span class="is-brand-profile__username"><?php echo esc_html( $author->first_name . ' ' . $author->last_name ); ?></span>
        </a>

        <?php

        if ( in_array( 'freshmes_personal', $author->roles ) ) {

            ?>

            <a href="#" class="button site-header__site-button is-get-premium is-hidden"><?php esc_html_e( 'Get Premium', 'vuelabs-framework' ); ?></a>

            <?php

        }

        if ( is_singular( 'post' ) || is_singular( 'product' ) ) {

            global $post;
            if ( get_post_status( $post->ID ) === 'private' ) {

                ?>

                <a href="#" class="button site-header__site-button is-post-type-publish" data-brand-id="<?php echo esc_attr( $post->ID ); ?>"><?php esc_html_e( 'Publish', 'vuelabs-framework' ); ?></a>
                <a href="#" class="button site-header__site-button is-post-type-edit" data-post-type="<?php echo esc_attr( get_post_type( $post->ID ) ); ?>" data-brand-id="<?php echo esc_attr( $post->ID ); ?>"><?php esc_html_e( 'Edit', 'vuelabs-framework' ); ?></a>

                <?php

            }

        }

    }

} else {

    // Login/Register
    ?>

    <a href="#" class="button site-header__site-button is-account-toggle"><?php esc_html_e( 'Log in', 'vuelabs-framework' ); ?></a>

    <?php

}
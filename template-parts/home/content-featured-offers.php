<section class="freshmes-home freshmes-home-featured-offers">
    <div class="container">
        <nav class="freshmes-home-nav">
            <div class="columns">
                <div class="column">
                    <h3 class="freshmes-home-title"><?php esc_html_e( 'Featured Offers', 'freshmes' ); ?></h3>
                </div>
                <div class="column has-text-right">
                    <a href="<?php echo esc_url( get_site_url() . '/shop/' ); ?>" class="freshmes-home-view-all"><?php esc_html_e( 'View All', 'freshmes' ); ?></a>
                </div>
            </div>
        </nav>
        <?php
        $args = [
            'post_type'      => 'product',
            'posts_per_page' => 3,
            'tax_query'      => [
                [
                    'taxonomy' => 'product_type',
                    'field'    => 'slug',
                    'terms'    => 'simple'
                ]
            ]
        ];
        $query = new WP_Query( $args );
        ?>
        <?php if ( $query->have_posts() ) : ?>
            <div class="freshmes-home-content">
                <div class="columns">
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        <div class="column">
                            <?php vlf_display_product( get_the_ID(), 'preview-page', true, true, esc_html__( 'Featured Sale', 'vuelabs-framework' ) ); ?>
                        </div>
                    <?php endwhile; wp_reset_postdata(); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>
<section class="freshmes-home freshmes-home-featured-members">
    <div class="container">
        <nav class="freshmes-home-nav">
            <div class="columns">
                <div class="column">
                    <h3 class="freshmes-home-title"><?php esc_html_e( 'Featured Members', 'freshmes' ); ?></h3>
                </div>
                <div class="column has-text-right">
                    <a href="<?php echo esc_url( get_site_url() . '/members/' ); ?>" class="freshmes-home-view-all"><?php esc_html_e( 'View All', 'freshmes' ); ?></a>
                </div>
            </div>
        </nav>
        <?php
            $args = [
                'post_type'      => 'brand',
                'posts_per_page' => 4,
                'post_status'    => 'publish'
            ];
            $query = new WP_Query( $args );
        ?>
        <?php if ( $query->have_posts() ) : ?>
            <div class="freshmes-home-content">
                <div class="columns">
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        <?php
                            $post_id = get_the_ID();
                            $author_id = get_post_field( 'post_author', $post_id );

                            $author = new WP_User( $author_id );

                            $member_button_class = 'button member__connect';
                            $member_button_label = esc_html__( 'Connect', 'vuelabs-framework' );
                            if ( vlf_has_connection( get_current_user_id(), $author_id ) ) {

                                $member_button_class .= ' member__connect--disconnect';
                                $member_button_label = esc_html__( 'Disconnect', 'vuelabs-framework' );

                            }
                        ?>
                        <div class="column">
                            <div class="member has-text-centered">
                                <div class="member__content">
                                    <div class="member__image">
                                        <?php echo wp_get_attachment_image( get_field( 'brand_profile_image', get_the_ID() ), 'thumbnail' ); ?>
                                    </div>
                                    <div class="member__name"><?php the_title(); ?></div>
                                    <div class="member__subtitle">
                                        <a href="<?php echo esc_url( add_query_arg( [ 'position' => get_field( 'brand_job_title' ) ], get_site_url() . '/members/' ) ); ?>"><?php the_field( 'brand_job_title' ); ?></a>
                                    </div>
                                    <?php
                                    if ( in_array( 'freshmes_personal', $author->roles ) ) {

                                        $organization = get_the_terms( get_the_ID(), 'brand_organization' );
                                        if ( isset( $organization ) && ! empty( $organization ) ) {

                                            $continue = true;
                                            foreach ( $organization as $org ) {

                                                if ( $continue === false ) {

                                                    continue;

                                                }

                                                if ( $org->parent === 0 ) {

                                                    $continue = false;
                                                    echo '<div class="member__company">' . esc_html( $org->name ) . '</div>';

                                                }

                                            }

                                        }

                                    } else {

                                        $company = get_field( 'brand_business_name' );
                                        echo '<div class="member__company">' . esc_html( $company ) . '</div>';

                                    }
                                    ?>
                                </div>
                                <footer class="member__footer">
                                    <?php if ( is_user_logged_in() ) : ?>
                                        <a href="#" class="<?php echo esc_attr( $member_button_class ); ?>" data-user-a="<?php echo esc_attr( get_current_user_id() ); ?>" data-user-b="<?php echo esc_attr( $author_id ); ?>"><?php echo $member_button_label; ?></a>
                                    <?php endif; ?>
                                </footer>
                            </div>
                        </div>
                    <?php endwhile; wp_reset_postdata(); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>
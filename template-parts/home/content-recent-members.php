<section class="freshmes-home freshmes-home-recent-members">
    <div class="container">
        <nav class="freshmes-home-nav">
            <div class="columns">
                <div class="column">
                    <h3 class="freshmes-home-title"><?php esc_html_e( 'Recent Members', 'freshmes' ); ?></h3>
                </div>
                <div class="column has-text-right">
                    <a href="<?php echo esc_url( get_site_url() . '/members/' ); ?>" class="freshmes-home-view-all"><?php esc_html_e( 'View All', 'freshmes' ); ?></a>
                </div>
            </div>
        </nav>
        <?php
        $args = [
            'post_type'      => 'brand',
            'posts_per_page' => 3
        ];
        $query = new WP_Query( $args );
        ?>
        <?php if ( $query->have_posts() ) : ?>
            <div class="freshmes-home-content">
                <div class="columns">
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        <div class="column">
                            <?php
                            $post_id = get_the_ID();

                            $post_author_id   = get_post_field( 'post_author', $post_id );
                            $post_author_data = get_userdata( $post_author_id );

                            $post_author_image = get_field( 'brand_profile_image', 'user_' . $post_author_id );
                            $post_author_image = vlf_check_field( $post_author_image );

                            $locations = get_the_terms( $post_id, 'brand_location' );
                            ?>
                            <article id="post-<?php echo esc_attr( $post_id ); ?>" class="<?php echo esc_attr( join( ' ', get_post_class( 'freshmes-member', $post_id ) ) ); ?>">
                                <header class="freshmes-member__header">
                                    <?php
                                    if ( has_post_thumbnail( $post_id ) ) {

                                        echo '<div class="freshmes-member__header__image">';
                                        echo get_the_post_thumbnail( $post_id, 'single-image' );
                                        echo '</div>';

                                    } else {

                                        echo '<div class="freshmes-member__header__image freshmes-member__header__image--no-image"></div>';

                                    }
                                    ?>
                                    <a href="<?php echo esc_url( get_permalink( vlf_get_brand_id_from_author_id( $post_author_id ) ) ); ?>" class="freshmes-member__header__author">
                                        <div class="freshmes-member__header__author__image">
                                            <?php echo wp_get_attachment_image( $post_author_image, 'thumbnail' ); ?>
                                        </div>
                                        <div class="freshmes-member__header__author__by"><?php echo esc_html( $post_author_data->first_name . ' ' . $post_author_data->last_name ); ?></div>
                                    </a>
                                </header>
                                <div class="freshmes-member__content">
                                    <div class="columns">
                                        <div class="column is-8 has-text-left">
                                            <div class="freshmes-member__content__job-title"><?php the_field( 'brand_job_title', $post_id ); ?></div>
                                        </div>
                                        <div class="column is-4 has-text-right">
                                            <div class="freshmes-member__content__location">
                                                <?php if ( isset( $locations ) && ! empty( $locations ) ) : ?>
                                                    <span>
                                                        <?php
                                                        foreach( $locations as $location ) {

                                                            if ( $location === end( $locations ) ) {

                                                                echo $location->name;

                                                            } else {

                                                                echo $location->name . ', ';

                                                            }

                                                        }
                                                        ?>
                                                    </span>
                                                    <i class="fal fa-map-marker-alt"></i>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <h2 class="freshmes-member__content__title"><?php echo esc_html( get_the_title( $post_id ) ); ?></h2>
                                    <div class="freshmes-member__content__tagline"><?php the_field( 'brand_tagline' ); ?></div>
                                </div>
                                <footer class="freshmes-member__footer">
                                    <div class="columns is-v-centered">
                                        <div class="column">
                                            <a href="<?php echo esc_url( get_permalink( $post_id ) ); ?>" class="button freshmes-member__footer__button"><?php esc_html_e( 'Connect', 'vuelabs-framework' ); ?></a>
                                        </div>
                                        <div class="column has-text-right">
                                            <div class="freshmes-member__footer__meta">
                                                <a href="<?php echo esc_url( get_permalink( vlf_get_brand_id_from_author_id( $post_author_id ) ) ); ?>" class="freshmes-member__footer__meta__item freshmes-member__footer__meta__item--profile">
                                                    <i class="fal fa-user-circle"></i>
                                                </a>
                                                <a href="#" class="freshmes-member__footer__meta__item freshmes-member__footer__meta__item--share">
                                                    <i class="fal fa-share"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </footer>
                            </article>
                        </div>
                    <?php endwhile; wp_reset_postdata(); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>
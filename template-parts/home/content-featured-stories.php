<section class="freshmes-home freshmes-home-featured-stories">
    <div class="container">
        <nav class="freshmes-home-nav">
            <div class="columns">
                <div class="column">
                    <h3 class="freshmes-home-title"><?php esc_html_e( 'Featured Stories', 'freshmes' ); ?></h3>
                </div>
                <div class="column has-text-right">
                    <a href="<?php echo esc_url( get_site_url() . '/blog/' ); ?>" class="freshmes-home-view-all"><?php esc_html_e( 'View All', 'freshmes' ); ?></a>
                </div>
            </div>
        </nav>
        <?php
        $args = [
            'post_type'      => 'post',
            'posts_per_page' => 2
        ];
        $query = new WP_Query( $args );
        ?>
        <?php if ( $query->have_posts() ) : ?>
            <div class="freshmes-home-content">
                <div class="columns">
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        <div class="column">
                            <?php vlf_display_story( get_the_ID() ); ?>
                        </div>
                    <?php endwhile; wp_reset_postdata(); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>
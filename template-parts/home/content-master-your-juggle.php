<section class="freshmes-home freshmes-home-master-your-juggle">
    <div class="container-medium">
        <h2 class="freshmes-home-title has-text-centered"><?php esc_html_e( 'Master Your Juggle', 'freshmes' ); ?></h2>
        <div class="columns">
            <?php
            $items = get_field( 'fp_myj_items' );
            foreach ( $items as $item ) {

                $class = 'freshmes-card has-text-centered ' . $item['class'];
                $link  = $item['link'];

                if ( $item['class'] === 'freshmes-card--myj-brand' ) {

                    $link = get_permalink( vlf_get_brand_id_from_author_id( get_current_user_id() ) );

                } elseif ( $item['class'] === 'freshmes-card--myj-brand-shop' ) {

                    $tags = new WP_Term_Query( [
                        'taxonomy' => 'product_tag',
                        'meta_query' => [
                            [
                                'key'   => 'vlf_products_vendor',
                                'value' => get_current_user_id()
                            ]
                        ]
                    ] );
                    $tags = $tags->get_terms();
                    $organization_tag =  $tags[0]->slug;
                    $link = get_site_url() . '/product-tag/' . $organization_tag;

                } elseif ( $item['class'] === 'freshmes-card--myj-brand-blog' ) {

                    $link = get_permalink( vlf_get_brand_id_from_author_id( get_current_user_id() ) ) . 'blog/';

                }

                ?>

                <div class="column">
                    <a href="<?php echo esc_url( $link ); ?>" class="<?php echo esc_attr( $class ); ?>">
                        <span class="card-icon"><?php echo $item['icon']; ?></span>
                        <span class="card-title"><?php echo esc_html( $item['title'] ); ?></span>
                        <span class="card-subtitle"><?php echo esc_html( $item['subtitle'] ); ?></span>
                    </a>
                </div>

                <?php

            }
            ?>
        </div>
        <?php
        if ( is_user_logged_in() ) {

            ?>

            <div class="has-text-centered">
                <a href="<?php echo esc_url( get_the_permalink( vlf_get_brand_id_from_author_id( get_current_user_id() ) ) ); ?>" class="freshmes-home-dashboard"><?php esc_html_e( 'View Dashboard', 'freshmes' ); ?></a>
            </div>

            <?php

        }
        ?>
    </div>
</section>
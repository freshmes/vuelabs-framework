<?php

$l_exclusive   = get_field( 'fp_ads_left_exclusive' );
$l_image       = get_field( 'fp_ads_left_image' );
$l_title       = get_field( 'fp_ads_left_title' );
$l_description = get_field( 'fp_ads_left_description' );
$l_btn_label   = get_field( 'fp_ads_left_button_label' );
$l_btn_value   = get_field( 'fp_ads_left_button_value' );
$l_price       = get_field( 'fp_ads_left_price' );
$l_below_price = get_field( 'fp_ads_left_below_price' );

$r_icon        = get_field( 'fp_ads_right_icon' );
$r_title       = get_field( 'fp_ads_right_title' );
$r_list        = get_field( 'fp_ads_right_list' );
$r_btn_label   = get_field( 'fp_ads_right_button_label' );
$r_btn_value   = get_field( 'fp_ads_right_button_value' );

?>
<section class="freshmes-home freshmes-home-ads">
    <div class="container">
        <div class="columns">
            <div class="column is-9">
                <div class="freshmes-left-ad" style="background-image: url('<?php echo esc_url( $l_image ); ?>')">
                    <div class="ad-group">
                        <?php
                        if ( isset( $l_exclusive ) && ! empty( $l_exclusive ) && $l_exclusive === true ) {

                            echo '<div class="ad-exclusive">' . esc_html__( 'EXCLUSIVE OFFER', 'freshmes' ) . '</div>';

                        }
                        ?>
                        <div class="ad-title"><?php echo esc_html( $l_title ); ?></div>
                        <div class="ad-description"><?php echo wp_kses( $l_description, [ 'br' => [] ] ); ?></div>
                        <div class="ad-button">
                            <a href="<?php echo esc_url( $l_btn_value ); ?>" class="button"><?php echo esc_html( $l_btn_label ); ?></a>
                        </div>
                    </div>
                    <div class="ad-price">
                        <div class="price"><?php echo esc_html( $l_price ); ?></div>
                        <div class="below-price"><?php echo esc_html( $l_below_price ); ?></div>
                    </div>
                </div>
            </div>
            <div class="column is-3">
                <div class="freshmes-right-ad">
                    <div class="ad-group">
                        <div class="ad-icon has-text-centered">
                            <?php echo wp_kses( $r_icon, [ 'i' => [ 'class' => [] ] ] ); ?>
                        </div>
                        <div class="ad-title has-text-centered"><?php echo esc_html( $r_title ); ?></div>
                        <ul class="ad-list">
                            <?php foreach ( $r_list as $item ) : ?>
                                <li><?php echo esc_html( $item['item'] ); ?></li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="has-text-centered">
                            <a href="<?php echo esc_url( $r_btn_value ); ?>" class="ad-button has-text-centered"><?php echo esc_html( $r_btn_label ); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
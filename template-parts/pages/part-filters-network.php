<?php

$network_id = intval( $_GET['network'] );
$organizations = get_the_terms( $network_id, 'brand_organization' );

$organization_to_use = '';

if ( isset( $organizations ) && ! empty( $organizations ) ) {

    foreach ( $organizations as $organization ) {

        if ( $organization->parent === 0 ) {

            $organization_to_use = $organization->term_id;

        }

    }

}

?>
<div class="content-filters">
    <div class="container">
        <div class="content-filters__filter">
            <select name="content-filters-networks" id="content-filters-networks" class="content-filters__filter__select content-filters__filter__select--network is-select2" placeholder="<?php esc_attr_e( 'Networks', 'vuelabs-framework' ); ?>" data-type="network">
                <option></option>
                <?php
                    $authors = vlf_get_network_authors();
                    $posts_per_page = -1;
                    $args = [
                        'post_type'      => 'brand',
                        'posts_per_page' => $posts_per_page,
                        'post_status'    => 'publish',
                        'author__in'     => $authors
                    ];
                    $networks = new WP_Query( $args );
                    if ( $networks->have_posts() ) {

                        while ( $networks->have_posts() ) : $networks->the_post();

                            $selected = '';
                            if ( intval( $network_id ) === intval( get_the_ID() ) ) {

                                $selected = 'selected';

                            }

                            echo '<option value="' . esc_attr( get_the_ID() ) . '" ' . $selected . '>' . esc_html( get_the_title() ) . '</option>';

                        endwhile; wp_reset_postdata();

                    }
                ?>
            </select>
        </div>
        <div class="content-filters__filter">
            <select name="content-filters-chapters" id="content-filters-chapters" class="content-filters__filter__select content-filters__filter__select--chapter is-select2" placeholder="<?php esc_attr_e( 'Chapters', 'vuelabs-framework' ); ?>" data-type="chapter">
                <option></option>
                <?php
                if ( ! empty( $organization_to_use ) ) {

                    $members = get_term_children( $organization_to_use, 'brand_organization' );

                    foreach ( $members as $member ) {

                        $member = get_term( $member, 'brand_organization' );

                        $selected = '';
                        if ( isset( $_GET['chapter'] ) && ! empty( $_GET['chapter'] ) ) {

                            if ( $member->slug === $_GET['chapter'] ) {

                                $selected = 'selected';

                            }

                        }

                        echo '<option value="' . esc_attr( $member->slug ) . '" ' . esc_attr( $selected ) . '>' . esc_html( $member->name ) . '</option>';

                    }

                }
                ?>
            </select>
        </div>
    </div>
</div>
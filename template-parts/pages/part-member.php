<?php
/**
 * Filter Classes
 */
global $post;
$author_id = $post->post_author;
$author    = new WP_User( $author_id );

$classes = [ 'column', 'is-3' ];

$member_box_class = 'member__box';
$member_button_class = 'button member__connect';
$member_button_label = esc_html__( 'Connect', 'vuelabs-framework' );
if ( vlf_has_connection( get_current_user_id(), $author_id ) ) {

    $member_box_class .= ' is-hidden';
    $member_button_class .= ' member__connect--disconnect';
    $member_button_label = esc_html__( 'Disconnect', 'vuelabs-framework' );

}

?>
<div class="<?php echo esc_attr( implode( ' ', $classes ) ); ?>">
    <div class="member member--page has-text-centered" data-user="<?php echo esc_attr( $author_id ); ?>">
        <?php if ( is_user_logged_in() ) : ?>
            <div class="<?php echo esc_attr( $member_box_class ); ?>"></div>
        <?php endif; ?>
        <div class="member__content">
            <div class="member__image">
                <?php echo wp_get_attachment_image( get_field( 'brand_profile_image', get_the_ID() ), 'thumbnail' ); ?>
            </div>
            <div class="member__name"><?php echo esc_html( $author->first_name . ' ' . $author->last_name ); ?></div>
            <div class="member__subtitle">
                <a href="<?php echo esc_url( add_query_arg( [ 'position' => get_field( 'brand_job_title' ) ], get_site_url() . '/members/' ) ); ?>"><?php the_field( 'brand_job_title' ); ?></a>
            </div>
            <?php
            if ( in_array( 'freshmes_personal', $author->roles ) ) {

                $organization = get_the_terms( get_the_ID(), 'brand_organization' );
                if ( isset( $organization ) && ! empty( $organization ) ) {

                    $continue = true;
                    foreach ( $organization as $org ) {

                        if ( $continue === false ) {

                            continue;

                        }

                        if ( $org->parent === 0 ) {

                            $continue = false;
                            echo '<div class="member__company">' . esc_html( $org->name ) . '</div>';

                        }

                    }

                } else {

                    $company = get_field( 'brand_business_name' );
                    echo '<div class="member__company">' . esc_html( $company ) . '</div>';

                }

            } else {

                $company = get_field( 'brand_business_name' );
                echo '<div class="member__company">' . esc_html( $company ) . '</div>';

            }
            ?>
        </div>
    </div>
</div>
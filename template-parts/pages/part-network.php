<?php
/**
 * Filter Classes
 */
global $post;
$author_id = $post->post_author;

$categories = get_the_terms( $post->ID, 'brand_category' );
$tags       = get_the_terms( $post->ID, 'brand_tag' );
$locations  = get_the_terms( $post->ID, 'brand_location' );

$organizations      = get_the_terms( $post->ID, 'brand_organization' );
$organization_count = 0;
if ( isset( $organizations ) && ! empty( $organizations ) ) {

    $organization_count = $organizations[0]->count;

}

$classes = [ 'column', 'is-4' ];

// Categories Class
if ( isset( $categories ) && ! empty( $categories ) ) {

    foreach ( $categories as $category ) {

        array_push( $classes, $category->slug );

    }

}

// Tags Class
if ( isset( $tags ) && ! empty( $tags ) ) {

    foreach ( $tags as $tag ) {

        array_push( $classes, $tag->slug );

    }

}

// Locations Class
if ( isset( $locations ) && ! empty( $locations ) ) {

    foreach ( $locations as $location ) {

        array_push( $classes, $location->slug );

    }

}

// Logo Icon
$logo_icon = get_field( 'brand_logo_icon' );

// Organization Members
?>
<div class="<?php echo esc_attr( implode( ' ', $classes ) ); ?>">
    <div class="network network--page">
        <header class="network__header">
            <?php if ( has_post_thumbnail() ) : ?>
                <div class="network__header__image">
                    <?php the_post_thumbnail( 'single-image' ); ?>
                </div>
            <?php else : ?>
                <div class="network__header__image network__header__image--no-image"></div>
            <?php endif; ?>
            <h2 class="network__header__title"><?php echo esc_html( get_field( 'brand_business_name' ) ); ?></h2>
            <?php
            if ( isset( $categories ) && ! empty( $categories ) ) {

                echo '<a href="' . esc_url( get_site_url() . '/networks/?category=' . $categories[0]->term_id ) . '" class="network__header__category">' . esc_html( $categories[0]->name ) . '</a>';

            }
            if ( isset( $logo_icon ) && ! empty( $logo_icon ) ) {

                echo '<div class="network__header__logo">';
                echo wp_get_attachment_image( $logo_icon, 'thumbnail' );
                echo '</div>';

            } else {

                echo '<div class="network__header__logo network__header__logo--no-logo"></div>';

            }
            ?>
        </header>
        <div class="network__content">
            <div class="network__content__excerpt"><?php echo wp_kses( vlf_get_excerpt( get_field( 'brand_business_description' ) ), [ 'p' => [] ] ); ?></div>
        </div>
        <footer class="network__footer has-text-centered">
            <a href="<?php echo esc_url( get_site_url() . '/networks/?preview=true&network=' . vlf_get_brand_id_from_author_id( $author_id ) ); ?>" class="button network__footer__button"><?php esc_html_e( 'View Network', 'vuelabs-framework' ); ?></a>
            <div class="has-text-centered">
                <div class="network__footer__members"><?php printf( _n( '%s Member', '%s Members', intval( $organization_count ), 'vuelabs-framework' ), intval( $organization_count ) ); ?></div>
            </div>
        </footer>
    </div>
</div>
<div class="content-filters">
    <div class="container">
        <?php if ( ! is_page_template( 'templates/template-networks.php' ) ) : ?>
            <div class="content-filters__filter">
                <select name="content-filters-membership" id="content-filters-membership" class="content-filters__filter__select content-filters__filter__select--membership is-select2" placeholder="<?php esc_attr_e( 'Membership', 'vuelabs-framework' ); ?>" data-type="membership">
                    <option></option>
                    <?php
                    $memberships = get_field( 'memberships', 'option' );
                    $memberships = vlf_check_field( $memberships );
                    if ( $memberships ) {

                        foreach ( $memberships as $membership ) {

                            $selected = '';

                            if ( isset( $_GET['membership'] ) && ! empty( $_GET['membership'] ) ) {

                                if ( $_GET['membership'] === $membership->post_name ) {

                                    $selected = 'selected="selected"';

                                }

                            }

                            echo '<option value="' . esc_attr( $membership->post_name ) . '" ' . esc_attr( $selected ) . '>' . esc_html( $membership->post_title ) . '</option>';

                        }

                    }
                    ?>
                </select>
            </div>
        <?php endif; ?>
        <div class="content-filters__filter">
            <select name="content-filters-category" id="content-filters-category" class="content-filters__filter__select content-filters__filter__select--category is-select2" placeholder="<?php esc_attr_e( 'Category', 'vuelabs-framework' ); ?>" data-type="category">
                <option></option>
                <?php
                $categories = get_terms( [
                    'hide_empty' => false,
                    'taxonomy'   => 'brand_category'
                ] );
                if ( ! is_wp_error( $categories ) ) {

                    foreach ( $categories as $category ) {

                        $selected = '';

                        if ( isset( $_GET['category'] ) && ! empty( $_GET['category'] ) ) {

                            if ( $_GET['category'] === $category->slug ) {

                                $selected = 'selected="selected"';

                            }

                        }

                        echo '<option value="' . esc_attr( $category->slug ) . '" ' . esc_attr( $selected ) . '>' . esc_html( $category->name ) . '</option>';

                    }

                }
                ?>
            </select>
        </div>
        <div class="content-filters__filter">
            <select name="content-filters-location" id="content-filters-location" class="content-filters__filter__select content-filters__filter__select--location is-select2" placeholder="<?php esc_attr_e( 'Location', 'vuelabs-framework' ); ?>" data-type="location">
                <option></option>
                <?php
                $locations = get_terms( [
                    'hide_empty' => false,
                    'taxonomy'   => 'brand_location'
                ] );
                if ( ! is_wp_error( $locations ) ) {

                    foreach ( $locations as $location ) {

                        $selected = '';

                        if ( isset( $_GET['location'] ) && ! empty( $_GET['location'] ) ) {

                            if ( $_GET['location'] === $location->slug ) {

                                $selected = 'selected="selected"';

                            }

                        }

                        echo '<option value="' . esc_attr( $location->slug ) . '" ' . esc_attr( $selected ) . '>' . esc_html( $location->name ) . '</option>';

                    }

                }
                ?>
            </select>
        </div>
        <div class="content-filters__filter">
            <select name="content-filters-tags" id="content-filters-tags" class="content-filters__filter__select content-filters__filter__select--tags is-select2" placeholder="<?php esc_attr_e( 'Filter by tags', 'vuelabs-framework' ); ?>" data-type="tag">
                <option></option>
                <?php
                $tags = get_terms( [
                    'hide_empty' => false,
                    'taxonomy'   => 'brand_tag'
                ] );
                if ( ! is_wp_error( $tags ) ) {

                    foreach ( $tags as $tag ) {

                        $selected = '';

                        if ( isset( $_GET['tag'] ) && ! empty( $_GET['tag'] ) ) {

                            if ( $_GET['tag'] === $tag->slug ) {

                                $selected = 'selected="selected"';

                            }

                        }

                        echo '<option value="' . esc_attr( $tag->slug ) . '" ' . esc_attr( $selected ) . '>' . esc_html( $tag->name ) . '</option>';

                    }

                }
                ?>
            </select>
        </div>
    </div>
</div>
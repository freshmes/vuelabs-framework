<section class="section section__none">
    <div class="container has-text-centered">
        <h1 class="title section__none__title"><?php esc_html_e( 'Networks', 'vuelabs-framework' ); ?></h1>
        <h2 class="subtitle section__none__subtitle"><?php esc_html_e( 'Unfortunately, there are no Networks to show.', 'vuelabs-framework' ); ?></h2>
    </div>
</section>
<?php
    $diane_id = get_post_field( 'post_author', 406 );


    $args = [
        'post_type'      => 'brand',
        'posts_per_page' => -1,
        'post__not_in'   => [ 406 ],
        'post_status'    => 'any'
    ];

    $query = new WP_Query( $args );
    if ( $query->have_posts() ) {

        while ( $query->have_posts() ) : $query->the_post();

            $brand_id = get_the_ID();
            $brand_author_id = get_post_field( 'post_author', $brand_id );

            vlf_create_connection( $brand_author_id, $diane_id, 'all' );
            vlf_create_connection( $diane_id, $brand_author_id, 'all' );

        endwhile; wp_reset_postdata();

    }
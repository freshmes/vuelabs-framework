<?php

$membership_personal_id     = 85;
$membership_personal        = wc_get_product( $membership_personal_id );

$membership_professional_id = 86;
$membership_professional    = wc_get_product( $membership_professional_id );

$membership_organization_id = 87;
$membership_organization    = wc_get_product( $membership_organization_id );

$import_file = fopen( get_theme_file_path( 'typeform-users.csv' ), 'r' );
$row = 1;
while ( ( $data = fgetcsv( $import_file, 10000, ',' ) ) !== FALSE ) {

    global $wpdb;

    // Create user.
    $user_id = wc_create_new_customer( $data[16], '', '' );

    // If User is Created
    if ( ! is_wp_error( $user_id ) ) {

        // Make sure user id is int.
        $user_id = intval( $user_id );

        // User Data and Class
        $user      = new WP_User( $user_id );
        $user_data = get_userdata( $user_id );

        $product   = $membership_personal;

        $brand_title = $data[2];

        // Set User Role based on Membership
        switch ( $data[5] ) {

            case 'Personal':
                $user->set_role( 'freshmes_personal' );
                break;
            case 'Business':
                $user->set_role( 'freshmes_business' );
                $product = $membership_professional;
                $brand_title = $data[9];
                update_user_meta( $user_id, 'shipping_company', $data[9] );
                update_user_meta( $user_id, 'billing_company', $data[9] );
                break;
            case 'Organization':
                $user->set_role( 'freshmes_organization' );
                $product = $membership_organization;
                $brand_title = $data[9];
                update_user_meta( $user_id, 'shipping_company', $data[9] );
                update_user_meta( $user_id, 'billing_company', $data[9] );
                break;

        }

        // Set Cart for the User
        WC()->cart->add_to_cart( $product->get_id() );

        // Create brand page for the user and cache it.
        $brand_id = wp_insert_post( [
            'post_author' => $user_id,
            'post_title'  => $brand_title,
            'post_status' => 'draft',
            'post_type'   => 'brand',
            'ping_status' => 'closed'
        ] );
        update_option( 'brand_page_id_for_' . $user_id, $brand_id );

        // Update user selected membership on brand page.
        update_field( 'brand_membership', $product->get_id(), $brand_id );

        // Update location everywhere.
        $address  = $data[19] . ', ' . $data[20] . ', ' . $data[18];
        $response = wp_remote_get( 'https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode( $address ) . '&key=AIzaSyCoNXog3BAiBLPNFRAWfIHhzrYF0o57kaY' );
        if ( ! is_wp_error( $response ) ) {

            // Get Data
            $response_data = wp_remote_retrieve_body( $response );
            if ( ! is_wp_error( $response_data ) ) {

                $address_real  = '';
                $city          = '';
                $state         = '';
                $state_short   = '';
                $country       = '';
                $country_short = '';
                $zip_code      = '';

                $response_data = json_decode( $response_data );

                foreach ( $response_data->results[0]->address_components as $component ) {

                    // Address Number
                    if ( in_array( 'street_number', $component->types ) ) {

                        $address_real = $component->long_name;

                    }

                    if ( in_array( 'route', $component->types ) ) {

                        $address_real .= ' ' . $component->long_name;

                    }

                    if ( in_array( 'postal_code' , $component->types ) ) {

                        $zip_code = $component->long_name;

                    }

                    // City
                    if ( in_array( 'locality', $component->types ) && in_array( 'political', $component->types ) ) {

                        $city = $component->long_name;

                    }

                    // State
                    if ( in_array( 'administrative_area_level_1', $component->types ) && in_array( 'political', $component->types ) ) {

                        $state = $component->long_name;
                        $state_short = $component->short_name;

                    }

                    // Country
                    if ( in_array( 'country', $component->types ) && in_array( 'political', $component->types ) ) {

                        $country = $component->long_name;
                        $country_short = $component->short_name;

                    }

                }

                // Update Address
                if ( isset( $address_real ) && ! empty( $address_real ) ) {

                    update_field( 'brand_address', $address_real, $brand_id );
                    update_user_meta( $user_id, 'shipping_address_1', $address_real );
                    update_user_meta( $user_id, 'billing_address_1', $address_real );

                }

                if ( isset( $zip_code ) && ! empty( $zip_code ) ) {

                    update_user_meta( $user_id, 'shipping_postcode', $zip_code );
                    update_user_meta( $user_id, 'billing_postcode', $zip_code );

                }

                // Update Country
                $country_id = term_exists( sanitize_title( $country ), 'brand_location' );
                if ( isset( $country_id ) && ! empty( $country_id ) ) {

                    $country_id = $country_id['term_id'];

                    update_user_meta( $user_id, 'shipping_country', $country_short );
                    update_user_meta( $user_id, 'billing_country', $country_short );

                    wp_set_object_terms( $brand_id, intval( $country_id ), 'brand_location', true );


                } else {

                    $country_id = wp_insert_term( $country, 'brand_location', [
                        'slug' => sanitize_title( $country )
                    ] );
                    $country_id = $country_id['term_id'];
                    update_field( 'location_abbreviated', $country_short, 'brand_location_' . $country_id );
                    update_user_meta( $user_id, 'shipping_country', $country_short );
                    update_user_meta( $user_id, 'billing_country', $country_short );
                    wp_set_object_terms( $brand_id, intval( $country_id ), 'brand_location', true );

                }

                // Update State
                if ( isset( $state ) && ! empty( $state ) ) {

                    $state_id = term_exists( sanitize_title( $state ), 'brand_location', intval( $country_id ) );
                    if ( isset( $state_id ) && ! empty( $state_id ) ) {

                        $state_id = $state_id['term_id'];

                        update_user_meta( $user_id, 'billing_state', $state_short );

                        wp_set_object_terms( $brand_id, intval( $state_id ), 'brand_location', true );

                    } else {

                        $state_id = wp_insert_term( $state, 'brand_location', [
                            'parent' => $country_id,
                            'slug'   => sanitize_title( $state )
                        ] );
                        $state_id = $state_id['term_id'];
                        update_field( 'location_abbreviated', $state_short, 'brand_location_' . $state_id );
                        update_user_meta( $user_id, 'shipping_state', $state_short );
                        update_user_meta( $user_id, 'billing_state', $state_short );
                        wp_set_object_terms( $brand_id, intval( $state_id ), 'brand_location', true );

                    }

                }


                // Update City
                if ( isset( $city ) && ! empty( $city ) ) {

                    if ( isset( $state_id ) && ! empty( $state_id ) ) {

                        $city_id = term_exists( sanitize_title( $city . '-city' ), 'brand_location', intval( $state_id ) );

                    } else {

                        $city_id = term_exists( sanitize_title( $city . '-city' ), 'brand_location', intval( $country_id ) );

                    }
                    if ( isset( $city_id ) && ! empty( $city_id ) ) {

                        $city_id = $city_id['term_id'];

                        update_user_meta( $user_id, 'shipping_city', $city );
                        update_user_meta( $user_id, 'billing_city', $city );

                        wp_set_object_terms( $brand_id, intval( $city_id ), 'brand_location', true );

                    } else {

                        $city_args = [
                            'slug' => sanitize_title( $city . '-city' )
                        ];
                        if ( isset( $state_id ) && ! empty( $state_id ) ) {

                            $city_args['parent'] = $state_id;

                        } else {

                            $city_args['parent'] = $country_id;

                        }

                        update_user_meta( $user_id, 'shipping_city', $city );
                        update_user_meta( $user_id, 'billing_city', $city );

                        $city_id = wp_insert_term( $city, 'brand_location', $city_args );
                        $city_id = $city_id['term_id'];
                        wp_set_object_terms( $brand_id, intval( $city_id ), 'brand_location', true );

                    }

                }

            }

        }

        // Update User Meta
        update_user_meta( $user_id, 'first_name', $data[3] );
        update_user_meta( $user_id, 'last_name', $data[4] );

        // Update Brand Personal Info
        update_field( 'brand_job_title', $data[13], $brand_id );
        update_field( 'brand_phone', $data[14], $brand_id );

        // Update Brand Business Info
        if ( $data[5] === 'Business' || $data[5] === 'Organization' ) {

            update_field( 'brand_business_name', $data[9], $brand_id );
            update_field( 'brand_tagline', $data[12], $brand_id );
            update_field( 'brand_business_phone', $data[14], $brand_id );

        }

        // Update Organization
        if ( $data[5] === 'Organization' ) {

            update_field( 'brand_is_organization', true, $brand_id );

            $organization_id = term_exists( sanitize_title( $data[9] ), 'brand_organization' );
            if ( isset( $organization_id ) && ! empty( $organization_id ) ) {

                wp_set_object_terms( $brand_id, intval( $organization_id['term_id'] ), 'brand_organization' );

            } else {

                $organization = wp_insert_term( $data[9], 'brand_organization', [ 'slug' => sanitize_title( $data[9] ) ] );
                wp_set_object_terms( $brand_id, intval( $organization['term_id'] ), 'brand_organization' );

            }

        }

        // Update Brand Other Info
        update_field( 'brand_membership', $product->get_id(), $brand_id );

        // Get Tags
        $brand_tags = get_terms( [
            'taxonomy'   => 'brand_tag',
            'hide_empty' => false
        ] );

        // Set Tags
        foreach ( $brand_tags as $brand_tag ) {

            wp_set_object_terms( intval( $brand_id ), intval( $brand_tag->term_id ), 'brand_tag', true );

        }

        // Set Category
        if ( isset( $data[8] ) && ! empty( $data[8] ) ) {

            // Check if term exists
            $category_id = term_exists( sanitize_title( $data[8] ), 'brand_category' );
            if ( isset( $category_id ) && ! empty( $category_id ) ) {

                wp_set_object_terms( $brand_id, intval( $category_id ), 'brand_category', true );

            } else {

                $category_id = wp_insert_term( $data[8], 'brand_category', [ 'slug' => sanitize_title( $data[8] ) ] );
                $category_id = $category_id['term_id'];
                wp_set_object_terms( $brand_id, intval( $category_id ), 'brand_category', true );

            }

        }

        // Set Status
        if ( isset( $data[7] ) && ! empty( $data[7] ) ) {

            $status = 'self-employed';
            if ( $data[7] === 'Employee' ) {

                $status = 'employee';

            } elseif ( $data[7] === 'Between jobs' ) {

                $status = 'between-jobs';

            } elseif ( $data[7] === 'I don\'t work' ) {

                $status = 'i-dont-work';

            }

            update_field( 'brand_status', $status, $brand_id );

        }

        $description = [];
        if ( isset( $data[31] ) && ! empty( $data[31] ) ) {

            $goal = [
                'title' => 'Platform Goal',
                'content' => esc_html( $data[31] )
            ];

            array_push( $description, $goal );

        }
        if ( isset( $data[32] ) && ! empty( $data[32] ) ) {

            $story = [
                'title' => 'Personal Story',
                'content' => esc_html( $data[32] )
            ];

            array_push( $description, $story );

        }
        if ( isset( $data[33] ) && ! empty( $data[33] ) ) {

            $challenge = [
                'title' => 'Personal Challenge',
                'content' => esc_html( $data[33] )
            ];

            array_push( $description, $challenge );

        }
        if ( isset( $description ) && ! empty( $description ) ) {

            update_field( 'brand_description', $description, $brand_id );

        }

        $business_description = [];
        if ( isset( $data[34] ) && ! empty( $data[34] ) ) {

            $challenge_pro = [
                'title' => 'Professional Challenge',
                'content' => esc_html( $data[34] )
            ];

            array_push( $business_description, $challenge_pro );

        }
        $advice = [];
        if ( isset( $data[35] ) && ! empty( $data[35] ) ) {

            array_push( $advice, $data[35] );

        }
        if ( isset( $data[36] ) && ! empty( $data[36] ) ) {

            array_push( $advice, $data[36] );

        }
        if ( isset( $data[37] ) && ! empty( $data[37] ) ) {

            array_push( $advice, $data[37] );

        }
        if ( isset( $advice ) && ! empty( $advice ) ) {

            $advice = implode( "\n", $advice );
            array_push( $business_description, [
                'title'   => 'Advices',
                'content' => $advice
            ] );

        }
        if ( isset( $business_description ) && ! empty( $business_description ) ) {

            update_field( 'brand_business_description', $business_description, $brand_id );

        }

        // Upload Images
        if ( isset( $data[25] ) && ! empty( $data[25] ) ) {

            //$logo = media_sideload_image( $data[25], $brand_id, '', 'id' );
            $logo = explode( '/', $data[25] );
            $logo = $logo[6];
            $logo = explode( '.', $logo );
            $logo = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_title='%s';", $logo ));
            $logo = $logo[0];
            update_field( 'brand_logo', intval( $logo ), $brand_id );
            update_field( 'brand_logo_icon', intval( $logo ), $brand_id );

            echo 'Brand Logo Added' . PHP_EOL;

        }
        if ( isset( $data[26] ) && ! empty( $data[26] ) ) {

            //$profile_image = media_sideload_image( $data[26], $brand_id, '', 'id' );
            $profile_image = explode( '/', $data[26] );
            $profile_image = $profile_image[6];
            $profile_image = explode( '.', $profile_image );
            $profile_image = $profile_image[0];
            $profile_image = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_title='%s';", $profile_image ));
            $profile_image = $profile_image[0];
            update_field( 'brand_profile_image', intval( $profile_image ), $brand_id );
            update_field( 'brand_profile_image', intval( $profile_image ), 'user_' . $user_id );

            echo 'Brand Profile Image Added' . PHP_EOL;

        }
        if ( isset( $data[27] ) && ! empty( $data[27] ) ) {

            //$cover_image = media_sideload_image( $data[27], $brand_id, '', 'id' );
            $cover_image = explode( '/', $data[27] );
            $cover_image = $cover_image[6];
            $cover_image = explode( '.', $cover_image );
            $cover_image = $cover_image[0];
            $cover_image = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_title='%s';", $cover_image ));
            $cover_image = $cover_image[0];
            update_field( 'brand_gallery', [ intval( $cover_image ) ], $brand_id );

            echo 'Brand Gallery Added' . PHP_EOL;

        }

        echo 'Brand #' . $row . ' Added!' . PHP_EOL;

    }

    $row++;

}
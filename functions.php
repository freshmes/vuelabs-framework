<?php
/**
 * VueLabs Framework Engine Room
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework
 * @since 1.0.0
 */

/**
 * Define theme version.
 */
$theme = wp_get_theme( 'vuelabs-framework' );
$theme_version = $theme['Version'];
define( 'VUELABS_FRAMEWORK', $theme_version );

/**
 * Composer Autoloader
 */
if ( file_exists( get_theme_file_path( 'vendor/autoload.php' ) ) ) {

    require_once get_theme_file_path( 'vendor/autoload.php' );

}

/**
 * Load our helper classes, acf.
 */
require 'inc/vuelabs-framework-functions.php';
require 'inc/vuelabs-framework-acf.php';
require 'inc/vuelabs-framework-additional.php';
require 'inc/vuelabs-framework-woocommerce.php';
require 'inc/vuelabs-framework-connections.php';
require 'inc/vuelabs-framework-join-now.php';
require 'inc/vuelabs-framework-products.php';
require 'inc/vuelabs-framework-posts.php';

$vlf = (object) [
    'main' => require 'inc/class-vuelabs-framework.php',
    'ajax' => require 'inc/class-vuelabs-framework-ajax.php',
    'rest' => require 'inc/class-vuelabs-framework-rest.php'
];

/**
 * Load our Widgets
 */
$widgets = scandir( get_template_directory() . '/inc/widgets/' );
$widgets = array_diff( $widgets, [ '.', '..', 'wc' ] );
foreach ( $widgets as $widget ) {

    require 'inc/widgets/' . $widget;

}
$wc_widgets = scandir( get_template_directory() . '/inc/widgets/wc/' );
$wc_widgets = array_diff( $wc_widgets, [ '.', '..' ] );
foreach ( $wc_widgets as $wc_widget ) {

    require 'inc/widgets/wc/' . $wc_widget;

}

add_filter( 'show_admin_bar', '__return_false' );
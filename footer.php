<?php
/**
 * Footer for the theme.
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework
 * @since 1.0.0
 */

$copyright = get_field( 'footer_copyright', 'option' );
$copyright = vlf_check_field( $copyright );
?>

<footer class="site-footer has-text-centered" id="site-footer">
    <div class="container">
        <?php if ( $copyright ) : ?>
            <span class="site-footer__copyright"><?php echo esc_html( vlf_replace_variable_curly( $copyright, 'year', date( 'Y' ) ) ); ?></span>
        <?php endif; ?>
        <?php
        wp_nav_menu( [
            'theme_location'  => 'secondary',
            'menu_class'      => '',
            'menu_id'         => '',
            'container'       => '',
            'container_class' => '',
            'container_id'    => '',
        ] );
        ?>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
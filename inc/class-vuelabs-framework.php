<?php
/**
 * VueLabs Framework Class
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {

    exit;

}

if ( ! class_exists( 'VueLabs_Framework' ) ) :

    class VueLabs_Framework
    {

        /**
         * Setup class.
         *
         * @since 1.0.0
         */
        public function __construct()
        {

            add_action( 'after_setup_theme', [ $this, 'vlf_setup' ], 0 );
            add_action( 'wp_enqueue_scripts', [ $this, 'vlf_scripts' ], 10 );
            add_action( 'wp_enqueue_scripts', [ $this, 'vlf_child_scripts' ], 30 );
            add_action( 'widgets_init', [ $this, 'vlf_widgets_init' ] );

            // Change WP Core Things
            add_filter( 'wp_nav_menu_objects', [ $this, 'vlf_wp_nav_menu_objects' ], 10, 2 );
            add_filter( 'get_search_form', [ $this, 'vlf_get_search_form' ], 10, 1 );
            add_action( 'login_redirect', [ $this, 'vlf_login_redirect' ], 10, 3 );
            add_filter( 'rewrite_rules_array', [ $this, 'vlf_rewrite_rules_array' ] );
            add_filter( 'query_vars', [ $this, 'vlf_query_vars' ] );
            add_filter( 'body_class', [ $this, 'vlf_body_class' ] );

            // Change WC Core Things
            add_action( 'init', [ $this, 'vlf_enable_author_for_product' ] );
            add_filter( 'loop_shop_per_page', [ $this, 'vlf_loop_shop_per_page' ], 20 );
            add_filter( 'woocommerce_product_reviews_heading', '__return_empty_string' );
            add_filter( 'woocommerce_product_description_heading', '__return_empty_string' );
            add_filter( 'woocommerce_output_related_products_args', [ $this, 'woocommerce_output_related_products_args' ] );
            add_filter( 'woocommerce_checkout_fields', [ $this, 'woocommerce_checkout_fields' ] );

        }

        /**
         * Sets up theme defaults and registers support for various WordPress features.
         *
         * @since 1.0.0
         */
        public function vlf_setup()
        {

            /**
             * Make theme available for translation.
             */
            // Loads wp-content/languages/themes/vuelabs-framework-it_IT.mo.
            load_theme_textdomain( 'vuelabs-framework', trailingslashit( WP_LANG_DIR ) . 'themes/' );
            // Loads wp-content/themes/child-theme-name/languages/it_IT.mo.
            load_theme_textdomain( 'vuelabs-framework', get_stylesheet_directory() . '/languages' );
            // Loads wp-content/themes/vuelabs-framework/languages/it_IT.mo.
            load_theme_textdomain( 'vuelabs-framework', get_template_directory() . '/languages' );

            /**
             * Set the content width in pixels, based on the theme's design and stylesheet.
             */
            $GLOBALS['content_width'] = apply_filters( 'content_width', 700 );

            /**
             * Add default posts and comments RSS feed links to head.
             */
            add_theme_support( 'automatic-feed-links' );

            /**
             * Let WordPress manage the document title.
             */
            add_theme_support( 'title-tag' );

            /**
             * Enable support for Post Thumbnails.
             */
            add_theme_support( 'post-thumbnails' );

            /**
             * Add image sizes.
             */
            add_image_size( 'brand-logo', 170, 'auto', true );
            add_image_size( 'product-image', 282, 170, true );
            add_image_size( 'single-image', 384, 230, true );
            add_image_size( 'medium-single-image', 760, 410, true );

            /**
             * Register nav menus.
             */
            register_nav_menus(
                apply_filters( 'vlf_register_nav_menus', [
                    'primary'   => esc_html__( 'Primary Menu', 'vuelabs-framework' ),
                    'secondary' => esc_html__( 'Secondary Menu', 'vuelabs-framework' ),
                ] )
            );

            /*
			 * Switch default core markup for search form, comment form, comments, galleries, captions and widgets
			 * to output valid HTML5.
			 */
            add_theme_support( 'html5', apply_filters( 'vlf_html5_args', [
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
                'widgets'
            ] ) );

            /**
             * Setup the WordPress core custom background feature.
             */
            add_theme_support( 'custom-background', apply_filters( 'vlf_custom_background_args', [
                'default-color' => apply_filters( 'vlf_default_background_color', 'FFFFFF' ),
                'default-image' => '',
            ] ) );

            /**
             * Add support for core custom logo.
             *
             * @link https://codex.wordpress.org/Theme_Logo
             */
            add_theme_support('custom-logo', apply_filters( 'vlf_custom_logo_args', [
                'size' => 'full'
            ] ) );

            /**
             * Declare support for selective refreshing of widgets.
             */
            add_theme_support( 'customize-selective-refresh-widgets' );

            /**
             * Add support for Block Styles.
             */
            add_theme_support( 'wp-block-styles' );

            /**
             * Add support for full and wide align images.
             */
            add_theme_support( 'align-wide' );

            /**
             * Add support for editor styles.
             */
            add_theme_support( 'editor-styles' );

            /**
             * Add support for responsive embedded content.
             */
            add_theme_support( 'responsive-embeds' );

            /**
             * Add support for WooCommerce
             */
            add_theme_support( 'woocommerce' );
            add_theme_support( 'wc-product-gallery-zoom' );
            add_theme_support( 'wc-product-gallery-lightbox' );
            add_theme_support( 'wc-product-gallery-slider' );

        }

        /**
         * Enqueue scripts and styles.
         *
         * @since 1.0.0
         */
        public function vlf_scripts()
        {

            if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
                wp_enqueue_script( 'comment-reply' );
            }

            // Global CSS
            wp_enqueue_style( 'vlf-google-fonts', '//fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext' );
            wp_enqueue_style( 'vlf-adobe-fonts', '//use.typekit.net/dha3xgg.css' );
            wp_enqueue_style( 'vlf-fontawesome-pro', get_theme_file_uri( 'assets/css/fontawesome-pro.min.css' ), '', '5.7.2' );
            wp_enqueue_style( 'vlf-quill', get_theme_file_uri( 'assets/css/quill.core.css' ), '', '1.3.6' );
            wp_enqueue_style( 'vlf-quill-snow', get_theme_file_uri( 'assets/css/quill.snow.css' ), '', '1.3.6' );
            wp_enqueue_style( 'vlf-nouislider', get_theme_file_uri( 'assets/css/nouislider.min.css' ), '', '12.1.0' );
            wp_enqueue_style( 'vlf-style', get_theme_file_uri( 'style.css' ), '', '1.0.0' );

            // Global JS
            wp_enqueue_script( 'vlf-select2', get_theme_file_uri( 'assets/js/select2.full.min.js' ), [ 'jquery' ], '4.0.6-rc.1', true );
            wp_enqueue_script( 'vlf-slick-carousel', get_theme_file_uri( 'assets/js/slick.min.js' ), [ 'jquery' ], '1.8.1', true );
            wp_enqueue_script( 'vlf-quill', get_theme_file_uri( 'assets/js/quill.min.js' ), [ 'jquery' ], '1.3.6', true );
            wp_enqueue_script( 'vlf-nouislider', get_theme_file_uri( 'assets/js/nouislider.min.js' ), [ 'jquery' ], '12.1.0', true );

            // Brand JS
            if ( is_singular( 'brand' ) ) {

                wp_enqueue_style( 'vlf-raty', get_theme_file_uri( 'assets/css/jquery.raty.css' ), '', '2.9.0' );
                wp_enqueue_script( 'vlf-raty', get_theme_file_uri( 'assets/js/jquery.raty.js' ), [ 'jquery' ], '2.9.0', true );

            }

            $google_key = get_field( 'google_api_key', 'option' );
            $google_key = vlf_check_field( $google_key );
            if ( $google_key ) {

                wp_enqueue_script( 'vlf-google', '//maps.googleapis.com/maps/api/js?key=' . esc_attr( $google_key ) . '&libraries=places', [ 'jquery' ], '', true );

            }
            wp_enqueue_script( 'vlf-js', get_theme_file_uri( 'assets/js/script.js' ), [ 'jquery' ], '1.0.0', true );

            // Localized JS
            wp_localize_script( 'vlf-js', 'vlfApp', [
                'ajaxURL'     => admin_url( 'admin-ajax.php' ),
                'placeholder' => get_theme_file_uri( 'assets/images/freshmes-symbol.png' ),
                'l10n'        => [
                    'members' => [
                        'connect'    => esc_html__( 'Connect', 'vuelabs-framework' ),
                        'disconnect' => esc_html__( 'Disconnect', 'vuelabs-framework' )
                    ]
                ]
            ] );

        }

        /**
         * Enqueue child theme stylesheet.
         *
         * A separate function is required as the child theme css needs to be enqueued _after_ the parent theme
         * primary css.
         *
         * @since 1.0.0
         */
        public function vlf_child_scripts()
        {

            if ( is_child_theme() ) {

                $child_theme = wp_get_theme( get_stylesheet() );
                wp_enqueue_style( 'vlf-child-style', get_stylesheet_uri(), [], $child_theme->get( 'Version' ) );

            }

        }

        /**
         * Register widget area.
         *
         * @since 1.0.0
         */
        public function vlf_widgets_init()
        {

            $sidebar_args['brand_sidebar'] = [
                'name' => esc_html__( 'Brand Sidebar', 'vuelabs-framework' ),
                'id'   => 'brand-sidebar'
            ];
            $sidebar_args['brand_main'] = [
                'name' => esc_html__( 'Brand Main', 'vuelabs-framework' ),
                'id'   => 'brand-main'
            ];
            $sidebar_args['brand_footer'] = [
                'name' => esc_html__( 'Brand Footer', 'vuelabs-framework' ),
                'id'   => 'brand-footer'
            ];
            $sidebar_args['brand_blog_sidebar'] = [
                'name' => esc_html__( 'Brand Blog Sidebar', 'vuelabs-framework' ),
                'id'   => 'brand-blog-sidebar'
            ];
            $sidebar_args['brand_shop_sidebar'] = [
                'name' => esc_html__( 'Brand Shop Sidebar', 'vuelabs-framework' ),
                'id'   => 'brand-shop-sidebar'
            ];

            $sidebar_args = apply_filters( 'vlf_sidebar_args', $sidebar_args );

            foreach ( $sidebar_args as $sidebar => $args ) {

                $widget_tags = array(
                    'before_widget' => '<div id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</div>',
                    'before_title'  => '<div class="gamma widget-title">',
                    'after_title'   => '</div>',
                );

                $filter_hook = sprintf( 'vlf_%s_widget_tags', $sidebar );
                $widget_tags = apply_filters( $filter_hook, $widget_tags );
                if ( is_array( $widget_tags ) ) {

                    register_sidebar( $args + $widget_tags );

                }

            }

            // Register Widgets
            $widgets = scandir( get_template_directory() . '/inc/widgets/' );
            $widgets = array_diff( $widgets, [ '.', '..', 'wc' ] );
            foreach ( $widgets as $widget ) {

                $widget = str_replace( 'widget-vuelabs-framework-', '', $widget );
                $widget = str_replace( '.php', '', $widget );
                $widget = ucwords( $widget );
                $widget = 'Freshmes_Brand_' . $widget;

                register_widget( $widget );

            }

            $wc_widgets = scandir( get_template_directory() . '/inc/widgets/wc/' );
            $wc_widgets = array_diff( $wc_widgets, [ '.', '..', 'wc' ] );
            foreach ( $wc_widgets as $wc_widget ) {

                $wc_widget = str_replace( 'widget-vuelabs-framework-wc-', '', $wc_widget );
                $wc_widget = str_replace( '.php', '', $wc_widget );
                $wc_widget = ucwords( $wc_widget );
                $wc_widget = 'Freshmes_WC_' . $wc_widget;

                register_widget( $wc_widget );

            }

        }

        /**
         * Change our WP Nav Menu Items to display our FontAwesome icon.
         *
         * @param $items
         * @param $args
         *
         * @return mixed
         */
        public function vlf_wp_nav_menu_objects( $items, $args )
        {

            // Loop trough the items.
            foreach ( $items as $item ) {

                // Get Icon
                $icon = get_field( 'menu_item_icon', $item );
                $icon = vlf_check_field( $icon );

                $title = get_field( 'menu_item_title', $item );
                $title = vlf_check_field( $title );

                $brand = get_field( 'menu_item_brand', $item );
                $brand = vlf_check_field( $brand );

                if ( $icon ) {

                    $item->title = wp_kses( $icon, [ 'i' => [ 'class' => [] ] ] ) . $item->title;

                }

                if ( $title ) {

                    $item->classes[] = 'is-item-title';

                }

                if ( $brand ) {

                    $user_id = get_current_user_id();
                    $brand_transient_name = 'vlf_brand_page_url_' . esc_attr( $user_id );
                    $brand_transient_value = get_transient( $brand_transient_name );
                    if ( isset( $brand_transient_value ) && ! empty( $brand_transient_value ) ) {

                        $item->url = esc_url( $brand_transient_value );

                    } else {

                        $brand_args = [
                            'post_type'      => 'brand',
                            'posts_per_page' => 1,
                            'author'         => $user_id
                        ];
                        $brand_query = new WP_Query( $brand_args );
                        if ( $brand_query->have_posts() ) : while ( $brand_query->have_posts() ) : $brand_query->the_post();

                            $brand_permalink = get_permalink();
                            $item->url = esc_url( $brand_permalink );

                        endwhile; wp_reset_postdata(); endif;

                    }

                }

            }

            return $items;

        }

        /**
         * Change search form html.
         *
         * @param $form
         *
         * @return string
         */
        public function vlf_get_search_form( $form )
        {

            $form = '<form role="search" method="get" class="site-header__site-search-form" action="' . esc_url( home_url( '/' ) ) . '">
                <input type="search" class="search-field" placeholder="' . esc_attr_x( 'Search and you shall find', 'placeholder', 'vuelabs-framework' ) . '" value="' . get_search_query() . '" name="s" />
                <button type="submit" class="is-sr-only"></button>
            </form>';

            return $form;

        }

        public function vlf_login_redirect( $redirect_to, $request, $user )
        {

            if ( isset( $user->roles ) && is_array( $user->roles ) ) {

                // Check if user any of our users.
                if ( in_array( 'freshmes_personal', $user->roles ) || in_array( 'freshmes_business', $user->roles ) || in_array( 'freshmes_organization', $user->roles ) ) {

                    $user_id = $user->ID;
                    $user_onboarding = get_user_meta( $user_id, 'user_onboarding', true );
                    if ( isset( $user_onboarding ) && ! empty( $user_onboarding ) && $user_onboarding === '1' ) {

                        // Do nothing
                        return $redirect_to;

                    } else {

                        update_user_meta( $user_id, 'user_onboarding', 1 );
                        return get_field( 'build_your_brand_page', 'option' );

                    }

                }

            }

            return $redirect_to;

        }

        /**
         * Enables author field for WC Product.
         */
        public function vlf_enable_author_for_product()
        {

            if ( post_type_exists( 'product' ) ) {

                add_post_type_support( 'product', 'author' );

            }

        }

        public function vlf_rewrite_rules_array( $rules )
        {

            $vlf_rules = [];
            $vlf_rules['brand/([^/]+)/blog/?$'] = 'index.php?brand=$matches[1]&fpage=blog';
            $vlf_rules['brand/([^/]+)/shop/?$'] = 'index.php?brand=$matches[1]&fpage=shop';

            $vlf_rules['dashboard/([^/]+)/brand/?$'] = 'index.php?brand=$matches[1]&fpage=brand';
            $vlf_rules['dashboard/([^/]+)/connect/?$'] = 'index.php?brand=$matches[1]&fpage=connect';
            $vlf_rules['dashboard/([^/]+)/promote/?$'] = 'index.php?brand=$matches[1]&fpage=promote';
            $vlf_rules['dashboard/([^/]+)/sell/?$'] = 'index.php?brand=$matches[1]&fpage=sell';
            $vlf_rules['dashboard/([^/]+)/settings/?$'] = 'index.php?brand=$matches[1]&fpage=settings';

            return $vlf_rules + $rules;

        }

        public function vlf_query_vars( $vars )
        {

            array_push( $vars, 'fpage' );
            return $vars;

        }

        public function vlf_body_class( $classes )
        {

            $current_brand_page = get_query_var( 'fpage' );
            if ( $current_brand_page === 'blog' || $current_brand_page === 'shop' ) {

                $classes[] = 'navigator-abs';

            }

            return $classes;

        }

        public function vlf_loop_shop_per_page( $cols )
        {

            $cols = 6;

            return $cols;

        }

        public function woocommerce_output_related_products_args( $args )
        {

            $args['posts_per_page'] = 3;

            return $args;

        }

        public function woocommerce_checkout_fields( $fields )
        {

            $fields['billing']['billing_address_2']['label_class'] = '';
            $fields['shipping']['shipping_address_2']['label_class'] = '';

            return $fields;

        }

    }

endif;

return new VueLabs_Framework();
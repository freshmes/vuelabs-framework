<?php
/**
 * VueLabs Framework Posts.
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc
 * @since 1.0.0
 */

if ( ! function_exists( 'vlf_display_post' ) ) {

    function vlf_display_post( $post_id, $post_image_size = 'single-image', $featured = false )
    {

        $current_brand_page = get_query_var( 'fpage' );

        $post_author_id   = get_post_field( 'post_author', $post_id );
        $post_author_data = get_userdata( $post_author_id );

        $post_author_image = get_field( 'brand_profile_image', 'user_' . $post_author_id );
        $post_author_image = vlf_check_field( $post_author_image );

        $post_categories = get_the_category( $post_id );

        ?>

        <article id="post-<?php echo esc_attr( $post_id ); ?>" class="<?php echo esc_attr( join( ' ', get_post_class( 'freshmes-post', $post_id ) ) ); ?>">
            <header class="freshmes-post__header">
                <?php
                if ( has_post_thumbnail( $post_id ) ) {

                    if ( is_singular( 'brand' ) ) {

                        $image_size = 'single-image';

                    } else {

                        $image_size = 'product-image';

                    }

                    if ( $post_image_size !== 'product-image' && $post_image_size !== 'single-image' ) {

                        $image_size = $post_image_size;

                    }

                    echo '<div class="freshmes-post__header__image">';
                    echo get_the_post_thumbnail( $post_id, $image_size );
                    echo '</div>';

                }
                if ( $featured ) {

                    echo '<div class="freshmes-post__header__featured">' . esc_html__( 'Featured Post', 'vuelabs-framework' ) . '</div>';

                }
                ?>
                <a href="<?php echo esc_url( get_permalink( vlf_get_brand_id_from_author_id( $post_author_id ) ) ); ?>" class="freshmes-post__header__author">
                    <div class="freshmes-post__header__author__image">
                        <?php echo wp_get_attachment_image( $post_author_image, 'thumbnail' ); ?>
                    </div>
                    <div class="freshmes-post__header__author__by"><?php printf( esc_html__( 'Post by %s', 'vuelabs-framework' ), $post_author_data->first_name . ' ' . $post_author_data->last_name ); ?></div>
                </a>
            </header>
            <div class="freshmes-post__content">
                <?php if ( isset( $post_categories ) && ! empty( $post_categories ) ) : ?>
                    <div class="freshmes-post__content__categories">
                        <?php
                        foreach ( $post_categories as $post_category ) {

                            if ( $post_category === end( $post_categories ) ) {

                                echo '<div class="freshmes-post__content__categories__category">' . sprintf( esc_html__( 'Juggle %s', 'vuelabs-framework' ), $post_category->name ) . '</div>';

                            } else {

                                echo '<div class="freshmes-post__content__categories__category">' . sprintf( esc_html__( 'Juggle %s, ', 'vuelabs-framework' ), $post_category->name ) . '</div>';

                            }

                        }
                        ?>
                    </div>
                <?php endif; ?>
                <h2 class="freshmes-post__content__title"><?php echo esc_html( get_the_title( $post_id ) ); ?></h2>
                <?php
                    if ( isset( $current_brand_page ) && ! empty( $current_brand_page ) && $current_brand_page === 'blog' || vlf_is_blog() ) {

                        echo '<div class="freshmes-post__content__excerpt">' . get_the_excerpt( $post_id ) . '</div>';

                    }
                ?>
            </div>
            <footer class="freshmes-post__footer">
                <div class="columns is-v-centered">
                    <div class="column">
                        <a href="<?php echo esc_url( get_permalink( $post_id ) ); ?>" class="button freshmes-post__footer__button"><?php esc_html_e( 'View Post', 'vuelabs-framework' ); ?></a>
                    </div>
                    <div class="column has-text-right">
                        <div class="freshmes-post__footer__meta">
                            <a href="<?php echo esc_url( get_permalink( vlf_get_brand_id_from_author_id( $post_author_id ) ) ); ?>" class="freshmes-post__footer__meta__item freshmes-post__footer__meta__item--profile">
                                <i class="fal fa-user-circle"></i>
                            </a>
                            <a href="#" class="freshmes-post__footer__meta__item freshmes-post__footer__meta__item--share">
                                <i class="fal fa-share"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </footer>
        </article>

        <?php

    }

}

if ( ! function_exists( 'vlf_display_story' ) ) {

    function vlf_display_story( $post_id )
    {

        $featured = get_field( 'post_is_featured', $post_id );
        $featured = vlf_check_field( $featured );

        $post_author_id   = get_post_field( 'post_author', $post_id );
        $post_author_data = get_userdata( $post_author_id );

        $post_author_image = get_field( 'brand_profile_image', 'user_' . $post_author_id );
        $post_author_image = vlf_check_field( $post_author_image );

        $post_categories = get_the_category( $post_id );

        ?>

        <article id="post-<?php echo esc_attr( $post_id ); ?>" class="<?php echo esc_attr( join( ' ', get_post_class( 'freshmes-story', $post_id ) ) ); ?>">
            <div class="freshmes-story__image-overlay"></div>
            <div class="freshmes-story__image" style="background-image: url(<?php echo esc_url( get_the_post_thumbnail_url( $post_id ) ); ?>);"></div>
            <header class="freshmes-story__header">
                <div class="columns is-v-centered">
                    <div class="column has-text-left">
                        <a href="<?php echo esc_url( get_permalink( vlf_get_brand_id_from_author_id( $post_author_id ) ) ); ?>" class="freshmes-story__header__author">
                            <div class="freshmes-story__header__author__image">
                                <?php echo wp_get_attachment_image( $post_author_image, 'thumbnail' ); ?>
                            </div>
                            <div class="freshmes-story__header__author__by"><?php echo esc_html( $post_author_data->first_name . ' ' . $post_author_data->last_name ); ?></div>
                        </a>
                    </div>
                    <div class="column has-text-right">
                        <?php if ( isset( $post_categories ) && ! empty( $post_categories ) ) : ?>
                            <div class="freshmes-story__header__categories">
                                <?php
                                foreach ( $post_categories as $post_category ) {

                                    if ( $post_category === end( $post_categories ) ) {

                                        echo '<div class="freshmes-story__header__categories__category">' . sprintf( __( 'Juggle <span>%s</span>', 'vuelabs-framework' ), $post_category->name ) . '</div>';

                                    } else {

                                        echo '<div class="freshmes-story__header__categories__category">' . sprintf( __( 'Juggle <span>%s</span>, ', 'vuelabs-framework' ), $post_category->name ) . '</div>';

                                    }

                                }
                                ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </header>
            <div class="freshmes-story__content">
                <h2 class="freshmes-story__content__title"><?php echo esc_html( get_the_title( $post_id ) ); ?></h2>
                <a href="<?php echo esc_url( get_permalink( $post_id ) ); ?>" class="button freshmes-story__content__button"><?php esc_html_e( 'Be Inspired', 'vuelabs-framework' ); ?></a>
            </div>
            <footer class="freshmes-story__footer">
                <div class="freshmes-story__footer__meta has-text-right">
                    <a href="<?php echo esc_url( get_permalink( vlf_get_brand_id_from_author_id( $post_author_id ) ) ); ?>" class="freshmes-story__footer__meta__item freshmes-story__footer__meta__item--profile">
                        <i class="fal fa-user-circle"></i>
                    </a>
                    <a href="#" class="freshmes-story__footer__meta__item freshmes-story__footer__meta__item--share">
                        <i class="fal fa-share"></i>
                    </a>
                </div>
            </footer>
        </article>

        <?php

    }

}
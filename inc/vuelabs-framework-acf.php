<?php
/**
 * VueLabs Framework ACF.
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc
 * @since 1.0.0
 */

if ( function_exists( 'acf_add_options_page' ) ) {

    acf_add_options_page( apply_filters( 'vlf_acf_add_options_page', [
        'page_title' => esc_html__( 'Freshmes Options', 'vuelabs-framework' ),
        'menu_title' => esc_html__( 'Freshmes Options', 'vuelabs-framework' ),
        'menu_slug'  => esc_attr__( 'freshmes-options', 'vuelabs-framework' )
    ] ) );

    add_filter('acf/load_field/name=product_role', 'vlf_populate_user_groups');
    function vlf_populate_user_groups( $field )
    {
        // reset choices
        $field['choices'] = array();

        global $wp_roles;
        $roles = $wp_roles->get_names();

        foreach ( $roles as $key => $value ) {

            $field['choices'][ $key ] = $value;

        }

        return $field;
    }

}
<?php
/**
 * VueLabs Framework REST Class
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {

    exit;

}

if ( ! class_exists( 'VueLabs_Framework_REST' ) ) :

    class VueLabs_Framework_REST
    {

        /**
         * Setup class.
         *
         * @since 1.0.0
         */
        public function __construct()
        {

            $this->vlf_init_rest();

        }

        /**
         * Initializes REST API
         */
        public function vlf_init_rest()
        {

            // Check for REST API
            if ( ! class_exists( 'WP_REST_Server' ) ) {

                return;

            }

            // Include our REST API classes.
            $this->vlf_include_rest_classes();

            // Initializes REST API routes.
            add_action( 'rest_api_init', [ $this, 'vlf_register_rest_routes' ], 10 );

        }

        /**
         * Includes our REST API classes.
         */
        public function vlf_include_rest_classes()
        {

            // Example:
            // require_once get_theme_file_uri( 'inc/rest/class-example-controller.php' );

        }

        /**
         * Registers our REST API routes.
         */
        public function vlf_register_rest_routes()
        {

            $controllers = [
                // 'VLF_REST_Example_Controller'
            ];

            if ( ! empty( $controllers ) ) {

                foreach ( $controllers as $controller ) {

                    $this->$controller = new $controller();
                    $this->$controller->register_routes();

                }

            }

        }

    }

endif;

return new VueLabs_Framework_REST();
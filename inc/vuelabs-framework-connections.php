<?php
/**
 * VueLabs Framework Connections.
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc
 * @since 1.0.0
 */

if ( ! function_exists( 'vlf_create_connection' ) ) {

    /**
     * @param $user_a
     * @param $user_b
     * @param $category
     *
     * @return bool
     */
    function vlf_create_connection( $user_a, $user_b, $category )
    {

        if ( isset( $user_a ) && isset( $user_b ) && isset( $category ) && ! empty( $user_a ) && ! empty( $user_b ) && ! empty( $category ) ) {

            global $wpdb;

            $user_a = intval( $user_a );
            $user_b = intval( $user_b );

            $insert = $wpdb->insert( "{$wpdb->prefix}freshmes_connections", [
                'user_a'   => intval( $user_a ),
                'user_b'   => intval( $user_b ),
                'category' => $category
            ] );

            if ( $insert !== false ) {

                return true;

            }

        }

        return false;

    }

}

if ( ! function_exists( 'vlf_read_connection' ) ) {

    /**
     * @param     $user
     * @param     $category
     * @param     $type
     * @param int $limit
     * @param int $offset
     *
     * @return array|object|null
     */
    function vlf_read_connection( $user, $category, $type, $limit = 10, $offset = 0 )
    {

        if ( isset( $user ) && isset( $category ) && isset( $type ) && ! empty( $user ) && ! empty( $category ) && ! empty( $type ) ) {

            global $wpdb;

            $user = intval( $user );
            if ( $limit == '-1' ) {

                $limit = '';

            } else {

                $limit = "LIMIT {$limit}";

            }

            switch ( $type ) {

                case 'count':
                    if ( $category !== 'all' ) {

                        $read_sql = "SELECT COUNT(*) FROM {$wpdb->prefix}freshmes_connections WHERE user_a = {$user} AND category = '{$category}'";

                    } else {

                        $read_sql = "SELECT COUNT(*) FROM {$wpdb->prefix}freshmes_connections WHERE user_a = {$user}";

                    }
                    $count = $wpdb->get_results( $read_sql );
                    return $count;
                    break;
                case 'connections':
                    if ( $category !== 'all' ) {

                        $read_sql = "SELECT user_b FROM {$wpdb->prefix}freshmes_connections WHERE (user_a = {$user} AND category = '{$category}') OR (user_a = {$user} AND category = 'all') {$limit} OFFSET {$offset}";

                    } else {

                        $read_sql = "SELECT user_b FROM {$wpdb->prefix}freshmes_connections WHERE user_a = {$user} {$limit} OFFSET {$offset}";

                    }
                    $connections = $wpdb->get_results( $read_sql, OBJECT );
                    return $connections;
                    break;

            }

        }

    }

}

if ( ! function_exists( 'vlf_update_connection' ) ) {

    /**
     * @param $user_a
     * @param $user_b
     * @param $category
     *
     * @return bool
     */
    function vlf_update_connection( $user_a, $user_b, $category )
    {

        if ( isset( $user_a ) && isset( $user_b ) && isset( $category ) && ! empty( $user_a ) && ! empty( $user_b ) && ! empty( $category ) ) {

            global $wpdb;

            $user_a = intval( $user_a );
            $user_b = intval( $user_b );

            $update = $wpdb->update( "{$wpdb->prefix}freshmes_connections",
                [
                    'category' => $category
                ],
                [
                    'user_a'   => intval( $user_a ),
                    'user_b'   => intval( $user_b )
                ]
            );

            if ( $update !== false ) {

                return true;

            }

        }

        return false;

    }

}

if ( ! function_exists( 'vlf_delete_connection' ) ) {

    /**
     * @param $user_a
     * @param $user_b
     *
     * @return bool
     */
    function vlf_delete_connection( $user_a, $user_b )
    {

        if ( isset( $user_a ) && isset( $user_b ) && ! empty( $user_a ) && ! empty( $user_b ) ) {

            global $wpdb;

            $user_a = intval( $user_a );
            $user_b = intval( $user_b );

            $delete = $wpdb->delete( "{$wpdb->prefix}freshmes_connections", [
                'user_a'   => intval( $user_a ),
                'user_b'   => intval( $user_b )
            ] );

            if ( $delete !== false ) {

                return true;

            }

        }

        return false;

    }

}

if ( ! function_exists( 'vlf_has_connection' ) ) {

    /**
     * @param $from
     * @param $to
     *
     * @return bool
     */
    function vlf_has_connection( $from, $to ) {

        if ( isset( $from ) && isset( $to ) && ! empty( $from ) && ! empty( $to ) ) {

            global $wpdb;

            $from = intval( $from );
            $to   = intval( $to );

            $result = $wpdb->get_var( "SELECT COUNT(*) FROM {$wpdb->prefix}freshmes_connections WHERE user_a = {$from} AND user_b = {$to}" );

            if ( $result !== '0' ) {

                return true;

            }

        }

        return false;

    }

}
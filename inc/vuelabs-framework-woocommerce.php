<?php
/**
 * VueLabs Framework WooCommerce.
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc
 * @since 1.0.0
 */

/**
 * Create new product type.
 */
add_action( 'init', 'vlf_wc_create_product_event' );
function vlf_wc_create_product_event()
{

    class WC_Product_VLF_Event extends WC_Product {

        public function __construct( $product )
        {

            $this->product_type = 'vlf_event';
            parent::__construct( $product );

        }

    }

}

/**
 * Add new product type to product type selector.
 */
add_filter( 'product_type_selector', 'vlf_wc_event_product_type_selector' );
function vlf_wc_event_product_type_selector( $types )
{

    $types['vlf_event'] = esc_html__( 'Freshmes Event', 'vuelabs-framework' );

    return $types;

}

/**
 * Show Price Tab when custom product type is selected.
 */
add_action( 'admin_footer', 'vlf_wc_custom_js' );
function vlf_wc_custom_js() {

    // Exit if we're not on our product page.
    if ( 'product' !== get_post_type() ) {

        return;

    }

    ?>

    <script>
        jQuery(document).ready(function($) {
          'use strict';

          $(window).on('load', function() {
            $('.product_data_tabs .general_tab').addClass('show_if_vlf_event').show();
            $('#general_product_data .pricing').addClass('show_if_vlf_event').show();
          });

          $('#product-type').on('change', function(e) {
            if ( $(this).val() === 'vlf_event' ) {
              $('.product_data_tabs .general_tab').addClass('show_if_vlf_event').show();
              $('#general_product_data .pricing').addClass('show_if_vlf_event').show();
            }
          });
        });
    </script>

    <?php

}
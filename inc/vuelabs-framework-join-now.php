<?php
/**
 * VueLabs Framework Join Now.
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc
 * @since 1.0.0
 */

if ( ! function_exists( 'vlf_is_organization_member' ) ) {

    /**
     * @param $organization
     * @param $user_id
     *
     * @return array|bool
     */
    function vlf_is_organization_member( $organization, $user_id ) {

        if ( isset( $organization ) && isset( $user_id ) && ! empty( $organization ) && ! empty( $user_id ) ) {

            // User
            $user = new WP_User( $user_id );

            // Get organization term.
            $organization_term_id = get_the_terms( $organization, 'brand_organization' );
            $organization_term_id = $organization_term_id[0]->term_id;

            // Get User Profile Page
            $args = [ 'post_type' => 'brand', 'posts_per_page' => 1, 'author' => $user_id ];
            $user_query = new WP_Query( $args );
            $user_profile = 0;
            while ( $user_query->have_posts() ) : $user_query->the_post();
                $user_profile = get_the_ID();
            endwhile; wp_reset_postdata();

            // Check if current user is organization.
            if ( in_array( 'freshmes_organization', $user->roles ) ) {

                // Check if our users brand is part of organization.
                if ( has_term( $organization_term_id, 'brand_organization', $user_profile ) ) {

                    return [
                        'status' => true
                    ];

                } else {

                    return [
                        'status' => false,
                        'prompt' => ''
                    ];

                }

            } else {

                return [
                    'status' => false,
                    'prompt' => 'upgrade'
                ];

            }

        }

        return false;

    }

}

if ( ! function_exists( 'vlf_join_brand_organization' ) ) {

    /**
     * @param $brand_id
     * @param $user_id
     *
     * @return bool
     */
    function vlf_join_brand_organization( $brand_id, $user_id )
    {

        // Get organization term.
        $organization_term_id = get_the_terms( $brand_id, 'brand_organization' );
        $organization_term_id = $organization_term_id[0]->term_id;

        // Get User Profile Page
        $args = [ 'post_type' => 'brand', 'posts_per_page' => 1, 'author' => $user_id ];
        $user_query = new WP_Query( $args );
        $user_profile = 0;
        while ( $user_query->have_posts() ) : $user_query->the_post();
            $user_profile = get_the_ID();
        endwhile; wp_reset_postdata();

        $remove = wp_set_object_terms( $user_profile, $organization_term_id, 'brand_organization' );

        if ( ! is_wp_error( $remove ) ) {

            return true;

        }

        return false;

    }

}

if ( ! function_exists( 'vlf_remove_brand_from_organization' ) ) {

    /**
     * @param $brand_id
     * @param $user_id
     *
     * @return bool
     */
    function vlf_remove_brand_from_organization( $brand_id, $user_id )
    {

        // Get organization term.
        $organization_term_id = get_the_terms( $brand_id, 'brand_organization' );
        $organization_term_id = $organization_term_id[0]->term_id;

        // Get User Profile Page
        $args = [ 'post_type' => 'brand', 'posts_per_page' => 1, 'author' => $user_id ];
        $user_query = new WP_Query( $args );
        $user_profile = 0;
        while ( $user_query->have_posts() ) : $user_query->the_post();
            $user_profile = get_the_ID();
        endwhile; wp_reset_postdata();

        $remove = wp_remove_object_terms( $user_profile, $organization_term_id, 'brand_organization' );

        if ( ! is_wp_error( $remove ) ) {

            return true;

        }

        return false;

    }

}
<?php
/**
 * Freshmes Brand Tags Class
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc/widgets
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {

    exit;

}

if ( ! class_exists( 'Freshmes_Brand_Tags' ) ) :

    class Freshmes_Brand_Tags extends WP_Widget
    {

        public function __construct() {

            parent::__construct(
                'vlf_tags', // Base ID
                '&#x1F536; ' . esc_html__( 'Freshmes Brand', 'vuelabs-framework' ) . ' &raquo; ' . esc_html__( 'Tags', 'vuelabs-framework' ), // Name
                [] // Args
            );

        }

        public function widget( $args, $instance ) {

            global $post;
            echo $args['before_widget'];

            $brand_id  = $post->ID;

            if ( isset( $_GET['network'] ) && ! empty( $_GET['network'] ) ) {

                $brand_id = intval( $_GET['network'] );
                $author_id = get_post_field( 'post_author', $brand_id );

            }

            $tags = get_the_terms( $brand_id, 'brand_tag' );

            ?>

            <div class="brand-single-page__sidebars__widget-tags">
                <?php
                if ( isset( $tags ) && ! empty( $tags ) ) {

                    echo '<div class="columns is-multiline">';
                    foreach ( $tags as $tag ) {

                        $tag_icon = get_field( 'brand_tag_icon', 'brand_tag_' . $tag->term_id );
                        $tag_icon = vlf_check_field( $tag_icon );

                        ?>

                        <div class="column is-3-desktop is-4-tablet is-6-mobile">
                            <div class="brand-single-page__sidebars__widget-tags__tag">
                                <?php if ( $tag_icon ) : ?>
                                    <span class="brand-single-page__sidebars__widget-tags__tag__icon">
                                        <?php echo wp_kses( $tag_icon, [ 'i' => [ 'class' => [] ] ] ); ?>
                                    </span>
                                <?php endif; ?>
                                <span class="brand-single-page__sidebars__widget-tags__tag__label"><?php echo esc_html( $tag->name ); ?></span>
                            </div>
                        </div>

                        <?php

                    }
                    echo '</div>';

                }
                ?>
            </div>

            <?php

            echo $args['after_widget'];

        }

        public function form( $instance ) {

            echo '<p>' . esc_html__( 'Add to brand sidebar.', 'vuelabs-framework' ) . '</p>';

        }

    }

endif;
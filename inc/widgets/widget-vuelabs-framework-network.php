<?php
/**
 * Freshmes Brand Network Class
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc/widgets
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {

    exit;

}

if ( ! class_exists( 'Freshmes_Brand_Network' ) ) :

    class Freshmes_Brand_Network extends WP_Widget
    {

        public function __construct() {

            parent::__construct(
                'vlf_network', // Base ID
                '&#x1F536; ' . esc_html__( 'Freshmes Brand', 'vuelabs-framework' ) . ' &raquo; ' . esc_html__( 'Network', 'vuelabs-framework' ), // Name
                [] // Args
            );

        }

        public function widget( $args, $instance ) {

            global $post;

            $brand_id  = $post->ID;
            $author_id = $post->post_author;
            $author    = get_userdata( $author_id );
            $user      = new WP_User( $author_id );

            echo $args['before_widget'];

            if ( is_user_logged_in() ) {

                if ( get_current_user_id() === intval( $author_id ) ) {

                    // Check for permissions.
                    if ( in_array( 'freshmes_organization', $user->roles ) ) {

                        // Check for chapters.
                        $organization = get_the_terms( $brand_id, 'brand_organization' );
                        if ( isset( $organization ) && ! empty( $organization ) ) {

                            $organization_id = 0;
                            foreach ( $organization as $org ) {

                                if ( $org->parent === 0 ) {

                                    $organization_id = $org->term_id;

                                }

                            }
                            $chapters        = get_term_children( intval( $organization_id ), 'brand_organization' );
                            if ( isset( $chapters ) && ! empty( $chapters ) ) {

                                ?>

                                <?php

                            } else {

                                ?>

                                <div class="brand-single-page__sidebars__widget-network brand-single-page__sidebars__widget-network--is-empty has-text-centered">
                                    <div class="brand-single-page__sidebars__widget-network__title"><?php esc_html_e( 'Create Network', 'vuelabs-framework' ); ?></div>
                                    <div class="brand-single-page__sidebars__widget-network__description"><?php esc_html_e( 'Manage and scale your organization, while providing your membership with  greater value and exposure. Become a partner affiliate to earn recurring revenue.', 'vuelabs-framework' ); ?></div>
                                    <a href="#" class="button brand-single-page__sidebars__widget-network__create-network"><?php esc_html_e( 'Create Network', 'vuelabs-framework' ); ?></a>
                                </div>

                                <?php

                            }

                        }

                    }

                }

            }

            echo $args['after_widget'];

        }

        public function form( $instance ) {

            echo '<p>' . esc_html__( 'Add to brand sidebar.', 'vuelabs-framework' ) . '</p>';

        }

    }

endif;
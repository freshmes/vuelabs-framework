<?php
/**
 * Freshmes WC Categories Class
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc/widgets/wc
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {

    exit;

}

if ( ! class_exists( 'Freshmes_WC_Categories' ) ) :

    class Freshmes_WC_Categories extends WP_Widget
    {

        public function __construct() {

            parent::__construct(
                'vlf_wc_categories', // Base ID
                '&#x1F536; ' . esc_html__( 'Freshmes WC Categories', 'vuelabs-framework' ), // Name
                [] // Args
            );

        }

        public function widget( $args, $instance ) {

            global $wp;

            echo $args['before_widget'];

            $title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'Product Categories' );
            $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

            if ( $title ) {
                echo $args['before_title'] . $title . $args['after_title'];
            }

            $categories = get_terms( [
                'hide_empty' => true,
                'taxonomy'   => 'product_cat'
            ] );

            if ( isset( $_GET['product_cat'] ) && ! empty( $_GET['product_cat'] ) ) {

                $current_categories = $_GET['product_cat'];
                $current_categories = explode( ',', $current_categories );

            } else {

                $current_categories = [];

            }

            ?>

            <?php if ( isset( $categories ) && ! empty( $categories ) ) : ?>
                <ul class="shop-categories">
                    <?php foreach ( $categories as $category ) : ?>
                        <?php
                            $checked = '';
                            if ( ! empty( $current_categories ) ) {

                                if ( in_array( $category->slug, $current_categories ) ) {

                                    $checked = 'checked';

                                }

                            }
                        ?>
                        <li class="shop-categories__category">
                            <div class="field">
                                <input class="is-checkradio" id="<?php echo esc_attr( 'product-cat-' . $category->term_id ); ?>" type="checkbox" name="product-cat[]" value="<?php echo esc_attr( $category->slug ); ?>" <?php echo esc_attr( $checked ); ?>>
                                <label for="<?php echo esc_attr( 'product-cat-' . $category->term_id ); ?>"><?php echo esc_html( $category->name ); ?></label>
                            </div>
                            <div class="shop-categories__category__count"><?php echo esc_html( $category->count ); ?></div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>

            <?php

            echo $args['after_widget'];

        }

        public function update( $new_instance, $old_instance )
        {

            $instance                 = $old_instance;
            $instance['title']        = sanitize_text_field( $new_instance['title'] );

            return $instance;

        }

        public function form( $instance ) {

            $instance = wp_parse_args( (array) $instance, [ 'title' => '' ] );

            ?>
            <p>
                <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
            </p>
            <?php

        }

    }

endif;
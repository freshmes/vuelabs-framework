<?php
/**
 * Freshmes Brand Contacts Class
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc/widgets
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {

    exit;

}

if ( ! class_exists( 'Freshmes_Brand_Contacts' ) ) :

    class Freshmes_Brand_Contacts extends WP_Widget
    {

        public function __construct() {

            parent::__construct(
                'vlf_contacts', // Base ID
                '&#x1F536; ' . esc_html__( 'Freshmes Brand', 'vuelabs-framework' ) . ' &raquo; ' . esc_html__( 'Contacts', 'vuelabs-framework' ), // Name
                [] // Args
            );

        }

        public function widget( $args, $instance ) {

            global $post;
            echo $args['before_widget'];

            $class = 'brand-single-page__sidebars__widget-contacts';
            if ( intval( vlf_read_connection( $post->post_author, 'all', 'count' ) ) === 0 ) {

                $class .= ' brand-single-page__sidebars__widget-contacts--is-empty';

            }

            echo '<div class="' . esc_attr( $class ) . '">';

                if ( intval( vlf_read_connection( $post->post_author, 'all', 'count' ) ) === 0 ) {

                    if ( intval( get_current_user_id() ) === intval( $post->post_author ) ) {

                        $contacts_page = get_home_url( '/' ) . 'brand/';

                        echo '<div class="brand-single-page__sidebars__widget-contacts__title">' . esc_html__( 'Add Contacts', 'vuelabs-framework' ) . '</div>';
                        echo '<div class="brand-single-page__sidebars__widget-contacts__description">' . esc_html__( 'Now that you’ve built your brand page it’s time to connect and grow your core network. Build deep, honest relationships that you can trust with your kids or your business. Now get to it.', 'vuelabs-framework' ) . '</div>';
                        if ( $contacts_page ) {

                            echo '<a href="' . esc_url( $contacts_page ) . '" class="button is-primary">' . esc_html__( 'Add Contacts', 'vuelabs-framework' ) . '</a>';

                        }

                    }

                } else {

                    $contacts_page = get_field( 'contacts_page', 'option' );
                    $contacts_page = vlf_check_field( $contacts_page );

                    $work_connections = vlf_read_connection( intval( $post->post_author ), 'work', 'connections', '10' );
                    $life_connections = vlf_read_connection( intval( $post->post_author ), 'life', 'connections', '10' );

                    ?>

                    <div class="brand-single-page__sidebars__widget-contacts__title has-text-centered"><?php esc_html_e( 'Core Connections', 'vuelabs-framework' ); ?></div>
                    <div class="brand-single-page__sidebars__tabs">
                        <header class="brand-single-page__sidebars__tabs__header has-text-centered">
                            <a href="#" class="brand-single-page__sidebars__tabs__header__item" data-tab="work"><?php esc_html_e( 'Work', 'vuelabs-framework' ); ?></a>
                            <a href="#" class="brand-single-page__sidebars__tabs__header__item" data-tab="life"><?php esc_html_e( 'Life', 'vuelabs-framework' ); ?></a>
                        </header>
                        <div class="brand-single-page__sidebars__tabs__content" data-tab="work">
                            <a href="<?php echo esc_url( $contacts_page . '?type=work' ); ?>" class="brand-single-page__sidebars__tabs__content__view-all"><?php esc_html_e( 'View All', 'vuelabs-framework' ); ?></a>
                            <?php
                                if ( ! empty( $work_connections ) ) {

                                    echo '<div class="brand-single-page__sidebars__tabs__content__slider">';
                                    foreach ($work_connections as $work_connection) {

                                        $connection_link     = '';
                                        $connection_image_id = 0;

                                        $connection_data = get_userdata($work_connection->user_b);

                                        $loop_args = [
                                            'post_type'      => 'brand',
                                            'posts_per_page' => 1,
                                            'author'         => $work_connection->user_b
                                        ];
                                        $loop      = new WP_Query($loop_args);
                                        while ($loop->have_posts()) : $loop->the_post();
                                            $connection_link     = get_permalink();
                                            $connection_image_id = get_field('brand_profile_image');
                                            $connection_image_id = vlf_check_field($connection_image_id);
                                        endwhile;
                                        wp_reset_postdata();

                                        ?>

                                        <div class="brand-single-page__sidebars__tabs__content__slider__item">
                                            <a href="<?php echo esc_url($connection_link); ?>">
                                                <span class="brand-single-page__sidebars__tabs__content__slider__item__image">
                                                    <?php echo wp_get_attachment_image($connection_image_id,
                                                        'thumbnail'); ?>
                                                </span>
                                                <span class="brand-single-page__sidebars__tabs__content__slider__item__name"><?php echo esc_html($connection_data->first_name . ' ' . $connection_data->last_name); ?></span>
                                            </a>
                                        </div>

                                        <?php

                                    }
                                    echo '</div>';

                                } else {

                                    echo '<p class="has-text-centered">' . esc_html__( 'This person doesn\'t have any connections at the moment.', 'vuelabs-framework' ) . '</p>';

                                }
                            ?>
                        </div>
                        <div class="brand-single-page__sidebars__tabs__content" data-tab="life">
                            <a href="<?php echo esc_url( $contacts_page . '?type=life' ); ?>" class="brand-single-page__sidebars__tabs__content__view-all"><?php esc_html_e( 'View All', 'vuelabs-framework' ); ?></a>
                            <?php
                            if ( ! empty( $life_connections ) ) {

                                echo '<div class="brand-single-page__sidebars__tabs__content__slider">';
                                foreach ($life_connections as $life_connection) {

                                    $connection_link     = '';
                                    $connection_image_id = 0;

                                    $connection_data = get_userdata($life_connection->user_b);

                                    $loop_args = [
                                        'post_type'      => 'brand',
                                        'posts_per_page' => 1,
                                        'author'         => $life_connection->user_b
                                    ];
                                    $loop      = new WP_Query($loop_args);
                                    while ($loop->have_posts()) : $loop->the_post();
                                        $connection_link     = get_permalink();
                                        $connection_image_id = get_field('brand_profile_image');
                                        $connection_image_id = vlf_check_field($connection_image_id);
                                    endwhile;
                                    wp_reset_postdata();

                                    ?>

                                    <div class="brand-single-page__sidebars__tabs__content__slider__item">
                                        <a href="<?php echo esc_url($connection_link); ?>">
                                                <span class="brand-single-page__sidebars__tabs__content__slider__item__image">
                                                    <?php echo wp_get_attachment_image($connection_image_id,
                                                        'thumbnail'); ?>
                                                </span>
                                            <span class="brand-single-page__sidebars__tabs__content__slider__item__name"><?php echo esc_html($connection_data->first_name . ' ' . $connection_data->last_name); ?></span>
                                        </a>
                                    </div>

                                    <?php

                                }
                                echo '</div>';

                            } else {

                                echo '<p class="has-text-centered">' . esc_html__( 'This person doesn\'t have any connections at the moment.', 'vuelabs-framework' ) . '</p>';

                            }
                            ?>
                        </div>
                    </div>

                    <?php

                }

            echo '</div>';

            echo $args['after_widget'];

        }

        public function form( $instance ) {

            echo '<p>' . esc_html__( 'Add to brand sidebar.', 'vuelabs-framework' ) . '</p>';

        }

    }

endif;
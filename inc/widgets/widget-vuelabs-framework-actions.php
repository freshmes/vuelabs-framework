<?php
/**
 * Freshmes Brand Actions Class
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc/widgets
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {

    exit;

}

if ( ! class_exists( 'Freshmes_Brand_Actions' ) ) :

    class Freshmes_Brand_Actions extends WP_Widget
    {

        public function __construct() {

            parent::__construct(
                'vlf_actions', // Base ID
                '&#x1F536; ' . esc_html__( 'Freshmes Brand', 'vuelabs-framework' ) . ' &raquo; ' . esc_html__( 'Actions', 'vuelabs-framework' ), // Name
                [] // Args
            );

        }

        public function widget( $args, $instance ) {

            global $post;
            echo $args['before_widget'];

            $brand_id  = $post->ID;
            $author_id = $post->post_author;

            if ( isset( $_GET['network'] ) && ! empty( $_GET['network'] ) ) {

                $brand_id = intval( $_GET['network'] );
                $author_id = get_post_field( 'post_author', $brand_id );

            }

            $author    = get_userdata( $author_id );
            $user      = new WP_User( $author_id );

            if ( in_array( 'freshmes_organization', $user->roles ) ) {

                $first_column_class  = 'column is-6 has-text-centered';
                $second_column_class = 'column is-4 has-text-centered';
                $third_column_class  = 'column is-6 has-text-centered';

            } else {

                $second_column_class = 'column is-6 has-text-centered';
                $third_column_class  = 'column is-12 has-text-centered';

            }

            ?>

            <div class="brand-single-page__sidebars__widget-actions">
                <div class="columns">
                    <?php if ( in_array( 'freshmes_organization', $user->roles ) ) : ?>
                        <div class="<?php echo esc_attr( $first_column_class ); ?>">
                            <?php
                                $join_class = 'brand-single-page__sidebars__widget-actions__action brand-single-page__sidebars__widget-actions__action--join-now';
                                $join_label = esc_html__( 'Join Now', 'vuelabs-framework' );
                                $join_icon  = '<i class="fal fa-user-plus"></i>';
                                if ( is_user_logged_in() ) {

                                    $current_user_id = get_current_user_id();
                                    $current_user_result = vlf_is_organization_member( $brand_id, $current_user_id );

                                    if ( $current_user_result['status'] === false ) {

                                        if ( $current_user_result['prompt'] === 'upgrade' ) {

                                            $join_class .= ' brand-single-page__sidebars__widget-actions__action--join-now--upgrade';

                                        } else {

                                            $join_class .= ' is-hidden';

                                        }

                                    } else {

                                        $join_class .= ' brand-single-page__sidebars__widget-actions__action--join-now--leave';
                                        $join_label = esc_html__( 'Leave Organization', 'vuelabs-framework' );
                                        $join_icon  = '<i class="fal fa-user-minus"></i>';

                                    }

                                } else {

                                    $join_class .= ' brand-single-page__sidebars__widget-actions__action--join-now--login';

                                }
                            ?>
                            <a href="#" class="<?php echo esc_attr( $join_class ); ?>" data-original-label="<?php esc_attr_e( 'Join Now', 'vuelabs-framework' ); ?>" data-brand-id="<?php echo esc_attr( $brand_id ); ?>" data-user-id="<?php echo esc_attr( get_current_user_id() ); ?>">
                                <span class="brand-single-page__sidebars__widget-actions__action__icon"><?php echo wp_kses( $join_icon, [ 'i' => [ 'class' => [] ] ] ); ?></span>
                                <span class="brand-single-page__sidebars__widget-actions__action__label"><?php echo $join_label; ?></span>
                            </a>
                        </div>
                    <?php endif; ?>
                    <!--<div class="<?php /*echo esc_attr( $second_column_class ); */?>">
                        <a href="#" class="brand-single-page__sidebars__widget-actions__action brand-single-page__sidebars__widget-actions__action--add-to-fav">
                            <span class="brand-single-page__sidebars__widget-actions__action__icon"><i class="fal fa-heart"></i></span>
                            <span class="brand-single-page__sidebars__widget-actions__action__label"><?php /*esc_html_e( 'Add to Favorites', 'vuelabs-framework' ); */?></span>
                        </a>
                    </div>-->
                    <div class="<?php echo esc_attr( $third_column_class ); ?>">
                        <a href="#" class="brand-single-page__sidebars__widget-actions__action brand-single-page__sidebars__widget-actions__action--share">
                            <span class="brand-single-page__sidebars__widget-actions__action__icon"><i class="fal fa-share"></i></span>
                            <span class="brand-single-page__sidebars__widget-actions__action__label"><?php esc_html_e( 'Share', 'vuelabs-framework' ); ?></span>
                        </a>
                    </div>
                </div>
            </div>

            <?php

            echo $args['after_widget'];

        }

        public function form( $instance ) {

            echo '<p>' . esc_html__( 'Add to brand sidebar.', 'vuelabs-framework' ) . '</p>';

        }

    }

endif;
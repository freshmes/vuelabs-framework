<?php
/**
 * Freshmes Brand About Class
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc/widgets
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {

    exit;

}

if ( ! class_exists( 'Freshmes_Brand_About' ) ) :

    class Freshmes_Brand_About extends WP_Widget
    {

        public function __construct() {

            parent::__construct(
                'vlf_about', // Base ID
                '&#x1F536; ' . esc_html__( 'Freshmes Brand', 'vuelabs-framework' ) . ' &raquo; ' . esc_html__( 'About', 'vuelabs-framework' ), // Name
                [] // Args
            );

        }

        public function widget( $args, $instance ) {

            global $post;
            echo $args['before_widget'];

            $brand_id  = $post->ID;
            $author_id = $post->post_author;

            if ( isset( $_GET['network'] ) && ! empty( $_GET['network'] ) ) {

                $brand_id = intval( $_GET['network'] );
                $author_id = get_post_field( 'post_author', $brand_id );

            }

            $author    = get_userdata( $author_id );
            $user      = new WP_User( $author_id );

            $business_description = get_field( 'brand_business_description', $brand_id );
            $business_description = vlf_check_field( $business_description );

            $description          = get_field( 'brand_description', $brand_id );
            $description          = vlf_check_field( $description );

            ?>

            <div class="brand-single-page__sidebars__widget-about">
                <div class="brand-single-page__sidebars__title brand-single-page__sidebars__title--has-tabs"><?php esc_html_e( 'About', 'vuelabs-framework' ); ?></div>
                <div class="brand-single-page__sidebars__tabs">
                    <header class="brand-single-page__sidebars__tabs__header">
                        <a href="#" class="brand-single-page__sidebars__tabs__header__item" data-tab="life"><?php esc_html_e( 'Life', 'vuelabs-framework' ); ?></a>
                        <a href="#" class="brand-single-page__sidebars__tabs__header__item" data-tab="work"><?php esc_html_e( 'Work', 'vuelabs-framework' ); ?></a>
                    </header>
                    <div class="brand-single-page__sidebars__tabs__content" data-tab="work">
                        <?php
                        if ( $business_description ) {

                            $work_count = 0;

                            ?>

                            <ul class="brand-single-page__sidebars__accordion">
                                <?php foreach ( $business_description as $item ) : ?>
                                    <?php
                                    $work_class = 'brand-single-page__sidebars__accordion__item';
                                    if ( $work_count === 0 ) {

                                        $work_class .= ' brand-single-page__sidebars__accordion__item--is-active';

                                    }
                                    ?>
                                    <li class="<?php echo esc_attr( $work_class ); ?>">
                                        <h3 class="brand-single-page__sidebars__accordion__item__title"><?php echo esc_html( $item['title'] ); ?></h3>
                                        <div class="brand-single-page__sidebars__accordion__item__content"><?php echo wp_kses( $item['content'], [ 'br' => [] ] ); ?></div>
                                    </li>
                                <?php $work_count++; endforeach; ?>
                            </ul>

                            <?php

                        }
                        ?>
                    </div>
                    <div class="brand-single-page__sidebars__tabs__content" data-tab="life">
                        <?php
                        if ( $description ) {

                            ?>

                            <ul class="brand-single-page__sidebars__accordion">
                                <?php $life_count = 0; ?>
                                <?php foreach ( $description as $item ) : ?>
                                    <?php
                                    $life_class = 'brand-single-page__sidebars__accordion__item';
                                    if ( $life_count === 0 ) {
                                        $life_class .= ' brand-single-page__sidebars__accordion__item--is-active';
                                    }
                                    ?>
                                    <li class="<?php echo esc_attr( $life_class ); ?>">
                                        <h3 class="brand-single-page__sidebars__accordion__item__title"><?php echo esc_html( $item['title'] ); ?></h3>
                                        <div class="brand-single-page__sidebars__accordion__item__content"><?php echo wp_kses( $item['content'], [ 'br' => [] ] ); ?></div>
                                    </li>
                                <?php $life_count++; endforeach; ?>
                            </ul>

                            <?php

                        }
                        ?>
                    </div>
                </div>
            </div>

            <?php

            echo $args['after_widget'];

        }

        public function form( $instance ) {

            echo '<p>' . esc_html__( 'Add to brand sidebar.', 'vuelabs-framework' ) . '</p>';

        }

    }

endif;
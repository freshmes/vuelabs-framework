<?php
/**
 * Freshmes Brand Reviews Class
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc/widgets
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {

    exit;

}

if ( ! class_exists( 'Freshmes_Brand_Reviews' ) ) :

    class Freshmes_Brand_Reviews extends WP_Widget
    {

        public function __construct() {

            parent::__construct(
                'vlf_reviews', // Base ID
                '&#x1F536; ' . esc_html__( 'Freshmes Brand', 'vuelabs-framework' ) . ' &raquo; ' . esc_html__( 'Reviews', 'vuelabs-framework' ), // Name
                [] // Args
            );

        }

        public function widget( $args, $instance ) {

            global $post;
            echo $args['before_widget'];

            $brand_id = $post->ID;

            $comments_args = [
                'post_id'   => $brand_id,
                'post_type' => 'brand',
                'number'    => 2,
                'include_unapproved' => false
            ];
            $comments_loop = new WP_Comment_Query();
            $comments      = $comments_loop->query( $comments_args );
            $has_comment   = false;

            $title_class = 'brand-single-page__sidebars__widget-reviews__title has-text-centered';
            if ( $comments ) {

                $title_class = 'brand-single-page__sidebars__widget-reviews__title brand-single-page__sidebars__widget-reviews__title--has-view-all has-text-centered';

            }

            ?>

            <div class="brand-single-page__sidebars__widget-reviews">
                <div class="<?php echo esc_html( $title_class ); ?>"><?php esc_html_e( 'Reviews', 'vuelabs-framework' ); ?></div>
                <?php
                if ( $comments ) {

                    ?>

                    <div class="has-text-centered">
                        <a href="#" class="brand-single-page__sidebars__widget-reviews__view-all"><?php esc_html_e( 'View All', 'vuelabs-framework' ); ?></a>
                    </div>
                    <div class="brand-single-page__sidebars__widget-reviews__comments">
                        <div class="columns is-multiline">
                            <?php foreach ( $comments as $comment ) : ?>

                                <?php
                                $comment_author_id = $comment->user_id;
                                $comment_author_image = get_field( 'brand_profile_image', 'user_' . $comment_author_id );
                                $comment_author_image = vlf_check_field( $comment_author_image );

                                $comment_title = get_field( 'comment_title', 'comment_' . $comment->comment_ID );
                                $comment_title = vlf_check_field( $comment_title );

                                $comment_date = $comment->comment_date;
                                $comment_date = new DateTime( $comment_date );
                                $comment_date = $comment_date->format( 'F jS, Y' );

                                $comment_rating = get_field( 'comment_rating', 'comment_' . $comment->comment_ID );
                                $comment_rating = vlf_check_field( $comment_rating );

                                if ( is_user_logged_in() ) {

                                    if ( intval( get_current_user_id() ) === intval( $comment_author_id ) ) {

                                        $has_comment = true;

                                    }

                                }
                                ?>

                                <div class="column is-6">
                                    <div id="brand-comment-<?php echo esc_attr( $comment->comment_ID ); ?>" class="brand-single-page__sidebars__widget-reviews__comments__comment">
                                        <div class="columns">
                                            <div class="column is-3">
                                                <div class="brand-single-page__sidebars__widget-reviews__comments__comment__author has-text-centered">
                                                    <div class="brand-single-page__sidebars__widget-reviews__comments__comment__author__image">
                                                        <?php echo wp_get_attachment_image( $comment_author_image, 'thumbnail' ); ?>
                                                    </div>
                                                    <div class="brand-single-page__sidebars__widget-reviews__comments__comment__author__name"><a href="<?php echo esc_url( vlf_get_brand_id_from_author_id( $comment->comment_ID ) ); ?>"><?php echo esc_html( $comment->comment_author ); ?></a></div>
                                                </div>
                                            </div>
                                            <div class="column is-9">
                                                <div class="brand-single-page__sidebars__widget-reviews__comments__comment__title">“<?php echo esc_html( $comment_title ); ?>”</div>
                                                <div class="brand-single-page__sidebars__widget-reviews__comments__comment__meta">
                                                    <div class="brand-single-page__sidebars__widget-reviews__comments__comment__meta__stars">
                                                        <?php
                                                        $comment_rating_bad = 5 - $comment_rating;
                                                        for ( $i = 1; $i <= $comment_rating; $i++ ) {

                                                            echo '<i class="fas fa-star"></i>';

                                                        }

                                                        for ( $i = 1; $i <= $comment_rating_bad; $i++ ) {

                                                            echo '<i class="fal fa-star"></i>';

                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="brand-single-page__sidebars__widget-reviews__comments__comment__meta__date"><?php echo esc_html( $comment_date ); ?></div>
                                                </div>
                                                <div class="brand-single-page__sidebars__widget-reviews__comments__comment__content content">
                                                    <?php echo apply_filters( 'the_content', $comment->comment_content ); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php endforeach; ?>
                        </div>
                    </div>

                    <?php

                }

                if ( is_user_logged_in() ) {

                    ?>

                    <?php if ( $has_comment === false && intval( $post->post_author ) !== intval( get_current_user_id() ) ) : ?>
                        <div class="has-text-centered">
                            <a href="#" class="button brand-single-page__sidebars__widget-reviews__submit-review"><?php esc_html_e( 'Submit your Review', 'vuelabs-framework' ); ?></a>
                        </div>

                        <form action="<?php echo get_home_url( '/' ) . '/wp-comments-post.php'; ?>" method="post" class="brand-single-page__sidebars__widget-reviews__form" id="commentform" novalidate>
                            <div class="field">
                                <label for="title" class="label"><?php esc_html_e( 'Title of your review', 'vuelabs-framework' ); ?></label>
                                <div class="control">
                                    <input type="text" class="input" id="review-title" name="title">
                                </div>
                            </div>
                            <div class="field">
                                <label for="comment" class="label"><?php esc_html_e( 'Your Review:', 'vuelabs-framework' ); ?></label>
                                <div class="control">
                                    <textarea class="textarea" name="comment" id="review-comment" cols="8" rows="45" aria-required="true" required placeholder="<?php esc_attr_e( 'Tell about your experience...', 'vuelabs-framework' ); ?>"></textarea>
                                </div>
                            </div>
                            <div class="field">
                                <label class="label"><?php esc_html_e( 'Your overall rating of this profile', 'vuelabs-framework' ); ?></label>
                                <div class="control brand-single-page__sidebars__widget-reviews__form__form-rating">
                                    <div class="brand-single-page__sidebars__widget-reviews__form__form-rating__target"></div>
                                    <input type="hidden" id="review-rating" name="rating" class="brand-single-page__sidebars__widget-reviews__form__rating" value="4">
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <button class="button is-fullwidth brand-single-page__sidebars__widget-reviews__form__button" data-brand-id="<?php echo esc_attr( $brand_id ); ?>" data-user-id="<?php echo esc_attr( get_current_user_id() ); ?>"><?php esc_html_e( 'Submit your Review', 'vuelabs-framework' ); ?></button>
                                </div>
                            </div>
                        </form>

                    <?php endif; ?>

                    <?php

                }

                ?>
            </div>

            <?php

            echo $args['after_widget'];

        }

        public function form( $instance ) {

            echo '<p>' . esc_html__( 'Add to brand sidebar.', 'vuelabs-framework' ) . '</p>';

        }

    }

endif;
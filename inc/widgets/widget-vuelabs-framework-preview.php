<?php
/**
 * Freshmes Brand About Class
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc/widgets
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {

    exit;

}

if ( ! class_exists( 'Freshmes_Brand_Preview' ) ) :

    class Freshmes_Brand_Preview extends WP_Widget
    {

        public function __construct() {

            parent::__construct(
                'vlf_preview', // Base ID
                '&#x1F536; ' . esc_html__( 'Freshmes Network', 'vuelabs-framework' ) . ' &raquo; ' . esc_html__( 'Preview', 'vuelabs-framework' ), // Name
                [] // Args
            );

        }

        public function widget( $args, $instance ) {

            global $post;
            echo $args['before_widget'];

            $brand_id  = $post->ID;
            $author_id = $post->post_author;

            if ( isset( $_GET['network'] ) && ! empty( $_GET['network'] ) ) {

                $brand_id = intval( $_GET['network'] );
                $author_id = get_post_field( 'post_author', $brand_id );

            }

            $author    = get_userdata( $author_id );
            $user      = new WP_User( $author_id );

            $categories = get_the_terms( $brand_id, 'brand_category' );
            if ( ! is_wp_error( $categories ) ) {

                $category = $categories[0]->name;

            }

            $business_name = get_field( 'brand_business_name', $brand_id );
            $business_name = vlf_check_field( $business_name );

            $verified      = get_field( 'brand_is_verified', $brand_id );
            $verified      = vlf_check_field( $verified );

            $logo          = get_field( 'brand_logo', $brand_id );
            $logo          = vlf_check_field( $logo );

            ?>

            <div class="widget-network-preview">
                <div class="columns is-vcentered">
                    <div class="column is-half">
                        <div class="widget-network-preview__top">
                            <?php if ( isset( $category ) && ! empty( $category ) ) : ?>
                                <div class="widget-network-preview__top__category"><?php echo esc_html( $category ); ?></div>
                            <?php endif; ?>
                            <?php vlf_show_brand_rating( $brand_id, '' ); ?>
                        </div>
                        <h1 class="title widget-network-preview__title">
                            <?php echo esc_html( $business_name ); ?>
                            <?php if ( $verified ) : ?>
                                <i class="fas fa-check-circle"></i>
                            <?php endif; ?>
                        </h1>
                    </div>
                    <div class="column is-half has-text-centered">
                        <?php echo wp_get_attachment_image( $logo, 'full' ); ?>
                    </div>
                </div>
            </div>

            <?php

            echo $args['after_widget'];

        }

        public function form( $instance ) {

            echo '<p>' . esc_html__( 'Add to brand sidebar.', 'vuelabs-framework' ) . '</p>';

        }

    }

endif;
<?php
/**
 * Freshmes Brand Posts Class
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc/widgets
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {

    exit;

}

if ( ! class_exists( 'Freshmes_Brand_Posts' ) ) :

    class Freshmes_Brand_Posts extends WP_Widget
    {

        public function __construct() {

            parent::__construct(
                'vlf_posts', // Base ID
                '&#x1F536; ' . esc_html__( 'Freshmes Brand', 'vuelabs-framework' ) . ' &raquo; ' . esc_html__( 'Posts', 'vuelabs-framework' ), // Name
                [] // Args
            );

        }

        public function widget( $args, $instance ) {

            global $post;
            echo $args['before_widget'];

            $brand_id  = $post->ID;
            $author_id = $post->post_author;
            $author    = get_userdata( $author_id );
            $user      = new WP_User( $author_id );

            $loop_args = [
                'post_type'      => 'post',
                'posts_per_page' => 4,
                'author'         => $post->post_author
            ];
            $loop = new WP_Query( $loop_args );

            if ( ! $loop->have_posts() ) {

                if ( is_user_logged_in() ) {

                    if ( intval( get_current_user_id() ) === intval( $post->post_author ) ) {

                        // TODO :: Trigger add post (opens navigator).

                        ?>

                        <div class="brand-single-page__sidebars__widget-posts brand-single-page__sidebars__widget-posts--is-empty has-text-centered">
                            <div class="brand-single-page__sidebars__widget-posts__title"><?php esc_html_e( 'Posts', 'vuelabs-framework' ); ?></div>
                            <div class="brand-single-page__sidebars__tabs">
                                <header class="brand-single-page__sidebars__tabs__header">
                                    <a href="#" class="brand-single-page__sidebars__tabs__header__item" data-tab="work"><?php esc_html_e( 'Work (0)', 'vuelabs-framework' ); ?></a>
                                    <a href="#" class="brand-single-page__sidebars__tabs__header__item" data-tab="life"><?php esc_html_e( 'Life (0)', 'vuelabs-framework' ); ?></a>
                                </header>
                                <div class="brand-single-page__sidebars__tabs__content" data-tab="work">
                                    <div class="columns">
                                        <div class="column">
                                            <div class="brand-single-page__sidebars__widget-posts__post-empty">
                                                <div class="brand-single-page__sidebars__widget-posts__post-empty__title"><?php esc_html_e( 'Add Post', 'vuelabs-framework' ); ?></div>
                                                <div class="brand-single-page__sidebars__widget-posts__post-empty__description"><?php esc_html_e( 'Easily create content to inspire, educate or simply showcase your chops to the community. This is a very powerful relationship and marketing tool.', 'vuelabs-framework' ); ?></div>
                                            </div>
                                        </div>
                                        <div class="column is-hidden-touch">
                                            <div class="brand-single-page__sidebars__widget-posts__post-empty brand-single-page__sidebars__widget-posts__post-empty--border">
                                                <div class="brand-single-page__sidebars__widget-posts__post-empty__icon">
                                                    <i class="fal fa-plus"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column is-hidden-touch">
                                            <div class="brand-single-page__sidebars__widget-posts__post-empty brand-single-page__sidebars__widget-posts__post-empty--border">
                                                <div class="brand-single-page__sidebars__widget-posts__post-empty__icon">
                                                    <i class="fal fa-plus"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column is-hidden-touch">
                                            <div class="brand-single-page__sidebars__widget-posts__post-empty brand-single-page__sidebars__widget-posts__post-empty--border">
                                                <div class="brand-single-page__sidebars__widget-posts__post-empty__icon">
                                                    <i class="fal fa-plus"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="brand-single-page__sidebars__tabs__content" data-tab="life">
                                    <div class="columns">
                                        <div class="column">
                                            <div class="brand-single-page__sidebars__widget-posts__post-empty brand-single-page__sidebars__widget-posts__post-empty--border">
                                                <div class="brand-single-page__sidebars__widget-posts__post-empty__title"><?php esc_html_e( 'Add Post', 'vuelabs-framework' ); ?></div>
                                                <div class="brand-single-page__sidebars__widget-posts__post-empty__description"><?php esc_html_e( 'Easily create content to inspire, educate or simply showcase your chops to the community. This is a very powerful relationship and marketing tool.', 'vuelabs-framework' ); ?></div>
                                            </div>
                                        </div>
                                        <div class="column is-hidden-touch">
                                            <div class="brand-single-page__sidebars__widget-posts__post-empty brand-single-page__sidebars__widget-posts__post-empty--border">
                                                <div class="brand-single-page__sidebars__widget-posts__post-empty__icon">
                                                    <i class="fal fa-plus"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column is-hidden-touch">
                                            <div class="brand-single-page__sidebars__widget-posts__post-empty brand-single-page__sidebars__widget-posts__post-empty--border">
                                                <div class="brand-single-page__sidebars__widget-posts__post-empty__icon">
                                                    <i class="fal fa-plus"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column is-hidden-touch">
                                            <div class="brand-single-page__sidebars__widget-posts__post-empty">
                                                <div class="brand-single-page__sidebars__widget-posts__post-empty__icon">
                                                    <i class="fal fa-plus"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="has-text-centered">
                                <a href="#" class="button brand-single-page__sidebars__widget-posts__add-post"><?php esc_html_e( 'Add Posts', 'vuelabs-framework' ); ?></a>
                            </div>
                        </div>

                        <?php

                    }

                }

            } else {

                // Group posts by category.
                $posts = [];

                while ( $loop->have_posts() ) : $loop->the_post();

                    $post_categories = get_the_category( get_the_ID() );
                    if ( isset( $post_categories ) && ! empty( $post_categories ) ) {

                        foreach ( $post_categories as $post_category ) {

                            if ( ! isset( $posts[ $post_category->slug ] ) ) {

                                $posts[ $post_category->slug ] = [];

                                array_push( $posts[ $post_category->slug ], get_the_ID() );

                            } else {

                                array_push( $posts[ $post_category->slug ], get_the_ID() );

                            }

                        }

                    }

                endwhile; wp_reset_postdata();

                // Set Counts
                $work_count = 0;
                $life_count = 0;
                if ( isset( $posts['work'] ) ) {

                    $work_count = count( $posts['work'] );

                }
                if ( isset( $posts['life'] ) ) {

                    $life_count = count( $posts['life'] );

                }

                ?>

                <div class="brand-single-page__sidebars__widget-posts">
                    <div class="brand-single-page__sidebars__widget-posts__title has-text-centered"><?php esc_html_e( 'Posts', 'vuelabs-framework' ); ?></div>
                    <div class="brand-single-page__sidebars__tabs">
                        <header class="brand-single-page__sidebars__tabs__header">
                            <?php if ( $work_count !== 0 ) : ?>
                                <a href="#" class="brand-single-page__sidebars__tabs__header__item" data-tab="work"><?php esc_html_e( 'Work (' . esc_html( $work_count ) . ')', 'vuelabs-framework' ); ?></a>
                            <?php endif; ?>
                            <?php if ( $life_count !== 0 ) : ?>
                                <a href="#" class="brand-single-page__sidebars__tabs__header__item" data-tab="life"><?php esc_html_e( 'Life (' . esc_html( $life_count ) . ')', 'vuelabs-framework' ); ?></a>
                            <?php endif; ?>
                        </header>
                        <?php if ( $work_count !== 0 ) : ?>
                            <div class="brand-single-page__sidebars__tabs__content" data-tab="work">
                                <div class="columns is-multiline is-centered">
                                    <?php
                                        if ( isset( $posts['work'] ) ) {

                                            foreach ( $posts['work'] as $post ) {

                                                echo '<div class="column is-3">';
                                                vlf_display_post( $post );
                                                echo '</div>';

                                            }

                                        }
                                    ?>
                                </div>
                                <?php
                                if ( $work_count === 4 ) {

                                    ?>

                                    <!--<div class="has-text-centered">
                                        <a href="#" class="button brand-single-page__sidebars__widget-posts__load-more" data-author="<?php /*echo esc_attr( $post->post_author ); */?>" data-category="work"><?php /*esc_html_e( 'Load More', 'vuelabs-framework' ); */?></a>
                                    </div>-->

                                    <?php

                                }
                                ?>
                            </div>
                        <?php endif; ?>
                        <?php if ( $life_count !== 0 ) : ?>
                            <div class="brand-single-page__sidebars__tabs__content" data-tab="life">
                                <div class="columns is-multiline is-centered">
                                    <?php
                                    if ( isset( $posts['life'] ) ) {

                                        foreach ( $posts['life'] as $post ) {

                                            echo '<div class="column is-3">';
                                            vlf_display_post( $post );
                                            echo '</div>';

                                        }

                                    }
                                    ?>
                                </div>
                                <?php
                                if ( $life_count === 4 ) {

                                    ?>

                                    <!--<div class="has-text-centered">
                                        <a href="#" class="button brand-single-page__sidebars__widget-posts__load-more" data-author="<?php /*echo esc_attr( $post->post_author ); */?>" data-category="life"><?php /*esc_html_e( 'Load More', 'vuelabs-framework' ); */?></a>
                                    </div>-->

                                    <?php

                                }
                                ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <?php

            }

            echo $args['after_widget'];

        }

        public function form( $instance ) {

            echo '<p>' . esc_html__( 'Add to brand sidebar.', 'vuelabs-framework' ) . '</p>';

        }

    }

endif;
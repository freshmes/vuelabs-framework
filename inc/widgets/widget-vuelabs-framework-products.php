<?php
/**
 * Freshmes Brand Products Class
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc/widgets
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {

    exit;

}

if ( ! class_exists( 'Freshmes_Brand_Products' ) ) :

    class Freshmes_Brand_Products extends WP_Widget
    {

        public function __construct() {

            parent::__construct(
                'vlf_products', // Base ID
                '&#x1F536; ' . esc_html__( 'Freshmes Brand', 'vuelabs-framework' ) . ' &raquo; ' . esc_html__( 'Products', 'vuelabs-framework' ), // Name
                [] // Args
            );

        }

        public function widget( $args, $instance ) {

            global $post;
            echo $args['before_widget'];

            $brand_id  = $post->ID;
            $author_id = $post->post_author;
            $author    = get_userdata( $author_id );
            $user      = new WP_User( $author_id );

            $loop_args = [
                'post_type'      => 'product',
                'posts_per_page' => 2,
                'author'         => $post->post_author
            ];
            $loop = new WP_Query( $loop_args );

            if ( ! $loop->have_posts() ) {

                if ( is_user_logged_in() ) {

                    if ( intval( get_current_user_id() ) === intval( $post->post_author ) ) {

                        // Check for permissions.
                        if ( in_array( 'freshmes_business', $user->roles ) || in_array( 'freshmes_organization', $user->roles ) ) {

                            ?>

                            <div class="brand-single-page__sidebars__widget-products brand-single-page__sidebars__widget-products--is-empty has-text-centered">
                                <div class="brand-single-page__sidebars__widget-products__icon">
                                    <i class="fal fa-plus"></i>
                                </div>
                                <div class="brand-single-page__sidebars__widget-products__title"><?php esc_html_e( 'Add Products', 'vuelabs-framework' ); ?></div>
                                <div class="brand-single-page__sidebars__widget-products__description"><?php esc_html_e( 'Effortlessly create your online store to sell anything from products, services, courses or anything else you can imagine. Once created it will be added to your products tab and distributed to the community if you choose. Now go ahead and sell, sell, sell!', 'vuelabs-framework' ); ?></div>
                                <a href="#" class="button brand-single-page__sidebars__widget-products__add-product"><?php esc_html_e( 'Add Products', 'vuelabs-framework' ); ?></a>
                            </div>

                            <?php

                        }

                    }

                }

            } else {

                ?>

                <div class="brand-single-page__sidebars__widget-products">
                    <div class="brand-single-page__sidebars__widget-products__title has-text-centered"><?php esc_html_e( 'Products', 'vuelabs-framework' ); ?></div>
                    <a href="#" class="brand-single-page__sidebars__widget-products__view-all"><?php esc_html_e( 'View All', 'vuelabs-framework' ); ?></a>
                    <div class="columns brand-single-page__sidebars__widget-products__content is-multiline">
                        <?php
                        while ( $loop->have_posts() ) : $loop->the_post();

                            echo '<div class="column is-6">';
                            vlf_display_product( get_the_ID(), 'brand-page' );
                            echo '</div>';

                        endwhile; wp_reset_postdata();
                        ?>
                    </div>
                    <?php

                    if ( intval( $loop->post_count ) >= 2 ) {

                        echo '<div class="has-text-centered"><a href="#" class="button brand-single-page__sidebars__widget-products__load-more" data-author="' . esc_attr( $post->post_author ) . '">' . esc_html__( 'Load More', 'vuelabs-framework' ) . '</a></div>';

                    }

                    ?>
                </div>

                <?php

            }

            echo $args['after_widget'];

        }

        public function form( $instance ) {

            echo '<p>' . esc_html__( 'Add to brand sidebar.', 'vuelabs-framework' ) . '</p>';

        }

    }

endif;
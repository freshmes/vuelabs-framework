<?php
/**
 * Freshmes Brand Offer Class
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc/widgets
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {

    exit;

}

if ( ! class_exists( 'Freshmes_Brand_Offer' ) ) :

    class Freshmes_Brand_Offer extends WP_Widget
    {

        public function __construct() {

            parent::__construct(
                'vlf_offer', // Base ID
                '&#x1F536; ' . esc_html__( 'Freshmes Brand', 'vuelabs-framework' ) . ' &raquo; ' . esc_html__( 'Offer', 'vuelabs-framework' ), // Name
                [] // Args
            );

        }

        public function widget( $args, $instance ) {

            global $post;
            echo $args['before_widget'];

            $brand_id = $post->ID;
            $author_id = $post->post_author;
            $user      = new WP_User( $author_id );

            $loop_args = [
                'post_type'      => 'product',
                'posts_per_page' => 1,
                'author'         => $author_id,
                'tax_query'      => [
                    [
                        'taxonomy' => 'product_type',
                        'field'    => 'slug',
                        'terms'    => 'simple'
                    ]
                ]
            ];
            $loop = new WP_Query( $loop_args );

            if ( ! $loop->have_posts() ) {

                if ( is_user_logged_in() ) {

                    if ( intval( $author_id ) === get_current_user_id() ) {

                        // Check for permissions.
                        if ( in_array( 'freshmes_business', $user->roles ) || in_array( 'freshmes_organization', $user->roles ) ) {

                            ?>

                            <div class="brand-single-page__sidebars__widget-offer brand-single-page__sidebars__widget-offer--is-empty has-text-centered">
                                <div class="brand-single-page__sidebars__widget-offer__icon">
                                    <i class="fal fa-plus"></i>
                                </div>
                                <div class="brand-single-page__sidebars__widget-offer__title"><?php esc_html_e( 'Add Special Offer', 'vuelabs-framework' ); ?></div>
                                <div class="brand-single-page__sidebars__widget-offer__description"><?php esc_html_e( 'Quickly add a special offer or deal to make this feature active. Once created it will be added to your products tab and distributed to the marketplace. Now go ahead and make yourself some money!', 'vuelabs-framework' ); ?></div>
                                <a href="#" class="button brand-single-page__sidebars__widget-offer__create-special-offer"><?php esc_html_e( 'Create Special Offer', 'vuelabs-framework' ); ?></a>
                            </div>

                            <?php

                        }

                    }

                }

            } else {

                while ( $loop->have_posts() ) : $loop->the_post();

                    $offer_id = get_the_ID();
                    echo '<div class="brand-single-page__sidebars__widget-offer-filled">';
                    echo '<div class="brand-single-page__sidebars__widget-offer-filled__title">' . esc_html__( 'Special Offers', 'vuelabs-framework' ) . '</div>';
                    vlf_display_product( $offer_id, 'brand-page' );
                    echo '</div>';

                endwhile; wp_reset_postdata();

            }

            echo $args['after_widget'];

        }

        public function form( $instance ) {

            echo '<p>' . esc_html__( 'Add to brand sidebar.', 'vuelabs-framework' ) . '</p>';

        }

    }

endif;
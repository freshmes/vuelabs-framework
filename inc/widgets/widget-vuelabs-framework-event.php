<?php
/**
 * Freshmes Brand Event Class
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc/widgets
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {

    exit;

}

if ( ! class_exists( 'Freshmes_Brand_Event' ) ) :

    class Freshmes_Brand_Event extends WP_Widget
    {

        public function __construct() {

            parent::__construct(
                'vlf_event', // Base ID
                '&#x1F536; ' . esc_html__( 'Freshmes Brand', 'vuelabs-framework' ) . ' &raquo; ' . esc_html__( 'Event', 'vuelabs-framework' ), // Name
                [] // Args
            );

        }

        public function widget( $args, $instance ) {

            global $post;
            echo $args['before_widget'];

            $brand_id = $post->ID;
            $author_id = $post->post_author;
            $user      = new WP_User( $author_id );

            $loop_args = [
                'post_type'      => 'product',
                'posts_per_page' => 1,
                'author'         => $author_id,
                'tax_query'      => [
                    [
                        'taxonomy' => 'product_type',
                        'field'    => 'slug',
                        'terms'    => 'vlf_event'
                    ]
                ]
            ];
            $loop = new WP_Query( $loop_args );

            if ( ! $loop->have_posts() ) {

                if ( is_user_logged_in() ) {

                    if ( intval( $author_id ) === get_current_user_id() ) {

                        // Check for permissions.
                        if ( in_array( 'freshmes_business', $user->roles ) || in_array( 'freshmes_organization', $user->roles ) ) {

                            ?>

                            <div class="brand-single-page__sidebars__widget-event brand-single-page__sidebars__widget-event--is-empty has-text-centered">
                                <div class="brand-single-page__sidebars__widget-event__icon">
                                    <i class="fal fa-plus"></i>
                                </div>
                                <div class="brand-single-page__sidebars__widget-event__title"><?php esc_html_e( 'Add Events', 'vuelabs-framework' ); ?></div>
                                <div class="brand-single-page__sidebars__widget-event__description"><?php esc_html_e( 'Easily schedule events, meet-ups or anything else you want members to attend both free and paid. Once created it will be added to your events tab and distributed to the community if you choose. Now go ahead and make things happen!', 'vuelabs-framework' ); ?></div>
                                <a href="#" class="button brand-single-page__sidebars__widget-event__add-events"><?php esc_html_e( 'Add Events', 'vuelabs-framework' ); ?></a>
                            </div>

                            <?php

                        }

                    }

                }

            } else {

                while ( $loop->have_posts() ) : $loop->the_post();

                    $event_id = get_the_ID();
                    $event    = wc_get_product( $event_id );

                    $location   = get_field( 'product_event_location', $event_id );
                    $address    = get_field( 'product_event_address', $event_id );
                    $start_date = get_field( 'product_start_date', $event_id );
                    $time       = get_field( 'product_time', $event_id );

                    ?>

                    <div class="brand-single-page__sidebars__widget-event-filled">
                        <div class="brand-single-page__sidebars__widget-event-filled__title"><?php esc_html_e( 'Featured Events', 'vuelabs-framework' ); ?></div>
                        <header class="brand-single-page__sidebars__widget-event-filled__header">
                            <div class="brand-single-page__sidebars__widget-event-filled__header__image">
                                <?php the_post_thumbnail( 'single-image' ); ?>
                            </div>
                        </header>
                        <div class="brand-single-page__sidebars__widget-event-filled__content">
                            <div class="brand-single-page__sidebars__widget-event-filled__content__title"><?php echo esc_html( $event->get_title() ); ?></div>
                            <div class="brand-single-page__sidebars__widget-event-filled__content__price"><?php echo $event->get_price_html(); ?></div>
                            <div class="brand-single-page__sidebars__widget-event-filled__content__description">
                                <?php echo apply_filters( 'the_content', $event->get_description() ); ?>
                                <p><?php echo esc_html__( 'Date: ', 'vuelabs-framework' ) . esc_html( $start_date ); ?></p>
                                <p><?php echo esc_html__( 'Time: ', 'vuelabs-framework' ) . esc_html( $time ); ?></p>
                                <p><?php echo esc_html( $location ); ?></p>
                                <p><?php echo esc_html( $address ); ?></p>
                            </div>
                            <div class="brand-single-page__sidebars__widget-event-filled__content__persons">
                                <div class="field">
                                    <label for="brand-event-persons" class="label"><?php esc_html_e( 'Persons:', 'vuelabs-framework' ); ?></label>
                                    <div class="control">
                                        <input type="number" class="input" value="1" min="1" step="1" id="brand-event-persons">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="brand-single-page__sidebars__widget-event-filled__footer has-text-centered">
                            <?php
                            printf(
                                '<a href="%s" class="%s" data-quantity="%s" data-product_id="%s" rel="nofollow">%s</a>',
                                esc_url( $event->add_to_cart_url() ),
                                'button brand-single-page__sidebars__widget-event-filled__footer__book-now product_type_vlf_event add_to_cart_button ajax_add_to_cart',
                                1,
                                esc_attr( $event_id ),
                                esc_html__( 'Book Now', 'vuelabs-framework' )
                            );
                            ?>
                        </div>
                    </div>

                <?php

                endwhile; wp_reset_postdata();

            }

            echo $args['after_widget'];

        }

        public function form( $instance ) {

            echo '<p>' . esc_html__( 'Add to brand sidebar.', 'vuelabs-framework' ) . '</p>';

        }

    }

endif;
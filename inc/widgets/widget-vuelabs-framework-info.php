<?php
/**
 * Freshmes Brand Info Class
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc/widgets
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {

    exit;

}

if ( ! class_exists( 'Freshmes_Brand_Info' ) ) :

    class Freshmes_Brand_Info extends WP_Widget
    {

        public function __construct() {

            parent::__construct(
                'vlf_info', // Base ID
                '&#x1F536; ' . esc_html__( 'Freshmes Brand', 'vuelabs-framework' ) . ' &raquo; ' . esc_html__( 'Info', 'vuelabs-framework' ), // Name
                [] // Args
            );

        }

        public function widget( $args, $instance ) {

            global $post;
            echo $args['before_widget'];

            $brand_id  = $post->ID;
            $author_id = $post->post_author;

            if ( isset( $_GET['network'] ) && ! empty( $_GET['network'] ) ) {

                $brand_id = intval( $_GET['network'] );
                $author_id = get_post_field( 'post_author', $brand_id );

            }

            $author    = get_userdata( $author_id );
            $user      = new WP_User( $author_id );

            $profile_image = get_field( 'brand_profile_image', $brand_id );
            $profile_image = vlf_check_field( $profile_image );

            $job_title     = get_field( 'brand_job_title', $brand_id );
            $job_title     = vlf_check_field( $job_title );

            $logo          = get_field( 'brand_logo', $brand_id );
            $logo          = vlf_check_field( $logo );

            $connect_class = 'button brand-single-page__sidebars__widget-info__profile-connect';
            $connect_label = esc_html__( 'Connect', 'vuelabs-framework' );
            $connect_label_original = $connect_label;

            if ( is_user_logged_in() ) {

                // Check if current user isn't post author.
                if ( get_current_user_id() !== intval( $author_id ) ) {

                    // Check if current user has this person as their connection.
                    if ( vlf_has_connection( get_current_user_id(), intval( $author_id ) ) === true ) {

                        $connect_class .= ' brand-single-page__sidebars__widget-info__profile-connect--is-connection';
                        $connect_label = esc_html__( 'Disconnect', 'vuelabs-framework' );

                    }

                } else {

                    $connect_class .= ' is-hidden';

                }

            } else {

                $connect_class .= ' need-account-prompt';

            }

            ?>

            <div class="brand-single-page__sidebars__widget-info">
                <div class="columns is-v-centered">
                    <div class="column is-3">
                        <?php
                        if ( $profile_image ) {

                            ?>

                            <div class="brand-single-page__sidebars__widget-info__profile-image">
                                <?php echo wp_get_attachment_image( $profile_image, 'thumbnail' ); ?>
                            </div>

                            <?php

                        }
                        ?>
                    </div>
                    <?php if ( $logo && in_array( 'freshmes_organization', $user->roles ) || in_array( 'freshmes_business', $user->roles ) ) : ?>
                        <div class="column is-5">
                            <div class="brand-single-page__sidebars__widget-info__profile-name"><?php echo esc_html( $author->first_name . ' ' . $author->last_name ); ?></div>
                            <?php if ( $job_title ) : ?>
                                <div class="brand-single-page__sidebars__widget-info__profile-job-title"><?php echo esc_html( $job_title ); ?></div>
                            <?php endif; ?>
                            <a href="#" class="<?php echo esc_attr( $connect_class ); ?>" data-original-label="<?php echo esc_attr( $connect_label_original ); ?>" data-user-a="<?php echo esc_attr( get_current_user_id() ); ?>" data-user-b="<?php echo esc_attr( $author_id ); ?>"><?php echo $connect_label; ?></a>
                        </div>
                        <div class="column is-4 has-text-right">
                            <div class="brand-single-page__sidebars__widget-info__profile-logo">
                                <?php echo wp_get_attachment_image( $logo, 'brand-logo' ); ?>
                            </div>
                        </div>
                    <?php else : ?>
                        <div class="column is-9 has-text-centered">
                            <div class="brand-single-page__sidebars__widget-info__profile-name"><?php echo esc_html( $author->first_name . ' ' . $author->last_name ); ?></div>
                            <?php if ( $job_title ) : ?>
                                <div class="brand-single-page__sidebars__widget-info__profile-job-title"><?php echo esc_html( $job_title ); ?></div>
                            <?php endif; ?>
                            <a href="#" class="button brand-single-page__sidebars__widget-info__profile-connect"><?php esc_html_e( 'Connect', 'vuelabs-framework' ); ?></a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <?php

            echo $args['after_widget'];

        }

        public function form( $instance ) {

            echo '<p>' . esc_html__( 'Add to brand sidebar.', 'vuelabs-framework' ) . '</p>';

        }

    }

endif;
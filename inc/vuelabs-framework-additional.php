<?php
/**
 * VueLabs Framework Additional.
 *
 * Used to create post types, custom taxonomies, etc.
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc
 * @since 1.0.0
 */

if ( ! function_exists('vlf_register_brand') ) {

    // Register Custom Post Type
    function vlf_register_brand() {

        $labels = [
            'name'                  => _x( 'Brands', 'Post Type General Name', 'vuelabs-framework' ),
            'singular_name'         => _x( 'Brand', 'Post Type Singular Name', 'vuelabs-framework' ),
            'menu_name'             => __( 'Brands', 'vuelabs-framework' ),
            'name_admin_bar'        => __( 'Brands', 'vuelabs-framework' ),
            'archives'              => __( 'Brand Archives', 'vuelabs-framework' ),
            'attributes'            => __( 'Brand Attributes', 'vuelabs-framework' ),
            'parent_item_colon'     => __( 'Parent Brand:', 'vuelabs-framework' ),
            'all_items'             => __( 'All Brands', 'vuelabs-framework' ),
            'add_new_item'          => __( 'Add New Brand', 'vuelabs-framework' ),
            'add_new'               => __( 'Add New', 'vuelabs-framework' ),
            'new_item'              => __( 'New Brand', 'vuelabs-framework' ),
            'edit_item'             => __( 'Edit Brand', 'vuelabs-framework' ),
            'update_item'           => __( 'Update Brand', 'vuelabs-framework' ),
            'view_item'             => __( 'View Brand', 'vuelabs-framework' ),
            'view_items'            => __( 'View Brands', 'vuelabs-framework' ),
            'search_items'          => __( 'Search Brand', 'vuelabs-framework' ),
            'not_found'             => __( 'Not found', 'vuelabs-framework' ),
            'not_found_in_trash'    => __( 'Not found in Trash', 'vuelabs-framework' ),
            'featured_image'        => __( 'Featured Image', 'vuelabs-framework' ),
            'set_featured_image'    => __( 'Set featured image', 'vuelabs-framework' ),
            'remove_featured_image' => __( 'Remove featured image', 'vuelabs-framework' ),
            'use_featured_image'    => __( 'Use as featured image', 'vuelabs-framework' ),
            'insert_into_item'      => __( 'Insert into brand', 'vuelabs-framework' ),
            'uploaded_to_this_item' => __( 'Uploaded to this brand', 'vuelabs-framework' ),
            'items_list'            => __( 'Brands list', 'vuelabs-framework' ),
            'items_list_navigation' => __( 'Brands list navigation', 'vuelabs-framework' ),
            'filter_items_list'     => __( 'Filter brands list', 'vuelabs-framework' ),
        ];
        $args = [
            'label'                 => __( 'Brand', 'vuelabs-framework' ),
            'description'           => __( 'Brands', 'vuelabs-framework' ),
            'labels'                => $labels,
            'supports'              => [ 'title', 'thumbnail', 'comments', 'author' ],
            'taxonomies'            => [ 'brand_category', ' brand_tag', 'brand_location', ' brand_organization' ],
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 20,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',
            'show_in_rest'          => true,
        ];
        register_post_type( 'brand', $args );

    }
    add_action( 'init', 'vlf_register_brand', 0 );

}

if ( ! function_exists( 'vlf_register_brand_category' ) ) {

    // Register Custom Taxonomy
    function vlf_register_brand_category() {

        $labels = array(
            'name'                       => _x( 'Categories', 'Taxonomy General Name', 'vuelabs-framework' ),
            'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'vuelabs-framework' ),
            'menu_name'                  => __( 'Categories', 'vuelabs-framework' ),
            'all_items'                  => __( 'All Categories', 'vuelabs-framework' ),
            'parent_item'                => __( 'Parent Category', 'vuelabs-framework' ),
            'parent_item_colon'          => __( 'Parent Category:', 'vuelabs-framework' ),
            'new_item_name'              => __( 'New Category Name', 'vuelabs-framework' ),
            'add_new_item'               => __( 'Add New Category', 'vuelabs-framework' ),
            'edit_item'                  => __( 'Edit Category', 'vuelabs-framework' ),
            'update_item'                => __( 'Update Category', 'vuelabs-framework' ),
            'view_item'                  => __( 'View Category', 'vuelabs-framework' ),
            'separate_items_with_commas' => __( 'Separate categories with commas', 'vuelabs-framework' ),
            'add_or_remove_items'        => __( 'Add or remove categories', 'vuelabs-framework' ),
            'choose_from_most_used'      => __( 'Choose from the most used', 'vuelabs-framework' ),
            'popular_items'              => __( 'Popular Categories', 'vuelabs-framework' ),
            'search_items'               => __( 'Search Categories', 'vuelabs-framework' ),
            'not_found'                  => __( 'Not Found', 'vuelabs-framework' ),
            'no_terms'                   => __( 'No categories', 'vuelabs-framework' ),
            'items_list'                 => __( 'Categories list', 'vuelabs-framework' ),
            'items_list_navigation'      => __( 'Categories list navigation', 'vuelabs-framework' ),
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => false,
            'show_tagcloud'              => false,
            'show_in_rest'               => true,
        );
        register_taxonomy( 'brand_category', array( 'brand' ), $args );

    }
    add_action( 'init', 'vlf_register_brand_category', 0 );

}

if ( ! function_exists( 'vlf_register_brand_organization' ) ) {

// Register Custom Taxonomy
    function vlf_register_brand_organization() {

        $labels = array(
            'name'                       => _x( 'Organizations', 'Taxonomy General Name', 'vuelabs-framework' ),
            'singular_name'              => _x( 'Organization', 'Taxonomy Singular Name', 'vuelabs-framework' ),
            'menu_name'                  => __( 'Organizations', 'vuelabs-framework' ),
            'all_items'                  => __( 'All Organizations', 'vuelabs-framework' ),
            'parent_item'                => __( 'Parent Organization', 'vuelabs-framework' ),
            'parent_item_colon'          => __( 'Parent Organization:', 'vuelabs-framework' ),
            'new_item_name'              => __( 'New Organization Name', 'vuelabs-framework' ),
            'add_new_item'               => __( 'Add New Organization', 'vuelabs-framework' ),
            'edit_item'                  => __( 'Edit Organization', 'vuelabs-framework' ),
            'update_item'                => __( 'Update Organization', 'vuelabs-framework' ),
            'view_item'                  => __( 'View Organization', 'vuelabs-framework' ),
            'separate_items_with_commas' => __( 'Separate organizations with commas', 'vuelabs-framework' ),
            'add_or_remove_items'        => __( 'Add or remove organizations', 'vuelabs-framework' ),
            'choose_from_most_used'      => __( 'Choose from the most used', 'vuelabs-framework' ),
            'popular_items'              => __( 'Popular Organizations', 'vuelabs-framework' ),
            'search_items'               => __( 'Search Organizations', 'vuelabs-framework' ),
            'not_found'                  => __( 'Not Found', 'vuelabs-framework' ),
            'no_terms'                   => __( 'No organizations', 'vuelabs-framework' ),
            'items_list'                 => __( 'Organizations list', 'vuelabs-framework' ),
            'items_list_navigation'      => __( 'Organizations list navigation', 'vuelabs-framework' ),
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => false,
            'show_tagcloud'              => false,
            'show_in_rest'               => true,
        );
        register_taxonomy( 'brand_organization', array( 'brand', 'post' ), $args );

    }
    add_action( 'init', 'vlf_register_brand_organization', 0 );

}

if ( ! function_exists( 'vlf_register_brand_location' ) ) {

    // Register Custom Taxonomy
    function vlf_register_brand_location() {

        $labels = array(
            'name'                       => _x( 'Locations', 'Taxonomy General Name', 'vuelabs-framework' ),
            'singular_name'              => _x( 'Location', 'Taxonomy Singular Name', 'vuelabs-framework' ),
            'menu_name'                  => __( 'Locations', 'vuelabs-framework' ),
            'all_items'                  => __( 'All Locations', 'vuelabs-framework' ),
            'parent_item'                => __( 'Parent Location', 'vuelabs-framework' ),
            'parent_item_colon'          => __( 'Parent Location:', 'vuelabs-framework' ),
            'new_item_name'              => __( 'New Location Name', 'vuelabs-framework' ),
            'add_new_item'               => __( 'Add New Location', 'vuelabs-framework' ),
            'edit_item'                  => __( 'Edit Location', 'vuelabs-framework' ),
            'update_item'                => __( 'Update Location', 'vuelabs-framework' ),
            'view_item'                  => __( 'View Location', 'vuelabs-framework' ),
            'separate_items_with_commas' => __( 'Separate locations with commas', 'vuelabs-framework' ),
            'add_or_remove_items'        => __( 'Add or remove locations', 'vuelabs-framework' ),
            'choose_from_most_used'      => __( 'Choose from the most used', 'vuelabs-framework' ),
            'popular_items'              => __( 'Popular Locations', 'vuelabs-framework' ),
            'search_items'               => __( 'Search Locations', 'vuelabs-framework' ),
            'not_found'                  => __( 'Not Found', 'vuelabs-framework' ),
            'no_terms'                   => __( 'No locations', 'vuelabs-framework' ),
            'items_list'                 => __( 'Locations list', 'vuelabs-framework' ),
            'items_list_navigation'      => __( 'Locations list navigation', 'vuelabs-framework' ),
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => false,
            'show_tagcloud'              => false,
            'show_in_rest'               => true,
        );
        register_taxonomy( 'brand_location', array( 'brand' ), $args );

    }
    add_action( 'init', 'vlf_register_brand_location', 0 );

}

if ( ! function_exists( 'vlf_register_brand_organization' ) ) {

    // Register Custom Taxonomy
    function vlf_register_brand_organization() {

        $labels = array(
            'name'                       => _x( 'Organizations', 'Taxonomy General Name', 'vuelabs-framework' ),
            'singular_name'              => _x( 'Organization', 'Taxonomy Singular Name', 'vuelabs-framework' ),
            'menu_name'                  => __( 'Organizations', 'vuelabs-framework' ),
            'all_items'                  => __( 'All Organizations', 'vuelabs-framework' ),
            'parent_item'                => __( 'Parent Organization', 'vuelabs-framework' ),
            'parent_item_colon'          => __( 'Parent Organization:', 'vuelabs-framework' ),
            'new_item_name'              => __( 'New Organization Name', 'vuelabs-framework' ),
            'add_new_item'               => __( 'Add New Organization', 'vuelabs-framework' ),
            'edit_item'                  => __( 'Edit Organization', 'vuelabs-framework' ),
            'update_item'                => __( 'Update Organization', 'vuelabs-framework' ),
            'view_item'                  => __( 'View Organization', 'vuelabs-framework' ),
            'separate_items_with_commas' => __( 'Separate organizations with commas', 'vuelabs-framework' ),
            'add_or_remove_items'        => __( 'Add or remove organizations', 'vuelabs-framework' ),
            'choose_from_most_used'      => __( 'Choose from the most used', 'vuelabs-framework' ),
            'popular_items'              => __( 'Popular Organizations', 'vuelabs-framework' ),
            'search_items'               => __( 'Search Organizations', 'vuelabs-framework' ),
            'not_found'                  => __( 'Not Found', 'vuelabs-framework' ),
            'no_terms'                   => __( 'No organizations', 'vuelabs-framework' ),
            'items_list'                 => __( 'Organizations list', 'vuelabs-framework' ),
            'items_list_navigation'      => __( 'Organizations list navigation', 'vuelabs-framework' ),
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => false,
            'show_tagcloud'              => false,
            'show_in_rest'               => true,
        );
        register_taxonomy( 'brand_organization', array( 'brand' ), $args );

    }
    add_action( 'init', 'vlf_register_brand_organization', 0 );

}

if ( ! function_exists( 'vlf_register_brand_tag' ) ) {

    // Register Custom Taxonomy
    function vlf_register_brand_tag() {

        $labels = array(
            'name'                       => _x( 'Tags', 'Taxonomy General Name', 'vuelabs-framework' ),
            'singular_name'              => _x( 'Tag', 'Taxonomy Singular Name', 'vuelabs-framework' ),
            'menu_name'                  => __( 'Tags', 'vuelabs-framework' ),
            'all_items'                  => __( 'All Tags', 'vuelabs-framework' ),
            'parent_item'                => __( 'Parent Tag', 'vuelabs-framework' ),
            'parent_item_colon'          => __( 'Parent Tag:', 'vuelabs-framework' ),
            'new_item_name'              => __( 'New Tag Name', 'vuelabs-framework' ),
            'add_new_item'               => __( 'Add New Tag', 'vuelabs-framework' ),
            'edit_item'                  => __( 'Edit Tag', 'vuelabs-framework' ),
            'update_item'                => __( 'Update Tag', 'vuelabs-framework' ),
            'view_item'                  => __( 'View Tag', 'vuelabs-framework' ),
            'separate_items_with_commas' => __( 'Separate tags with commas', 'vuelabs-framework' ),
            'add_or_remove_items'        => __( 'Add or remove tags', 'vuelabs-framework' ),
            'choose_from_most_used'      => __( 'Choose from the most used', 'vuelabs-framework' ),
            'popular_items'              => __( 'Popular Tags', 'vuelabs-framework' ),
            'search_items'               => __( 'Search Tags', 'vuelabs-framework' ),
            'not_found'                  => __( 'Not Found', 'vuelabs-framework' ),
            'no_terms'                   => __( 'No tags', 'vuelabs-framework' ),
            'items_list'                 => __( 'Tags list', 'vuelabs-framework' ),
            'items_list_navigation'      => __( 'Tags list navigation', 'vuelabs-framework' ),
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => false,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => false,
            'show_tagcloud'              => true,
            'show_in_rest'               => true,
        );
        register_taxonomy( 'brand_tag', array( 'brand' ), $args );

    }
    add_action( 'init', 'vlf_register_brand_tag', 0 );

}
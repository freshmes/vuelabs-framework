<?php
/**
 * VueLabs Framework Products.
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc
 * @since 1.0.0
 */

if ( ! function_exists( 'vlf_display_product' ) ) {

    function vlf_display_product( $product_id, $product_type, $shop_page = false, $show_featured = false, $featured = '' )
    {

        $product = wc_get_product( $product_id );
        $product_author_id = get_post_field( 'post_author', $product_id );
        $product_author_data = get_userdata( $product_author_id );

        $product_author_image = get_field( 'brand_profile_image', 'user_' . $product_author_id );
        $product_author_image = vlf_check_field( $product_author_image );

        $product_by_prefix = esc_html__( 'Product by ', 'vuelabs-framework' );
        $product_button_label = esc_html__( 'Purchase', 'vuelabs-framework' );
        $product_button_class = 'button freshmes-product__footer__button product_type_simple add_to_cart_button ajax_add_to_cart';
        if ( $product->is_on_sale() ) {

            $product_by_prefix = esc_html__( 'Deal by ', 'vuelabs-framework' );
            $product_button_label = esc_html__( 'Purchase Deal', 'vuelabs-framework' );

        }
        $add_class = '';
        if ( $product->get_type() === 'vlf_event' ) {

            $add_class = ' is-event';

            $product_by_prefix = esc_html__( 'Event by ', 'vuelabs-framework' );
            $product_button_label = esc_html__( 'Sign Up', 'vuelabs-framework' );
            $product_button_class = 'button freshmes-product__footer__button product_type_vlf_event add_to_cart_button ajax_add_to_cart';

            $product_date = get_field( 'product_start_date', $product_id );
            $product_date = new DateTime( $product_date );
            $product_date_day   = $product_date->format( 'd' );
            $product_date_month = $product_date->format( 'M' );
            $product_date_year  = $product_date->format( 'Y' );

        }

        if ( is_singular( 'brand' ) ) {

            $image_size = 'single-image';

        } else {

            $image_size = 'product-image';

        }

        if ( $shop_page === true ) {

            $image_size = 'single-image';

        }

        ?>

        <article id="product-<?php echo esc_attr( $product_id ); ?>" class="<?php echo esc_attr( join( ' ', wc_get_product_class( 'freshmes-product' . $add_class, $product_id ) ) ); ?>">
            <header class="freshmes-product__header">
                <?php
                if ( has_post_thumbnail( $product_id ) ) {

                    echo '<div class="freshmes-product__header__image">';
                    echo get_the_post_thumbnail( $product_id, $image_size );
                    echo '</div>';

                }
                ?>
                <?php if ( $show_featured === true ) : ?>
                    <div class="freshmes-product__header__featured"><?php echo esc_html( $featured ); ?></div>
                <?php endif; ?>
                <a href="<?php echo esc_url( get_permalink( vlf_get_brand_id_from_author_id( $product_author_id ) ) ); ?>" class="freshmes-product__header__author">
                    <div class="freshmes-product__header__author__image">
                        <?php echo wp_get_attachment_image( $product_author_image, 'thumbnail' ); ?>
                    </div>
                    <div class="freshmes-product__header__author__by"><?php echo $product_by_prefix . $product_author_data->first_name . ' ' . $product_author_data->last_name; ?></div>
                </a>
            </header>
            <div class="freshmes-product__content">
                <?php
                if ( $product->get_type() === 'vlf_event' ) {

                    ?>

                    <div class="freshmes-product__content__location">
                        <i class="fal fa-map-marker-alt"></i>
                        <span><?php the_field( 'product_event_location' ); ?></span>
                    </div>

                    <div class="freshmes-product__content__date">
                        <div class="dp-table">
                            <div class="dp-table-cell">
                                <div class="freshmes-product__content__date__day"><?php echo esc_html( $product_date_day ); ?></div>
                            </div>
                            <div class="dp-table-cell">
                                <div class="freshmes-product__content__date__month"><?php echo esc_html( $product_date_month ); ?></div>
                                <div class="freshmes-product__content__date__year"><?php echo esc_html( $product_date_year ); ?></div>
                            </div>
                        </div>
                    </div>

                    <?php

                }
                ?>
                <h2 class="freshmes-product__content__title"><?php echo esc_html( $product->get_title() ); ?></h2>
                <div class="freshmes-product__content__price">
                    <?php echo $product->get_price_html(); ?>
                </div>
                <?php if ( is_singular( 'brand' ) ) : ?>
                    <div class="freshmes-product__content__description">
                        <?php echo apply_filters( 'the_content', get_post_field( 'post_content', $product_id ) ); ?>
                    </div>
                <?php endif; ?>
            </div>
            <footer class="freshmes-product__footer">
                <div class="columns is-v-centered">
                    <div class="column has-text-left">
                        <?php
                            switch ( $product_type ) {

                                case 'brand-page':
                                    printf(
                                        '<a href="%s" class="%s" data-quantity="%s" data-product_id="%s" rel="nofollow">%s</a>',
                                        esc_url( $product->add_to_cart_url() ),
                                        esc_attr( $product_button_class ),
                                        1,
                                        esc_attr( $product_id ),
                                        $product_button_label
                                    );
                                    break;
                                case 'preview-page':
                                    if ( $product->get_type() === 'vlf_event' ) {
                                        printf(
                                            '<a href="%s" class="%s">%s</a>',
                                            esc_url( $product->get_permalink() ),
                                            esc_attr( 'button freshmes-product__footer__button product_type_vlf_event' ),
                                            esc_html__( 'View Event', 'vuelabs-framework' )
                                        );
                                    } else {
                                        if ( $product->is_on_sale() ) {
                                            printf(
                                                '<a href="%s" class="%s">%s</a>',
                                                esc_url( $product->get_permalink() ),
                                                esc_attr( 'button freshmes-product__footer__button product_type_simple' ),
                                                esc_html__( 'View Deal', 'vuelabs-framework' )
                                            );
                                        } else {
                                            printf(
                                                '<a href="%s" class="%s">%s</a>',
                                                esc_url( $product->get_permalink() ),
                                                esc_attr( 'button freshmes-product__footer__button product_type_simple' ),
                                                esc_html__( 'View Product', 'vuelabs-framework' )
                                            );
                                        }
                                    }
                                    break;

                            }
                        ?>
                    </div>
                    <div class="column has-text-right">
                        <?php
                        switch ( $product_type ) {

                            case 'brand-page':
                                ?>

                                <a href="<?php echo esc_url( get_permalink( $product_id ) ); ?>" class="freshmes-product__footer__view" data-product-id="<?php echo esc_attr( $product_id ); ?>">
                                    <span class="freshmes-product__footer__view__icon"><i class="fal fa-eye"></i></span>
                                    <span class="freshmes-product__footer__view__label"><?php esc_html_e( 'View', 'vuelabs-framework' ); ?></span>
                                </a>

                                <?php
                                break;
                            case 'preview-page':
                                ?>

                                <div class="freshmes-product__footer__meta">
                                    <a href="<?php echo esc_url( get_permalink( vlf_get_brand_id_from_author_id( $product_author_id ) ) ); ?>" class="freshmes-product__footer__meta__item freshmes-product__footer__meta__item--profile">
                                        <i class="fal fa-user-circle"></i>
                                    </a>
                                    <a href="#" class="freshmes-product__footer__meta__item freshmes-product__footer__meta__item--share">
                                        <i class="fal fa-share"></i>
                                    </a>
                                </div>

                                <?php
                                break;

                        }
                        ?>
                    </div>
                </div>
            </footer>
        </article>

        <?php

    }

}
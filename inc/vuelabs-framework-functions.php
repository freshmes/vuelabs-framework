<?php
/**
 * VueLabs Framework Functions.
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc
 * @since 1.0.0
 */

if ( ! function_exists( 'vlf_check_nonce' ) ) {

    /**
     * Checks nonce used in ajax requests.
     *
     * @param $gump   New init of gump class.
     * @param $post   HTTP $_POST.
     * @param $action Nonce action.
     *
     * @return array If status is false, return a message, otherwise return sanitized $_POST.
     */
    function vlf_check_nonce( $gump, $post, $action )
    {

        $_POST = $post;

        if ( isset( $_POST['nonce'] ) && ! empty( $_POST['nonce'] ) ) {

            if ( ! wp_verify_nonce( $_POST['nonce'], $action ) ) {

                $return = [
                    'status'  => false,
                    'message' => esc_html__( 'Nonce check has failed.', 'vuelabs-framework' )
                ];

                return $return;

            } else {

                $return = [
                    'status' => true,
                    'data'   => $_POST
                ];

                return $return;

            }

        }

        $return = [
            'status'  => false,
            'message' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
        ];

        return $return;

    }

}

if ( ! function_exists( 'vlf_check_field' ) ) {

    /**
     * Checks if our field is set and if it's not empty.
     *
     * @param $field Field that we want to check.
     *
     * @return mixed Returns field date or false.
     */
    function vlf_check_field( $field )
    {

        if ( isset( $field ) && ! empty( $field ) ) {

            return $field;

        } else {

            return false;

        }

    }

}

if ( ! function_exists( 'vlf_replace_variable_curly' ) ) {

    /**
     * Replace {{variable}} with a value.
     *
     * @param $string
     * @param $find
     * @param $replace
     * @return string
     */
    function vlf_replace_variable_curly( $string, $find, $replace ) {

        if ( preg_match( "/[a-zA-Z\_]+/", $find ) ) {

            return (string) preg_replace( "/\{\{(\s+)?($find)(\s+)?\}\}/", $replace, $string );

        } else {

            return $string;

        }

    }

}

if ( ! function_exists( 'vlf_upload_image' ) ) {

    /**
     * Upload an image and set it to our post.
     *
     * @param        $image
     * @param        $post_id
     * @param        $field
     * @param string $featured
     * @param string $gallery
     *
     * @return mixed
     */
    function vlf_upload_image( $image, $post_id, $field, $featured = 'no', $gallery = 'no' ) {

        // Include these dependencies so we can actually upload the image.
        if ( ! function_exists( 'wp_handle_upload' ) ) {

            require_once( ABSPATH . 'wp-admin/includes/file.php' );

        }
        require_once( ABSPATH . 'wp-admin/includes/image.php' );

        $move_file = wp_handle_upload( $image, [ 'test_form' => false ] );

        if ( $move_file && ! isset( $move_file['error'] ) ) {

            $filename = $move_file['file'];
            $attachment = array(
                'post_mime_type' => $move_file['type'],
                'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
                'post_content' => '',
                'post_status' => 'inherit',
                'guid' => $move_file['url']
            );
            if ( isset( $post_id ) && ! empty( $post_id ) ) {

                $image_id = wp_insert_attachment( $attachment, $filename, $post_id );

            } else {

                $image_id = wp_insert_attachment( $attachment, $filename );

            }
            $attachment_data = wp_generate_attachment_metadata( $image_id, $filename );
            wp_update_attachment_metadata( $image_id, $attachment_data );

            if ( 0 < intval( $image_id ) ) {

                if ( $gallery === 'yes' ) {

                    return $image_id;

                } else {

                    if ( $featured === 'yes' ) {

                        set_post_thumbnail( $post_id, $image_id );

                    } else {

                        update_field( $field, $image_id, $post_id );

                    }

                }

            }

        }

    }

}

if ( ! function_exists( 'vlf_get_brand_rating' ) ) {

    /**
     * Get and return brand rating.
     *
     * @param $brand_id
     *
     * @return array|bool
     */
    function vlf_get_brand_rating( $brand_id ) {

        $comments_args = [
            'status'    => 'approve',
            'post_type' => 'brand',
            'post_id'   => $brand_id
        ];
        $comments_query = new WP_Comment_Query;
        $comments = $comments_query->query( $comments_args );

        if ( isset( $comments ) && ! empty( $comments ) ) {

            $comments_number = 0;
            $comments_rating = 0;

            foreach ( $comments as $comment ) {

                $rating = get_field( 'comment_rating', $comment );
                $rating = vlf_check_field( $rating );

                if ( $rating ) {

                    $comments_rating += intval( $rating );
                    $comments_number++;

                }

            }

            return [
                'ratings' => $comments_number,
                'rating'  => round( $comments_rating / $comments_number )
            ];

        }

        return false;

    }

}

if ( ! function_exists( 'vlf_show_brand_rating' ) ) {

    /**
     * Display our rating.
     *
     * @param $brand_id
     * @param $ratings
     */
    function vlf_show_brand_rating( $brand_id, $ratings ) {

        if ( ! empty( $ratings ) ) {

            $rating = $ratings;

        } else {

            $rating = vlf_get_brand_rating( $brand_id );

        }

        if ( $rating ) {

            $rating_good = $rating['rating'];
            $rating_bad  = 5 - $rating_good;

            echo '<span class="rating-stars">';
            for ( $i = 1; $i <= $rating_good; $i++ ) {

                echo '<i class="fas fa-star"></i>';

            }

            for ( $i = 1; $i <= $rating_bad; $i++ ) {

                echo '<i class="fal fa-star"></i>';

            }
            echo '</span>';
            echo '<span class="rating-number">(' . esc_html( $rating['ratings'] ) . ')</span>';

        }

    }

}

if ( ! function_exists( 'vlf_get_brand_id_from_author_id' ) ) {

    function vlf_get_brand_id_from_author_id( $author_id ) {

        $brand_id = get_transient( 'vlf_brand_id_' . $author_id );
        if ( isset( $brand_id ) && ! empty( $brand_id ) ) {

            return $brand_id;

        } else {

            $args = [
                'post_type'      => 'brand',
                'posts_per_page' => 1,
                'author'         => $author_id,
                'post_status'    => 'any'
            ];
            $loop = new WP_Query( $args );
            if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();

                set_transient( 'vlf_brand_id_' . $author_id, get_the_ID() );

            endwhile; wp_reset_postdata(); endif;

        }

    }

}

if ( ! function_exists( 'vlf_get_discount_number' ) ) {

    /**
     * @param $regular_price
     * @param $discount_price
     *
     * @return float|int
     */
    function vlf_get_discount_number( $regular_price, $discount_price )
    {

        return wc_format_decimal( ( ( $regular_price - $discount_price ) * 100 ) / $regular_price, 2 );

    }

}

if ( ! function_exists( 'vlf_set_discount_price' ) ) {

    /**
     * @param $regular_price
     * @param $discount_value
     *
     * @return string
     */
    function vlf_set_discount_price( $regular_price, $discount_value )
    {

        return wc_format_decimal( ( $regular_price - ( $regular_price * ( $discount_value / 100 ) ) ), 2 );

    }

}

if ( ! function_exists( 'vlf_get_network_authors' ) ) {

    function vlf_get_network_authors()
    {

        $user_query = new WP_User_Query( [
            'role'   => 'freshmes_organization',
            'fields' => 'ID'
        ] );

        if ( ! empty( $user_query->get_results() ) ) {

            return $user_query->get_results();

        }

        return null;

    }

}

if ( ! function_exists( 'vlf_get_excerpt' ) ) {

    function vlf_get_excerpt( $string )
    {

        if ( isset( $string ) && ! empty( $string ) ) {

            $string = wordwrap( $string[0]['content'], '120' );
            $string = explode( "\n", $string );
            $string = $string[0] . '...';

            return $string;

        }

    }

}

if ( ! function_exists( 'vlf_comment' ) ) {
    /**
     * VueLabs Framework comment template
     *
     * @param array $comment the comment array.
     * @param array $args the comment args.
     * @param int   $depth the comment depth.
     * @since 1.0.0
     */
    function vlf_comment( $comment, $args, $depth ) {
        if ( 'div' === $args['style'] ) {
            $tag       = 'div';
            $add_below = 'comment';
        } else {
            $tag       = 'li';
            $add_below = 'div-comment';
        }
        ?>
        <<?php echo esc_attr( $tag ); ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?> id="comment-<?php comment_ID(); ?>">
        <div class="comment-body columns is-multiline">
        <div class="comment-meta commentmetadata column is-2">
            <div class="comment-author vcard">
                <?php echo get_avatar( $comment, 128 ); ?>
                <?php printf( wp_kses_post( '<cite class="fn">%s</cite>', 'vuelabs-framework' ), get_comment_author_link() ); ?>
            </div>
            <?php if ( '0' === $comment->comment_approved ) : ?>
                <em class="comment-awaiting-moderation"><?php esc_attr_e( 'Your comment is awaiting moderation.', 'vuelabs-framework' ); ?></em>
                <br />
            <?php endif; ?>

            <a href="<?php echo esc_url( htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ); ?>" class="comment-date">
                <?php echo '<time datetime="' . get_comment_date( 'c' ) . '">' . get_comment_date() . '</time>'; ?>
            </a>
        </div>
        <?php if ( 'div' !== $args['style'] ) : ?>
        <div id="div-comment-<?php comment_ID(); ?>" class="comment-content column is-10">
    <?php endif; ?>
        <div class="comment-text">
            <?php comment_text(); ?>
        </div>
        <div class="reply">
            <?php
            comment_reply_link(
                array_merge(
                    $args, array(
                        'add_below' => $add_below,
                        'depth'     => $depth,
                        'max_depth' => $args['max_depth'],
                    )
                )
            );
            ?>
            <?php edit_comment_link( __( 'Edit', 'vlf' ), '  ', '' ); ?>
        </div>
        </div>
        <?php if ( 'div' !== $args['style'] ) : ?>
            </div>
        <?php endif; ?>
        <?php
    }
}

if ( ! function_exists( 'vlf_is_blog' ) ) {

    function vlf_is_blog()
    {

        return ( is_archive() || is_author() || is_category() || is_home() || is_single() || is_tag()) && 'post' == get_post_type();

    }

}
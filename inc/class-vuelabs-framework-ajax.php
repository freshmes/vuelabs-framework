<?php
/**
 * VueLabs Framework AJAX Class
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework/inc
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {

    exit;

}

if ( ! class_exists( 'VueLabs_Framework_AJAX' ) ) :

    class VueLabs_Framework_AJAX
    {

        /**
         * Variables
         */
        private $gump;

        /**
         * Setup class.
         *
         * @since 1.0.0
         */
        public function __construct()
        {

            $this->gump = new GUMP();

            // Register user.
            add_action( 'wp_ajax_vlf_register_user', [ $this, 'vlf_register_user' ] );
            add_action( 'wp_ajax_nopriv_vlf_register_user', [ $this, 'vlf_register_user' ] );

            // Onboard our user.
            add_action( 'wp_ajax_vlf_onboard_user_1', [ $this, 'vlf_onboard_user_1' ] );
            add_action( 'wp_ajax_nopriv_vlf_onboard_user_1', [ $this, 'vlf_onboard_user_1' ] );
            add_action( 'wp_ajax_vlf_onboard_user_2', [ $this, 'vlf_onboard_user_2' ] );
            add_action( 'wp_ajax_nopriv_vlf_onboard_user_2', [ $this, 'vlf_onboard_user_2' ] );
            add_action( 'wp_ajax_vlf_onboard_user_3', [ $this, 'vlf_onboard_user_3' ] );
            add_action( 'wp_ajax_nopriv_vlf_onboard_user_3', [ $this, 'vlf_onboard_user_3' ] );
            add_action( 'wp_ajax_vlf_onboard_user_4', [ $this, 'vlf_onboard_user_4' ] );
            add_action( 'wp_ajax_nopriv_vlf_onboard_user_4', [ $this, 'vlf_onboard_user_4' ] );
            add_action( 'wp_ajax_vlf_onboard_user_5', [ $this, 'vlf_onboard_user_5' ] );
            add_action( 'wp_ajax_nopriv_vlf_onboard_user_5', [ $this, 'vlf_onboard_user_5' ] );

            /**
             * User Profile - Connections
             */
            add_action( 'wp_ajax_vlf_connect_to_user', [ $this, 'vlf_connect_to_user' ] );
            add_action( 'wp_ajax_nopriv_vlf_connect_to_user', [ $this, 'vlf_connect_to_user' ] );
            add_action( 'wp_ajax_vlf_disconnect_from_user', [ $this, 'vlf_disconnect_from_user' ] );
            add_action( 'wp_ajax_nopriv_vlf_disconnect_from_user', [ $this, 'vlf_disconnect_from_user' ] );
            add_action( 'wp_ajax_vlf_update_connection', [ $this, 'vlf_update_connection' ] );
            add_action( 'wp_ajax_nopriv_vlf_update_connection', [ $this, 'vlf_update_connection' ] );

            /**
             * User Profile - Join Organization
             */
            add_action( 'wp_ajax_vlf_join_brand_organization', [ $this, 'vlf_join_brand_organization' ] );
            add_action( 'wp_ajax_nopriv_vlf_join_brand_organization', [ $this, 'vlf_join_brand_organization' ] );
            add_action( 'wp_ajax_vlf_leave_brand_organization', [ $this, 'vlf_leave_brand_organization' ] );
            add_action( 'wp_ajax_nopriv_vlf_leave_brand_organization', [ $this, 'vlf_leave_brand_organization' ] );
            add_action( 'wp_ajax_vlf_get_brand_organization_update', [ $this, 'vlf_get_brand_organization_update' ] );
            add_action( 'wp_ajax_nopriv_vlf_get_brand_organization_update', [ $this, 'vlf_get_brand_organization_update' ] );
            add_action( 'wp_ajax_vlf_get_brand_organization_login', [ $this, 'vlf_get_brand_organization_login' ] );
            add_action( 'wp_ajax_nopriv_vlf_get_brand_organization_login', [ $this, 'vlf_get_brand_organization_login' ] );

            /**
             * User Profile - Products
             */
            add_action( 'wp_ajax_vlf_load_more_products', [ $this, 'vlf_load_more_products' ] );
            add_action( 'wp_ajax_nopriv_vlf_load_more_products', [ $this, 'vlf_load_more_products' ] );

            /**
             * Reviews
             */
            add_action( 'wp_ajax_vlf_submit_review', [ $this, 'vlf_submit_review' ] );
            add_action( 'wp_ajax_nopriv_vlf_submit_review', [ $this, 'vlf_submit_review' ] );

            /**
             * Navigator
             */
            add_action( 'wp_ajax_vlf_navigator_get_create', [ $this, 'vlf_navigator_get_create' ] );
            add_action( 'wp_ajax_nopriv_vlf_navigator_get_create', [ $this, 'vlf_navigator_get_create' ] );
            add_action( 'wp_ajax_vlf_navigator_get_popup', [ $this, 'vlf_navigator_get_popup' ] );
            add_action( 'wp_ajax_nopriv_vlf_navigator_get_popup', [ $this, 'vlf_navigator_get_popup' ] );

            /**
             * Navigator Actions
             */
            add_action( 'wp_ajax_vlf_navigator_publish_item', [ $this, 'vlf_navigator_publish_item' ] );
            add_action( 'wp_ajax_nopriv_vlf_navigator_publish_item', [ $this, 'vlf_navigator_publish_item' ] );
            add_action( 'wp_ajax_vlf_navigator_delete_item', [ $this, 'vlf_navigator_delete_item' ] );
            add_action( 'wp_ajax_nopriv_vlf_navigator_delete_item', [ $this, 'vlf_navigator_delete_item' ] );

            add_action( 'wp_ajax_vlf_navigator_cu_event', [ $this, 'vlf_navigator_cu_event' ] );
            add_action( 'wp_ajax_nopriv_vlf_navigator_cu_event', [ $this, 'vlf_navigator_cu_event' ] );
            add_action( 'wp_ajax_vlf_navigator_cu_product', [ $this, 'vlf_navigator_cu_product' ] );
            add_action( 'wp_ajax_nopriv_vlf_navigator_cu_product', [ $this, 'vlf_navigator_cu_product' ] );
            add_action( 'wp_ajax_vlf_navigator_cu_post', [ $this, 'vlf_navigator_cu_post' ] );
            add_action( 'wp_ajax_nopriv_vlf_navigator_cu_post', [ $this, 'vlf_navigator_cu_post' ] );

            add_action( 'wp_ajax_vlf_upload_image_editor', [ $this, 'vlf_upload_image_editor' ] );
            add_action( 'wp_ajax_nopriv_vlf_upload_image_editor', [ $this, 'vlf_upload_image_editor' ] );

            add_action( 'wp_ajax_vlf_upload_image_editor_link', [ $this, 'vlf_upload_image_editor_link' ] );
            add_action( 'wp_ajax_nopriv_vlf_upload_image_editor_link', [ $this, 'vlf_upload_image_editor_link' ] );

            add_action( 'wp_ajax_vlf_navigator_cu_network', [ $this, 'vlf_navigator_cu_network' ] );
            add_action( 'wp_ajax_nopriv_vlf_navigator_cu_network', [ $this, 'vlf_navigator_cu_network' ] );

            /**
             * Publish Things
             */
            add_action( 'wp_ajax_vlf_publish_brand', [ $this, 'vlf_publish_brand' ] );
            add_action( 'wp_ajax_nopriv_vlf_publish_brand', [ $this, 'vlf_publish_brand' ] );
            add_action( 'wp_ajax_vlf_publish_post_type', [ $this, 'vlf_publish_post_type' ] );
            add_action( 'wp_ajax_nopriv_vlf_publish_post_type', [ $this, 'vlf_publish_post_type' ] );

            /**
             * Get Brands
             */
            add_action( 'wp_ajax_vlf_get_brands', [ $this, 'vlf_get_brands' ] );
            add_action( 'wp_ajax_nopriv_vlf_get_brands', [ $this, 'vlf_get_brands' ] );

            /**
             * Load More
             */
            add_action( 'wp_ajax_vlf_load_more_members', [ $this, 'vlf_load_more_members' ] );
            add_action( 'wp_ajax_nopriv_vlf_load_more_members', [ $this, 'vlf_load_more_members' ] );
            add_action( 'wp_ajax_load_more_networks', [ $this, 'load_more_networks' ] );
            add_action( 'wp_ajax_nopriv_load_more_networks', [ $this, 'load_more_networks' ] );
            add_action( 'wp_ajax_vlf_load_more_products_shop', [ $this, 'vlf_load_more_products_shop' ] );
            add_action( 'wp_ajax_nopriv_vlf_load_more_products_shop', [ $this, 'vlf_load_more_products_shop' ] );
            add_action( 'wp_ajax_vlf_load_more_posts', [ $this, 'vlf_load_more_posts' ] );
            add_action( 'wp_ajax_nopriv_vlf_load_more_posts', [ $this, 'vlf_load_more_posts' ] );

            /**
             * Add Members
             */
            add_action( 'wp_ajax_vlf_connect_to_users', [ $this, 'vlf_connect_to_users' ] );
            add_action( 'wp_ajax_nopriv_vlf_connect_to_users', [ $this, 'vlf_connect_to_users' ] );

        }

        /**
         * Register user.
         *
         * @since 1.0.0
         */
        public function vlf_register_user()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $validation_rules = [
                    'membership' => 'required|integer',
                    'email'      => 'required|valid_email',
                    'password'   => 'required|max_len,100|min_len,6'
                ];

                $filter_rules = [
                    'membership' => 'trim',
                    'email'      => 'trim|sanitize_email',
                    'password'   => 'trim'
                ];

                $this->gump->set_field_name( 'membership', esc_html__( 'Membership', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'email', esc_html__( 'Email', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'password', esc_html__( 'Password', 'vuelabs-framework' ) );

                $this->gump->validation_rules( $validation_rules );
                $this->gump->filter_rules( $filter_rules );

                $data = $this->gump->run( $data );

                if ( $data === false ) {

                    wp_send_json_error( [
                        'code' => '2',
                        'data' => $this->gump->get_readable_errors()
                    ] );

                } else {

                    // Create new user using WooCommerce.
                    $user_id = wc_create_new_customer( $data['email'], null, $data['password'] );

                    // Set user onboarding to false.
                    update_user_meta( $user_id, 'user_onboarding', '0' );

                    if ( ! is_wp_error( $user_id ) ) {

                        $product_selected = wc_get_product( $data['membership'] );

                        // Get Membership Trial Info
                        $trial_membership = get_field( 'trial_membership', 'option' );
                        $trial_membership = vlf_check_field( $trial_membership );
                        $trial_membership = $trial_membership[0];

                        // Set user role based on the membership.
                        $user = new WP_User( $user_id );
                        $user_data = get_userdata( $user_id );

                        if ( $product_selected->get_name() === 'Personal' ) {

                            $product   = wc_get_product( $trial_membership['membership'] );
                            $user_role = $trial_membership['product_role'];

                        } else {

                            $product   = wc_get_product( $data['membership'] );
                            if ( $product->get_name() === 'Professional' ) {

                                $user_role = 'freshmes_business';

                            } else {

                                $user_role = 'freshmes_organization';

                            }

                        }

                        $user->add_role( $user_role );

                        WC()->cart->add_to_cart( $product->get_id() );

                        // Create brand page for the user and cache it.
                        $brand_id = wp_insert_post( [
                            'post_author' => $user_id,
                            'post_title'  => esc_html__( 'Brand page for: ' . $user_data->user_email, 'vuelabs-framework' ),
                            'post_status' => 'private',
                            'post_type'   => 'brand',
                            'ping_status' => 'closed'
                        ] );
                        update_option( 'brand_page_id_for_' . $user_id, $brand_id );

                        // Update user selected membership on brand page.
                        update_field( 'brand_membership', $data['membership'], $brand_id );

                        wp_send_json_success( [
                            'data'     => __( 'Your account has been created successfully.<br>You will be redirected to checkout page now.', 'vuelabs-framework' ),
                            'location' => get_site_url() . '/checkout'
                        ] );

                    }

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Onboard our user.
         */
        public function vlf_onboard_user_1()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $validation_rules = [
                    'user'                 => 'required',
                    'first_name'           => 'required',
                    'last_name'            => 'required',
                    'phone'                => 'required',
                    'job_title'            => 'required',
                    'description'          => 'required'
                ];

                $filter_rules = [
                    'user'                 => 'trim',
                    'first_name'           => 'trim|sanitize_string',
                    'last_name'            => 'trim|sanitize_string',
                    'phone'                => 'trim',
                    'job_title'            => 'trim|sanitize_string',
                    'description'          => 'trim|sanitize_string',
                ];

                $this->gump->set_field_name( 'first_name', esc_html__( 'First Name', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'last_name', esc_html__( 'Last Name', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'phone', esc_html__( 'Phone', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'job_title', esc_html__( 'Job Title', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'description', esc_html__( 'Description', 'vuelabs-framework' ) );

                $this->gump->validation_rules( $validation_rules );
                $this->gump->filter_rules( $filter_rules );

                $data = $this->gump->run( $data );

                if ( $data === false ) {

                    wp_send_json_error( [
                        'code' => '2',
                        'data' => $this->gump->get_readable_errors()
                    ] );

                } else {

                    update_user_meta( $data['user'], 'first_name', $data['first_name'] );
                    update_user_meta( $data['user'], 'last_name', $data['last_name'] );

                    $brand_id = get_option( 'brand_page_id_for_' . $data['user'] );
                    $user     = new WP_User( $data['user'] );
                    if ( isset( $brand_id ) && ! empty( $brand_id ) ) {

                        wp_update_post( [
                            'ID'         => $brand_id,
                            'post_title' => $data['first_name'] . ' ' . $data['last_name'],
                            'post_name'  => ''
                        ] );

                        update_field( 'brand_job_title', $data['job_title'], $brand_id );
                        update_field( 'brand_phone', $data['phone'], $brand_id );
                        update_field( 'brand_description', [
                            [
                                'title' => 'My Story',
                                'content' => $data['description']
                            ]
                        ], $brand_id );

                        update_post_meta( $brand_id, 'brand_onboarding_step', 2 );

                    }

                    wp_send_json_success();

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Onboard our user.
         */
        public function vlf_onboard_user_2()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $validation_rules = [
                    'user'                 => 'required',
                    'tags'                 => 'required'
                ];

                $filter_rules = [
                    'user'                 => 'trim',
                    'business_name'        => 'trim|sanitize_string',
                    'tagline'              => 'trim|sanitize_string',
                    'business_description' => 'trim|sanitize_string',
                    'tags'                 => 'trim'
                ];

                $this->gump->set_field_name( 'business_name', esc_html__( 'Business Name', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'tagline', esc_html__( 'Tagline', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'business_description', esc_html__( 'Business Description', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'tags', esc_html__( 'Tags', 'vuelabs-framework' ) );

                $this->gump->validation_rules( $validation_rules );
                $this->gump->filter_rules( $filter_rules );

                $data = $this->gump->run( $data );

                if ( $data === false ) {

                    wp_send_json_error( [
                        'code' => '2',
                        'data' => $this->gump->get_readable_errors()
                    ] );

                } else {

                    $brand_id = get_option( 'brand_page_id_for_' . $data['user'] );
                    if ( isset( $brand_id ) && ! empty( $brand_id ) ) {

                        if ( isset( $data['business_name'] ) && ! empty( $data['business_name'] ) ) {

                            wp_update_post( [
                                'ID'         => $brand_id,
                                'post_title' => $data['business_name'],
                                'post_name'  => ''
                            ] );

                            update_field( 'brand_business_name', $data['business_name'], $brand_id );

                            $user_id = $data['user'];
                            $user = new WP_User( $user_id );

                            if ( in_array( 'freshmes_organization', $user->roles ) ) {

                                $organization = wp_insert_term( $data['business_name'], 'brand_organization' );
                                if ( ! is_wp_error( $organization ) ) {

                                    wp_set_object_terms( $brand_id, intval( $organization['term_id'] ), 'brand_organization', true );

                                }

                            }

                            // Create Product Tag
                            if ( in_array( 'freshmes_business', $user->roles ) || in_array( 'freshmes_organization', $user->roles ) ) {

                                $tag = wp_insert_term( esc_html( $data['business_name'] ), 'product_tag' );
                                update_field( 'vlf_products_vendor', $data['user'], 'product_tag_' . $tag['term_id'] );

                                $user->add_role( 'wc_product_vendors_manager_vendor' );

                                // Create Vendor
                                $vendor = wp_insert_term( esc_html( $data['business_name'] ), 'wcpv_product_vendors' );
                                $vendor_data = [
                                    'notes'   => '',
                                    'logo'    => '',
                                    'profile' => '',
                                    'email'   => $user->user_email,
                                    'admins'  => [
                                        0 => intval( $user_id )
                                    ],
                                    'commission'           => '',
                                    'commission_type'      => 'percentage',
                                    'paypal'               => '',
                                    'timezone'             => 'UTC+0',
                                    'enable_bookings'      => 'no',
                                    'per_product_shipping' => 'no',
                                    'instant_payout'       => 'no',
                                ];
                                update_term_meta( $vendor['term_id'], 'vendor_data', $vendor_data );

                            }

                        }

                        if ( isset( $data['tagline'] ) && ! empty( $data['tagline'] ) ) {

                            update_field( 'brand_tagline', $data['tagline'], $brand_id );

                        }

                        if ( isset( $data['business_description'] ) && ! empty( $data['business_description'] ) ) {

                            update_field( 'brand_business_description', [
                                [
                                    'title'   => 'Work Story',
                                    'content' => $data['business_description']
                                ]
                            ], $brand_id );

                        }

                        if ( isset( $data['tags'] ) && ! empty( $data['tags'] ) ) {

                            $tags_ids = [];
                            $tags = $data['tags'];
                            foreach ( $tags as $tag ) {

                                wp_set_object_terms( intval( $brand_id ), intval( $tag ), 'brand_tag', true );

                                //array_push( $tags_ids, intval( $tag ) );

                            }

                        }

                    }

                    update_post_meta( $brand_id, 'brand_onboarding_step', 3 );

                    wp_send_json_success();

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Onboard our user.
         */
        public function vlf_onboard_user_3()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $validation_rules = [
                    'user'           => 'required',
                    'street_number'  => 'required',
                    'address'        => 'required',
                    'city'           => 'required',
                    'country'        => 'required',
                    'website'        => 'valid_url'
                ];

                $filter_rules = [
                    'user'           => 'trim',
                    'street_number'  => 'trim|sanitize_string',
                    'address'        => 'trim|sanitize_string',
                    'city'           => 'trim|sanitize_string',
                    'state'          => 'trim|sanitize_string',
                    'country'        => 'trim|sanitize_string',
                    'business_phone' => 'trim|sanitize_string',
                    'website'        => 'trim|sanitize_string'
                ];

                $this->gump->set_field_name( 'street_number', esc_html__( 'Street Number', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'address', esc_html__( 'Address', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'city', esc_html__( 'City', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'state', esc_html__( 'State', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'country', esc_html__( 'Country', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'business_phone', esc_html__( 'Business Phone', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'website', esc_html__( 'Website', 'vuelabs-framework' ) );

                $this->gump->validation_rules( $validation_rules );
                $this->gump->filter_rules( $filter_rules );

                $data = $this->gump->run( $data );

                if ( $data === false ) {

                    wp_send_json_error( [
                        'code' => '2',
                        'data' => $this->gump->get_readable_errors()
                    ] );

                } else {

                    $brand_id = get_option( 'brand_page_id_for_' . $data['user'] );
                    if ( isset( $brand_id ) && ! empty( $brand_id ) ) {

                        update_field( 'brand_address', $data['street_number'] . ' ' . $data['address'], $brand_id );

                        if ( isset( $data['business_phone'] ) && ! empty( $data['business_phone'] ) ) {

                            update_field( 'brand_business_phone', $data['business_phone'], $brand_id );

                        }

                        if ( isset( $data['website'] ) && ! empty( $data['website'] ) ) {

                            update_field( 'brand_website', $data['website'], $brand_id );

                        }

                        $country = $data['country'];
                        $city    = $data['city'];
                        $state   = $data['state'];

                        // Set Location in Taxonomy
                        $country_id = term_exists( sanitize_title( $country ), 'brand_location' );
                        if ( isset( $country_id ) && ! empty( $country_id ) ) {

                            $country_id = $country_id['term_id'];

                            wp_set_object_terms( $brand_id, intval( $country_id ), 'brand_location', true );

                        } else {

                            $country_id = wp_insert_term( $country, 'brand_location', [
                                'slug' => sanitize_title( $country )
                            ] );
                            $country_id = $country_id['term_id'];
                            wp_set_object_terms( $brand_id, intval( $country_id ), 'brand_location', true );

                        }

                        // Update State
                        if ( isset( $state ) && ! empty( $state ) ) {

                            $state_id = term_exists( sanitize_title( $state ), 'brand_location', intval( $country_id ) );
                            if ( isset( $state_id ) && ! empty( $state_id ) ) {

                                $state_id = $state_id['term_id'];

                                wp_set_object_terms( $brand_id, intval( $state_id ), 'brand_location', true );

                            } else {

                                $state_id = wp_insert_term( $state, 'brand_location', [
                                    'parent' => $country_id,
                                    'slug'   => sanitize_title( $state )
                                ] );
                                $state_id = $state_id['term_id'];
                                wp_set_object_terms( $brand_id, intval( $state_id ), 'brand_location', true );

                            }

                        }


                        // Update City
                        if ( isset( $city ) && ! empty( $city ) ) {

                            if ( isset( $state_id ) && ! empty( $state_id ) ) {

                                $city_id = term_exists( sanitize_title( $city . '-city' ), 'brand_location', intval( $state_id ) );

                            } else {

                                $city_id = term_exists( sanitize_title( $city . '-city' ), 'brand_location', intval( $country_id ) );

                            }
                            if ( isset( $city_id ) && ! empty( $city_id ) ) {

                                $city_id = $city_id['term_id'];

                                wp_set_object_terms( $brand_id, intval( $city_id ), 'brand_location', true );

                            } else {

                                $city_args = [
                                    'slug' => sanitize_title( $city . '-city' )
                                ];
                                if ( isset( $state_id ) && ! empty( $state_id ) ) {

                                    $city_args['parent'] = $state_id;

                                } else {

                                    $city_args['parent'] = $country_id;

                                }

                                $city_id = wp_insert_term( $city, 'brand_location', $city_args );
                                $city_id = $city_id['term_id'];
                                wp_set_object_terms( $brand_id, intval( $city_id ), 'brand_location', true );

                            }

                        }

                        update_post_meta( $brand_id, 'brand_onboarding_step', 4 );

                    }

                    wp_send_json_success();

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Onboard our user.
         */
        public function vlf_onboard_user_4()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );
            if ( $return['status'] === true ) {

                $data = $return['data'];

                $validation_rules = [
                    'user'      => 'required'
                ];

                $filter_rules = [
                    'user'      => 'trim',
                    'facebook'  => 'trim|sanitize_string',
                    'instagram' => 'trim|sanitize_string',
                    'linkedin'  => 'trim|sanitize_string',
                ];

                $this->gump->set_field_name( 'facebook', esc_html__( 'Facebook', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'instagram', esc_html__( 'Instagram', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'linkedin', esc_html__( 'LinkedIn', 'vuelabs-framework' ) );

                $this->gump->validation_rules( $validation_rules );
                $this->gump->filter_rules( $filter_rules );

                $data = $this->gump->run( $data );

                if ( $data === false ) {

                    wp_send_json_error( [
                        'code' => '2',
                        'data' => $this->gump->get_readable_errors()
                    ] );

                } else {

                    $brand_id = get_option( 'brand_page_id_for_' . $data['user'] );
                    if ( isset( $brand_id ) && ! empty( $brand_id ) ) {

                        if ( isset( $data['facebook'] ) && ! empty( $data['facebook'] ) ) {

                            update_field( 'brand_facebook', $data['facebook'], $brand_id );

                        }

                        if ( isset( $data['instagram'] ) && ! empty( $data['instagram'] ) ) {

                            update_field( 'brand_instagram', $data['instagram'], $brand_id );

                        }

                        if ( isset( $data['linkedin'] ) && ! empty( $data['linkedin'] ) ) {

                            update_field( 'brand_linkedin', $data['linkedin'], $brand_id );

                        }

                        update_post_meta( $brand_id, 'brand_onboarding_step', 5 );

                    }

                    wp_send_json_success();

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Onboard our user.
         */
        public function vlf_onboard_user_5()
        {

            $return = vlf_check_nonce( $this->gump, array_merge( $_POST, $_FILES ), 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $validation_rules = [
                    'user'            => 'required',
                    'profile_image'   => 'required_file|extension,png;jpg;jpeg',
                    'logo'            => 'extension,png;jpg;jpeg',
                    'logo_icon'       => 'extension,png;jpg;jpeg',
                    'gallery_image_1' => 'required_file|extension,png;jpg;jpeg',
                    'gallery_image_2' => 'extension,png;jpg;jpeg',
                    'gallery_image_3' => 'extension,png;jpg;jpeg'
                ];

                $filter_rules = [
                    'user'      => 'trim',
                ];

                $this->gump->set_field_name( 'profile_image', esc_html__( 'Profile Image', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'logo', esc_html__( 'Logo', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'logo_icon', esc_html__( 'Logo Icon', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'gallery_image_1', esc_html__( 'Gallery Image #1', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'gallery_image_2', esc_html__( 'Gallery Image #2', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'gallery_image_3', esc_html__( 'Gallery Image #3', 'vuelabs-framework' ) );

                $this->gump->validation_rules( $validation_rules );
                $this->gump->filter_rules( $filter_rules );

                $data = $this->gump->run( $data );

                if ( $data === false ) {

                    wp_send_json_error( [
                        'code' => '2',
                        'data' => $this->gump->get_readable_errors()
                    ] );

                } else {

                    $brand_id = get_option( 'brand_page_id_for_' . $data['user'] );
                    if ( isset( $brand_id ) && ! empty( $brand_id ) ) {

                        vlf_upload_image( $data['profile_image'], $brand_id, 'brand_profile_image', 'no', 'no' );
                        $image_id = get_field( 'brand_profile_image', $brand_id );
                        update_field( 'brand_profile_image', $image_id, 'user_' . $data['user'] );

                        if ( isset( $data['logo'] ) && ! empty( $data['logo'] ) ) {

                            vlf_upload_image( $data['logo'], $brand_id, 'brand_logo', 'no', 'no' );

                        }

                        if ( isset( $data['logo_icon'] ) && ! empty( $data['logo_icon'] ) ) {

                            vlf_upload_image( $data['logo_icon'], $brand_id, 'brand_logo_icon', 'no', 'no' );

                        }

                        $gallery_image_1 = vlf_upload_image( $data['gallery_image_1'], $brand_id, '', 'no', 'yes' );
                        set_post_thumbnail( $brand_id, $gallery_image_1 );

                        $gallery_images = [];
                        array_push( $gallery_images, $gallery_image_1 );

                        if ( isset( $data['gallery_image_2'] ) && ! empty( $data['gallery_image_2'] ) ) {

                            $gallery_image_2 = vlf_upload_image( $data['gallery_image_2'], $brand_id, '', 'no', 'yes' );
                            array_push( $gallery_images, $gallery_image_2 );

                        }

                        if ( isset( $data['gallery_image_3'] ) && ! empty( $data['gallery_image_3'] ) ) {

                            $gallery_image_3 = vlf_upload_image( $data['gallery_image_3'], $brand_id, '', 'no', 'yes' );
                            array_push( $gallery_images, $gallery_image_3 );

                        }

                        if ( isset( $gallery_images ) && ! empty( $gallery_images ) ) {

                            update_field( 'brand_gallery', $gallery_images, $brand_id );

                        }

                        update_post_meta( $brand_id, 'brand_onboarding_step', 'done' );

                    }

                    wp_send_json_success( esc_url( get_permalink( $brand_id ) ) );

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Connect to a user.
         */
        public function vlf_connect_to_user()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $validation_rules = [
                    'user_a'   => 'required|numeric',
                    'user_b'   => 'required|numeric',
                    'category' => 'required|min_len,3|max_len,4'
                ];

                $filter_rules = [
                    'user_a'   => 'trim|whole_number|sanitize_numbers',
                    'user_b'   => 'trim|whole_number|sanitize_numbers',
                    'category' => 'trim|sanitize_string'
                ];

                $this->gump->set_field_name( 'user_a', esc_html__( 'User A', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'user_b', esc_html__( 'User B', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'category', esc_html__( 'Category', 'vuelabs-framework' ) );

                $this->gump->validation_rules( $validation_rules );
                $this->gump->filter_rules( $filter_rules );

                $data = $this->gump->run( $data );

                if ( $data === false ) {

                    wp_send_json_error( [
                        'code' => '2',
                        'data' => $this->gump->get_readable_errors()
                    ] );

                } else {

                    $result = vlf_create_connection( $data['user_a'], $data['user_b'], $data['category'] );

                    if ( $result !== false ) {

                        $person = get_userdata( $data['user_b'] );

                        wp_send_json_success( [
                            'brand_user_name' => esc_html( $person->first_name ),
                            'brand_header_title' => esc_html__( 'Congratulations', 'vuelabs-framework' ),
                            'brand_header_subtitle' => sprintf( esc_html__( 'You are now connected to [%s].', 'vuelabs-framework' ), esc_html( $person->first_name ) ),
                            'brand_content_title' => '<div class="connect-popup__content__title">' . sprintf( esc_html__( 'Select a category for [%s]', 'vuelabs-framework' ), esc_html( $person->first_name ) ) . '</div>',
                            'brand_content_fields' => '<div class="field"><input class="is-checkradio" type="checkbox" id="connection-category-work" name="connection-category[]" value="work"><label for="connection-category-work">' . esc_html__( 'Work', 'vuelabs-framework' ) . '</label><input class="is-checkradio" type="checkbox" id="connection-category-life" name="connection-category[]" value="life"><label for="connection-category-life">' . esc_html__( 'Life', 'vuelabs-framework' ) . '</label></div>'
                        ] );

                    }

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Disconnect from a user.
         */
        public function vlf_disconnect_from_user()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $validation_rules = [
                    'user_a'   => 'required|numeric',
                    'user_b'   => 'required|numeric'
                ];

                $filter_rules = [
                    'user_a'   => 'trim|whole_number|sanitize_numbers',
                    'user_b'   => 'trim|whole_number|sanitize_numbers'
                ];

                $this->gump->set_field_name( 'user_a', esc_html__( 'User A', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'user_b', esc_html__( 'User B', 'vuelabs-framework' ) );

                $this->gump->validation_rules( $validation_rules );
                $this->gump->filter_rules( $filter_rules );

                $data = $this->gump->run( $data );

                if ( $data === false ) {

                    wp_send_json_error( [
                        'code' => '2',
                        'data' => $this->gump->get_readable_errors()
                    ] );

                } else {

                    $result = vlf_delete_connection( $data['user_a'], $data['user_b'] );

                    if ( $result !== false ) {

                        $person = get_userdata( $data['user_b'] );

                        wp_send_json_success( [
                            'brand_user_name' => esc_html( $person->first_name ),
                            'brand_header_title' => esc_html__( 'Congratulations', 'vuelabs-framework' ),
                            'brand_header_subtitle' => sprintf( esc_html__( 'You are now disconnected from [%s].', 'vuelabs-framework' ), esc_html( $person->first_name ) )
                        ] );

                    }

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Update connection category.
         */
        public function vlf_update_connection()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $validation_rules = [
                    'user_a'   => 'required|numeric',
                    'user_b'   => 'required|numeric',
                    'category' => 'required|min_len,3|max_len,4'
                ];

                $filter_rules = [
                    'user_a'   => 'trim|whole_number|sanitize_numbers',
                    'user_b'   => 'trim|whole_number|sanitize_numbers',
                    'category' => 'trim|sanitize_string'
                ];

                $this->gump->set_field_name( 'user_a', esc_html__( 'User A', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'user_b', esc_html__( 'User B', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'category', esc_html__( 'Category', 'vuelabs-framework' ) );

                $this->gump->validation_rules( $validation_rules );
                $this->gump->filter_rules( $filter_rules );

                $data = $this->gump->run( $data );

                if ( $data === false ) {

                    wp_send_json_error( [
                        'code' => '2',
                        'data' => $this->gump->get_readable_errors()
                    ] );

                } else {

                    $result = vlf_update_connection( $data['user_a'], $data['user_b'], $data['category'] );

                    if ( $result !== false ) {

                        wp_send_json_success();

                    }

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Join Brand Organization
         */
        public function vlf_join_brand_organization()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $validation_rules = [
                    'brand_id' => 'required|numeric',
                    'user_id'  => 'required|numeric',
                ];

                $filter_rules = [
                    'brand_id' => 'trim|whole_number|sanitize_numbers',
                    'user_id'  => 'trim|whole_number|sanitize_numbers',
                ];

                $this->gump->set_field_name( 'brand_id', esc_html__( 'Brand ID', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'user_id', esc_html__( 'User ID', 'vuelabs-framework' ) );

                $this->gump->validation_rules( $validation_rules );
                $this->gump->filter_rules( $filter_rules );

                $data = $this->gump->run( $data );

                if ( $data === false ) {

                    wp_send_json_error( [
                        'code' => '2',
                        'data' => $this->gump->get_readable_errors()
                    ] );

                } else {

                    $join = vlf_join_brand_organization( $data['brand_id'], $data['user_id'] );
                    if ( $join === true ) {

                        wp_send_json_success( [
                            'brand_header_title' => esc_html__( 'Congratulations!', 'vuelabs-framework' ),
                            'brand_content'      => esc_html__( 'You have successfully joined this organization.', 'vuelabs-framework' )
                        ] );

                    }

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Leave Brand Organization
         */
        public function vlf_leave_brand_organization()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $validation_rules = [
                    'brand_id' => 'required|numeric',
                    'user_id'  => 'required|numeric',
                ];

                $filter_rules = [
                    'brand_id' => 'trim|whole_number|sanitize_numbers',
                    'user_id'  => 'trim|whole_number|sanitize_numbers',
                ];

                $this->gump->set_field_name( 'brand_id', esc_html__( 'Brand ID', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'user_id', esc_html__( 'User ID', 'vuelabs-framework' ) );

                $this->gump->validation_rules( $validation_rules );
                $this->gump->filter_rules( $filter_rules );

                $data = $this->gump->run( $data );

                if ( $data === false ) {

                    wp_send_json_error( [
                        'code' => '2',
                        'data' => $this->gump->get_readable_errors()
                    ] );

                } else {

                    $remove = vlf_remove_brand_from_organization( $data['brand_id'], $data['user_id'] );
                    if ( $remove === true ) {

                        wp_send_json_success( [
                            'brand_header_title' => esc_html__( 'Congratulations!', 'vuelabs-framework' ),
                            'brand_content'      => esc_html__( 'You have successfully left this organization.', 'vuelabs-framework' )
                        ] );

                    }

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Get Brand Organization Upgrade Text
         */
        public function vlf_get_brand_organization_update()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                wp_send_json_success( [
                    'brand_header_title' => esc_html__( 'Please upgrade!', 'vuelabs-framework' ),
                    'brand_content'      => sprintf( '<p class="has-text-centered">%s</p><p class="has-text-centered"><a href="#" class="button is-upgrade">%s</a></p>', esc_html__( 'You need to upgrade to Organization Membership Plan in order to be able to join this organization.', 'vuelabs-framework' ), esc_html__( 'Upgrade Now', 'vuelabs-framework' ) )
                ] );

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Get Brand Organization Login Text
         */
        public function vlf_get_brand_organization_login()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                wp_send_json_success( [
                    'brand_header_title' => esc_html__( 'Please login!', 'vuelabs-framework' ),
                    'brand_content'      => sprintf( '<p class="has-text-centered">%s</p><p class="has-text-centered"><a href="#" class="button is-account-toggle">%s</a></p>', esc_html__( 'You need to login or create an account to join this persons network.', 'vuelabs-framework' ), esc_html__( 'Log in', 'vuelabs-framework' ) )
                ] );

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Load more products.
         */
        public function vlf_load_more_products()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $validation_rules = [
                    'author' => 'required|numeric',
                    'offset' => 'required|numeric',
                ];

                $filter_rules = [
                    'author' => 'trim|whole_number|sanitize_numbers',
                    'offset' => 'trim|whole_number|sanitize_numbers',
                ];

                $this->gump->set_field_name( 'author', esc_html__( 'Author ID', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'offset', esc_html__( 'Offset', 'vuelabs-framework' ) );

                $this->gump->validation_rules( $validation_rules );
                $this->gump->filter_rules( $filter_rules );

                $data = $this->gump->run( $data );

                if ( $data === false ) {

                    wp_send_json_error( [
                        'code' => '2',
                        'data' => $this->gump->get_readable_errors()
                    ] );

                } else {

                    $args = [
                        'post_type'      => 'product',
                        'posts_per_page' => 2,
                        'author'         => $data['author'],
                        'offset'         => $data['offset']
                    ];
                    $loop = new WP_Query( $args );
                    if ( $loop->have_posts() ) {

                        $more = true;

                        if ( intval( $loop->post_count ) < 2 ) {

                            $more = false;

                        }

                        ob_start();

                        while ( $loop->have_posts() ) : $loop->the_post();

                            echo '<div class="column is-6">';
                            vlf_display_product( get_the_ID(), 'brand-page' );
                            echo '</div>';

                        endwhile; wp_reset_postdata();

                        $content = ob_get_clean();

                        wp_send_json_success( [
                            'more'    => $more,
                            'content' => $content
                        ] );

                    }

                    wp_send_json_error( [
                        'code' => '3'
                    ] );

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Submit review
         */
        public function vlf_submit_review()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $validation_rules = [
                    'user'     => 'required|numeric',
                    'brand'    => 'required|numeric',
                    'rating'   => 'required|numeric',
                    'title'    => 'required',
                    'comment'  => 'required'
                ];

                $filter_rules = [
                    'user'     => 'trim|whole_number|sanitize_numbers',
                    'brand'    => 'trim|whole_number|sanitize_numbers',
                    'rating'   => 'trim|whole_number|sanitize_numbers',
                    'title'    => 'trim|sanitize_string',
                    'comment'  => 'trim|sanitize_string'
                ];

                $this->gump->set_field_name( 'user', esc_html__( 'User ID', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'brand', esc_html__( 'Brand ID', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'rating', esc_html__( 'Rating', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'title', esc_html__( 'Title', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'comment', esc_html__( 'Comment', 'vuelabs-framework' ) );

                $this->gump->validation_rules( $validation_rules );
                $this->gump->filter_rules( $filter_rules );

                $data = $this->gump->run( $data );

                if ( $data === false ) {

                    wp_send_json_error( [
                        'code' => '2',
                        'data' => $this->gump->get_readable_errors()
                    ] );

                } else {

                    $user = get_userdata( $data['user'] );

                    $comment = wp_insert_comment( [
                        'comment_approved'     => 0,
                        'comment_author'       => $user->first_name . ' ' . $user->last_name,
                        'comment_author_email' => $user->user_email,
                        'comment_content'      => $data['comment'],
                        'comment_post_ID'      => $data['brand'],
                        'comment_type'         => 'comment',
                        'user_id'              => $data['user']
                    ] );

                    if ( ! empty( $comment ) ) {

                        update_field( 'comment_title', $data['title'], 'comment_' . $comment );
                        update_field( 'comment_rating', $data['rating'], 'comment_' . $comment );

                        wp_send_json_success( esc_html__( 'Your review has been submitted. You will see it when it is approved.', 'vuelabs-framework' ) );

                    }

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Navigator Create
         */
        public function vlf_navigator_get_create()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                ob_start();

                ?>

                <ul class="navigator-create">
                    <li>
                        <a href="#" data-popup-id="event" data-popup-type="create">
                            <i class="fal fa-calendar fa-fw"></i>
                            <span><?php esc_html_e( 'Event', 'vuelabs-framework' ); ?></span>
                        </a>
                    </li>
                    <li>
                        <a href="#" data-popup-id="product" data-popup-type="create">
                            <i class="fal fa-box-usd fa-fw"></i>
                            <span><?php esc_html_e( 'Product', 'vuelabs-framework' ); ?></span>
                        </a>
                    </li>
                    <li>
                        <a href="#" data-popup-id="post" data-popup-type="create">
                            <i class="fal fa-leaf fa-fw"></i>
                            <span><?php esc_html_e( 'Post', 'vuelabs-framework' ); ?></span>
                        </a>
                    </li>
                    <?php
                    $user_id = get_current_user_id();
                    $user    = new WP_User( $user_id );
                    if ( in_array( 'freshmes_organization', $user->roles ) ) {

                        ?>

                        <li>
                            <a href="#" data-popup-id="network" data-popup-type="create">
                                <i class="fal fa-users fa-fw"></i>
                                <span><?php esc_html_e( 'Network', 'vuelabs-framework' ); ?></span>
                            </a>
                        </li>

                        <?php

                    }
                    ?>
                    <li>
                        <a href="#" data-popup-id="photo" data-popup-type="create">
                            <i class="fal fa-camera-retro fa-fw"></i>
                            <span><?php esc_html_e( 'Photo', 'vuelabs-framework' ); ?></span>
                            <button class="button is-white is-loading is-hidden"><span class="is-sr-only"><?php esc_html_e( 'Is Loading', 'vuelabs-framework' ); ?></span></button>
                        </a>
                    </li>
                </ul>
                <input type="file" id="navigator-create__image" name="navigator-create__image">

                <?php

                $html_content = ob_get_clean();

                ob_start();

                ?>

                <div class="columns is-gapless">
                    <div class="column has-text-right">
                        <a href="#" class="navigator-popup__footer__close"><?php esc_html_e( 'Close', 'vuelabs-framework' ); ?></a>
                    </div>
                </div>

                <?php

                $html_footer = ob_get_clean();

                wp_send_json_success( [
                    'popup_title'    => esc_html__( 'Create', 'vuelabs-framework' ),
                    'popup_content'  => $html_content,
                    'popup_footer'   => $html_footer
                ] );

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Get required navigator popup.
         */
        public function vlf_navigator_get_popup()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data       = $return['data'];
                $popup_id   = $data['popup_id'];
                $popup_type = $data['popup_type'];
                $user_id    = $data['user_id'];

                $product_categories_exclude = get_field( 'product_category_exclude', 'option' );
                $product_categories_exclude = vlf_check_field( $product_categories_exclude );

                $product_categories = get_terms( [
                    'hide_empty' => false,
                    'taxonomy'   => 'product_cat',
                    'exclude'    => $product_categories_exclude
                ] );

                $post_categories = get_terms( [
                    'hide_empty' => false,
                    'taxonomy'   => 'category'
                ] );

                $return = [];

                if ( $popup_id === 'event' ) {
                    // Event Popup
                    switch ( $popup_type ) {

                        case 'create':

                            ob_start();

                            ?>

                            <div class="field is-image-field">
                                <label for="event-image" class="label">
                                    <input type="file" id="event-image" name="event-image">
                                    <span class="file-image is-hidden"></span>
                                    <span class="file-no-image">
                                        <i class="fal fa-image"></i>
                                        <span class="file-no-image-text"><?php esc_html_e( 'Cover Photo', 'vuelabs-framework' ); ?></span>
                                    </span>
                                </label>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input" id="event-name" placeholder="<?php esc_attr_e( 'Event Name', 'vuelabs-framework' ); ?>">
                                </div>
                            </div>
                            <div class="columns is-gapless">
                                <div class="column">
                                    <div class="field">
                                        <div class="control">
                                            <input type="number" class="input" id="event-price" placeholder="<?php esc_attr_e( 'Full Price', 'vuelabs-framework' ); ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="column">
                                    <div class="field">
                                        <div class="control">
                                            <input type="number" class="input" id="event-discount" placeholder="<?php esc_attr_e( 'Discount %', 'vuelabs-framework' ); ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <textarea class="textarea" id="event-description" placeholder="<?php esc_attr_e( 'Description', 'vuelabs-framework' ); ?>"></textarea>
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input" id="event-location" placeholder="<?php esc_attr_e( 'Event Location', 'vuelabs-framework' ); ?>">
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input" id="event-address" placeholder="<?php esc_attr_e( 'Event Address', 'vuelabs-framework' ); ?>">
                                </div>
                            </div>
                            <div class="columns is-gapless">
                                <div class="column">
                                    <div class="field">
                                        <div class="control">
                                            <input type="text" class="input" id="event-date" placeholder="<?php esc_attr_e( 'Start Date', 'vuelabs-framework' ); ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="column">
                                    <div class="field">
                                        <div class="control">
                                            <input type="text" class="input" id="event-time" placeholder="<?php esc_attr_e( 'Event Time', 'vuelabs-framework' ); ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php

                            $html_content = ob_get_clean();

                            ob_start();

                            ?>

                            <div class="columns">
                                <div class="column has-text-left">
                                    <a href="#" class="navigator-popup__footer__back"><?php esc_html_e( 'Back', 'vuelabs-framework' ); ?></a>
                                </div>
                                <div class="column has-text-centered">
                                    <a href="#" class="button navigator-popup__footer__create-event"><?php esc_html_e( 'Create Event', 'vuelabs-framework' ); ?></a>
                                </div>
                                <div class="column has-text-right">
                                    <a href="#" class="navigator-popup__footer__close"><?php esc_html_e( 'Close', 'vuelabs-framework' ); ?></a>
                                </div>
                            </div>

                            <?php

                            $html_footer = ob_get_clean();

                            $return['popup_title'] = esc_html__( 'Add Event', 'vuelabs-framework' );
                            $return['popup_view_all'] = true;
                            $return['popup_new'] = false;

                            break;
                        case 'read':
                            break;
                        case 'update':

                            $event_id = intval( $data['post_id'] );
                            $image    = get_the_post_thumbnail_url( $event_id, 'product-image' );

                            $event    = wc_get_product( $event_id );

                            // These are already required so we don't have to check them.
                            $event_location = get_field( 'product_event_location', $event_id );
                            $event_address  = get_field( 'product_event_address', $event_id );
                            $event_date     = get_field( 'product_start_date', $event_id );
                            $event_time     = get_field( 'product_time', $event_id );

                            ob_start();

                            ?>

                            <div class="field is-image-field">
                                <label for="event-image" class="label">
                                    <input type="file" id="event-image" name="event-image">
                                    <span class="file-image" style="background-image: url( <?php echo esc_url( $image ); ?> );"></span>
                                    <span class="file-no-image is-hidden">
                                        <i class="fal fa-image"></i>
                                        <span class="file-no-image-text"><?php esc_html_e( 'Cover Photo', 'vuelabs-framework' ); ?></span>
                                    </span>
                                </label>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input" id="event-name" placeholder="<?php esc_attr_e( 'Event Name', 'vuelabs-framework' ); ?>" value="<?php echo esc_attr( $event->get_title() ); ?>">
                                </div>
                            </div>
                            <div class="columns is-gapless">
                                <div class="column">
                                    <div class="field">
                                        <div class="control">
                                            <input type="number" class="input" id="event-price" placeholder="<?php esc_attr_e( 'Full Price', 'vuelabs-framework' ); ?>" value="<?php echo esc_attr( $event->get_regular_price() ); ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="column">
                                    <div class="field">
                                        <div class="control">
                                            <input type="number" class="input" id="event-discount" placeholder="<?php esc_attr_e( 'Discount %', 'vuelabs-framework' ); ?>" value="<?php echo esc_attr( vlf_get_discount_number( $event->get_regular_price(), $event->get_sale_price() ) ); ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <textarea class="textarea" id="event-description" placeholder="<?php esc_attr_e( 'Description', 'vuelabs-framework' ); ?>"><?php echo esc_html( $event->get_description() ); ?></textarea>
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input" id="event-location" placeholder="<?php esc_attr_e( 'Event Location', 'vuelabs-framework' ); ?>" value="<?php echo esc_attr( $event_location ); ?>">
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input" id="event-address" placeholder="<?php esc_attr_e( 'Event Address', 'vuelabs-framework' ); ?>" value="<?php echo esc_attr( $event_address ); ?>">
                                </div>
                            </div>
                            <div class="columns is-gapless">
                                <div class="column">
                                    <div class="field">
                                        <div class="control">
                                            <input type="text" class="input" id="event-date" placeholder="<?php esc_attr_e( 'Start Date', 'vuelabs-framework' ); ?>" value="<?php echo esc_attr( $event_date ); ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="column">
                                    <div class="field">
                                        <div class="control">
                                            <input type="text" class="input" id="event-time" placeholder="<?php esc_attr_e( 'Event Time', 'vuelabs-framework' ); ?>" value="<?php echo esc_attr( $event_time ); ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php

                            $html_content = ob_get_clean();

                            ob_start();

                            ?>

                            <div class="columns">
                                <div class="column has-text-left">
                                    <a href="#" class="navigator-popup__footer__back"><?php esc_html_e( 'Back', 'vuelabs-framework' ); ?></a>
                                </div>
                                <div class="column has-text-centered">
                                    <a href="#" class="button navigator-popup__footer__update-event" data-post-id="<?php echo esc_attr( $event_id ); ?>"><?php esc_html_e( 'Update Event', 'vuelabs-framework' ); ?></a>
                                </div>
                                <div class="column has-text-right">
                                    <a href="#" class="navigator-popup__footer__close"><?php esc_html_e( 'Close', 'vuelabs-framework' ); ?></a>
                                </div>
                            </div>

                            <?php

                            $html_footer = ob_get_clean();

                            $return['popup_title'] = esc_html__( 'Edit Event', 'vuelabs-framework' );
                            $return['popup_view_all'] = true;
                            $return['popup_new'] = false;

                            break;
                        case 'all':

                            ob_start();

                            $events_args = [
                                'post_type'      => 'product',
                                'posts_per_page' => -1,
                                'post_status'    => 'any',
                                'author'         => intval( $user_id ),
                                'tax_query'      => [
                                    [
                                        'taxonomy' => 'product_type',
                                        'field'    => 'slug',
                                        'terms'    => 'vlf_event'
                                    ]
                                ]
                            ];
                            $events_loop = new WP_Query( $events_args );

                            if ( $events_loop->have_posts() ) {

                                ?>

                                <ul class="navigator-popup__content__list">
                                    <?php
                                        while ( $events_loop->have_posts() ) : $events_loop->the_post();

                                        $event_id     = get_the_ID();
                                        $event        = wc_get_product( $event_id );
                                        $event_status = get_post_status( $event_id );
                                    ?>

                                    <li class="navigator-popup__content__list__item" data-post-id="<?php echo esc_attr( $event_id ); ?>">
                                        <span class="navigator-popup__content__list__item__icon">
                                            <i class="fal fa-calendar fa-fw"></i>
                                        </span>
                                        <span class="navigator-popup__content__list__item__title"><?php echo esc_html( $event->get_title() ); ?></span>
                                        <a href="#" class="navigator-popup__content__list__item__actions-toggle"><i class="fal fa-ellipsis-v"></i></a>
                                        <ul class="navigator-popup__content__list__item__actions-list">
                                            <?php if ( $event_status !== 'publish' ) : ?>
                                                <li class="navigator-popup__content__list__item__actions-list__item">
                                                    <a href="#" class="button navigator-popup__content__list__item__actions-list__item__publish is-fullwidth"><?php esc_html_e( 'Publish', 'vuelabs-framework' ); ?></a>
                                                </li>
                                            <?php endif; ?>
                                            <li class="navigator-popup__content__list__item__actions-list__item">
                                                <a href="#" class="button navigator-popup__content__list__item__actions-list__item__edit is-fullwidth"><?php esc_html_e( 'Edit', 'vuelabs-framework' ); ?></a>
                                            </li>
                                            <li class="navigator-popup__content__list__item__actions-list__item">
                                                <a href="#" class="button navigator-popup__content__list__item__actions-list__item__delete is-fullwidth"><?php esc_html_e( 'Delete', 'vuelabs-framework' ); ?></a>
                                            </li>
                                        </ul>
                                    </li>

                                    <?php endwhile; wp_reset_postdata(); ?>
                                </ul>

                                <?php

                            } else {

                                ?>

                                <div class="navigator-popup__content__nothing"><?php esc_html_e( 'You don\'t have any events at the moment.', 'vuelabs-framework' ); ?></div>

                                <?php

                            }

                            $html_content = ob_get_clean();

                            ob_start();

                            ?>

                            <div class="columns">
                                <div class="column has-text-left">
                                    <a href="#" class="navigator-popup__footer__back"><?php esc_html_e( 'Back', 'vuelabs-framework' ); ?></a>
                                </div>
                                <div class="column has-text-right">
                                    <a href="#" class="navigator-popup__footer__close"><?php esc_html_e( 'Close', 'vuelabs-framework' ); ?></a>
                                </div>
                            </div>

                            <?php

                            $html_footer = ob_get_clean();

                            $return['popup_title'] = esc_html__( 'All Events', 'vuelabs-framework' );
                            $return['popup_view_all'] = false;
                            $return['popup_new'] = true;
                            $return['popup_new_title'] = esc_html__( 'New Event', 'vuelabs-framework' );

                            break;

                    }
                } elseif ( $popup_id === 'product' ) {
                    // Product Popup
                    switch ( $popup_type ) {

                        case 'create':

                            ob_start();

                            ?>

                            <div class="field is-image-field">
                                <label for="product-image" class="label">
                                    <input type="file" id="product-image" name="product-image">
                                    <span class="file-image is-hidden"></span>
                                    <span class="file-no-image">
                                        <i class="fal fa-image"></i>
                                        <span class="file-no-image-text"><?php esc_html_e( 'Cover Photo', 'vuelabs-framework' ); ?></span>
                                    </span>
                                </label>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input" id="product-name" placeholder="<?php esc_attr_e( 'Product Name', 'vuelabs-framework' ); ?>">
                                </div>
                            </div>
                            <div class="columns is-gapless">
                                <div class="column">
                                    <div class="field">
                                        <div class="control">
                                            <input type="number" class="input" id="product-price" placeholder="<?php esc_attr_e( 'Full Price', 'vuelabs-framework' ); ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="column">
                                    <div class="field">
                                        <div class="control">
                                            <input type="number" class="input" id="product-discount" placeholder="<?php esc_attr_e( 'Discount %', 'vuelabs-framework' ); ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <textarea class="textarea" id="product-description" placeholder="<?php esc_attr_e( 'Description', 'vuelabs-framework' ); ?>"></textarea>
                                </div>
                            </div>
                            <?php if ( ! is_wp_error( $product_categories ) ) : ?>
                            <div class="field">
                                <div class="control">
                                    <select id="product-category" class="is-select2" placeholder="<?php esc_attr_e( 'Category', 'vuelabs-framework' ); ?>">
                                        <option></option>
                                        <?php
                                        foreach ( $product_categories as $product_category ) {

                                            echo '<option value="' . esc_attr( $product_category->term_id ) . '">' . esc_html( $product_category->name ) . '</option>';

                                        }
                                        ?>
                                    </select>
                                    <script>
                                        jQuery(document).ready(function($) {
                                          'use strict';

                                          $('.is-select2').select2({
                                            placeholder: $('.is-select2').attr('placeholder'),
                                            minimumResultsForSearch: -1
                                          });
                                        });
                                    </script>
                                </div>
                            </div>
                            <?php endif; ?>

                            <?php

                            $html_content = ob_get_clean();

                            ob_start();

                            ?>

                            <div class="columns">
                                <div class="column has-text-left">
                                    <a href="#" class="navigator-popup__footer__back"><?php esc_html_e( 'Back', 'vuelabs-framework' ); ?></a>
                                </div>
                                <div class="column has-text-centered">
                                    <a href="#" class="button navigator-popup__footer__create-product"><?php esc_html_e( 'Create Product', 'vuelabs-framework' ); ?></a>
                                </div>
                                <div class="column has-text-right">
                                    <a href="#" class="navigator-popup__footer__close"><?php esc_html_e( 'Close', 'vuelabs-framework' ); ?></a>
                                </div>
                            </div>

                            <?php

                            $html_footer = ob_get_clean();

                            $return['popup_title'] = esc_html__( 'Add Product', 'vuelabs-framework' );
                            $return['popup_view_all'] = true;
                            $return['popup_new'] = false;

                            break;
                        case 'read':
                            break;
                        case 'update':

                            $product_id = intval( $data['post_id'] );
                            $image      = get_the_post_thumbnail_url( $product_id, 'product-image' );

                            $product    = wc_get_product( $product_id );

                            $product_categories_selected = get_the_terms( $product_id, 'product_cat' );
                            $product_categories_selected_ids = [];
                            foreach ( $product_categories_selected as $product_category_selected ) {

                                array_push( $product_categories_selected_ids, $product_category_selected->term_id );

                            }

                            ob_start();

                            ?>

                            <div class="field is-image-field">
                                <label for="product-image" class="label">
                                    <input type="file" id="product-image" name="product-image">
                                    <span class="file-image" style="background-image: url( <?php echo esc_url( $image ); ?> );"></span>
                                    <span class="file-no-image is-hidden">
                                        <i class="fal fa-image"></i>
                                        <span class="file-no-image-text"><?php esc_html_e( 'Cover Photo', 'vuelabs-framework' ); ?></span>
                                    </span>
                                </label>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input" id="product-name" placeholder="<?php esc_attr_e( 'Product Name', 'vuelabs-framework' ); ?>" value="<?php echo esc_attr( $product->get_name() ); ?>">
                                </div>
                            </div>
                            <div class="columns is-gapless">
                                <div class="column">
                                    <div class="field">
                                        <div class="control">
                                            <input type="number" class="input" id="product-price" placeholder="<?php esc_attr_e( 'Full Price', 'vuelabs-framework' ); ?>" value="<?php echo esc_attr( $product->get_regular_price() ); ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="column">
                                    <div class="field">
                                        <div class="control">
                                            <input type="number" class="input" id="product-discount" placeholder="<?php esc_attr_e( 'Discount %', 'vuelabs-framework' ); ?>" value="<?php echo esc_attr( vlf_get_discount_number( $product->get_regular_price(), $product->get_sale_price() ) ); ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <textarea class="textarea" id="product-description" placeholder="<?php esc_attr_e( 'Description', 'vuelabs-framework' ); ?>"><?php echo esc_html( $product->get_description() ); ?></textarea>
                                </div>
                            </div>
                            <?php if ( ! is_wp_error( $product_categories ) ) : ?>
                            <div class="field">
                                <div class="control">
                                    <select id="product-category" class="is-select2" placeholder="<?php esc_attr_e( 'Category', 'vuelabs-framework' ); ?>">
                                        <option></option>
                                        <?php
                                        foreach ( $product_categories as $product_category ) {

                                            if ( in_array( $product_category->term_id, $product_categories_selected_ids ) ) {

                                                $selected = 'selected';

                                            } else {

                                                $selected = '';

                                            }

                                            echo '<option value="' . esc_attr( $product_category->term_id ) . '" ' . esc_attr( $selected ) . '>' . esc_html( $product_category->name ) . '</option>';

                                        }
                                        ?>
                                    </select>
                                    <script>
                                      jQuery(document).ready(function($) {
                                        'use strict';

                                        $('.is-select2').select2({
                                          placeholder: $('.is-select2').attr('placeholder'),
                                          minimumResultsForSearch: -1
                                        });
                                      });
                                    </script>
                                </div>
                            </div>
                        <?php endif; ?>

                            <?php

                            $html_content = ob_get_clean();

                            ob_start();

                            ?>

                            <div class="columns">
                                <div class="column has-text-left">
                                    <a href="#" class="navigator-popup__footer__back"><?php esc_html_e( 'Back', 'vuelabs-framework' ); ?></a>
                                </div>
                                <div class="column has-text-centered">
                                    <a href="#" class="button navigator-popup__footer__update-product" data-post-id="<?php echo esc_attr( $product_id ); ?>"><?php esc_html_e( 'Update Product', 'vuelabs-framework' ); ?></a>
                                </div>
                                <div class="column has-text-right">
                                    <a href="#" class="navigator-popup__footer__close"><?php esc_html_e( 'Close', 'vuelabs-framework' ); ?></a>
                                </div>
                            </div>

                            <?php

                            $html_footer = ob_get_clean();

                            $return['popup_title'] = esc_html__( 'Edit Product', 'vuelabs-framework' );
                            $return['popup_view_all'] = true;
                            $return['popup_new'] = false;

                            break;
                        case 'all':

                            ob_start();

                            $products_args = [
                                'post_type'      => 'product',
                                'posts_per_page' => -1,
                                'post_status'    => 'any',
                                'author'         => intval( $user_id ),
                                'tax_query'      => [
                                    [
                                        'taxonomy' => 'product_type',
                                        'field'    => 'slug',
                                        'terms'    => 'simple'
                                    ]
                                ]
                            ];
                            $products_loop = new WP_Query( $products_args );

                            if ( $products_loop->have_posts() ) {

                                ?>

                                <ul class="navigator-popup__content__list">
                                    <?php
                                    while ( $products_loop->have_posts() ) : $products_loop->the_post();

                                        $product_id     = get_the_ID();
                                        $product        = wc_get_product( $product_id );
                                        $product_status = get_post_status( $product_id );
                                        ?>

                                        <li class="navigator-popup__content__list__item" data-post-id="<?php echo esc_attr( $product_id ); ?>">
                                        <span class="navigator-popup__content__list__item__icon">
                                            <i class="fal fa-box-usd fa-fw"></i>
                                        </span>
                                            <span class="navigator-popup__content__list__item__title"><?php echo esc_html( $product->get_title() ); ?></span>
                                            <a href="#" class="navigator-popup__content__list__item__actions-toggle"><i class="fal fa-ellipsis-v"></i></a>
                                            <ul class="navigator-popup__content__list__item__actions-list">
                                                <?php if ( $product_status !== 'publish' ) : ?>
                                                    <li class="navigator-popup__content__list__item__actions-list__item">
                                                        <a href="#" class="button navigator-popup__content__list__item__actions-list__item__publish is-fullwidth"><?php esc_html_e( 'Publish', 'vuelabs-framework' ); ?></a>
                                                    </li>
                                                <?php endif; ?>
                                                <li class="navigator-popup__content__list__item__actions-list__item">
                                                    <a href="#" class="button navigator-popup__content__list__item__actions-list__item__edit is-fullwidth"><?php esc_html_e( 'Edit', 'vuelabs-framework' ); ?></a>
                                                </li>
                                                <li class="navigator-popup__content__list__item__actions-list__item">
                                                    <a href="#" class="button navigator-popup__content__list__item__actions-list__item__delete is-fullwidth"><?php esc_html_e( 'Delete', 'vuelabs-framework' ); ?></a>
                                                </li>
                                            </ul>
                                        </li>

                                    <?php endwhile; wp_reset_postdata(); ?>
                                </ul>

                                <?php

                            } else {

                                ?>

                                <div class="navigator-popup__content__nothing"><?php esc_html_e( 'You don\'t have any products at the moment.', 'vuelabs-framework' ); ?></div>

                                <?php

                            }

                            $html_content = ob_get_clean();

                            ob_start();

                            ?>

                            <div class="columns">
                                <div class="column has-text-left">
                                    <a href="#" class="navigator-popup__footer__back"><?php esc_html_e( 'Back', 'vuelabs-framework' ); ?></a>
                                </div>
                                <div class="column has-text-right">
                                    <a href="#" class="navigator-popup__footer__close"><?php esc_html_e( 'Close', 'vuelabs-framework' ); ?></a>
                                </div>
                            </div>

                            <?php

                            $html_footer = ob_get_clean();

                            $return['popup_title'] = esc_html__( 'All Products', 'vuelabs-framework' );
                            $return['popup_view_all'] = false;
                            $return['popup_new'] = true;
                            $return['popup_new_title'] = esc_html__( 'New Product', 'vuelabs-framework' );

                            break;

                    }
                } elseif ( $popup_id === 'post' ) {
                    // Post Popup
                    switch ( $popup_type ) {

                        case 'create':

                            ob_start();

                            ?>

                            <div class="field is-image-field">
                                <label for="post-image" class="label">
                                    <input type="file" id="post-image" name="post-image">
                                    <span class="file-image is-hidden"></span>
                                    <span class="file-no-image">
                                        <i class="fal fa-image"></i>
                                        <span class="file-no-image-text"><?php esc_html_e( 'Cover Photo', 'vuelabs-framework' ); ?></span>
                                    </span>
                                </label>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input" id="post-name" placeholder="<?php esc_attr_e( 'Post Name', 'vuelabs-framework' ); ?>">
                                </div>
                            </div>

                            <div class="field is-quill">
                                <div class="control">
                                    <div id="post-description" class="post-description"></div>
                                </div>
                            </div>
                            <?php if ( ! is_wp_error( $post_categories ) ) : ?>
                            <div class="field">
                                <div class="control">
                                    <select id="post-category" class="is-select2" placeholder="<?php esc_attr_e( 'Category', 'vuelabs-framework' ); ?>">
                                        <option></option>
                                        <?php
                                        foreach ( $post_categories as $post_category ) {

                                            echo '<option value="' . esc_attr( $post_category->term_id ) . '">' . esc_html( $post_category->name ) . '</option>';

                                        }
                                        ?>
                                    </select>
                                    <script>
                                      jQuery(document).ready(function($) {
                                        'use strict';

                                        $('.is-select2').select2({
                                          placeholder: $('.is-select2').attr('placeholder'),
                                          minimumResultsForSearch: -1
                                        });
                                      });
                                    </script>
                                </div>
                            </div>
                        <?php endif; ?>

                            <?php

                            $html_content = ob_get_clean();

                            ob_start();

                            ?>

                            <div class="columns">
                                <div class="column has-text-left">
                                    <a href="#" class="navigator-popup__footer__back"><?php esc_html_e( 'Back', 'vuelabs-framework' ); ?></a>
                                </div>
                                <div class="column has-text-centered">
                                    <a href="#" class="button navigator-popup__footer__create-post"><?php esc_html_e( 'Create Post', 'vuelabs-framework' ); ?></a>
                                </div>
                                <div class="column has-text-right">
                                    <a href="#" class="navigator-popup__footer__close"><?php esc_html_e( 'Close', 'vuelabs-framework' ); ?></a>
                                </div>
                            </div>

                            <?php

                            $html_footer = ob_get_clean();

                            $return['popup_title'] = esc_html__( 'Add Post', 'vuelabs-framework' );
                            $return['popup_view_all'] = true;
                            $return['popup_new'] = false;

                            break;
                        case 'read':
                            break;
                        case 'update':

                            $post_id = intval( $data['post_id'] );
                            $image   = get_the_post_thumbnail_url( $post_id, 'product-image' );

                            $post_categories_selected = get_the_terms( $post_id, 'category' );
                            $post_categories_selected_ids = [];
                            foreach ( $post_categories_selected as $post_category_selected ) {

                                array_push( $post_categories_selected_ids, $post_category_selected->term_id );

                            }

                            ob_start();

                            ?>

                            <div class="field is-image-field">
                                <label for="post-image" class="label">
                                    <input type="file" id="post-image" name="post-image">
                                    <span class="file-image" style="background-image: url( <?php echo esc_url( $image ); ?> );"></span>
                                    <span class="file-no-image is-hidden">
                                        <i class="fal fa-image"></i>
                                        <span class="file-no-image-text"><?php esc_html_e( 'Cover Photo', 'vuelabs-framework' ); ?></span>
                                    </span>
                                </label>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input" id="post-name" placeholder="<?php esc_attr_e( 'Post Name', 'vuelabs-framework' ); ?>" value="<?php echo esc_attr( get_the_title( $post_id ) ); ?>">
                                </div>
                            </div>

                            <div class="field is-quill">
                                <div class="control">
                                    <div id="post-description" class="post-description"><?php echo apply_filters( 'the_content', get_post_field( 'post_content', $post_id ) ); ?></div>
                                </div>
                            </div>
                            <?php if ( ! is_wp_error( $post_categories ) ) : ?>
                            <div class="field">
                                <div class="control">
                                    <select id="post-category" class="is-select2" placeholder="<?php esc_attr_e( 'Category', 'vuelabs-framework' ); ?>">
                                        <option></option>
                                        <?php
                                        foreach ( $post_categories as $post_category ) {

                                            if ( in_array( $post_category->term_id, $post_categories_selected_ids ) ) {

                                                $selected = 'selected';

                                            } else {

                                                $selected = '';

                                            }

                                            echo '<option value="' . esc_attr( $post_category->term_id ) . '" ' . esc_attr( $selected ) . '>' . esc_html( $post_category->name ) . '</option>';

                                        }
                                        ?>
                                    </select>
                                    <script>
                                      jQuery(document).ready(function($) {
                                        'use strict';

                                        $('.is-select2').select2({
                                          placeholder: $('.is-select2').attr('placeholder'),
                                          minimumResultsForSearch: -1
                                        });
                                      });
                                    </script>
                                </div>
                            </div>
                        <?php endif; ?>

                            <?php

                            $html_content = ob_get_clean();

                            ob_start();

                            ?>

                            <div class="columns">
                                <div class="column has-text-left">
                                    <a href="#" class="navigator-popup__footer__back"><?php esc_html_e( 'Back', 'vuelabs-framework' ); ?></a>
                                </div>
                                <div class="column has-text-centered">
                                    <a href="#" class="button navigator-popup__footer__update-post" data-post-id="<?php echo esc_attr( $post_id ); ?>"><?php esc_html_e( 'Create Post', 'vuelabs-framework' ); ?></a>
                                </div>
                                <div class="column has-text-right">
                                    <a href="#" class="navigator-popup__footer__close"><?php esc_html_e( 'Close', 'vuelabs-framework' ); ?></a>
                                </div>
                            </div>

                            <?php

                            $html_footer = ob_get_clean();

                            $return['popup_title'] = esc_html__( 'Add Post', 'vuelabs-framework' );
                            $return['popup_view_all'] = true;
                            $return['popup_new'] = false;

                            break;
                        case 'all':

                            ob_start();

                            $post_args = [
                                'post_type'      => 'post',
                                'posts_per_page' => -1,
                                'post_status'    => 'any',
                                'author'         => intval( $user_id )
                            ];
                            $post_loop = new WP_Query( $post_args );

                            if ( $post_loop->have_posts() ) {

                                ?>

                                <ul class="navigator-popup__content__list">
                                    <?php
                                    while ( $post_loop->have_posts() ) : $post_loop->the_post();

                                        $post_id     = get_the_ID();
                                        $post_status = get_post_status( $post_id );
                                        ?>

                                        <li class="navigator-popup__content__list__item" data-post-id="<?php echo esc_attr( $post_id ); ?>">
                                        <span class="navigator-popup__content__list__item__icon">
                                            <i class="fal fa-leaf fa-fw"></i>
                                        </span>
                                            <span class="navigator-popup__content__list__item__title"><?php echo esc_html( get_the_title( $post_id ) ); ?></span>
                                            <a href="#" class="navigator-popup__content__list__item__actions-toggle"><i class="fal fa-ellipsis-v"></i></a>
                                            <ul class="navigator-popup__content__list__item__actions-list">
                                                <?php if ( $post_status !== 'publish' ) : ?>
                                                    <li class="navigator-popup__content__list__item__actions-list__item">
                                                        <a href="#" class="button navigator-popup__content__list__item__actions-list__item__publish is-fullwidth"><?php esc_html_e( 'Publish', 'vuelabs-framework' ); ?></a>
                                                    </li>
                                                <?php endif; ?>
                                                <li class="navigator-popup__content__list__item__actions-list__item">
                                                    <a href="#" class="button navigator-popup__content__list__item__actions-list__item__edit is-fullwidth"><?php esc_html_e( 'Edit', 'vuelabs-framework' ); ?></a>
                                                </li>
                                                <li class="navigator-popup__content__list__item__actions-list__item">
                                                    <a href="#" class="button navigator-popup__content__list__item__actions-list__item__delete is-fullwidth"><?php esc_html_e( 'Delete', 'vuelabs-framework' ); ?></a>
                                                </li>
                                            </ul>
                                        </li>

                                    <?php endwhile; wp_reset_postdata(); ?>
                                </ul>

                                <?php

                            } else {

                                ?>

                                <div class="navigator-popup__content__nothing"><?php esc_html_e( 'You don\'t have any posts at the moment.', 'vuelabs-framework' ); ?></div>

                                <?php

                            }

                            $html_content = ob_get_clean();

                            ob_start();

                            ?>

                            <div class="columns">
                                <div class="column has-text-left">
                                    <a href="#" class="navigator-popup__footer__back"><?php esc_html_e( 'Back', 'vuelabs-framework' ); ?></a>
                                </div>
                                <div class="column has-text-right">
                                    <a href="#" class="navigator-popup__footer__close"><?php esc_html_e( 'Close', 'vuelabs-framework' ); ?></a>
                                </div>
                            </div>

                            <?php

                            $html_footer = ob_get_clean();

                            $return['popup_title'] = esc_html__( 'All Posts', 'vuelabs-framework' );
                            $return['popup_view_all'] = false;
                            $return['popup_new'] = true;
                            $return['popup_new_title'] = esc_html__( 'New Post', 'vuelabs-framework' );

                            break;

                    }
                } elseif ( $popup_id === 'network' ) {

                    // Post Popup
                    switch ( $popup_type ) {

                        case 'create':

                            ob_start();

                            ?>

                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input" id="network-name" placeholder="<?php esc_attr_e( 'Network Name', 'vuelabs-framework' ); ?>">
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <select name="network-members[]" id="network-members" multiple="multiple" class="is-select2" placeholder="<?php esc_attr_e( 'Network Members', 'vuelabs-framework' ); ?>">
                                        <option></option>
                                    </select>
                                </div>
                            </div>

                            <?php

                            $html_content = ob_get_clean();

                            ob_start();

                            ?>

                            <div class="columns">
                                <div class="column has-text-left">
                                    <a href="#" class="navigator-popup__footer__back"><?php esc_html_e( 'Back', 'vuelabs-framework' ); ?></a>
                                </div>
                                <div class="column has-text-centered">
                                    <a href="#" class="button navigator-popup__footer__create-network"><?php esc_html_e( 'Create Network', 'vuelabs-framework' ); ?></a>
                                </div>
                                <div class="column has-text-right">
                                    <a href="#" class="navigator-popup__footer__close"><?php esc_html_e( 'Close', 'vuelabs-framework' ); ?></a>
                                </div>
                            </div>

                            <?php

                            $html_footer = ob_get_clean();

                            $return['popup_title'] = esc_html__( 'Create Network', 'vuelabs-framework' );
                            $return['popup_view_all'] = true;
                            $return['popup_new'] = false;

                            break;

                        case 'read':
                            break;

                        case 'update':

                            $chapter_id = $data['post_id'];
                            $term       = get_term( intval( $chapter_id ), 'brand_organization' );

                            $brands_args = [
                                'post_type' => 'brand',
                                'posts_per_page' => -1,
                                'post_status'    => 'any',
                                'tax_query'      => [
                                    [
                                        'taxonomy' => 'brand_organization',
                                        'field'    => 'term_id',
                                        'terms'    => intval( $chapter_id )
                                    ]
                                ]
                            ];
                            $brands_query = new WP_Query( $brands_args );
                            $brands = [];

                            ob_start();

                            ?>

                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input" id="network-name" placeholder="<?php esc_attr_e( 'Network Name', 'vuelabs-framework' ); ?>" value="<?php echo esc_attr( $term->name ); ?>">
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <select name="network-members[]" id="network-members" multiple="multiple" class="is-select2" placeholder="<?php esc_attr_e( 'Network Members', 'vuelabs-framework' ); ?>">
                                        <option></option>
                                        <?php
                                        if ( $brands_query->have_posts() ) {

                                            while ( $brands_query->have_posts() ) : $brands_query->the_post();

                                            ?>

                                                <option value="<?php echo esc_attr( get_the_ID() ); ?>"><?php echo esc_html( get_the_title() ); ?></option>

                                            <?php

                                            array_push( $brands, get_the_ID() );

                                            endwhile; wp_reset_postdata();

                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <?php

                            $html_content = ob_get_clean();

                            ob_start();

                            ?>

                            <div class="columns">
                                <div class="column has-text-left">
                                    <a href="#" class="navigator-popup__footer__back"><?php esc_html_e( 'Back', 'vuelabs-framework' ); ?></a>
                                </div>
                                <div class="column has-text-centered">
                                    <a href="#" class="button navigator-popup__footer__create-network"><?php esc_html_e( 'Create Network', 'vuelabs-framework' ); ?></a>
                                </div>
                                <div class="column has-text-right">
                                    <a href="#" class="navigator-popup__footer__close"><?php esc_html_e( 'Close', 'vuelabs-framework' ); ?></a>
                                </div>
                            </div>

                            <?php

                            $html_footer = ob_get_clean();

                            $return['popup_title'] = esc_html__( 'Edit Network', 'vuelabs-framework' );
                            $return['popup_view_all'] = true;
                            $return['popup_new'] = false;
                            $return['brands'] = $brands;

                            break;

                        case 'all':

                            $brand_id        = vlf_get_brand_id_from_author_id( $user_id );
                            $organization    = get_the_terms( $brand_id, 'brand_organization' );
                            $organization_id = 0;
                            foreach ( $organization as $org ) {

                                if ( $org->parent === 0 ) {

                                    $organization_id = $org->term_id;

                                }

                            }
                            $chapters        = get_term_children( intval( $organization_id ), 'brand_organization' );

                            ob_start();

                            if ( isset( $chapters ) && ! empty( $chapters ) ) {

                                ?>

                                <ul class="navigator-popup__content__list">
                                    <?php
                                    foreach ( $chapters as $chapter ) {

                                        $post_id = $chapter;
                                        $term    = get_term( intval( $chapter ), 'brand_organization' );
                                        ?>

                                        <li class="navigator-popup__content__list__item" data-post-id="<?php echo esc_attr( $post_id ); ?>">
                                        <span class="navigator-popup__content__list__item__icon">
                                            <i class="fal fa-users fa-fw"></i>
                                        </span>
                                            <span class="navigator-popup__content__list__item__title"><?php echo esc_html( $term->name ); ?></span>
                                            <a href="#" class="navigator-popup__content__list__item__actions-toggle"><i class="fal fa-ellipsis-v"></i></a>
                                            <ul class="navigator-popup__content__list__item__actions-list">
                                                <li class="navigator-popup__content__list__item__actions-list__item">
                                                    <a href="#" class="button navigator-popup__content__list__item__actions-list__item__edit is-fullwidth"><?php esc_html_e( 'Edit', 'vuelabs-framework' ); ?></a>
                                                </li>
                                                <li class="navigator-popup__content__list__item__actions-list__item">
                                                    <a href="#" class="button navigator-popup__content__list__item__actions-list__item__delete is-fullwidth"><?php esc_html_e( 'Delete', 'vuelabs-framework' ); ?></a>
                                                </li>
                                            </ul>
                                        </li>

                                    <?php } ?>
                                </ul>

                                <?php

                            } else {

                                ?>

                                <div class="navigator-popup__content__nothing"><?php esc_html_e( 'You don\'t have any networks at the moment.', 'vuelabs-framework' ); ?></div>

                                <?php

                            }

                            $html_content = ob_get_clean();

                            ob_start();

                            ?>

                            <div class="columns">
                                <div class="column has-text-left">
                                    <a href="#" class="navigator-popup__footer__back"><?php esc_html_e( 'Back', 'vuelabs-framework' ); ?></a>
                                </div>
                                <div class="column has-text-right">
                                    <a href="#" class="navigator-popup__footer__close"><?php esc_html_e( 'Close', 'vuelabs-framework' ); ?></a>
                                </div>
                            </div>

                            <?php

                            $html_footer = ob_get_clean();

                            $return['popup_title'] = esc_html__( 'All Networks', 'vuelabs-framework' );
                            $return['popup_view_all'] = false;
                            $return['popup_new'] = true;
                            $return['popup_new_title'] = esc_html__( 'New Network', 'vuelabs-framework' );

                            break;
                    }

                }

                $return['popup_content'] = $html_content;
                $return['popup_footer'] = $html_footer;

                wp_send_json_success( $return );

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Publish item from navigator.
         */
        public function vlf_navigator_publish_item()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $updated = wp_update_post( [
                    'ID'          => $data['post_id'],
                    'post_status' => 'publish'
                ] );

                if ( ! is_wp_error( $updated ) ) {

                    wp_send_json_success();

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Delete item from navigator.
         */
        public function vlf_navigator_delete_item()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $attachments = get_attached_media( 'image', $data['post_id'] );
                if ( isset( $attachments ) && ! empty( $attachments ) ) {

                    foreach ( $attachments as $attachment ) {

                        wp_delete_attachment( $attachment->ID, true );

                    }

                }

                $deleted = wp_delete_post( $data['post_id'], true );
                if ( isset( $deleted ) && ! empty( $deleted ) ) {

                    wp_send_json_success();

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Create/update event from navigator.
         */
        public function vlf_navigator_cu_event()
        {

            $return = vlf_check_nonce( $this->gump, array_merge( $_POST, $_FILES ), 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $validation_rules = [
                    'name'        => 'required',
                    'price'       => 'required|float',
                    'discount'    => 'float',
                    'description' => 'required',
                    'location'    => 'required',
                    'address'     => 'required',
                    'date'        => 'required',
                    'time'        => 'required',
                    'user_id'     => 'required|integer'
                ];

                $filter_rules = [
                    'name'        => 'trim|sanitize_string',
                    'price'       => 'trim|sanitize_floats',
                    'discount'    => 'trim|sanitize_floats',
                    'description' => 'trim|sanitize_string',
                    'location'    => 'trim|sanitize_string',
                    'address'     => 'trim|sanitize_string',
                    'date'        => 'trim|sanitize_string',
                    'time'        => 'trim|sanitize_string',
                    'user_id'     => 'trim|sanitize_numbers'
                ];

                if ( $data['type'] !== 'update' ) {

                    $validation_rules['image'] = 'required_file|extension,png;jpg;jpeg';
                    $this->gump->set_field_name( 'image', esc_html__( 'Cover Photo', 'vuelabs-framework' ) );

                } else {

                    $validation_rules['post_id'] = 'required|integer';
                    $filter_rules['post_id'] = 'trim|sanitize_numbers';
                    $this->gump->set_field_name( 'post_id', esc_html__( 'Event ID', 'vuelabs-framework' ) );

                }

                $this->gump->set_field_name( 'name', esc_html__( 'Event Name', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'price', esc_html__( 'Full Price', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'discount', esc_html__( 'Discount %', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'description', esc_html__( 'Description', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'location', esc_html__( 'Event Location', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'address', esc_html__( 'Event Address', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'date', esc_html__( 'Start Date', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'time', esc_html__( 'Event Time', 'vuelabs-framework' ) );

                $this->gump->validation_rules( $validation_rules );
                $this->gump->filter_rules( $filter_rules );

                $data = $this->gump->run( $data );

                if ( $data === false ) {

                    wp_send_json_error( [
                        'code' => '2',
                        'data' => $this->gump->get_readable_errors()
                    ] );

                } else {

                    if ( $data['type'] !== 'update' ) {

                        // Create the event.
                        $inserted = wp_insert_post( [
                            'post_author'  => $data['user_id'],
                            'post_content' => $data['description'],
                            'post_title'   => $data['name'],
                            'post_status'  => 'private',
                            'post_type'    => 'product'
                        ] );

                        if ( ! is_wp_error( $inserted ) ) {

                            // Let's add other stuff.
                            // Featured Image:
                            vlf_upload_image( $data['image'], $inserted, '', 'yes', 'no' );

                            // Type
                            wp_set_object_terms( $inserted, 'vlf_event', 'product_type' );

                            // Regular Price
                            update_post_meta( $inserted, '_regular_price', $data['price'] );

                            // Discount Price
                            update_post_meta( $inserted, '_sale_price', vlf_set_discount_price( $data['price'], $data['discount'] ) );

                            // General Price
                            update_post_meta( $inserted, '_price', vlf_set_discount_price( $data['price'], $data['discount'] ) );

                            // Meta Fields
                            update_field( 'product_event_location', $data['location'], $inserted );
                            update_field( 'product_event_address', $data['address'], $inserted );
                            update_field( 'product_start_date', $data['date'], $inserted );
                            update_field( 'product_time', $data['time'], $inserted );

                            // Tag
                            $tags = new WP_Term_Query( [
                                'taxonomy' => 'product_tag',
                                'meta_query' => [
                                    [
                                        'key'   => 'vlf_products_vendor',
                                        'value' => $data['user']
                                    ]
                                ]
                            ] );
                            $tags = $tags->get_terms();
                            if ( isset( $tags ) && ! empty( $tags ) ) {


                                $organization_tag =  $tags[0]->term_id;
                                wp_set_object_terms( $inserted, intval( $organization_tag ), 'product_tag' );

                            }

                            // Return Preview
                            ob_start();

                            ?>

                            <div class="preview-popup" data-popup-id="event" data-post-id="<?php echo esc_attr( $inserted ); ?>">
                                <a href="#" class="preview-popup__close"><i class="fal fa-times"></i></a>
                                <header class="preview-popup__header">
                                    <div class="preview-popup__header__title"><?php esc_html_e( 'Your event has been created and added to your dashboard.', 'vuelabs-framework' ); ?></div>
                                </header>
                                <div class="preview-popup__content">
                                    <?php vlf_display_product( $inserted, 'preview-page' ); ?>
                                </div>
                                <footer class="preview-popup__footer">
                                    <div class="columns">
                                        <div class="column has-text-left">
                                            <a href="<?php echo esc_url( get_permalink( $inserted ) ); ?>" class="preview-popup__footer__view"><?php esc_html_e( 'View', 'vuelabs-framework' ); ?></a>
                                        </div>
                                        <div class="column has-text-centered">
                                            <a href="#" class="preview-popup__footer__edit"><?php esc_html_e( 'Edit', 'vuelabs-framework' ); ?></a>
                                        </div>
                                        <div class="column has-text-right">
                                            <a href="#" class="preview-popup__footer__close"><?php esc_html_e( 'Close', 'vuelabs-framework' ); ?></a>
                                        </div>
                                    </div>
                                </footer>
                            </div>

                            <?php

                            $html = ob_get_clean();

                            wp_send_json_success( $html );

                        }

                    } else {

                        $updated = wp_update_post( [
                            'ID'           => intval( $data['post_id'] ),
                            'post_content' => $data['description'],
                            'post_title'   => $data['name'],
                            'post_status'  => 'private',
                            'post_type'    => 'product'
                        ] );

                        if ( ! is_wp_error( $updated ) ) {

                            // Let's add other stuff.
                            // Featured Image:
                            if ( isset( $data['image'] ) && ! empty( $data['image'] ) ) {

                                vlf_upload_image( $data['image'], $updated, '', 'yes', 'no' );

                            }

                            // Type
                            wp_set_object_terms( $updated, 'vlf_event', 'product_type' );

                            // Regular Price
                            update_post_meta( $updated, '_regular_price', $data['price'] );

                            // Discount Price
                            update_post_meta( $updated, '_sale_price', vlf_set_discount_price( $data['price'], $data['discount'] ) );

                            // General Price
                            update_post_meta( $updated, '_price', vlf_set_discount_price( $data['price'], $data['discount'] ) );

                            // Meta Fields
                            update_field( 'product_event_location', $data['location'], $updated );
                            update_field( 'product_event_address', $data['address'], $updated );
                            update_field( 'product_start_date', $data['date'], $updated );
                            update_field( 'product_time', $data['time'], $updated );

                            // Return Preview
                            ob_start();

                            ?>

                            <div class="preview-popup" data-popup-id="event" data-post-id="<?php echo esc_attr( $updated ); ?>">
                                <a href="#" class="preview-popup__close"><i class="fal fa-times"></i></a>
                                <header class="preview-popup__header">
                                    <div class="preview-popup__header__title"><?php esc_html_e( 'Your event has been updated and added to your dashboard.', 'vuelabs-framework' ); ?></div>
                                </header>
                                <div class="preview-popup__content">
                                    <?php vlf_display_product( $updated, 'preview-page' ); ?>
                                </div>
                                <footer class="preview-popup__footer">
                                    <div class="columns">
                                        <div class="column has-text-left">
                                            <a href="<?php echo esc_url( get_permalink( $updated ) ); ?>" class="preview-popup__footer__view"><?php esc_html_e( 'View', 'vuelabs-framework' ); ?></a>
                                        </div>
                                        <div class="column has-text-centered">
                                            <a href="#" class="preview-popup__footer__edit"><?php esc_html_e( 'Edit', 'vuelabs-framework' ); ?></a>
                                        </div>
                                        <div class="column has-text-right">
                                            <a href="#" class="preview-popup__footer__close"><?php esc_html_e( 'Close', 'vuelabs-framework' ); ?></a>
                                        </div>
                                    </div>
                                </footer>
                            </div>

                            <?php

                            $html = ob_get_clean();

                            wp_send_json_success( $html );

                        }

                    }

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Create/update product from navigator.
         */
        public function vlf_navigator_cu_product()
        {

            $return = vlf_check_nonce( $this->gump, array_merge( $_POST, $_FILES ), 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $validation_rules = [
                    'name'        => 'required',
                    'price'       => 'required|float',
                    'discount'    => 'required|float',
                    'description' => 'required',
                    'category'    => 'required',
                    'user_id'     => 'required|integer'
                ];

                $filter_rules = [
                    'name'        => 'trim|sanitize_string',
                    'price'       => 'trim|sanitize_floats',
                    'discount'    => 'trim|sanitize_floats',
                    'description' => 'trim|sanitize_string',
                    'category'    => 'trim',
                    'user_id'     => 'trim|sanitize_numbers'
                ];

                if ( $data['type'] !== 'update' ) {

                    $validation_rules['image'] = 'required_file|extension,png;jpg;jpeg';
                    $this->gump->set_field_name( 'image', esc_html__( 'Cover Photo', 'vuelabs-framework' ) );

                } else {

                    $validation_rules['post_id'] = 'required|integer';
                    $filter_rules['post_id'] = 'trim|sanitize_numbers';
                    $this->gump->set_field_name( 'post_id', esc_html__( 'Event ID', 'vuelabs-framework' ) );

                }

                $this->gump->set_field_name( 'name', esc_html__( 'Product Name', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'price', esc_html__( 'Full Price', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'discount', esc_html__( 'Discount %', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'category', esc_html__( 'Category', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'description', esc_html__( 'Description', 'vuelabs-framework' ) );

                $this->gump->validation_rules( $validation_rules );
                $this->gump->filter_rules( $filter_rules );

                $data = $this->gump->run( $data );

                if ( $data === false ) {

                    wp_send_json_error( [
                        'code' => '2',
                        'data' => $this->gump->get_readable_errors()
                    ] );

                } else {

                    if ( $data['type'] !== 'update' ) {

                        // Create the product.
                        $inserted = wp_insert_post( [
                            'post_author'  => $data['user_id'],
                            'post_content' => $data['description'],
                            'post_title'   => $data['name'],
                            'post_status'  => 'private',
                            'post_type'    => 'product'
                        ] );

                        if ( ! is_wp_error( $inserted ) ) {

                            // Let's add other stuff.
                            // Featured Image:
                            vlf_upload_image( $data['image'], $inserted, '', 'yes', 'no' );

                            // Category
                            wp_set_object_terms( $inserted, intval( $data['category'] ), 'product_cat' );
                            wp_set_object_terms( $inserted, 'simple', 'product_type' );

                            // Regular Price
                            update_post_meta( $inserted, '_regular_price', $data['price'] );

                            // Discount Price
                            update_post_meta( $inserted, '_sale_price', vlf_set_discount_price( $data['price'], $data['discount'] ) );

                            // Tag
                            $tags = new WP_Term_Query( [
                                'taxonomy' => 'product_tag',
                                'meta_query' => [
                                    [
                                        'key'   => 'vlf_products_vendor',
                                        'value' => $data['user']
                                    ]
                                ]
                            ] );
                            $tags = $tags->get_terms();
                            if ( isset( $tags ) && ! empty( $tags ) ) {


                                $organization_tag =  $tags[0]->term_id;
                                wp_set_object_terms( $inserted, intval( $organization_tag ), 'product_tag' );

                            }

                            // Return Preview
                            ob_start();

                            ?>

                            <div class="preview-popup" data-popup-id="product" data-post-id="<?php echo esc_attr( $inserted ); ?>">
                                <a href="#" class="preview-popup__close"><i class="fal fa-times"></i></a>
                                <header class="preview-popup__header">
                                    <div class="preview-popup__header__title"><?php esc_html_e( 'Your product has been created and added to your dashboard.', 'vuelabs-framework' ); ?></div>
                                </header>
                                <div class="preview-popup__content">
                                    <?php vlf_display_product( $inserted, 'preview-page' ); ?>
                                </div>
                                <footer class="preview-popup__footer">
                                    <div class="columns">
                                        <div class="column has-text-left">
                                            <a href="<?php echo esc_url( get_permalink( $inserted ) ); ?>" class="preview-popup__footer__view"><?php esc_html_e( 'View', 'vuelabs-framework' ); ?></a>
                                        </div>
                                        <div class="column has-text-centered">
                                            <a href="#" class="preview-popup__footer__edit"><?php esc_html_e( 'Edit', 'vuelabs-framework' ); ?></a>
                                        </div>
                                        <div class="column has-text-right">
                                            <a href="#" class="preview-popup__footer__close"><?php esc_html_e( 'Close', 'vuelabs-framework' ); ?></a>
                                        </div>
                                    </div>
                                </footer>
                            </div>

                            <?php

                            $html = ob_get_clean();

                            wp_send_json_success( $html );

                        }

                    } else {

                        // Update the product.
                        $updated = wp_update_post( [
                            'ID'           => intval( $data['post_id'] ),
                            'post_content' => $data['description'],
                            'post_title'   => $data['name'],
                            'post_status'  => 'private',
                            'post_type'    => 'product'
                        ] );

                        if ( ! is_wp_error( $updated ) ) {

                            // Let's add other stuff.
                            // Featured Image:
                            if ( isset( $data['image'] ) && ! empty( $data['image'] ) ) {

                                vlf_upload_image( $data['image'], $updated, '', 'yes', 'no' );

                            }

                            // Category
                            wp_set_object_terms( $updated, intval( $data['category'] ), 'product_cat' );
                            wp_set_object_terms( $updated, 'simple', 'product_type' );

                            // Regular Price
                            update_post_meta( $updated, '_regular_price', $data['price'] );

                            // Discount Price
                            update_post_meta( $updated, '_sale_price', vlf_set_discount_price( $data['price'], $data['discount'] ) );

                            // Return Preview
                            ob_start();

                            ?>

                            <div class="preview-popup" data-popup-id="product" data-post-id="<?php echo esc_attr( $updated ); ?>">
                                <a href="#" class="preview-popup__close"><i class="fal fa-times"></i></a>
                                <header class="preview-popup__header">
                                    <div class="preview-popup__header__title"><?php esc_html_e( 'Your product has been updated and added to your dashboard.', 'vuelabs-framework' ); ?></div>
                                </header>
                                <div class="preview-popup__content">
                                    <?php vlf_display_product( $updated, 'preview-page' ); ?>
                                </div>
                                <footer class="preview-popup__footer">
                                    <div class="columns">
                                        <div class="column has-text-left">
                                            <a href="<?php echo esc_url( get_permalink( $updated ) ); ?>" class="preview-popup__footer__view"><?php esc_html_e( 'View', 'vuelabs-framework' ); ?></a>
                                        </div>
                                        <div class="column has-text-centered">
                                            <a href="#" class="preview-popup__footer__edit"><?php esc_html_e( 'Edit', 'vuelabs-framework' ); ?></a>
                                        </div>
                                        <div class="column has-text-right">
                                            <a href="#" class="preview-popup__footer__close"><?php esc_html_e( 'Close', 'vuelabs-framework' ); ?></a>
                                        </div>
                                    </div>
                                </footer>
                            </div>

                            <?php

                            $html = ob_get_clean();

                            wp_send_json_success( $html );

                        }

                    }

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Create/update post from navigator.
         */
        public function vlf_navigator_cu_post()
        {

            $return = vlf_check_nonce( $this->gump, array_merge( $_POST, $_FILES ), 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $validation_rules = [
                    'name'        => 'required',
                    'description' => 'required',
                    'category'    => 'required',
                    'user_id'     => 'required|integer'
                ];

                $filter_rules = [
                    'name'        => 'trim|sanitize_string',
                    'description' => 'trim',
                    'category'    => 'trim',
                    'user_id'     => 'trim|sanitize_numbers'
                ];

                if ( $data['type'] !== 'update' ) {

                    $validation_rules['image'] = 'required_file|extension,png;jpg;jpeg';
                    $this->gump->set_field_name( 'image', esc_html__( 'Cover Photo', 'vuelabs-framework' ) );

                } else {

                    $validation_rules['post_id'] = 'required|integer';
                    $filter_rules['post_id'] = 'trim|sanitize_numbers';
                    $this->gump->set_field_name( 'post_id', esc_html__( 'Event ID', 'vuelabs-framework' ) );

                }

                $this->gump->set_field_name( 'name', esc_html__( 'Post Name', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'category', esc_html__( 'Category', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'description', esc_html__( 'Description', 'vuelabs-framework' ) );

                $this->gump->validation_rules( $validation_rules );
                $this->gump->filter_rules( $filter_rules );

                $data = $this->gump->run( $data );

                if ( $data === false ) {

                    wp_send_json_error( [
                        'code' => '2',
                        'data' => $this->gump->get_readable_errors()
                    ] );

                } else {

                    if ( $data['type'] !== 'update' ) {

                        // Create the post.
                        $inserted = wp_insert_post( [
                            'post_author'  => $data['user_id'],
                            'post_title'   => $data['name'],
                            'post_content' => $data['description'],
                            'post_status'  => 'private',
                            'post_type'    => 'post',
                            'filter'       => false
                        ] );

                        if ( ! is_wp_error( $inserted ) ) {

                            // Let's add other stuff.
                            // Featured Image:
                            vlf_upload_image( $data['image'], $inserted, '', 'yes', 'no' );

                            // Category
                            wp_set_object_terms( $inserted, intval( $data['category'] ), 'category' );

                            // Check if author belongs to an organization.
                            $author_brand_page_id = vlf_get_brand_id_from_author_id( $data['user_id'] );
                            $organization = get_the_terms( $author_brand_page_id, 'brand_organization' );
                            if ( isset( $organization ) && ! empty( $organization ) ) {

                                if ( isset( $organization[0] ) && ! empty( $organization[0] ) ) {

                                    wp_set_object_terms( $inserted, intval( $organization[0]->term_id ), 'brand_organization' );

                                }

                            }

                            // Return Preview
                            ob_start();

                            ?>

                            <div class="preview-popup" data-popup-id="post" data-post-id="<?php echo esc_attr( $inserted ); ?>">
                                <a href="#" class="preview-popup__close"><i class="fal fa-times"></i></a>
                                <header class="preview-popup__header">
                                    <div class="preview-popup__header__title"><?php esc_html_e( 'Your post has been created and added to your dashboard.', 'vuelabs-framework' ); ?></div>
                                </header>
                                <div class="preview-popup__content">
                                    <?php vlf_display_post( $inserted ); ?>
                                </div>
                                <footer class="preview-popup__footer">
                                    <div class="columns">
                                        <div class="column has-text-left">
                                            <a href="<?php echo esc_url( get_permalink( $inserted ) ); ?>" class="preview-popup__footer__view"><?php esc_html_e( 'View', 'vuelabs-framework' ); ?></a>
                                        </div>
                                        <div class="column has-text-centered">
                                            <a href="#" class="preview-popup__footer__edit"><?php esc_html_e( 'Edit', 'vuelabs-framework' ); ?></a>
                                        </div>
                                        <div class="column has-text-right">
                                            <a href="#" class="preview-popup__footer__close"><?php esc_html_e( 'Close', 'vuelabs-framework' ); ?></a>
                                        </div>
                                    </div>
                                </footer>
                            </div>

                            <?php

                            $html = ob_get_clean();

                            wp_send_json_success( $html );

                        }

                    } else {

                        // Update the post.
                        $updated = wp_update_post( [
                            'ID'           => intval( $data['post_id'] ),
                            'post_title'   => $data['name'],
                            'post_content' => $data['description'],
                            'post_status'  => 'private',
                            'post_type'    => 'post',
                            'filter'       => false
                        ] );

                        if ( ! is_wp_error( $updated ) ) {

                            // Let's add other stuff.
                            // Featured Image:
                            if ( isset( $data['image'] ) && ! empty( $data['image'] ) ) {

                                vlf_upload_image( $data['image'], $updated, '', 'yes', 'no' );

                            }

                            // Category
                            wp_set_object_terms( $updated, intval( $data['category'] ), 'category' );

                            // Return Preview
                            ob_start();

                            ?>

                            <div class="preview-popup" data-popup-id="post" data-post-id="<?php echo esc_attr( $updated ); ?>">
                                <a href="#" class="preview-popup__close"><i class="fal fa-times"></i></a>
                                <header class="preview-popup__header">
                                    <div class="preview-popup__header__title"><?php esc_html_e( 'Your post has been updated and added to your dashboard.', 'vuelabs-framework' ); ?></div>
                                </header>
                                <div class="preview-popup__content">
                                    <?php vlf_display_post( $updated ); ?>
                                </div>
                                <footer class="preview-popup__footer">
                                    <div class="columns">
                                        <div class="column has-text-left">
                                            <a href="<?php echo esc_url( get_permalink( $updated ) ); ?>" class="preview-popup__footer__view"><?php esc_html_e( 'View', 'vuelabs-framework' ); ?></a>
                                        </div>
                                        <div class="column has-text-centered">
                                            <a href="#" class="preview-popup__footer__edit"><?php esc_html_e( 'Edit', 'vuelabs-framework' ); ?></a>
                                        </div>
                                        <div class="column has-text-right">
                                            <a href="#" class="preview-popup__footer__close"><?php esc_html_e( 'Close', 'vuelabs-framework' ); ?></a>
                                        </div>
                                    </div>
                                </footer>
                            </div>

                            <?php

                            $html = ob_get_clean();

                            wp_send_json_success( $html );

                        }

                    }

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Upload image via ajax.
         */
        public function vlf_upload_image_editor()
        {

            $return = vlf_check_nonce( $this->gump, array_merge( $_POST, $_FILES ), 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $brand_id = vlf_get_brand_id_from_author_id( intval( $data['user_id'] ) );

                $image_id = vlf_upload_image( $data['image'], $brand_id, '', 'no', 'yes' );
                $images   = get_field( 'brand_gallery', $brand_id );
                $images   = vlf_check_field( $images );
                if ( $images ) {

                    array_push( $images, $image_id );
                    update_field( 'brand_gallery', $images, $brand_id );

                } else {

                    $images = [];
                    array_push( $images, $image_id );
                    update_field( 'brand_gallery', $images, $brand_id );

                }

                wp_send_json_success();

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Upload image via ajax.
         */
        public function vlf_upload_image_editor_link()
        {

            $return = vlf_check_nonce( $this->gump, array_merge( $_POST, $_FILES ), 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $brand_id = vlf_get_brand_id_from_author_id( intval( $data['user_id'] ) );

                $image_id = vlf_upload_image( $data['image'], $brand_id, '', 'no', 'yes' );
                $image = wp_get_attachment_image_src( $image_id, 'large' );

                wp_send_json_success( $image[0] );

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         *
         */
        public function vlf_navigator_cu_network()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $validation_rules = [
                    'name'   => 'required',
                    'brands' => 'required',
                ];

                $filter_rules = [
                    'name'   => 'trim|sanitize_string',
                    'brands' => 'trim',
                ];

                $this->gump->set_field_name( 'name', esc_html__( 'Network Name', 'vuelabs-framework' ) );
                $this->gump->set_field_name( 'brands', esc_html__( 'Network Members', 'vuelabs-framework' ) );

                $this->gump->validation_rules( $validation_rules );
                $this->gump->filter_rules( $filter_rules );

                $data = $this->gump->run( $data );

                if ( $data === false ) {

                    wp_send_json_error( [
                        'code' => '2',
                        'data' => $this->gump->get_readable_errors()
                    ] );

                } else {

                    $brand_id = vlf_get_brand_id_from_author_id( intval( $data['user_id'] ) );

                    if ( $data['type'] !== 'update' ) {

                        // Create Network
                        $organization = get_the_terms( $brand_id, 'brand_organization' );
                        $organization_id = $organization[0]->term_id;

                        $network = wp_insert_term( $data['name'], 'brand_organization', [ 'parent' => intval( $organization_id ) ] );

                        foreach ( $data['brands'] as $brand_person_id ) {

                            wp_set_object_terms( intval( $brand_person_id ), intval( $network['term_id'] ), 'brand_organization', true );

                        }

                        // Return Preview
                        ob_start();

                        ?>

                        <div class="preview-popup preview-popup--network">
                            <a href="#" class="preview-popup__close"><i class="fal fa-times"></i></a>
                            <header class="preview-popup__header">
                                <div class="preview-popup__header__title"><?php esc_html_e( 'Your network has been created and added to your dashboard.', 'vuelabs-framework' ); ?></div>
                            </header>
                            <div class="preview-popup__content">
                                <div class="preview-popup__content__network-name"><?php echo esc_html( $data['name'] ); ?></div>
                                <ul class="preview-popup__content__network-members">
                                    <?php foreach ( $data['brands'] as $brand_person_id ) : ?>
                                        <li class="preview-popup__content__network-members__item"><?php echo get_the_title( $brand_person_id ); ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            <footer class="preview-popup__footer">
                                <div class="columns">
                                    <div class="column has-text-right">
                                        <a href="#" class="preview-popup__footer__close"><?php esc_html_e( 'Close', 'vuelabs-framework' ); ?></a>
                                    </div>
                                </div>
                            </footer>
                        </div>

                        <?php

                        $html = ob_get_clean();

                        wp_send_json_success( $html );

                    } else {

                    }

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        /**
         * Publish Profile
         */
        public function vlf_publish_brand()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $brand_id = $data['brand_id'];

                $updated = wp_update_post( [
                    'ID'          => $brand_id,
                    'post_status' => 'publish'
                ] );

                if ( ! is_wp_error( $updated ) ) {

                    wp_send_json_success( esc_html__( 'Your brand profile has been published and is now live.', 'vuelabs-framework' ) );

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        public function vlf_get_brands()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $search = $data['search'];

                $search_args = [
                    'post_type'      => 'brand',
                    'posts_per_page' => -1,
                    's'              => $search,
                    'post_status'    => 'any'
                ];
                $search_loop = new WP_Query( $search_args );
                if ( $search_loop->have_posts() ) {

                    $users = [];

                    while ( $search_loop->have_posts() ) : $search_loop->the_post();

                    $user = [
                        'id' => get_the_ID(),
                        'text' => get_the_title()
                    ];

                    array_push( $users, $user );

                    endwhile; wp_reset_postdata();

                    wp_send_json_success( $users );

                } else {

                    wp_send_json_success( [] );

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        public function vlf_load_more_members()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];
                $offset = $data['offset'];

                $args = [
                    'posts_per_page' => 20,
                    'post_type'      => 'brand',
                    'post_status'    => 'publish',
                    'offset'         => intval( $offset )
                ];

                if ( isset( $data['membership'] ) ) {

                    $product_id = get_page_by_path( $data['membership'], OBJECT, 'product' );
                    $product_id = $product_id->ID;

                    $membership_query = [
                        'key'   => 'brand_membership',
                        'value' => intval( $product_id ),
                    ];

                    $args['meta_query'] = [];
                    array_push( $args['meta_query'], $membership_query );

                }

                if ( isset( $data['category'] ) || isset( $data['location'] ) || isset( $data['tag'] ) ) {

                    $tax_query = [];
                    if ( isset( $data['category'] ) ) {

                        $category_query = [
                            'taxonomy' => 'brand_category',
                            'field'    => 'slug',
                            'terms'    => $data['category']
                        ];

                        array_push( $tax_query, $category_query );

                    }

                    if ( isset( $data['location'] ) ) {

                        $location_query = [
                            'taxonomy' => 'brand_location',
                            'field'    => 'slug',
                            'terms'    => $data['location']
                        ];

                        array_push( $tax_query, $location_query );

                    }

                    if ( isset( $data['tag'] ) ) {

                        $tag_query = [
                            'taxonomy' => 'brand_tag',
                            'field'    => 'slug',
                            'terms'    => $data['tag']
                        ];

                        array_push( $tax_query, $tag_query );

                    }

                    $args['tax_query'] = $tax_query;

                }

                $members_query = new WP_Query( $args );

                if ( $members_query->have_posts() ) {

                    ob_start();
                    while ( $members_query->have_posts() ) : $members_query->the_post();

                    get_template_part( 'template-parts/pages/part', 'member' );

                    endwhile; wp_reset_postdata();
                    $html = ob_get_clean();

                    $total = intval( $offset ) + intval( $members_query->post_count );

                    if ( intval( $members_query->post_count ) < 20 ) {

                        $button = false;

                    } else {

                        $button = true;

                    }

                    wp_send_json_success( [
                        'html'    => $html,
                        'results' => sprintf( _n( '%s Member', '%s Members', intval( $total ), 'vuelabs-framework' ), intval( $total ) ),
                        'button'  => $button
                    ] );

                } else {

                    wp_send_json_error();

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        public function vlf_load_more_networks()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];
                $offset = $data['offset'];

                $authors = vlf_get_network_authors();

                $members_query = new WP_Query( [
                    'posts_per_page' => 20,
                    'post_type'      => 'brand',
                    'post_status'    => 'publish',
                    'offset'         => intval( $offset ),
                    'author__in'     => $authors
                ] );

                if ( $members_query->have_posts() ) {

                    ob_start();
                    while ( $members_query->have_posts() ) : $members_query->the_post();

                        get_template_part( 'template-parts/pages/part', 'network' );

                    endwhile; wp_reset_postdata();
                    $html = ob_get_clean();

                    $total = intval( $offset ) + intval( $members_query->post_count );

                    if ( intval( $members_query->post_count ) < 20 ) {

                        $button = false;

                    } else {

                        $button = true;

                    }

                    wp_send_json_success( [
                        'html'    => $html,
                        'results' => sprintf( _n( '%s Network', '%s Networks', intval( $total ), 'vuelabs-framework' ), intval( $total ) ),
                        'button'  => $button
                    ] );

                } else {

                    wp_send_json_error();

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        public function vlf_connect_to_users()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                foreach ( $data['users'] as $user_b ) {

                    vlf_create_connection( intval( $data['user'] ), intval( $user_b ), 'all' );

                }

                wp_send_json_success();

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        public function vlf_load_more_products_shop()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data   = $return['data'];
                $offset = $data['offset'];
                $author = $data['author'];

                $products_query = new WP_Query( [
                    'posts_per_page' => 6,
                    'post_type'      => 'product',
                    'post_status'    => 'publish',
                    'offset'         => intval( $offset ),
                    'author'         => $author
                ] );

                $categories = [];

                if ( $products_query->have_posts() ) {

                    ob_start();
                    while ( $products_query->have_posts() ) : $products_query->the_post();

                        // Categories
                        $product_cats = get_the_terms( get_the_ID(), 'product_cat' );
                        if ( ! is_wp_error( $product_cats ) ) {

                            foreach ( $product_cats as $product_cat ) {

                                if ( ! isset( $categories[ $product_cat->term_id ] ) ) {

                                    $categories[ $product_cat->term_id ] = 0;

                                }
                                $categories[ $product_cat->term_id ]++;

                            }

                        }

                        echo '<div class="column is-4">';

                        vlf_display_product( get_the_ID(), 'brand-page', true );

                        echo '</div>';

                    endwhile; wp_reset_postdata();
                    $html = ob_get_clean();

                    $total = intval( $offset ) + intval( $products_query->post_count );

                    if ( intval( $products_query->post_count ) < 6 ) {

                        $button = false;

                    } else {

                        $button = true;

                    }

                    wp_send_json_success( [
                        'html'    => $html,
                        'results' => sprintf( _n( '%s Result', '%s Results', intval( $total ), 'vuelabs-framework' ), intval( $total ) ),
                        'button'  => $button,
                        'cats'    => $categories
                    ] );

                } else {

                    wp_send_json_error();

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        public function vlf_load_more_posts()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data     = $return['data'];
                $offset   = $data['offset'];
                $author   = $data['author'];
                $category = $data['category'];
                $tag      = $data['tag'];

                $products_args = [
                    'posts_per_page' => 10,
                    'post_type'      => 'post',
                    'post_status'    => 'publish',
                    'offset'         => intval( $offset ),
                ];

                if ( isset( $author ) && ! empty( $author ) ) {

                    $products_args['author'] = intval( $author );

                }

                if ( isset( $category ) && ! empty( $category ) ) {

                    $products_args['cat'] = intval( $category );

                }

                if ( isset( $tag ) && ! empty( $tag ) ) {

                    $products_args['tag_id'] = intval( $tag );

                }

                $products_query = new WP_Query( $products_args );

                $categories = [];

                if ( $products_query->have_posts() ) {

                    ob_start();
                    while ( $products_query->have_posts() ) : $products_query->the_post();

                        // Categories
                        $product_cats = get_the_terms( get_the_ID(), 'product_cat' );
                        if ( ! is_wp_error( $product_cats ) ) {

                            foreach ( $product_cats as $product_cat ) {

                                if ( ! isset( $categories[ $product_cat->term_id ] ) ) {

                                    $categories[ $product_cat->term_id ] = 0;

                                }
                                $categories[ $product_cat->term_id ]++;

                            }

                        }

                        echo '<div class="column is-4">';

                        vlf_display_product( get_the_ID(), 'brand-page', true );

                        echo '</div>';

                    endwhile; wp_reset_postdata();
                    $html = ob_get_clean();

                    $total = intval( $offset ) + intval( $products_query->post_count );

                    if ( intval( $products_query->post_count ) < 6 ) {

                        $button = false;

                    } else {

                        $button = true;

                    }

                    wp_send_json_success( [
                        'html'    => $html,
                        'results' => sprintf( _n( '%s Result', '%s Results', intval( $total ), 'vuelabs-framework' ), intval( $total ) ),
                        'button'  => $button,
                        'cats'    => $categories
                    ] );

                } else {

                    wp_send_json_error();

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

        public function vlf_publish_post_type()
        {

            $return = vlf_check_nonce( $this->gump, $_POST, 'vlf_nonce' );

            if ( $return['status'] === true ) {

                $data = $return['data'];

                $post_id = $data['post_id'];

                $updated = wp_update_post( [
                    'ID'          => $post_id,
                    'post_status' => 'publish'
                ] );

                if ( ! is_wp_error( $updated ) ) {

                    wp_send_json_success();

                }

            }

            wp_send_json_error( [
                'code' => '1',
                'data' => esc_html__( 'Something went wrong.', 'vuelabs-framework' )
            ] );

        }

    }

endif;

return new VueLabs_Framework_AJAX();
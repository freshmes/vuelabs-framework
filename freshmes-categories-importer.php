<?php
$import_file = fopen( get_theme_file_path( 'freshmes-categories.csv' ), 'r' );
$row = 1;
while ( ( $data = fgetcsv( $import_file, 10000, ',' ) ) !== FALSE ) {

    wp_insert_term( $data[0], 'brand_category' );
    echo 'Imported: ' . $data[0], PHP_EOL;

}
let mix = require('laravel-mix');

mix.setPublicPath('/');
mix.options({
  processCssUrls: false
});

// Copy Files
mix.copy( 'node_modules/select2/dist/js/select2.full.min.js', 'assets/js/select2.full.min.js' );
mix.copy( 'node_modules/slick-carousel/slick/slick.min.js', 'assets/js/slick.min.js' );
mix.copy( 'node_modules/@fortawesome/fontawesome-pro/css/all.min.css', 'assets/css/fontawesome-pro.min.css' );
mix.copy( 'node_modules/@fortawesome/fontawesome-pro/webfonts', 'assets/webfonts' );
mix.copy( 'node_modules/raty-js/lib/jquery.raty.css', 'assets/css/jquery.raty.css' );
mix.copy( 'node_modules/raty-js/lib/jquery.raty.js', 'assets/js/jquery.raty.js' );
mix.copy( 'node_modules/raty-js/lib/fonts', 'assets/css/fonts' );
mix.copy( 'node_modules/quill/dist/quill.min.js', 'assets/js/quill.min.js' );
mix.copy( 'node_modules/quill/dist/quill.core.css', 'assets/css/quill.core.css' );
mix.copy( 'node_modules/quill/dist/quill.snow.css', 'assets/css/quill.snow.css' );
mix.copy(  'node_modules/nouislider/distribute/nouislider.min.css', 'assets/css/nouislider.min.css' );
mix.copy( 'node_modules/nouislider/distribute/nouislider.min.js', 'assets/js/nouislider.min.js' );

// Minify
mix.minify( 'assets/js/jquery.raty.js' );

// Compress SCSS
mix.sass( 'style.scss', 'style.css' );
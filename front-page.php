<?php
get_header();

/**
 * Header
 */
$images   = get_field( 'fp_header_images' );
if ( isset( $images ) && ! empty( $images ) ) {

    $image = array_rand( $images, '1' );

}

?>

    <section class="freshmes-home-header" style="background-image: url('<?php echo esc_url( $images[$image]['url'] ); ?>');">
        <div class="container">
            <h1><?php the_field( 'fp_header_title' ); ?></h1>
            <h2><?php the_field( 'fp_header_subtitle' ); ?></h2>
            <?php get_template_part( 'job_manager/job-filters-hero' ); ?>
        </div>
    </section>

<?php

echo '<div class="page-wrapper">';

/**
 * Get Content
 */
get_template_part( 'template-parts/home/content', 'master-your-juggle' );
get_template_part( 'template-parts/home/content', 'featured-members' );
get_template_part( 'template-parts/home/content', 'featured-stories' );
get_template_part( 'template-parts/home/content', 'featured-networks' );
get_template_part( 'template-parts/home/content', 'recent-members' );
get_template_part( 'template-parts/home/content', 'featured-offers' );
get_template_part( 'template-parts/home/content', 'featured-events' );
get_template_part( 'template-parts/home/content', 'featured-posts' );
//get_template_part( 'template-parts/home/content', 'ads' );

echo '</div>';

get_footer();
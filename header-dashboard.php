<?php
/**
 * Header for the theme.
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework
 * @since 1.0.0
 */

if ( ! is_user_logged_in() ) {

    if ( ! is_front_page() ) {

        wp_safe_redirect( get_home_url() );

    }

}

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Meta -->
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Link -->
    <link rel="profile" href="https://gmpg.org/xfn/11" />

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <?php wp_nonce_field( 'vlf_nonce' ) ?>

    <!-- Overlays -->
    <div class="overlay"></div>
### ASSETS:
* **CSS** folder contains css copied from node_modules using Laravel Mix.
* **WEBFONTS** folder contains fonts that have no CDN (Google Fonts, etc).
* **IMAGES** folder contains theme's images.
* **JS** folder contains js copied from node_modules using Laravel Mix as well as some custom js files.
* **SCSS** folder contains our theme's scss files.

### INC:
* `class-vuelabs-framework.php` - contains all theme setup functions. It should be used also for any kind of modifications via filters/actions.
* `class-vuelabs-framework-ajax.php` - contains all theme ajax functions.
* `class-vuelabs-framework-rest.php` - contains all things necessary to init custom REST API endpoints.

##### INC/REST:
Here we will create classes for rest api controllers.
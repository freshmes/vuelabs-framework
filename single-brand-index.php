<?php
if ( have_posts() ) : while ( have_posts() ) : the_post();

    get_template_part( 'template-parts/brand/part', 'share' );
    get_template_part( 'template-parts/brand/part', 'join-organization' );

    ?>

    <div id="brand-<?php the_ID(); ?>" <?php post_class( 'brand-single-page' ); ?>>
        <?php

        echo '<input id="brand-id" type="hidden" value="' . esc_attr( get_the_ID() ) . '">';

        // Get Brand Header Images
        get_template_part( 'template-parts/brand/part', 'header-images' );

        // Get Brand Header
        get_template_part( 'template-parts/brand/part', 'header' );

        ?>
        <div class="brand-single-page__sidebars page-wrapper">
            <div class="container">
                <div class="columns is-multiline">
                    <div class="column is-10-tablet is-offset-1-tablet is-8-desktop is-offset-0-desktop brand-single-page__sidebars__main">
                        <?php dynamic_sidebar( 'brand-main' ); ?>
                    </div>
                    <div class="column is-10-tablet is-offset-1-tablet is-4-desktop is-offset-0-desktop brand-single-page__sidebars__sidebar">
                        <?php dynamic_sidebar( 'brand-sidebar' ); ?>
                    </div>
                    <div class="column is-12 brand-single-page__sidebars__footer">
                        <?php dynamic_sidebar( 'brand-footer' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php

endwhile; endif;
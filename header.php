<?php
/**
 * Header for the theme.
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework
 * @since 1.0.0
 */

if ( ! is_user_logged_in() ) {

    if ( ! is_front_page() ) {

        wp_safe_redirect( get_home_url() );

    }

}

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Meta -->
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Link -->
    <link rel="profile" href="https://gmpg.org/xfn/11" />

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <?php wp_nonce_field( 'vlf_nonce' ) ?>

    <!-- Overlays -->
    <div class="overlay"></div>

    <!-- Account Popup or Navigator Popup -->
    <?php
    if ( ! is_user_logged_in() ) {

        get_template_part( 'template-parts/global/part', 'account-popup' );

    } else {

        if ( ! is_woocommerce() ) {

            get_template_part( 'template-parts/global/part', 'navigator-popup' );

        }

    }

    get_template_part( 'template-parts/brand/part', 'connect' );
    ?>

    <!-- Header -->
    <header id="site-header" class="site-header">
        <div class="container">
            <div class="columns is-gapless is-mobile is-v-centered">
                <div class="column is-6-mobile is-6-tablet is-5-desktop is-5-widescreen is-5-fullhd has-text-left">
                    <div class="dp-table">
                        <div class="dp-table-cell">
                            <?php get_template_part( 'template-parts/header/part', 'logo' ); ?>
                        </div>
                        <div class="dp-table-cell">
                            <?php get_template_part( 'template-parts/header/part', 'search' ); ?>
                        </div>
                    </div>
                </div>
                <div class="column is-6-mobile is-6-tablet is-7-desktop is-7-widescreen is-7-fullhd has-text-right">
                    <?php get_template_part( 'template-parts/header/part', 'nav' ); ?>
                    <?php get_template_part( 'template-parts/header/part', 'buttons' ); ?>
                </div>
            </div>
        </div>
    </header>

    <?php
    if ( is_singular( 'post' ) || is_singular( 'product' ) ) {

        global $post;
        if ( get_post_status( $post->ID ) === 'private' ) {

            $post_type = get_post_type( $post->ID );

            ?>

            <div class="notification is-freshmes notification--no-border-radius has-text-centered">
                <?php esc_html_e( 'This ' . $post_type . ' is in the draft mode.', 'vuelabs-framework' ); ?>
            </div>

            <?php

        }

    }
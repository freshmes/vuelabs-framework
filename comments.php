<?php
/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
    return;
}
?>

<section id="comments" class="comments-area" aria-label="<?php esc_html_e( 'Post Comments', 'vuelabs-framework' ); ?>">
    <div class="comments-area__title"><?php esc_html_e( 'Comments', 'vuelabs-framework' ); ?></div>
    <?php
    if ( have_comments() ) :
        ?>

        <ol class="comment-list">
            <?php
            wp_list_comments(
                array(
                    'style'      => 'ol',
                    'short_ping' => true,
                    'callback'   => 'vlf_comment',
                )
            );
            ?>
        </ol><!-- .comment-list -->

        <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through. ?>
        <nav id="comment-nav-below" class="comment-navigation" role="navigation" aria-label="<?php esc_html_e( 'Comment Navigation Below', 'vuelabs-framework' ); ?>">
            <span class="screen-reader-text"><?php esc_html_e( 'Comment navigation', 'vuelabs-framework' ); ?></span>
            <div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'vuelabs-framework' ) ); ?></div>
            <div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'vuelabs-framework' ) ); ?></div>
        </nav><!-- #comment-nav-below -->
    <?php
    endif; // Check for comment navigation.

    endif;

    if ( ! comments_open() && 0 !== intval( get_comments_number() ) && post_type_supports( get_post_type(), 'comments' ) ) :
        ?>
        <p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'vuelabs-framework' ); ?></p>
    <?php
    endif;

    $commenter = wp_get_current_commenter();

    $args = apply_filters(
        'vlf_comment_form_args', array(
            'title_reply_before' => '<span id="reply-title" class="gamma comment-reply-title">',
            'title_reply_after'  => '</span>',
            'comment_field'      => '<textarea id="comment" class="textarea" name="comment" cols="45" rows="8" aria-required="true" placeholder="' . esc_attr( 'Type your message here', 'vuelabs-framework' ) . '"></textarea>',
            'fields'             => [
                'author' => '<input id="author" class="input" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" required placeholder="' . esc_attr( 'Type your name here', 'vuelabs-framework' ) . '" />',
                'email'  => '<input id="email" class="input" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" required placeholder="' . esc_attr( 'Type your email here', 'vuelabs-framework' ) . '" />',
                'url'    => '<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" placeholder="' . esc_attr( 'Type your website link here', 'vuelabs-framework' ) . '" />'
            ],
            'label_submit'       => esc_html__( 'Submit', 'vuelabs-framework' )
        )
    );

    comment_form( $args );
    ?>

</section><!-- #comments -->
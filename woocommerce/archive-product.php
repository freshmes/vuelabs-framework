<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

// Check if Tag Taxonomy
if ( is_tax( 'product_tag' ) ) {

    // Get Author
    $author_id = get_field( 'vlf_products_vendor', 'product_tag_' . get_queried_object_id() );

    $brand_id  = vlf_get_brand_id_from_author_id( $author_id );

    $author    = get_userdata( $author_id );
    $user      = new WP_User( $author_id );

    $gallery   = get_field( 'brand_gallery', $brand_id );
    $tagline   = get_field( 'brand_tagline', $brand_id );

    $author_image       = get_field( 'brand_profile_image', $brand_id );
    $author_name        = $author->first_name . ' ' . $author->last_name;
    $author_description = get_field( 'brand_blog_description', $brand_id );

    $header_title = $author->first_name . ' ' . $author->last_name;

    if ( in_array( 'freshmes_business', $user->roles ) || in_array( 'freshmes_organization', $user->roles ) ) {

        $header_title = get_field( 'brand_business_name', $brand_id );

    }

    if ( isset( $gallery ) && ! empty( $gallery ) ) {

        $header_image_link = $gallery[0]['url'];
        $header_image = 'style="background-image: url(' . esc_url( $header_image_link ) . ');"';

    }

} else {

    $author_name = 'Freshmes';
    $header_image_id = get_field( 'shop_image', 'option' );
    $header_image_link = wp_get_attachment_image_url( $header_image_id, 'full' );
    $header_image = 'style="background-image: url(' . esc_url( $header_image_link ) . ');"';

}

get_header( 'shop' ); ?>

    <div class="brand-shop">

        <header class="brand-shop__header" <?php echo $header_image; ?>>
            <div class="brand-shop__header__overlay"></div>
            <div class="brand-shop__header__content">
                <div class="brand-shop__header__content__image">
                    <?php echo wp_get_attachment_image( $author_image, 'thumbnail' ); ?>
                    <div class="brand-shop__header__content__image__text"><?php echo esc_html__( 'Sold by ', 'vuelabs-framework' ) . esc_html( $author_name ); ?></div>
                </div>
                <h1 class="brand-shop__header__content__title"><?php echo esc_html( $header_title ) . esc_html__( ' Shop', 'vuelabs-framework' ); ?></h1>
                <?php if ( isset( $tagline ) && ! empty( $tagline ) ) : ?>
                    <h2 class="brand-shop__header__content__subtitle"><?php echo esc_html( $tagline ); ?></h2>
                <?php endif; ?>
            </div>
        </header>

        <div class="brand-shop__content">
            <div class="container">
                <header class="brand-shop__content__header">
                    <div class="columns">
                        <div class="column is-6">
                            <h2 class="brand-shop__content__header__title"><?php esc_html_e( 'The Shop', 'vuelabs-framework' ); ?></h2>
                            <ul class="brand-shop__content__header__meta">
                                <li class="brand-shop__content__header__meta__item brand-shop__content__header__meta__item--filter">
                                    <a href="#">
                                        <span class="brand-shop__content__header__meta__item__icon"><i class="fal fa-sliders-h"></i></span>
                                        <span class="brand-shop__content__header__meta__item__label"><?php esc_html_e( 'Filter', 'vuelabs-framework' ); ?></span>
                                    </a>
                                </li>
                                <li class="brand-shop__content__header__meta__item brand-shop__content__header__meta__item--results">
                                    <?php woocommerce_result_count(); ?>
                                </li>
                            </ul>
                        </div>
                        <div class="column is-6 has-text-right">
                            <div class="brand-shop__content__header__sort-by">
                                <?php woocommerce_catalog_ordering(); ?>
                            </div>
                            <div class="brand-shop__content__header__breadcrumbs">
                                <?php if ( is_tax( 'product_tag' ) ) : ?>
                                    <a href="<?php echo esc_url( get_permalink( $brand_id ) ); ?>"><?php echo esc_html( $header_title ); ?></a>
                                <?php else : ?>
                                    <a href="<?php echo esc_url( get_site_url() ); ?>"><?php esc_html_e( 'Freshmes', 'vuelabs-framework' ); ?></a>
                                <?php endif; ?>
                                <span class="brand-shop__content__header__breadcrumbs__separator">/</span>
                                <span class="brand-shop__content__header__breadcrumbs__current"><?php esc_html_e( 'Shop', 'vuelabs-framework' ); ?></span>
                            </div>
                        </div>
                    </div>
                </header>
                <div class="brand-shop__content__products">
                    <?php
                    woocommerce_product_loop_start();

                    if ( wc_get_loop_prop( 'total' ) ) {
                        echo '<div class="columns is-multiline">';

                        while ( have_posts() ) {

                            the_post();

                            /**
                             * Hook: woocommerce_shop_loop.
                             *
                             * @hooked WC_Structured_Data::generate_product_data() - 10
                             */
                            do_action( 'woocommerce_shop_loop' );

                            echo '<div class="column is-4">';

                            vlf_display_product( get_the_ID(), 'brand-page', true );

                            echo '</div>';

                        }

                        echo '</div>';

                    }

                    woocommerce_product_loop_end();
                    ?>
                    <div class="brand-shop__content__products__filters">
                        <?php do_action( 'woocommerce_sidebar' ); ?>
                    </div>
                </div>
                <div class="brand-shop__content__pagination has-text-centered">
                    <?php woocommerce_pagination(); ?>
                </div>
            </div>
        </div>

    </div>

<?php get_footer( 'shop' );

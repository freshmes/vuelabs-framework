jQuery(document).ready(function($) {
  'use strict';

  /**
   * Notification Function
   */
  function freshmesNotification( text, where, type, timeout = false ) {

    if ( where.find( '.notification' ).length > 0 ) {
      where.find('.notification').remove();
    }

    where.prepend( '<div class="notification ' + type + '"></div>' );

    if ( Array.isArray( text ) ) {
      $.each(text, function(key, value) {
        where.find('.notification').append( '<div>' + value + '</div>' );
      });
    } else {
      where.find('.notification').append( text );
    }

    if ( timeout === true ) {

      setTimeout(function() {
        where.find('.notification').remove();
      }, 5000);

    }

  }

  /**
   * Close Account Popup
   */
  function freshmesCloseAccountPopup() {

    $('.account-popup').removeClass('is-show');
    $('.account-popup__content__item').removeClass('is-show');
    $('.account-popup__content__item[data-step-id="1"]').addClass('is-show');
    $('body').removeClass('is-active-overlay');

  }

  /**
   * Close Popup
   */
  function freshmesClosePopup( element ) {

    element.removeClass('is-show');
    $('body').removeClass('is-active-overlay');

  }

  /**
   * Quill
   */
  function freshmesInitQuill() {

    var icons = Quill.import('ui/icons');
    icons['bold'] = '<i class="far fa-bold fa-fw"></i>';
    icons['italic'] = '<i class="far fa-italic fa-fw"></i>';
    icons['list']['bullet'] = '<i class="fal fa-list-ul fa-fw"></i>';
    icons['list']['ordered'] = '<i class="fal fa-list-ol fa-fw"></i>';
    icons['header']['2'] = '<i class="far fa-h2 fa-fw"></i>';
    icons['header']['3'] = '<i class="far fa-h3 fa-fw"></i>';
    icons['image'] = '<i class="fal fa-image fa-fw"></i>';
    icons['link'] = '<i class="far fa-link fa-fw"></i>';

    var quill = new Quill('#post-description', {
      modules: {
        toolbar: {
          container: [
            [{ 'header': 2 }, { 'header': 3 }, 'bold', 'italic' ],
            [{ 'list': 'ordered'}, { 'list': 'bullet' }],
            [ 'image', 'link' ]
          ],
          handlers: {
            image: function(value) {
              var input = document.createElement('input');

              input.setAttribute('type', 'file');
              input.setAttribute('accept', 'image/*');
              input.click();

              input.onchange = () => {
                var object = this;
                var file = input.files[0];
                var formData = new FormData();

                formData.append( 'image', file );
                formData.append( 'nonce', $('#_wpnonce').val() );
                formData.append( 'action', 'vlf_upload_image_editor_link' );

                // Save current cursor state
                var range = this.quill.getSelection(true);

                // Insert temporary loading placeholder image
                this.quill.insertEmbed(range.index, 'image', vlfApp.placeholder);

                // Move cursor to right side of image (easier to continue typing)
                this.quill.setSelection(range.index + 1);

                $.ajax({
                  type: 'POST',
                  url: vlfApp.ajaxURL,
                  processData: false,
                  contentType: false,
                  data: formData,
                  success: function( response ) {
                    if ( response.success === true ) {
                      var res = response.data;

                      // Remove placeholder image
                      object.quill.deleteText(range.index, 1);

                      // Insert uploaded image
                      object.quill.insertEmbed(range.index, 'image', res);
                    }
                  },
                  error: function( response ) {
                    console.log(response);
                  }
                });
              };
            }
          }
        }
      },
      theme: 'snow'
    });

  }

  /**
   * Concat Values for Filters
   */
  function concatValues( obj ) {
    var value = '';
    for ( var prop in obj ) {
      value += '.' + obj[ prop ];
    }
    return value;
  }

  function applyFilters( filtered ) {
    if ( $('.freshmes-members__content').length > 0 ) {
      $('.freshmes-members__content > .columns > .column').addClass('is-hidden');
    }
    if ( $('.freshmes-networks__content').length > 0 ) {
      $('.freshmes-networks__content > .columns > .column').addClass('is-hidden');
    }
    filtered.each(function() {
      $(this).removeClass('is-hidden');
    });
  }

  /**
   * Google Places API
   */
  var placeSearch, autocomplete;
  var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'long_name',
    country: 'long_name',
    postal_code: 'short_name'
  };
  function freshmesInitGoogleAutocomplete() {

    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('business_address')),
        {types: ['geocode']});
    autocomplete.addListener( 'place_changed', freshmesFillInAddress );

  }
  function freshmesFillInAddress() {

    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();
    console.log(place.address_components);

    for (var component in componentForm) {
      document.getElementById(component).value = '';
    }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
      var addressType = place.address_components[i].types[0];
      if (componentForm[addressType]) {
        var val = place.address_components[i][componentForm[addressType]];
        document.getElementById(addressType).value = val;
      }
    }

  }
  google.maps.event.addDomListener( window, 'load', freshmesInitGoogleAutocomplete );

  /**
   * Site Header - Navigation - Dropdown
   */
  $(document).on('click', 'header.site-header .menu-item-has-children > a', function(e) {
    e.preventDefault();

    var current = $(this).parent().attr('id');
    
    $('.site-header__site-navigation .site-header__primary-navigation > li').each(function() {
      if ( $(this).attr('id') !== current ) {
        $(this).removeClass('is-active-sub-menu');
      }
    });

    $(this).parent().toggleClass('is-active-sub-menu');
  });

  /**
   * Site Header - Navigation - Disable clicks on our titles.
   */
  $(document).on('click', 'header.site-header .is-item-title > a', function(e) {
    e.preventDefault();
  });

  /**
   * Select2 Init - Everywhere
   */
  if ( $('.form-card .is-select2').length > 0 ) {
    $('.form-card .is-select2').each(function() {
      var this_select = $(this);
      this_select.select2({
        placeholder: this_select.attr('placeholder')
      });
    });
  }
  if ( $('.account-popup .is-select2').length > 0 ) {
    $('.account-popup .is-select2').each(function() {
      var this_select = $(this);
      this_select.select2({
        placeholder: this_select.attr('placeholder'),
        minimumResultsForSearch: -1
      });
    });
  }

  /**
   * Onboarding form
   */
  if ( $('.build-your-brand__content__form__carousel').length > 0 ) {
    $('.build-your-brand__content__form__carousel .form-card__footer__continue').on('click', function(e) {
      e.preventDefault();

      var this_id = $(this).parents('.build-your-brand__content__form__carousel__item').data('step-id');
      var next_id = parseInt( this_id ) + 1;

      var current_item = $('.build-your-brand__content__form__carousel__item'),
          next_item    = $('.build-your-brand__content__form__carousel__item[data-step-id="' + next_id + '"]'),
          offset_top   = $('.build-your-brand__content__form__carousel').offset().top;

      var button     = $(this),
          parrent    = button.parent();

      var nonce      = $('#_wpnonce').val();
      var data       = {
        user: $('#user-id').val(),
        nonce: nonce,
        action: 'vlf_onboard_user_' + this_id
      };

      // Get our data.
      if ( this_id === 1 ) {
        var first_name  = $('#first_name').val(),
            last_name   = $('#last_name').val(),
            phone       = $('#phone').val(),
            job_title   = $('#job_title').val(),
            description = $('#description').val();

        data.first_name = first_name;
        data.last_name = last_name;
        data.phone = phone;
        data.job_title = job_title;
        data.description = description;
      } else if ( this_id === 2 ) {
        var business_name        = $('#business_name').val(),
            tagline              = $('#tagline').val(),
            business_description = $('#business_description').val(),
            tags_total           = $('#tags').select2('data'),
            tags = [];

        $.each(tags_total, function(key, value) {
          tags.push( value.id );
        });

        data.business_name = business_name;
        data.tagline = tagline;
        data.business_description = business_description;
        data.tags = tags;
      } else if ( this_id === 3 ) {
        var street_number  = $('#street_number').val(),
            address        = $('#route').val(),
            city           = $('#locality').val(),
            state          = $('#administrative_area_level_1').val(),
            country        = $('#country').val(),
            business_phone = $('#business_phone').val(),
            website        = $('#website').val();

        data.street_number = street_number;
        data.address = address;
        data.city = city;
        data.state = state;
        data.country = country;
        data.business_phone = business_phone;
        data.website = website;
      } else if ( this_id === 4 ) {
        var facebook  = $('#facebook').val(),
            instagram = $('#instagram').val(),
            linkedin  = $('#linkedin').val();

        data.facebook = facebook;
        data.instagram = instagram;
        data.linkedin = linkedin;
      }

      $.ajax({
        type: 'POST',
        url: vlfApp.ajaxURL,
        data: data,
        beforeSend: function() {
          button.addClass('is-loading');
        },
        complete: function() {
          button.removeClass('is-loading');
        },
        success: function( response ) {
          if ( response.success === true ) {
            current_item.removeClass('is-show');
            next_item.addClass('is-show');

            $('html, body').animate({
              scrollTop: offset_top
            }, 500);
          } else {
            freshmesNotification( response.data.data, parrent, 'is-danger' );
          }
        },
        error: function( response ) {
          console.log( response );
        }
      });
    });
    $('.build-your-brand__content__form__carousel .form-card__footer__previous').on('click', function(e) {
      e.preventDefault();

      var this_id = $(this).parents('.build-your-brand__content__form__carousel__item').data('step-id');
      var next_id = parseInt( this_id ) - 1;

      $('.build-your-brand__content__form__carousel__item').removeClass('is-show');
      $('.build-your-brand__content__form__carousel__item[data-step-id="' + next_id + '"]').addClass('is-show');

      $('html, body').animate({
        scrollTop: $('.build-your-brand__content__form__carousel').offset().top
      }, 500);
    });
  }

  $('.build-your-brand__content__form__carousel .form-card__footer__preview').on('click', function(e) {
    e.preventDefault();

    var button     = $(this),
        parrent    = button.parent();

    var nonce      = $('#_wpnonce').val();

    var data = new FormData();

    data.append( 'user', $('#user-id').val() );
    data.append( 'nonce', nonce );
    data.append( 'action', 'vlf_onboard_user_5' );

    if ( jQuery.type( $('#profile_image').prop('files')[0] ) !== 'undefined' ) {
      data.append( 'profile_image', $('#profile_image').prop('files')[0] );
    }
    if ( jQuery.type( $('#logo').prop('files')[0] ) !== 'undefined' ) {
      data.append( 'logo', $('#logo').prop('files')[0] );
    }
    if ( jQuery.type( $('#logo_icon').prop('files')[0] ) !== 'undefined' ) {
      data.append( 'logo_icon', $('#logo_icon').prop('files')[0] );
    }
    if ( jQuery.type( $('#gallery_image_1').prop('files')[0] ) !== 'undefined' ) {
      data.append( 'gallery_image_1', $('#gallery_image_1').prop('files')[0] );
    }
    if ( jQuery.type( $('#gallery_image_2').prop('files')[0] ) !== 'undefined' ) {
      data.append( 'gallery_image_2', $('#gallery_image_2').prop('files')[0] );
    }
    if ( jQuery.type( $('#gallery_image_3').prop('files')[0] ) !== 'undefined' ) {
      data.append( 'gallery_image_3', $('#gallery_image_3').prop('files')[0] );
    }

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: data,
      contentType: false,
      processData: false,
      beforeSend: function() {
        button.addClass('is-loading');
      },
      complete: function() {
        button.removeClass('is-loading');
      },
      success: function( response ) {
        if ( response.success === true ) {
          window.location.href = response.data;
        } else {
          freshmesNotification( response.data.data, parrent, 'is-danger' );
        }
      },
      error: function( response ) {
        console.log( response );
      }
    });
  });

  /**
   * Register
   */
  $(document).on('click', '.is-account-toggle', function(e) {
    e.preventDefault();

    $('body').addClass('is-active-overlay');
    if ( $('.join-organization-popup').length > 0 ) {
      $('.join-organization-popup').removeClass('is-show');
    }
    $('.account-popup').addClass('is-show');
  });
  $(document).on('click', '.account-popup__close', function(e) {
    e.preventDefault();

    $('body').removeClass('is-active-overlay');
    $('.account-popup').removeClass('is-show');
  });
  $(document).on('click', '.account-popup__content__item__email', function(e) {
    e.preventDefault();

    $('.account-popup__content__item').removeClass('is-show');
    $('.account-popup__content__item[data-step-id="2"]').addClass('is-show');
  });

  $(document).on('click', '.account-popup__content__item__sign-up', function(e) {
    e.preventDefault();

    var button     = $(this),
        parrent    = button.parent();

    var nonce      = $('#_wpnonce').val(),
        membership = $('#account-register-membership option:selected').val(),
        email      = $('#account-register-email').val(),
        password   = $('#account-register-password').val();

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: {
        membership: membership,
        email: email,
        password: password,
        nonce: nonce,
        action: 'vlf_register_user'
      },
      beforeSend: function() {
        button.addClass('is-loading');
      },
      complete: function() {
        button.removeClass('is-loading');
      },
      success: function( response ) {
        if ( response.success === true ) {
          parrent.empty();
          freshmesNotification( response.data.data, parrent, 'is-success' );
          setTimeout(function() {
            freshmesCloseAccountPopup();
          }, 3000);
          setTimeout(function() {
            window.location.href = response.data.location;
          }, 3500);
        } else {
          freshmesNotification( response.data.data, parrent, 'is-danger' );
        }
      },
      error: function( response ) {
        console.log( response );
      }
    });
  });

  /**
   * Slick Everywhere
   */
  if ( $('.brand-single-page__header-images__carousel').length > 0 ) {
    $('.brand-single-page__header-images__carousel').slick({
      dots: false,
      arrows: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      prevArrow: '<button type="button" class="button slick-prev"><i class="fal fa-chevron-circle-left"></i></button>',
      nextArrow: '<button type="button" class="button slick-next"><i class="fal fa-chevron-circle-right"></i></button>'
    });
  }

  /**
   * Set first tab as shown.
   */
  if ( $('.brand-single-page__sidebars__tabs__header__item').length > 0 ) {
    $('.brand-single-page__sidebars__tabs__header__item:first-child').each(function() {
      var tab_id = $(this).data('tab'),
          parent = $(this).parents('.brand-single-page__sidebars__tabs');
      $(this).addClass('is-show');
      parent.find('.brand-single-page__sidebars__tabs__content[data-tab="' + tab_id + '"]').addClass('is-show');
    });

    // Switch tabs.
    $(document).on('click', '.brand-single-page__sidebars__tabs__header__item', function(e) {
      e.preventDefault();

      var parent = $(this).parent().parent().parent();

      if ( ! $(this).hasClass('is-show') ) {
        parent.find('.brand-single-page__sidebars__tabs__header__item, .brand-single-page__sidebars__tabs__content').removeClass('is-show');
        $(this).addClass('is-show');
        parent.find('.brand-single-page__sidebars__tabs__content[data-tab="' + $(this).data('tab') + '"]').addClass('is-show');
        if ( parent.hasClass('brand-single-page__sidebars__widget-contacts') ) {
          $('.brand-single-page__sidebars__tabs__content[data-tab="' + $(this).data('tab') + '"] .brand-single-page__sidebars__tabs__content__slider').slick({
            arrows: true,
            slidesToShow: 5,
            slidesToScroll: 5,
            prevArrow: '<button type="button" class="slick-prev"><i class="fal fa-angle-left"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="fal fa-angle-right"></i></button>'
          });
        }
      }
    });
  }

  /**
   * Social Share
   */
  $(document).on('click', '.brand-single-page__sidebars__widget-actions__action--share', function(e) {
    e.preventDefault();

    $('body').addClass('is-active-overlay');
    $('.share-popup').addClass('is-show');
  });
  $(document).on('click', '.share-popup__close', function(e) {
    e.preventDefault();

    freshmesClosePopup( $('.share-popup') );
  });
  $.fn.freshmesSocialShare = function(opts) {
    var $this = this;
    var $win = $(window);

    opts = $.extend({
      attr : 'href',
      facebook : false,
      twitter : false,
      linkedin : false,
    }, opts);

    for(var opt in opts) {

      if(opts[opt] === false) {
        continue;
      }

      switch (opt) {
        case 'facebook':
          var url = 'https://www.facebook.com/sharer/sharer.php?u=';
          var name = 'Facebook';
          _popup(url, name, opts[opt], 400, 640);
          break;

        case 'twitter':
          var posttitle = $('.share-popup__content__item--twitter').data("title");
          var url = 'https://twitter.com/intent/tweet?text='+posttitle+'&url=';
          var name = 'Twitter';
          _popup(url, name, opts[opt], 440, 600);
          break;

        case 'linkedin':
          var url = 'https://www.linkedin.com/shareArticle?mini=true&url=';
          var name = 'LinkedIn';
          _popup(url, name, opts[opt], 570, 520);
          break;
        default:
          break;
      }
    }

    function isUrl(data) {
      var regexp = new RegExp( '(^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|(www\\.)?))[\\w-]+(\\.[\\w-]+)+([\\w-.,@?^=%&:/~+#-]*[\\w@?^=%&;/~+#-])?', 'gim' );
      return regexp.test(data);
    }

    function _popup(url, name, opt, height, width) {
      if(opt !== false && $this.find(opt).length) {
        $this.on('click', opt, function(e){
          e.preventDefault();

          var top = (screen.height/2) - height/2;
          var left = (screen.width/2) - width/2;
          var share_link = $(this).attr(opts.attr);

          if(!isUrl(share_link)) {
            share_link = window.location.href;
          }

          window.open(
              url+encodeURIComponent(share_link),
              name,
              'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height='+height+',width='+width+',top='+top+',left='+left
          );

          return false;
        });
      }
    }
    return;
  };

  $('.share-popup__content').freshmesSocialShare({
    facebook : '.share-popup__content__item--facebook',
    twitter : '.share-popup__content__item--twitter',
    linkedin : '.share-popup__content__item--linkedin',
  });

  $('.brand-blog__footer__share').freshmesSocialShare({
    facebook : '.brand-blog__footer__share__item--facebook',
    twitter : '.brand-blog__footer__share__item--twitter',
    linkedin : '.brand-blog__footer__share__item--linkedin',
  });
  $('.brand-blog-single__footer__share').freshmesSocialShare({
    facebook : '.brand-blog-single__footer__share__item--facebook',
    twitter : '.brand-blog-single__footer__share__item--twitter',
    linkedin : '.brand-blog-single__footer__share__item--linkedin',
  });

  /**
   * Connect/Disconnect
   */
  $(document).on('click', '.brand-single-page__sidebars__widget-info__profile-connect', function(e) {
    e.preventDefault();

    var button = $(this);
    var nonce  = $('#_wpnonce').val();

    var user_a = button.data('user-a'),
        user_b = button.data('user-b');

    var $body = $('body'),
        $popup = $('.connect-popup');

    if ( ! button.hasClass('brand-single-page__sidebars__widget-info__profile-connect--is-connection') ) {
      $.ajax({
        type: 'POST',
        url: vlfApp.ajaxURL,
        data: {
          user_a: user_a,
          user_b: user_b,
          category: 'all',
          nonce: nonce,
          action: 'vlf_connect_to_user'
        },
        success: function( response ) {
          if ( response.success === true ) {
            // Empty Fields
            $popup.find('.connect-popup__header__title').empty();
            $popup.find('.connect-popup__header__subtitle').empty();
            $popup.find('.connect-popup__content').removeClass('is-hidden').empty();

            $popup.find('.connect-popup__header__title').html( response.data.brand_header_title );
            $popup.find('.connect-popup__header__subtitle').html( response.data.brand_header_subtitle );
            $popup.find('.connect-popup__content').append( response.data.brand_content_title );
            $popup.find('.connect-popup__content').append( response.data.brand_content_fields );

            $body.addClass('is-active-overlay');
            $popup.addClass('is-show');

            button.addClass('brand-single-page__sidebars__widget-info__profile-connect--is-connection');
            button.text( 'Disconnect' );
          }
        },
        error: function( error ) {
          console.log( error );
        }
      });
    } else {
      $.ajax({
        type: 'POST',
        url: vlfApp.ajaxURL,
        data: {
          user_a: user_a,
          user_b: user_b,
          nonce: nonce,
          action: 'vlf_disconnect_from_user'
        },
        success: function( response ) {
          if ( response.success === true ) {
            // Empty Fields
            $popup.find('.connect-popup__header__title').empty();
            $popup.find('.connect-popup__header__subtitle').empty();
            $popup.find('.connect-popup__content').empty().addClass('is-hidden');

            $popup.find('.connect-popup__header__title').html( response.data.brand_header_title );
            $popup.find('.connect-popup__header__subtitle').html( response.data.brand_header_subtitle );

            $body.addClass('is-active-overlay');
            $popup.addClass('is-show');

            button.removeClass('brand-single-page__sidebars__widget-info__profile-connect--is-connection');
            button.text( button.data('original-label') );
          }
        },
        error: function( error ) {
          console.log( error );
        }
      });
    }
  });
  var connection_category_input = '.connect-popup input[name="connection-category[]"]',
      connection_button         = '.brand-single-page__sidebars__widget-info__profile-connect';

  if ( $('.brand-single-page__sidebars__widget-info__profile-connect').length <= 0 ) {
    connection_button = '.member__connect';
  }
  $(document).on('click', '.is-checkradio[type=checkbox] + label', function(e) {
    e.preventDefault();

    var labelFor = $(this).attr('for');
    if ( $('.is-checkradio[id="' + labelFor + '"]').is(':checked') ) {
      $('.is-checkradio[id="' + labelFor + '"]').removeAttr('checked');
    } else {
      $('.is-checkradio[id="' + labelFor + '"]').attr('checked', 'checked');
    }
  });
  $(document).on('change', connection_category_input, function(e) {
    var checked = 'all',
        count   = $(connection_category_input).length,
        checked_count = 0;

    $(connection_category_input).each(function() {
      if ( $(this).is(':checked') ) {
        checked = $(this).val();
        checked_count++;
      }
    });

    if ( checked_count === 2 ) {
      checked = 'all';
    }

    var nonce  = $('#_wpnonce').val(),
        $body  = $('body');

    var user_a = $(connection_button).data('user-a'),
        user_b = $(connection_button).data('user-b');

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: {
        user_a: user_a,
        user_b: user_b,
        category: checked,
        nonce: nonce,
        action: 'vlf_update_connection',
        success: function( response ) {
          /*if ( $body.hasClass('single-brand') ) {
            window.location.reload( true );
          }*/
        },
        error: function( response ) {
          // Nothing
        }
      }
    });
  });
  $(document).on('click', '.connect-popup__close', function(e) {
    e.preventDefault();

    freshmesClosePopup( $('.connect-popup') );
  });

  $(document).on('click', '.brand-single-page__sidebars__widget-actions__action--join-now, .benefits__footer__join-now', function(e) {
    e.preventDefault();

    var button   = $(this),
        nonce    = $('#_wpnonce').val(),
        brand_id = button.data('brand-id'),
        user_id  = button.data('user-id');

    var $popup = $('.join-organization-popup'),
        $body  = $('body');

    if ( button.hasClass('brand-single-page__sidebars__widget-actions__action--join-now--leave') ) {
      $.ajax({
        type: 'POST',
        url: vlfApp.ajaxURL,
        data: {
          brand_id: brand_id,
          user_id: user_id,
          action: 'vlf_leave_brand_organization',
          nonce: nonce
        },
        success: function( response ) {
          if ( response.success === true ) {
            // Empty Fields
            $popup.find('.join-organization-popup__header__title').empty();
            $popup.find('.join-organization-popup__content').empty();

            $popup.find('.join-organization-popup__header__title').html( response.data.brand_header_title );
            $popup.find('.join-organization-popup__content').append('<p>' + response.data.brand_content + '</p>');

            $body.addClass('is-active-overlay');
            $popup.addClass('is-show');

            button.removeClass('brand-single-page__sidebars__widget-actions__action--join-now--leave');
            button.find('.brand-single-page__sidebars__widget-actions__action__label').text( button.data('original-label') );
            button.find('i').removeClass( 'fa-user-minus' ).addClass('fa-user-plus');
          }
        },
        error: function( response ) {
          console.log( response );
        }
      });
    } else if ( button.hasClass( 'brand-single-page__sidebars__widget-actions__action--join-now--upgrade' ) ) {
      $.ajax({
        type: 'POST',
        url: vlfApp.ajaxURL,
        data: {
          action: 'vlf_get_brand_organization_update',
          nonce: nonce
        },
        success: function( response ) {
          if ( response.success === true ) {
            // Empty Fields
            $popup.find('.join-organization-popup__header__title').empty();
            $popup.find('.join-organization-popup__content').empty();

            $popup.find('.join-organization-popup__header__title').html( response.data.brand_header_title );
            $popup.find('.join-organization-popup__content').html( response.data.brand_content );

            $body.addClass('is-active-overlay');
            $popup.addClass('is-show');
          }
        },
        error: function( response ) {
          console.log( response );
        }
      });
    } else if ( button.hasClass( 'brand-single-page__sidebars__widget-actions__action--join-now--login' ) ) {
      $.ajax({
        type: 'POST',
        url: vlfApp.ajaxURL,
        data: {
          action: 'vlf_get_brand_organization_login',
          nonce: nonce
        },
        success: function( response ) {
          if ( response.success === true ) {
            // Empty Fields
            $popup.find('.join-organization-popup__header__title').empty();
            $popup.find('.join-organization-popup__content').empty();

            $popup.find('.join-organization-popup__header__title').html( response.data.brand_header_title );
            $popup.find('.join-organization-popup__content').html( response.data.brand_content );

            $body.addClass('is-active-overlay');
            $popup.addClass('is-show');
          }
        },
        error: function( response ) {
          console.log( response );
        }
      });
    } else {
      $.ajax({
        type: 'POST',
        url: vlfApp.ajaxURL,
        data: {
          brand_id: brand_id,
          user_id: user_id,
          action: 'vlf_join_brand_organization',
          nonce: nonce
        },
        success: function( response ) {
          if ( response.success === true ) {
            // Empty Fields
            $popup.find('.join-organization-popup__header__title').empty();
            $popup.find('.join-organization-popup__content').empty();

            $popup.find('.join-organization-popup__header__title').html( response.data.brand_header_title );
            $popup.find('.join-organization-popup__content').append('<p>' + response.data.brand_content + '</p>');

            $body.addClass('is-active-overlay');
            $popup.addClass('is-show');

            button.addClass('brand-single-page__sidebars__widget-actions__action--join-now--leave');
            button.find('.brand-single-page__sidebars__widget-actions__action__label').text( 'Leave Organization' );
            button.find('i').removeClass( 'fa-user-plus' ).addClass('fa-user-minus');
          }
        },
        error: function( response ) {
          console.log( response );
        }
      });
    }
  });
  $(document).on('click', '.join-organization-popup__close', function(e) {
    e.preventDefault();

    freshmesClosePopup( $('.join-organization-popup') );
  });

  if ( $('.brand-single-page__sidebars__tabs__content__slider').length > 0 ) {
    $('.brand-single-page__sidebars__tabs__content.is-show .brand-single-page__sidebars__tabs__content__slider').each(function() {
      $(this).slick({
        arrows: true,
        slidesToShow: 5,
        slidesToScroll: 5,
        prevArrow: '<button type="button" class="slick-prev"><i class="fal fa-angle-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="fal fa-angle-right"></i></button>'
      });
    });
  }

  $(document).on('click', '.brand-single-page__sidebars__widget-products__load-more', function(e) {
    e.preventDefault();

    var button     = $(this),
        nonce      = $('#_wpnonce').val(),
        author     = button.data('author'),
        $container = $('.brand-single-page__sidebars__widget-products__content'),
        items      = $container.find('> .column').length;

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: {
        offset: items,
        author: author,
        nonce: nonce,
        action: 'vlf_load_more_products'
      },
      beforeSend: function() {
        button.addClass('is-loading');
      },
      complete: function() {
        button.removeClass('is-loading');
      },
      success: function( response ) {
        if ( response.success === true ) {
          $container.append( response.data.content );
          if ( response.data.more === false ) {
            button.addClass('is-hidden');
          }
        } else {
          button.addClass('is-hidden');
        }
      },
      error: function( response ) {
        console.log( response );
      }
    });
  });

  if ( $('.brand-single-page__sidebars__widget-reviews__form').length > 0 ) {
    $('.brand-single-page__sidebars__widget-reviews__form__form-rating').raty({
      starType: 'i',
      hints: [ 'Terrible', 'Poor', 'Good', 'Very Good', 'Excellent' ],
      targetKeep: true,
      targetType: 'hint',
      score: 4,
      target: '.brand-single-page__sidebars__widget-reviews__form__form-rating__target',
      click: function(rating, evt) {
        $('.brand-single-page__sidebars__widget-reviews__form__rating').val( rating );
      },
    });
  }
  if ( $('.brand-single-page__sidebars__widget-reviews__submit-review').length > 0 ) {
    $(document).on('click', '.brand-single-page__sidebars__widget-reviews__submit-review', function(e) {
      e.preventDefault();

      $(this).addClass('is-hidden');
      $('.brand-single-page__sidebars__widget-reviews__form').addClass('is-show');
      $('html, body').animate({
        scrollTop: $('.brand-single-page__sidebars__widget-reviews__form').offset().top - 50 + 'px'
      }, 500);
    });
  }
  if ( $('.brand-single-page__sidebars__widget-reviews__form__button').length > 0 ) {
    $(document).on('click', '.brand-single-page__sidebars__widget-reviews__form__button', function(e) {
      e.preventDefault();

      var button     = $(this),
          nonce      = $('#_wpnonce').val(),
          brand_id   = button.data('brand-id'),
          user_id    = button.data('user-id'),
          title      = $('#review-title').val(),
          comment    = $('#review-comment').val(),
          rating     = $('#review-rating').val(),
          parent     = $('.brand-single-page__sidebars__widget-reviews__form');

      $.ajax({
        type: 'POST',
        url: vlfApp.ajaxURL,
        data: {
          title: title,
          comment: comment,
          rating: rating,
          nonce: nonce,
          brand: brand_id,
          user: user_id,
          action: 'vlf_submit_review',
        },
        beforeSend: function() {
          button.addClass('is-loading');
        },
        complete: function() {
          button.removeClass('is-loading');
        },
        success: function( response ) {
          parent.find('.notification').remove();
          if ( response.success === true ) {

            freshmesNotification( response.data, parent, 'is-success' );
            parent.find('.field').remove();
            $('html, body').animate({
              scrollTop: parent.offset().top - 50 + 'px'
            }, 500);

          } else {

            freshmesNotification( response.data.data, parent, 'is-danger' );

          }
        },
        error: function( response ) {
          console.log( response );
        }
      });
    });
  }

  /**
   * Navigator
   */
  $(document).on('click', '.navigator-toggle, .navigator-popup__footer__back, .toggle-create', function(e) {
    e.preventDefault();

    if ( $(this).hasClass( 'toggle-create' ) ) {
      $(this).parents('ul.sub-menu').parent().removeClass('is-active-sub-menu')
    }

    var nonce = $('#_wpnonce').val(),
        $body = $('body'),
        $popup = $('.navigator-popup');

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: {
        nonce: nonce,
        action: 'vlf_navigator_get_create'
      },
      success: function( response ) {
        if ( response.success === true ) {

          $popup.attr( 'data-popup-id', 'create' );
          $popup.attr( 'data-popup-type', '' );
          $popup.find('.navigator-popup__header__title').text( response.data.popup_title );
          $popup.find('.navigator-popup__header__view-all').addClass('is-hidden').html();
          $popup.find('.navigator-popup__header__add-new').remove();
          $popup.find('.navigator-popup__content').html( response.data.popup_content );
          $popup.find('.navigator-popup__footer').html( response.data.popup_footer );
          $popup.addClass('is-show');
          $body.addClass('is-active-overlay');

        }
      },
      error: function( error ) {

      }
    });
  });
  $(document).on('click', '.preview-popup__close, .preview-popup__footer__close', function(e) {
    e.preventDefault();

    $('.preview-popup').removeClass('is-show');
    $('body').removeClass('is-active-overlay');

    setTimeout(function() {
      $('.preview-popup').remove();
    }, 600);
  });
  $(document).on('click', '.navigator-popup__close, .navigator-popup__footer__close', function(e) {
    e.preventDefault();

    $('.navigator-popup').removeClass('is-show');
    $('body').removeClass('is-active-overlay');

    setTimeout(function() {
      $('.navigator-popup').attr('data-popup-id', '');
      $('.navigator-popup').attr('data-popup-type', '');
      $('.navigator-popup').find('.navigator-popup__header__view-all, .navigator-popup__header__add-new').addClass('is-hidden');
    }, 600);
  });
  function freshmesReadURL( input, $input ) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $input.find('.file-no-image').addClass('is-hidden');
        $input.find('.file-image').removeClass('is-hidden');
        $input.find('.file-image').attr( 'style', 'background-image: url(' + e.target.result + ');' );
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
  $(document).on('change', '.navigator-popup__content .is-image-field input[type="file"]', function() {
    freshmesReadURL( this, $(this).parent() );
  });
  $(document).on('change', '.build-your-brand__content__form__carousel__item input[type="file"]', function() {
    if ( this.files && this.files[0] ) {
      var reader = new FileReader(),
          parent = $(this).parent();

      reader.onload = function( e ) {
        parent.find('.center').addClass('is-hidden');
        parent.find('.image-placeholder').removeClass('is-hidden');
        parent.find('.image-placeholder').attr( 'style', 'background-image: url(' + e.target.result + ');' );
      };
      reader.readAsDataURL(this.files[0]);
    }
  });
  $(document).on('click', '.navigator-popup__content__list__item__actions-toggle', function(e) {
    e.preventDefault();

    $(this).find('i').toggleClass('fa-times');
    $(this).parent().find('.navigator-popup__content__list__item__actions-list').toggleClass('is-show');
    $(this).parent().find('.navigator-popup__content__list__item__actions-list').css({
      'top': $(this).parent().position().top + 'px'
    });
  });
  $(document).on('click', '.navigator-create a, .navigator-popup__header__view-all, .navigator-popup__header__add-new, .navigator-popup__content__list__item__actions-list__item__edit, .toggle-event, .toggle-product, .toggle-post, .toggle-network, .brand-single-page__sidebars__widget-products__add-product, .brand-single-page__sidebars__widget-posts__add-post, .brand-single-page__sidebars__widget-offer__create-special-offer, .brand-single-page__sidebars__widget-event__add-events, .brand-single-page__sidebars__widget-network__create-network, .preview-popup__footer__edit, .is-post-type-edit', function(e) {
    e.preventDefault();

    var button     = $(this),
        popup_id   = button.attr( 'data-popup-id' ),
        popup_type = button.attr( 'data-popup-type' ),
        user_id    = button.attr( 'data-user-id' ),
        nonce      = $('#_wpnonce').val(),
        $body      = $('body'),
        $popup     = $('.navigator-popup');

    var post_id    = 0;
    var brands     = '';

    if ( $(this).hasClass('navigator-popup__header__view-all') ) {
      popup_id   = $popup.attr( 'data-popup-id' );
      popup_type = 'all';
      user_id    = $popup.attr( 'data-user-id' );
    }

    if ( $(this).hasClass('navigator-popup__header__add-new') ) {
      popup_id   = $popup.attr( 'data-popup-id' );
      popup_type = 'create';
      user_id    = $popup.attr( 'data-user-id' );
    }

    if ( $(this).hasClass( 'navigator-popup__content__list__item__actions-list__item__edit' ) ) {
      popup_id   = $popup.attr( 'data-popup-id' );
      popup_type = 'update';
      user_id    = $popup.attr( 'data-user-id' );
      post_id    = $(this).parents('.navigator-popup__content__list__item').data( 'post-id' );
    }

    if ( $(this).hasClass( 'toggle-event' ) ) {
      popup_id   = 'event';
      popup_type = 'create';
      user_id    = $popup.attr( 'data-user-id' );
      $(this).parents('ul.sub-menu').parent().removeClass('is-active-sub-menu')
    }

    if ( $(this).hasClass( 'brand-single-page__sidebars__widget-event__add-events' ) ) {
      popup_id   = 'event';
      popup_type = 'create';
      user_id    = $popup.attr( 'data-user-id' );
    }

    if ( $(this).hasClass( 'toggle-product' ) ) {
      popup_id   = 'product';
      popup_type = 'create';
      user_id    = $popup.attr( 'data-user-id' );
      $(this).parents('ul.sub-menu').parent().removeClass('is-active-sub-menu')
    }

    if ( $(this).hasClass( 'brand-single-page__sidebars__widget-offer__create-special-offer' ) || $(this).hasClass( 'brand-single-page__sidebars__widget-products__add-product' ) ) {
      popup_id   = 'product';
      popup_type = 'create';
      user_id    = $popup.attr( 'data-user-id' );
    }

    if ( $(this).hasClass( 'toggle-post' ) ) {
      popup_id   = 'post';
      popup_type = 'create';
      user_id    = $popup.attr( 'data-user-id' );
      $(this).parents('ul.sub-menu').parent().removeClass('is-active-sub-menu')
    }

    if ( $(this).hasClass( 'brand-single-page__sidebars__widget-posts__add-post' ) ) {
      popup_id   = 'post';
      popup_type = 'create';
      user_id    = $popup.attr( 'data-user-id' );
    }

    if ( $(this).hasClass( 'brand-single-page__sidebars__widget-network__create-network' ) ) {
      popup_id   = 'network';
      popup_type = 'create';
      user_id    = $popup.attr( 'data-user-id' );
    }

    if ( $(this).hasClass( 'preview-popup__footer__edit' ) ) {
      popup_id   = $('.preview-popup').attr( 'data-popup-id' );
      popup_type = 'update';
      post_id    = $('.preview-popup').attr( 'data-post-id' );
      user_id    = $popup.attr( 'data-user-id' );
      $('.preview-popup').remove();
    }

    if ( $(this).hasClass('is-post-type-edit') ) {
      popup_id = $(this).data('post-type');
      popup_type = 'update';
      post_id    = $(this).data('brand-id');
      user_id    = $popup.attr('data-user-id');
      $('.preview-popup').remove();
    }

    if ( $(this).hasClass( 'toggle-network' ) ) {
      popup_id   = 'network';
      popup_type = 'create';
      user_id    = $popup.attr( 'data-user-id' );
      $(this).parents('ul.sub-menu').parent().removeClass('is-active-sub-menu')
    }

    // Different thing for popup that is photo.
    if ( popup_id === 'photo' ) {
      // Trigger Input
      $('#navigator-create__image').trigger('click');

      $(document).on('change', '#navigator-create__image', function(e) {
        var data = new FormData();
        data.append( 'user_id', $popup.attr( 'data-user-id' ) );
        data.append( 'image', $('#navigator-create__image').prop('files')[0] );
        data.append( 'nonce', nonce );
        data.append( 'action', 'vlf_upload_image_editor' );

        var real_button = button.find('button');
        var button_original = real_button.html();

        $.ajax({
          type: 'POST',
          url: vlfApp.ajaxURL,
          data: data,
          contentType: false,
          processData: false,
          beforeSend: function() {
            real_button.removeClass('is-hidden');
          },
          complete: function() {
            setTimeout(function() {
              real_button.addClass('is-hidden');
              real_button.addClass('is-loading');
            }, 3100);
          },
          success: function( response ) {
            real_button.removeClass('is-loading');
            if ( response.success === true ) {
              real_button.html('<i class="fal fa-check"></i>');
              setTimeout(function() {
                real_button.html( button_original )
              }, 3000);
            } else {
              real_button.html('<i class="fal fa-times"></i>');
              setTimeout(function() {
                real_button.html( button_original )
              }, 3000);
            }
          },
          error: function( error ) {
            console.log( error );
          }
        });
      });
    } else {

      var data = {
        popup_id: popup_id,
        popup_type: popup_type,
        user_id: user_id,
        nonce: nonce,
        action: 'vlf_navigator_get_popup'
      };

      if ( popup_type === 'update' ) {

        data.post_id = post_id;

      }

      $.ajax({
        type: 'POST',
        url: vlfApp.ajaxURL,
        data: data,
        success: function( response ) {
          if ( response.success === true ) {
            $popup.attr('data-popup-id', popup_id);
            $popup.attr('data-popup-type', popup_type);
            $popup.find('.navigator-popup__header__title').text( response.data.popup_title );
            $popup.find('.navigator-popup__content').html( response.data.popup_content );
            $popup.find('.navigator-popup__footer').html( response.data.popup_footer );
            if ( response.data.popup_view_all === true ) {
              $popup.find('.navigator-popup__header__view-all').removeClass('is-hidden');
            } else {
              $popup.find('.navigator-popup__header__view-all').addClass('is-hidden');
            }
            if ( response.data.popup_new === true ) {
              $popup.find('.navigator-popup__header__add-new').html( response.data.popup_new_title );
              $popup.find('.navigator-popup__header__add-new').removeClass('is-hidden');
            } else {
              $popup.find('.navigator-popup__header__add-new').addClass('is-hidden');
            }
            if ( popup_id === 'post' ) {
              if ( popup_type === 'create' || popup_type === 'update' ) {
                freshmesInitQuill();
              }
            }
            if ( popup_id === 'network' ) {
              if ( popup_type === 'create' ) {
                $popup.find('.is-select2').select2({
                  placeholder: $popup.find('.is-select2').attr('placeholder'),
                  ajax: {
                    type: 'POST',
                    url: vlfApp.ajaxURL,
                    delay: 250,
                    minimumInputLength: 3,
                    data: function( params ) {
                      var query = {
                        search: params.term,
                        nonce: nonce,
                        action: 'vlf_get_brands'
                      };
                      return query;
                    },
                    processResults: function (response) {
                      // Tranforms the top-level key of the response object from 'items' to 'results'
                      return {
                        results: response.data
                      };
                    }
                  }
                });
              } else if ( popup_type === 'update' ) {

                $popup.find('.is-select2').select2({
                  placeholder: $popup.find('.is-select2').attr('placeholder'),
                  data: response.data.brands,
                  ajax: {
                    type: 'POST',
                    url: vlfApp.ajaxURL,
                    delay: 250,
                    minimumInputLength: 3,
                    data: function( params ) {
                      var query = {
                        search: params.term,
                        nonce: nonce,
                        action: 'vlf_get_brands'
                      };
                      return query;
                    },
                    processResults: function (response) {
                      // Tranforms the top-level key of the response object from 'items' to 'results'
                      return {
                        results: response.data
                      };
                    }
                  }
                });

                $popup.find('.is-select2').val( response.data.brands ).trigger( 'change' );

              }
            }
            $popup.addClass('is-show');
            $body.addClass('is-active-overlay');
          }
        },
        error: function( error ) {

        }
      });

    }
  });

  // Publish/Delete Item
  $(document).on('click', '.navigator-popup__content__list__item__actions-list__item__publish, .navigator-popup__content__list__item__actions-list__item__delete', function(e) {
    e.preventDefault();

    var button = $(this),
        post_id = button.parents('.navigator-popup__content__list__item').data('post-id'),
        nonce   = $('#_wpnonce').val(),
        action  = '',
        parent  = button.parents('.navigator-popup__content__list__item');

    if ( button.hasClass( 'navigator-popup__content__list__item__actions-list__item__publish' ) ) {
      action = 'vlf_navigator_publish_item';
    } else if ( button.hasClass( 'navigator-popup__content__list__item__actions-list__item__delete' ) ) {
      action = 'vlf_navigator_delete_item';
    }

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: {
        post_id: post_id,
        nonce: nonce,
        action: action
      },
      beforeSend: function() {
        button.addClass('is-loading');
      },
      complete: function() {
        button.removeClass('is-loading');
      },
      success: function( response ) {
        if ( response.success === true ) {
          if ( action === 'vlf_navigator_publish_item' ) {
            button.remove();
          } else {
            parent.remove();
          }
        }
      },
      error: function( error ) {
        console.log( error );
      }
    });
  });

  // Create Event
  $(document).on('click', '.navigator-popup__footer__create-event', function(e) {
    e.preventDefault();

    var button  = $(this),
        user_id = $('.navigator-popup').data( 'user-id' ),
        nonce   = $('#_wpnonce').val(),
        data    = new FormData(),
        parent = $('.navigator-popup__content'),
        $popup  = $('.navigator-popup');

    if ( $.type( $('#event-image').prop('files')[0] ) !== 'undefined' ) {
      var image = $('#event-image').prop('files')[0];
      data.append( 'image', image );
    }

    var name        = $('#event-name').val(),
        price       = $('#event-price').val(),
        discount    = $('#event-discount').val(),
        description = $('#event-description').val(),
        location    = $('#event-location').val(),
        address     = $('#event-address').val(),
        date        = $('#event-date').val(),
        time        = $('#event-time').val();

    data.append( 'name', name );
    data.append( 'price', price );
    data.append( 'discount', discount );
    data.append( 'description', description );
    data.append( 'location', location );
    data.append( 'address', address );
    data.append( 'date', date );
    data.append( 'time', time );

    data.append( 'type', 'create' );
    data.append( 'user_id', user_id );
    data.append( 'nonce', nonce );
    data.append( 'action', 'vlf_navigator_cu_event' );

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: data,
      contentType: false,
      processData: false,
      beforeSend: function() {
        button.addClass('is-loading');
      },
      complete: function() {
        button.removeClass('is-loading');
      },
      success: function( response ) {
        if ( response.success === true ) {
          $('body').append( response.data );
          $('.preview-popup').addClass('is-show');
          $popup.removeClass('is-show');
          $popup.attr( 'data-popup-id', '' );
          $popup.attr( 'data-popup-type', '' );
        } else {
          freshmesNotification( response.data.data, parent, 'is-danger' );
          parent.animate({
            scrollTop: '0px'
          }, 500);
        }
      },
      error: function( error ) {
        console.log( error );
      }
    });
  });

  // Update Event
  $(document).on('click', '.navigator-popup__footer__update-event', function(e) {
    e.preventDefault();

    var button  = $(this),
        user_id = $('.navigator-popup').data( 'user-id' ),
        nonce   = $('#_wpnonce').val(),
        data    = new FormData(),
        parent  = $('.navigator-popup__content'),
        $popup  = $('.navigator-popup');

    if ( $.type( $('#event-image').prop('files')[0] ) !== 'undefined' ) {
      var image = $('#event-image').prop('files')[0];
      data.append( 'image', image );
    }

    var name        = $('#event-name').val(),
        price       = $('#event-price').val(),
        discount    = $('#event-discount').val(),
        description = $('#event-description').val(),
        location    = $('#event-location').val(),
        address     = $('#event-address').val(),
        date        = $('#event-date').val(),
        time        = $('#event-time').val(),
        post_id     = button.data( 'post-id' );

    data.append( 'name', name );
    data.append( 'price', price );
    data.append( 'discount', discount );
    data.append( 'description', description );
    data.append( 'location', location );
    data.append( 'address', address );
    data.append( 'date', date );
    data.append( 'time', time );

    data.append( 'post_id', post_id );
    data.append( 'type', 'update' );
    data.append( 'user_id', user_id );
    data.append( 'nonce', nonce );
    data.append( 'action', 'vlf_navigator_cu_event' );

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: data,
      contentType: false,
      processData: false,
      beforeSend: function() {
        button.addClass('is-loading');
      },
      complete: function() {
        button.removeClass('is-loading');
      },
      success: function( response ) {
        if ( response.success === true ) {
          $('body').append( response.data );
          $('.preview-popup').addClass('is-show');
          $popup.removeClass('is-show');
          $popup.attr( 'data-popup-id', '' );
          $popup.attr( 'data-popup-type', '' );
        } else {
          freshmesNotification( response.data.data, parent, 'is-danger' );
          parent.animate({
            scrollTop: '0px'
          }, 500);
        }
      },
      error: function( error ) {
        console.log( error );
      }
    });
  });

  // Create Product
  $(document).on('click', '.navigator-popup__footer__create-product', function(e) {
    e.preventDefault();

    var button  = $(this),
        user_id = $('.navigator-popup').data( 'user-id' ),
        nonce   = $('#_wpnonce').val(),
        data    = new FormData(),
        parent = $('.navigator-popup__content'),
        $popup  = $('.navigator-popup');

    if ( $.type( $('#product-image').prop('files')[0] ) !== 'undefined' ) {
      var image = $('#product-image').prop('files')[0];
      data.append( 'image', image );
    }

    var name        = $('#product-name').val(),
        price       = $('#product-price').val(),
        discount    = $('#product-discount').val(),
        description = $('#product-description').val(),
        category    = $('#product-category option:selected').val();

    data.append( 'name', name );
    data.append( 'price', price );
    data.append( 'discount', discount );
    data.append( 'description', description );
    data.append( 'category', category );

    data.append( 'type', 'create' );
    data.append( 'user_id', user_id );
    data.append( 'nonce', nonce );
    data.append( 'action', 'vlf_navigator_cu_product' );

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: data,
      contentType: false,
      processData: false,
      beforeSend: function() {
        button.addClass('is-loading');
      },
      complete: function() {
        button.removeClass('is-loading');
      },
      success: function( response ) {
        if ( response.success === true ) {
          $('body').append( response.data );
          $('.preview-popup').addClass('is-show');
          $popup.removeClass('is-show');
          $popup.attr( 'data-popup-id', '' );
          $popup.attr( 'data-popup-type', '' );
        } else {
          freshmesNotification( response.data.data, parent, 'is-danger' );
          parent.animate({
            scrollTop: '0px'
          }, 500);
        }
      },
      error: function( error ) {
        console.log( error );
      }
    });
  });

  // Update Product
  $(document).on('click', '.navigator-popup__footer__update-product', function(e) {
    e.preventDefault();

    var button  = $(this),
        user_id = $('.navigator-popup').data( 'user-id' ),
        nonce   = $('#_wpnonce').val(),
        data    = new FormData(),
        parent = $('.navigator-popup__content'),
        $popup  = $('.navigator-popup');

    if ( $.type( $('#product-image').prop('files')[0] ) !== 'undefined' ) {
      var image = $('#product-image').prop('files')[0];
      data.append( 'image', image );
    }

    var name        = $('#product-name').val(),
        price       = $('#product-price').val(),
        discount    = $('#product-discount').val(),
        description = $('#product-description').val(),
        category    = $('#product-category option:selected').val(),
        post_id     = button.data( 'post-id' );

    data.append( 'name', name );
    data.append( 'price', price );
    data.append( 'discount', discount );
    data.append( 'description', description );
    data.append( 'category', category );

    data.append( 'post_id', post_id );
    data.append( 'type', 'update' );
    data.append( 'user_id', user_id );
    data.append( 'nonce', nonce );
    data.append( 'action', 'vlf_navigator_cu_product' );

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: data,
      contentType: false,
      processData: false,
      beforeSend: function() {
        button.addClass('is-loading');
      },
      complete: function() {
        button.removeClass('is-loading');
      },
      success: function( response ) {
        if ( response.success === true ) {
          $('body').append( response.data );
          $('.preview-popup').addClass('is-show');
          $popup.removeClass('is-show');
          $popup.attr( 'data-popup-id', '' );
          $popup.attr( 'data-popup-type', '' );
        } else {
          freshmesNotification( response.data.data, parent, 'is-danger' );
          parent.animate({
            scrollTop: '0px'
          }, 500);
        }
      },
      error: function( error ) {
        console.log( error );
      }
    });
  });

  // Create Post
  $(document).on('click', '.navigator-popup__footer__create-post', function(e) {
    e.preventDefault();

    var button  = $(this),
        user_id = $('.navigator-popup').data( 'user-id' ),
        nonce   = $('#_wpnonce').val(),
        data    = new FormData(),
        parent = $('.navigator-popup__content'),
        $popup  = $('.navigator-popup');

    if ( $.type( $('#post-image').prop('files')[0] ) !== 'undefined' ) {
      var image = $('#post-image').prop('files')[0];
      data.append( 'image', image );
    }

    var name        = $('#post-name').val(),
        description = $('.navigator-popup__content .field.is-quill .control .ql-editor').html(),
        category    = $('#post-category option:selected').val();

    data.append( 'name', name );
    data.append( 'description', description );
    data.append( 'category', category );

    data.append( 'type', 'create' );
    data.append( 'user_id', user_id );
    data.append( 'nonce', nonce );
    data.append( 'action', 'vlf_navigator_cu_post' );

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: data,
      contentType: false,
      processData: false,
      beforeSend: function() {
        button.addClass('is-loading');
      },
      complete: function() {
        button.removeClass('is-loading');
      },
      success: function( response ) {
        if ( response.success === true ) {
          $('body').append( response.data );
          $('.preview-popup').addClass('is-show');
          $popup.removeClass('is-show');
          $popup.attr( 'data-popup-id', '' );
          $popup.attr( 'data-popup-type', '' );
        } else {
          freshmesNotification( response.data.data, parent, 'is-danger' );
          parent.animate({
            scrollTop: '0px'
          }, 500);
        }
      },
      error: function( error ) {
        console.log( error );
      }
    });
  });

  // Update Post
  $(document).on('click', '.navigator-popup__footer__update-post', function(e) {
    e.preventDefault();

    var button  = $(this),
        user_id = $('.navigator-popup').data( 'user-id' ),
        nonce   = $('#_wpnonce').val(),
        data    = new FormData(),
        parent = $('.navigator-popup__content'),
        $popup  = $('.navigator-popup');

    if ( $.type( $('#post-image').prop('files')[0] ) !== 'undefined' ) {
      var image = $('#post-image').prop('files')[0];
      data.append( 'image', image );
    }

    var name        = $('#post-name').val(),
        description = $('.navigator-popup__content .field.is-quill .control .ql-editor').html(),
        category    = $('#post-category option:selected').val(),
        post_id     = button.data( 'post-id' );

    data.append( 'name', name );
    data.append( 'description', description );
    data.append( 'category', category );

    data.append( 'post_id', post_id );
    data.append( 'type', 'update' );
    data.append( 'user_id', user_id );
    data.append( 'nonce', nonce );
    data.append( 'action', 'vlf_navigator_cu_post' );

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: data,
      contentType: false,
      processData: false,
      beforeSend: function() {
        button.addClass('is-loading');
      },
      complete: function() {
        button.removeClass('is-loading');
      },
      success: function( response ) {
        if ( response.success === true ) {
          $('body').append( response.data );
          $('.preview-popup').addClass('is-show');
          $popup.removeClass('is-show');
          $popup.attr( 'data-popup-id', '' );
          $popup.attr( 'data-popup-type', '' );
        } else {
          freshmesNotification( response.data.data, parent, 'is-danger' );
          parent.animate({
            scrollTop: '0px'
          }, 500);
        }
      },
      error: function( error ) {
        console.log( error );
      }
    });
  });

  // Create Network
  $(document).on('click', '.navigator-popup__footer__create-network', function(e) {
    e.preventDefault();

    var button  = $(this),
        user_id = $('.navigator-popup').data( 'user-id' ),
        nonce   = $('#_wpnonce').val(),
        data    = {},
        parent = $('.navigator-popup__content'),
        $popup  = $('.navigator-popup');

    var name         = $('#network-name').val(),
        brands_total = $('#network-members').select2('data'),
        brands = [];

    $.each(brands_total, function(key, value) {
      brands.push( value.id );
    });

    data.name = name;
    data.brands = brands;
    data.type = 'create';
    data.user_id = user_id;
    data.nonce = nonce;
    data.action = 'vlf_navigator_cu_network';

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: data,
      beforeSend: function() {
        button.addClass('is-loading');
      },
      complete: function() {
        button.removeClass('is-loading');
      },
      success: function( response ) {
        if ( response.success === true ) {
          $('body').append( response.data );
          $('.preview-popup').addClass('is-show');
          $popup.removeClass('is-show');
          $popup.attr( 'data-popup-id', '' );
          $popup.attr( 'data-popup-type', '' );
        } else {
          freshmesNotification( response.data.data, parent, 'is-danger' );
          parent.animate({
            scrollTop: '0px'
          }, 500);
        }
      },
      error: function( error ) {
        console.log( error );
      }
    });
  });

  // Publish Brand Page
  $(document).on('click', '.is-brand-publish', function(e) {
    e.preventDefault();

    var button   = $(this),
        brand_id = button.data( 'brand-id' ),
        nonce    = $('#_wpnonce').val();

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: {
        brand_id: brand_id,
        nonce: nonce,
        action: 'vlf_publish_brand'
      },
      beforeSend: function() {
        button.addClass('is-loading');
      },
      complete: function() {
        button.removeClass('is-loading');
      },
      success: function( response ) {
        if ( response.success === true ) {
          button.remove();
          $('.brand-single-page').prepend( '<div class="notification is-freshmes is-brand-published">' + response.data + '</div>' );
          setTimeout( function() {
            $('.is-brand-published').remove();
          }, 5000);
        }
      },
      error: function( error ) {
        console.log( error );
      }
    });
  });

  if ( $('.page-wrapper > .container').length > 0 ) {
    var container_left_offset   = $('.page-wrapper > .container').offset().left;
    $('.navigator-toggle').css({
      'left': container_left_offset - 24 - 78 + 'px'
    });
  }

  if ( $('.page-wrapper > .freshmes-home > .container').length > 0 ) {
    var container_left_offset   = $('.page-wrapper > .freshmes-home > .container').offset().left;
    $('.navigator-toggle').css({
      'left': container_left_offset - 24 - 78 + 'px'
    });
  }

  if ( $('.brand-single-page__sidebars__widget-event-filled__content__persons .field .input').length > 0 ) {
    $('.brand-single-page__sidebars__widget-event-filled__content__persons .field .input').on('change', function() {
      var qty = $('.brand-single-page__sidebars__widget-event-filled__content__persons .field .input').val();
      $('.brand-single-page__sidebars__widget-event-filled__footer__book-now').attr('data-quantity', qty);
    });
  }

  function gup( name, url ) {
      if (!url) url = location.href;
      name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
      var regexS = "[\\?&]"+name+"=([^&#]*)";
      var regex = new RegExp( regexS );
      var results = regex.exec( url );
      return results == null ? null : results[1];
  }

  if ( $('.content-filters').length > 0 ) {
    $('.content-filters select').each(function() {
      $(this).select2({
        placeholder: $(this).attr('placeholder'),
        allowClear: true
      });
    });
    $(document).on('change.select2', '.content-filters select', function(e) {

      var urlParams = {};
      // Get All Filters
      $('.content-filters select').each(function() {
        if ( $(this).find('option:selected').val().length > 0 ) {

          var urlParam = $(this).find('option:selected').val();
          var urlParamType = $(this).data('type');

          urlParams[urlParamType] = urlParam;

        }
      });

      // Get Base Url
      var baseUrl = $('#freshmes-template-url').val();

      // Check for the select
      if ( e.currentTarget.id === 'content-filters-networks' || e.currentTarget.id === 'content-filters-chapters' ) {

        if ( jQuery.type( urlParams['network'] ) === "undefined" ) {

          window.location.href = baseUrl;

        } else {

          urlParams = $.param( urlParams );
          window.location.href = baseUrl + '?preview=true&' + urlParams;

        }

      } else {

        if ( $.isEmptyObject( urlParams ) ) {

          window.location.href = baseUrl;

        } else {

          urlParams = $.param( urlParams );
          window.location.href = baseUrl + '?' + urlParams;

        }

      }
    });
  }

  if ( $('.freshmes-members__header__select-all').length > 0 ) {
    $(document).on('click', '.freshmes-members__header__select-all', function(e) {
      e.preventDefault();

      if ( $(this).hasClass('is-selected') ) {
        $(this).removeClass('is-selected');
        $('.member').removeClass('is-selected');
      } else {
        $(this).addClass('is-selected');
        $('.member').addClass('is-selected');
      }
    });
    $(document).on('click', '.member .member__box', function(e) {
      e.preventDefault();

      $(this).parent().toggleClass('is-selected');
      if ( $('.member:not(.is-selected)').length > 0 ) {
        $('.freshmes-members__header__select-all').removeClass('is-selected');
      } else {
        $('.freshmes-members__header__select-all').addClass('is-selected');
      }
    });
    $(document).on('click', '.freshmes-members__footer__load-more', function(e) {
      e.preventDefault();

      $('.freshmes-members__content > .columns > .column').removeClass('is-hidden');

      var button = $(this),
          nonce  = $('#_wpnonce').val(),
          offset = $('.freshmes-members__content .column').length;

      var data = {
        offset: offset,
        nonce: nonce,
        action: 'vlf_load_more_members'
      };

      // Filters
      $('.content-filters select').each(function() {
        if ( $(this).find('option:selected').val().length > 0 ) {
          var type = $(this).data('type');
          data[ type ] = $(this).find('option:selected').val();
        }
      });

      var topOffset = button.offset().top;

      $.ajax({
        type: 'POST',
        url: vlfApp.ajaxURL,
        data: data,
        beforeSend: function() {
          button.addClass('is-loading');
        },
        complete: function() {
          button.removeClass('is-loading');
          $('html, body').animate({
            scrollTop: topOffset - 60
          }, 500);
        },
        success: function( response ) {
          if ( response.success === true ) {
            $('.freshmes-members__header__results').html( response.data.results );
            $('.freshmes-members__content .columns').append( response.data.html );

            $('.freshmes-members__header__select-all').removeClass('is-selected');

            if ( response.data.button === false ) {
              button.remove();
            }
          } else {
            button.remove();
          }
        },
        error: function( error ) {
          console.log( error );
        }
      });
    });
  }

  $(document).on('click', '.freshmes-networks__footer__load-more', function(e) {
    e.preventDefault();

    $('.content-filters__filter > select').val(null).trigger('change');
    $('.freshmes-networks__content > .columns > .column').removeClass('is-hidden');

    var button = $(this),
        nonce  = $('#_wpnonce').val(),
        offset = $('.freshmes-networks__content .column').length;

    var topOffset = button.offset().top;

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: {
        offset: offset,
        nonce: nonce,
        action: 'vlf_load_more_networks'
      },
      beforeSend: function() {
        button.addClass('is-loading');
      },
      complete: function() {
        button.removeClass('is-loading');
        $('html, body').animate({
          scrollTop: topOffset - 60
        }, 500);
      },
      success: function( response ) {
        if ( response.success === true ) {
          $('.freshmes-networks__header__results').html( response.data.results );
          $('.freshmes-networks__content .columns').append( response.data.html );

          $('.freshmes-networks__header__select-all').removeClass('is-selected');

          if ( response.data.button === false ) {
            button.remove();
          }
        } else {
          button.remove();
        }
      },
      error: function( error ) {
        console.log( error );
      }
    });
  });

  $(document).on('click', '.member__connect:not(.member__connect--disconnect)', function(e) {
    e.preventDefault();

    var button = $(this),
        nonce  = $('#_wpnonce').val(),
        user_a = button.data('user-a'),
        user_b = button.data('user-b');

    var $body = $('body'),
        $popup = $('.connect-popup');

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: {
        user_a: user_a,
        user_b: user_b,
        category: 'all',
        nonce: nonce,
        action: 'vlf_connect_to_user'
      },
      beforeSend: function() {
        button.addClass('is-loading');
      },
      complete: function() {
        button.removeClass('is-loading');
      },
      success: function( response ) {
        if ( response.success === true ) {
          $popup.find('.connect-popup__header__title').empty();
          $popup.find('.connect-popup__header__subtitle').empty();
          $popup.find('.connect-popup__content').removeClass('is-hidden').empty();

          $popup.find('.connect-popup__header__title').html( response.data.brand_header_title );
          $popup.find('.connect-popup__header__subtitle').html( response.data.brand_header_subtitle );
          $popup.find('.connect-popup__content').append( response.data.brand_content_title );
          $popup.find('.connect-popup__content').append( response.data.brand_content_fields );

          $body.addClass('is-active-overlay');
          $popup.addClass('is-show');

          button.parents('.member').find('.member__box').addClass('is-hidden');
          button.addClass('member__connect--disconnect');
          button.text( vlfApp.l10n.members.disconnect );
        }
      },
      error: function( error ) {
        console.log( error );
      }
    });
  });
  $(document).on('click', '.member__connect--disconnect', function(e) {
    e.preventDefault();

    var button = $(this),
        nonce  = $('#_wpnonce').val(),
        user_a = button.data('user-a'),
        user_b = button.data('user-b');

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: {
        user_a: user_a,
        user_b: user_b,
        category: 'all',
        nonce: nonce,
        action: 'vlf_disconnect_from_user'
      },
      beforeSend: function() {
        button.addClass('is-loading');
      },
      complete: function() {
        button.removeClass('is-loading');
      },
      success: function( response ) {
        if ( response.success === true ) {
          button.parents('.member').find('.member__box').removeClass('is-hidden');
          button.removeClass('member__connect--disconnect');
          button.text( vlfApp.l10n.members.connect );
        }
      },
      error: function( error ) {
        console.log( error );
      }
    });
  });
  $(document).on('click', '.freshmes-members__header__add-members', function(e) {
    e.preventDefault();

    var button = $(this),
        nonce  = $('#_wpnonce').val(),
        user_a = button.data('user'),
        users  = [];

    $('.member.is-selected').each(function() {
      users.push( $(this).data('user') );
    });

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: {
        user: user_a,
        users: users,
        nonce: nonce,
        action: 'vlf_connect_to_users'
      },
      beforeSend: function() {
        button.addClass('is-loading');
      },
      complete: function() {
        button.removeClass('is-loading');
      },
      success: function( response ) {
        if ( response.success === true ) {
          $('.member.is-selected').each(function() {
            $(this).removeClass('is-selected');
            $('.freshmes-members__header__select-all').removeClass('is-selected');
            $(this).find('.member__box').addClass('is-hidden');
            $(this).find('.button').addClass('member__connect--disconnect');
            $(this).find('.button').text( vlfApp.l10n.members.disconnect );
          });
        }
      },
      error: function( error ) {
        console.log( error );
      }
    });
  });

  if ( $('.brand-blog-single__main__post__title').length > 0 ) {
    var post_title_height = $('.brand-blog-single__main__post__title').outerHeight();
    $('.brand-blog-single__main__sidebar').css({
      'margin-top': post_title_height + 'px'
    });
  }

  $(document).on('click', '.brand-blog-single__author__connect:not(.brand-blog-single__author__connect--disconnect)', function(e) {
    e.preventDefault();

    var button = $(this),
        nonce  = $('#_wpnonce').val(),
        user_a = button.data('user-a'),
        user_b = button.data('user-b');

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: {
        user_a: user_a,
        user_b: user_b,
        category: 'all',
        nonce: nonce,
        action: 'vlf_connect_to_user'
      },
      beforeSend: function() {
        button.addClass('is-loading');
      },
      complete: function() {
        button.removeClass('is-loading');
      },
      success: function( response ) {
        if ( response.success === true ) {
          button.text( vlfApp.l10n.members.disconnect );
        }
      },
      error: function( error ) {
        console.log( error );
      }
    });
  });
  $(document).on('click', '.brand-blog-single__author__connect--disconnect', function(e) {
    e.preventDefault();

    var button = $(this),
        nonce  = $('#_wpnonce').val(),
        user_a = button.data('user-a'),
        user_b = button.data('user-b');

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: {
        user_a: user_a,
        user_b: user_b,
        category: 'all',
        nonce: nonce,
        action: 'vlf_disconnect_from_user'
      },
      beforeSend: function() {
        button.addClass('is-loading');
      },
      complete: function() {
        button.removeClass('is-loading');
      },
      success: function( response ) {
        if ( response.success === true ) {
          button.text( vlfApp.l10n.members.connect );
        }
      },
      error: function( error ) {
        console.log( error );
      }
    });
  });

  $(document).on('click', '.brand-shop__content__footer__load-more', function(e) {
    e.preventDefault();

    var button = $(this),
        nonce  = $('#_wpnonce').val(),
        offset = $('.brand-shop__content__products > .columns > .column').length,
        author = button.data('author-id');

    var topOffset = button.offset().top;

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: {
        author: author,
        offset: offset,
        nonce: nonce,
        action: 'vlf_load_more_products_shop'
      },
      beforeSend: function() {
        button.addClass('is-loading');
      },
      complete: function() {
        button.removeClass('is-loading');
        $('html, body').animate({
          scrollTop: topOffset - 60
        }, 500);
      },
      success: function( response ) {
        if ( response.success === true ) {
          $('.brand-shop__content__header__meta__item--results').html( response.data.results );
          $('.brand-shop__content__products > .columns').append( response.data.html );

          if ( response.data.button === false ) {
            button.remove();
          }
        } else {
          button.remove();
        }
      },
      error: function( error ) {
        console.log( error );
      }
    });
  });
  $(document).on('click', '.brand-shop__content__header__meta__item--filter', function(e) {
    e.preventDefault();

    var windowWidth = $(window).width(),
        productsWidth = $('.brand-shop__content__products').width();

    var offset = windowWidth - productsWidth;
    if ( offset <= 0 ) {

      offset = 0;

    } else {

      offset = offset / 2;
      offset = offset * -1;

    }

    if ( $('.brand-shop__content__products__filters').hasClass('brand-shop__content__products__filters--is-shown') ) {
      $('.brand-shop__content__products__filters').removeClass('brand-shop__content__products__filters--is-shown');
      $('.brand-shop__content__products__filters').attr('style', '');
    } else {
      $('.brand-shop__content__products__filters').addClass('brand-shop__content__products__filters--is-shown');
      $('.brand-shop__content__products__filters').css({
        'transform': 'translateX(' + offset + 'px)'
      });
    }
  });
  $(document).on('click', '.quantity__dec, .quantity__inc', function(e) {
    e.preventDefault();

    var parent = $(this).parent();

    var $qty = parent.find('.qty'),
        qty_current = Number( $qty.attr('value') ),
        min = Number( $qty.attr('min') ),
        max = $qty.attr('max'),
        qty_new = 0;

    if ( max.length <= 0 ) {
      max = 1000000;
    } else {
      max = Number( max );
    }

    if ( $(this).hasClass('quantity__dec') ) {
      qty_new = qty_current - 1;
    } else {
      qty_new = qty_current + 1;
    }

    if ( qty_new < min || qty_new > max ) {
      qty_new = qty_current;
    }

    $qty.attr('value', qty_new);
    $qty.trigger('change');
  });
  $(document).on('click', '.freshmes-product__footer__view', function(e) {
    e.preventDefault();

    var button = $(this),
        product_id = button.data('product-id'),
        nonce      = $('#_wpnonce').val();
  });
  $(window).on('scroll', function() {
    if ( $(this).scrollTop() > 70 ) {
      $('.is-brand-profile').addClass('is-hidden');
      $('.is-get-premium').removeClass('is-hidden');
    } else {
      $('.is-brand-profile').removeClass('is-hidden');
      $('.is-get-premium').addClass('is-hidden');
    }
  });
  $(document).on('click', '.is-post-type-publish', function(e) {
    e.preventDefault();

    var $button = $(this),
        $edit   = $('.is-post-type-edit'),
        $noti   = $('.notification.is-freshmes'),
        nonce   = $('#_wpnonce').val();

    $.ajax({
      type: 'POST',
      url: vlfApp.ajaxURL,
      data: {
        post_id: $button.data('brand-id'),
        nonce: nonce,
        action: 'vlf_publish_post_type'
      },
      beforeSend: function() {
        $button.addClass('is-loading');
      },
      complete: function() {
        $button.removeClass('is-loading');
      },
      success: function( response ) {
        if ( response.success === true ) {
          $button.remove();
          $edit.remove();
          $noti.remove();
        }
      },
      error: function( error ) {
        console.log( error );
      }
    });
  });
  $(document).on('click', '.freshmes-product__footer__view', function(e) {
    window.location.href = $(this).attr('href');
  });

  function removeParam(parameter)
  {
    var url=document.location.href;
    var urlparts= url.split('?');

    if (urlparts.length>=2)
    {
      var urlBase=urlparts.shift();
      var queryString=urlparts.join("?");

      var prefix = encodeURIComponent(parameter)+'=';
      var pars = queryString.split(/[&;]/g);
      for (var i= pars.length; i-->0;)
        if (pars[i].lastIndexOf(prefix, 0)!==-1)
          pars.splice(i, 1);
      url = urlBase+'?'+pars.join('&');
      window.history.pushState('',document.title,url); // added this line to push the new url directly to url bar .

    }
    return url;
  }

  // Product Filter Click
  $(document).on('click', '.shop-categories__category', function() {
    // Get Current URL
    var url = removeParam( 'product_cat' );
    url = new URL( url );

    // Get All Checked
    var checked_array = [];
    $('.shop-categories__category input:checked').each(function() {
      checked_array.push( $(this).val() );
    });
    if ( checked_array.length > 0 ) {
      var checked = checked_array.join(',');

      url.searchParams.set( 'product_cat', checked );
    }

    window.location.href = url;
  });

  if ( $('.brand-single-page__sidebars__sidebar > div > div').length === 0 ) {
    $('.brand-single-page__sidebars__main').removeClass( 'is-offset-0-desktop' );
    $('.brand-single-page__sidebars__main').addClass( 'is-offset-2-desktop' );
    $('.brand-single-page__header__column').addClass(' is-offset-2' );
  }

  $(document).on('click', '.brand-single-page__sidebars__accordion__item__title', function(e) {
    e.preventDefault();

    $(this).parent().toggleClass( 'brand-single-page__sidebars__accordion__item--is-active' );
  });
});
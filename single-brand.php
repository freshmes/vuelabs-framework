<?php
/**
 * Single brand template file.
 *
 * @package VueLabs
 * @subpackage VueLabs_Framework
 * @since 1.0.0
 */
get_header();

$current_brand_page = get_query_var( 'fpage' );

switch ( $current_brand_page ) {
    case 'blog':
        get_template_part( 'single-brand', 'blog' );
        break;
    case 'shop':
        get_template_part( 'single-brand', 'shop' );
        break;
    default:
        get_template_part( 'single-brand', 'index' );
        break;
}

get_footer();